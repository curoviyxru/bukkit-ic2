// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package forge;

import java.io.PrintStream;
import java.io.Serializable;
import java.util.*;
import net.minecraft.server.*;

// Referenced classes of package forge:
//            ICraftingHandler, IDestroyToolHandler, IBonemealHandler, ISleepHandler, 
//            MinecraftForge

public class ForgeHooks
{
    static class ProbableItem
    {

        int wstart;
        int wend;
        int itemid;
        int meta;
        int qty;

        public ProbableItem(int i, int j, int k, int l, int i1)
        {
            wstart = l;
            wend = i1;
            itemid = i;
            meta = j;
            qty = k;
        }
    }


    public ForgeHooks()
    {
    }

    public static void onTakenFromCrafting(EntityPlayer entityplayer, ItemStack itemstack, IInventory iinventory)
    {
        ICraftingHandler icraftinghandler;
        for(Iterator iterator = craftingHandlers.iterator(); iterator.hasNext(); icraftinghandler.onTakenFromCrafting(entityplayer, itemstack, iinventory))
        {
            icraftinghandler = (ICraftingHandler)iterator.next();
        }

    }

    public static void onDestroyCurrentItem(EntityPlayer entityplayer, ItemStack itemstack)
    {
        IDestroyToolHandler idestroytoolhandler;
        for(Iterator iterator = destroyToolHandlers.iterator(); iterator.hasNext(); idestroytoolhandler.onDestroyCurrentItem(entityplayer, itemstack))
        {
            idestroytoolhandler = (IDestroyToolHandler)iterator.next();
        }

    }

    public static boolean onUseBonemeal(World world, int i, int j, int k, int l)
    {
        for(Iterator iterator = bonemealHandlers.iterator(); iterator.hasNext();)
        {
            IBonemealHandler ibonemealhandler = (IBonemealHandler)iterator.next();
            if(ibonemealhandler.onUseBonemeal(world, i, j, k, l))
            {
                return true;
            }
        }

        return false;
    }

    public static EnumBedError sleepInBedAt(EntityPlayer entityplayer, int i, int j, int k)
    {
        for(Iterator iterator = sleepHandlers.iterator(); iterator.hasNext();)
        {
            ISleepHandler isleephandler = (ISleepHandler)iterator.next();
            EnumBedError enumstatus = isleephandler.sleepInBedAt(entityplayer, i, j, k);
            if(enumstatus != null)
            {
                return enumstatus;
            }
        }

        return null;
    }

    static ProbableItem getRandomItem(List list, int i)
    {
        int j = Collections.binarySearch(list, Integer.valueOf(i), new Comparator() {

            public int compare(Object obj, Object obj1)
            {
                ProbableItem probableitem = (ProbableItem)obj;
                Integer integer = (Integer)obj1;
                if(integer.intValue() < probableitem.wstart)
                {
                    return 1;
                }
                return integer.intValue() < probableitem.wend ? 0 : -1;
            }

        }
);
        if(j < 0)
        {
            return null;
        } else
        {
            return (ProbableItem)list.get(j);
        }
    }

    public static void plantGrassPlant(World world, int i, int j, int k)
    {
        int l = world.random.nextInt(plantGrassWeight);
        ProbableItem probableitem = getRandomItem(plantGrassList, l);
        if(probableitem == null)
        {
            return;
        } else
        {
            world.setTypeIdAndData(i, j, k, probableitem.itemid, probableitem.meta);
            return;
        }
    }

    public static void addPlantGrass(int i, int j, int k)
    {
        plantGrassList.add(new ProbableItem(i, j, 1, plantGrassWeight, plantGrassWeight + k));
        plantGrassWeight += k;
    }

    public static ItemStack getGrassSeed(World world)
    {
        int i = world.random.nextInt(seedGrassWeight);
        ProbableItem probableitem = getRandomItem(seedGrassList, i);
        if(probableitem == null)
        {
            return null;
        } else
        {
            return new ItemStack(probableitem.itemid, probableitem.qty, probableitem.meta);
        }
    }

    public static void addGrassSeed(int i, int j, int k, int l)
    {
        seedGrassList.add(new ProbableItem(i, j, k, seedGrassWeight, seedGrassWeight + l));
        seedGrassWeight += l;
    }

    public static boolean canHarvestBlock(Block block, EntityPlayer entityplayer, int i)
    {
        if(block.material.i())
        {
            return true;
        }
        ItemStack itemstack = entityplayer.inventory.getItemInHand();
        if(itemstack == null)
        {
            return false;
        }
        List list = (List)toolClasses.get(Integer.valueOf(itemstack.id));
        if(list == null)
        {
            return itemstack.b(block);
        }
        Object aobj[] = list.toArray();
        String s = (String)aobj[0];
        int j = ((Integer)aobj[1]).intValue();
        Integer integer = (Integer)toolHarvestLevels.get(Arrays.asList(new Serializable[] {
            Integer.valueOf(block.id), Integer.valueOf(i), s
        }));
        if(integer == null)
        {
            return itemstack.b(block);
        }
        return integer.intValue() <= j;
    }

    public static float blockStrength(Block block, EntityPlayer entityplayer, int i)
    {
        float f = block.j();
        if(f < 0.0F)
        {
            return 0.0F;
        }
        if(!canHarvestBlock(block, entityplayer, i))
        {
            return 1.0F / f / 100F;
        } else
        {
            //Wreck it, Bukkit!
            ItemStack stack = entityplayer.inventory.getItemInHand();
            if (stack != null) {
                return stack.getItem().a(stack, block) / f / 30F;
            }
            
            return entityplayer.a(block) / f / 30F;
        }
    }

    public static boolean isToolEffective(ItemStack itemstack, Block block, int i)
    {
        List list = (List)toolClasses.get(Integer.valueOf(itemstack.id));
        if(list == null)
        {
            return false;
        } else
        {
            Object aobj[] = list.toArray();
            String s = (String)aobj[0];
            return toolEffectiveness.contains(Arrays.asList(new Serializable[] {
                Integer.valueOf(block.id), Integer.valueOf(i), s
            }));
        }
    }

    static void initTools()
    {
        if(toolInit)
        {
            return;
        }
        toolInit = true;
        MinecraftForge.setToolClass(Item.WOOD_PICKAXE, "pickaxe", 0);
        MinecraftForge.setToolClass(Item.STONE_PICKAXE, "pickaxe", 1);
        MinecraftForge.setToolClass(Item.IRON_PICKAXE, "pickaxe", 2);
        MinecraftForge.setToolClass(Item.GOLD_PICKAXE, "pickaxe", 0);
        MinecraftForge.setToolClass(Item.DIAMOND_PICKAXE, "pickaxe", 3);
        MinecraftForge.setToolClass(Item.WOOD_AXE, "axe", 0);
        MinecraftForge.setToolClass(Item.STONE_AXE, "axe", 1);
        MinecraftForge.setToolClass(Item.IRON_AXE, "axe", 2);
        MinecraftForge.setToolClass(Item.GOLD_AXE, "axe", 0);
        MinecraftForge.setToolClass(Item.DIAMOND_AXE, "axe", 3);
        MinecraftForge.setToolClass(Item.WOOD_SPADE, "shovel", 0);
        MinecraftForge.setToolClass(Item.STONE_SPADE, "shovel", 1);
        MinecraftForge.setToolClass(Item.IRON_SPADE, "shovel", 2);
        MinecraftForge.setToolClass(Item.GOLD_SPADE, "shovel", 0);
        MinecraftForge.setToolClass(Item.DIAMOND_SPADE, "shovel", 3);
        MinecraftForge.setBlockHarvestLevel(Block.OBSIDIAN, "pickaxe", 3);
        MinecraftForge.setBlockHarvestLevel(Block.DIAMOND_ORE, "pickaxe", 2);
        MinecraftForge.setBlockHarvestLevel(Block.DIAMOND_BLOCK, "pickaxe", 2);
        MinecraftForge.setBlockHarvestLevel(Block.GOLD_ORE, "pickaxe", 2);
        MinecraftForge.setBlockHarvestLevel(Block.GOLD_BLOCK, "pickaxe", 2);
        MinecraftForge.setBlockHarvestLevel(Block.IRON_ORE, "pickaxe", 1);
        MinecraftForge.setBlockHarvestLevel(Block.IRON_BLOCK, "pickaxe", 1);
        MinecraftForge.setBlockHarvestLevel(Block.LAPIS_ORE, "pickaxe", 1);
        MinecraftForge.setBlockHarvestLevel(Block.LAPIS_BLOCK, "pickaxe", 1);
        MinecraftForge.setBlockHarvestLevel(Block.REDSTONE_ORE, "pickaxe", 2);
        MinecraftForge.setBlockHarvestLevel(Block.GLOWING_REDSTONE_ORE, "pickaxe", 2);
        MinecraftForge.removeBlockEffectiveness(Block.REDSTONE_ORE, "pickaxe");
        MinecraftForge.removeBlockEffectiveness(Block.GLOWING_REDSTONE_ORE, "pickaxe");
        MinecraftForge.removeBlockEffectiveness(Block.OBSIDIAN, "pickaxe");
        Block ablock[] = {
            Block.COBBLESTONE, Block.DOUBLE_STEP, Block.STEP, Block.STONE, Block.SANDSTONE, Block.MOSSY_COBBLESTONE, Block.COAL_ORE, Block.ICE, Block.NETHERRACK, Block.LAPIS_ORE,
            Block.LAPIS_BLOCK
        };
        Block ablock1[] = ablock;
        int i = ablock1.length;
        for(int j = 0; j < i; j++)
        {
            Block block = ablock1[j];
            MinecraftForge.setBlockHarvestLevel(block, "pickaxe", 0);
        }

    }

    static LinkedList craftingHandlers = new LinkedList();
    static LinkedList destroyToolHandlers = new LinkedList();
    static LinkedList bonemealHandlers = new LinkedList();
    static LinkedList sleepHandlers = new LinkedList();
    static List plantGrassList;
    static int plantGrassWeight = 30;
    static List seedGrassList;
    static int seedGrassWeight = 10;
    public static final int majorVersion = 1;
    public static final int minorVersion = 1;
    public static final int revisionVersion = 2;
    static boolean toolInit = false;
    static HashMap toolClasses = new HashMap();
    static HashMap toolHarvestLevels = new HashMap();
    static HashSet toolEffectiveness = new HashSet();

    static 
    {
        plantGrassList = new ArrayList();
        plantGrassList.add(new ProbableItem(Block.YELLOW_FLOWER.id, 0, 1, 0, 20));
        plantGrassList.add(new ProbableItem(Block.RED_ROSE.id, 0, 1, 20, 30));
        seedGrassList = new ArrayList();
        seedGrassList.add(new ProbableItem(Item.SEEDS.id, 0, 1, 0, 10));
        System.out.printf("MinecraftForge V%d.%d.%d Initialized\n", new Object[] {
            Integer.valueOf(1), Integer.valueOf(1), Integer.valueOf(2)
        });
    }
}
