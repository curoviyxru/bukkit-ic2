// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.platform;

import ic2.common.INetworkItemEventListener;
import ic2.common.TileEntityBlock;
import java.lang.reflect.Field;
import java.util.*;
import net.minecraft.server.*;

public class NetworkManager
{
    static class TileEntityField
    {

        public boolean equals(Object obj)
        {
            if(obj instanceof TileEntityField)
            {
                TileEntityField tileentityfield = (TileEntityField)obj;
                return tileentityfield.te == te && tileentityfield.field.equals(field);
            } else
            {
                return false;
            }
        }

        public int hashCode()
        {
            return te.hashCode() * 31 ^ field.hashCode();
        }

        TileEntity te;
        String field;
        EntityPlayer target;

        TileEntityField(TileEntity tileentity, String s)
        {
            target = null;
            te = tileentity;
            field = s;
        }

        TileEntityField(TileEntity tileentity, String s, EntityPlayer entityplayermp)
        {
            target = null;
            te = tileentity;
            field = s;
            target = entityplayermp;
        }
    }


    public NetworkManager()
    {
    }

    public static void onTick()
    {
        if(--ticksLeftToUpdate == 0)
        {
            if(!fieldsToUpdateSet.isEmpty())
            {
                sendUpdatePacket();
            }
            ticksLeftToUpdate = 2;
        }
    }

    public static void updateTileEntityField(TileEntity tileentity, String s)
    {
        fieldsToUpdateSet.add(new TileEntityField(tileentity, s));
        if(fieldsToUpdateSet.size() > 10000)
        {
            sendUpdatePacket();
        }
    }

    public static void initiateTileEntityEvent(TileEntity tileentity, int i, boolean flag)
    {
        int j = flag ? 400 : ModLoader.getMinecraftServerInstance().serverConfigurationManager.a();
        World world = tileentity.world;
        Iterator iterator = world.players.iterator();
        do
        {
            if(!iterator.hasNext())
            {
                break;
            }
            Object obj = iterator.next();
            EntityPlayer entityplayermp = (EntityPlayer)obj;
            int k = tileentity.x - (int)entityplayermp.locX;
            int l = tileentity.z - (int)entityplayermp.locZ;
            int i1;
            if(flag)
            {
                i1 = k * k + l * l;
            } else
            {
                i1 = Math.max(Math.abs(k), Math.abs(l));
            }
            if(i1 <= j)
            {
                Packet230ModLoader packet230modloader = new Packet230ModLoader();
                packet230modloader.modId = ModLoaderMp.GetModInstance(net.minecraft.server.mod_IC2.class).getId();
                packet230modloader.k = true;
                packet230modloader.packetType = 1;
                packet230modloader.dataInt = new int[5];
                packet230modloader.dataInt[0] = world.worldProvider.dimension;
                packet230modloader.dataInt[1] = tileentity.x;
                packet230modloader.dataInt[2] = tileentity.y;
                packet230modloader.dataInt[3] = tileentity.z;
                packet230modloader.dataInt[4] = i;
                packet230modloader.dataFloat = new float[0];
                packet230modloader.dataString = new String[0];
                ModLoaderMp.SendPacketTo(ModLoaderMp.GetModInstance(net.minecraft.server.mod_IC2.class), entityplayermp, packet230modloader);
            }
        } while(true);
    }

    public static void initiateItemEvent(EntityPlayer entityplayer, ItemStack itemstack, int i, boolean flag)
    {
        int j = flag ? 400 : ModLoader.getMinecraftServerInstance().serverConfigurationManager.a();
        Iterator iterator = entityplayer.world.players.iterator();
        do
        {
            if(!iterator.hasNext())
            {
                break;
            }
            Object obj = iterator.next();
            EntityPlayer entityplayermp = (EntityPlayer)obj;
            int k = (int)entityplayer.locX - (int)entityplayermp.locX;
            int l = (int)entityplayer.locZ - (int)entityplayermp.locZ;
            int i1;
            if(flag)
            {
                i1 = k * k + l * l;
            } else
            {
                i1 = Math.max(Math.abs(k), Math.abs(l));
            }
            if(i1 <= j)
            {
                Packet230ModLoader packet230modloader = new Packet230ModLoader();
                packet230modloader.modId = ModLoaderMp.GetModInstance(net.minecraft.server.mod_IC2.class).getId();
                packet230modloader.k = true;
                packet230modloader.packetType = 2;
                packet230modloader.dataInt = new int[3];
                packet230modloader.dataInt[0] = itemstack.id;
                packet230modloader.dataInt[1] = itemstack.getData();
                packet230modloader.dataInt[2] = i;
                packet230modloader.dataFloat = new float[0];
                packet230modloader.dataString = new String[1];
                packet230modloader.dataString[0] = entityplayer.name;
                ModLoaderMp.SendPacketTo(ModLoaderMp.GetModInstance(net.minecraft.server.mod_IC2.class), entityplayermp, packet230modloader);
            }
        } while(true);
    }

    public static void announceBlockUpdate(World world, int i, int j, int k)
    {
        Iterator iterator = world.players.iterator();
        do
        {
            if(!iterator.hasNext())
            {
                break;
            }
            Object obj = iterator.next();
            EntityPlayer entityplayermp = (EntityPlayer)obj;
            int l = Math.min(Math.abs(i - (int)entityplayermp.locX), Math.abs(k - (int)entityplayermp.locZ));
            if(l <= ModLoader.getMinecraftServerInstance().serverConfigurationManager.a())
            {
                Packet230ModLoader packet230modloader = new Packet230ModLoader();
                packet230modloader.modId = ModLoaderMp.GetModInstance(net.minecraft.server.mod_IC2.class).getId();
                packet230modloader.k = true;
                packet230modloader.packetType = 3;
                packet230modloader.dataInt = new int[4];
                packet230modloader.dataInt[0] = world.worldProvider.dimension;
                packet230modloader.dataInt[1] = i;
                packet230modloader.dataInt[2] = j;
                packet230modloader.dataInt[3] = k;
                packet230modloader.dataFloat = new float[0];
                packet230modloader.dataString = new String[0];
                ModLoaderMp.SendPacketTo(ModLoaderMp.GetModInstance(net.minecraft.server.mod_IC2.class), entityplayermp, packet230modloader);
            }
        } while(true);
    }

    public static void requestInitialTileEntityData(World world, int i, int j, int k)
    {
    }

    public static void initiateClientItemEvent(ItemStack itemstack, int i)
    {
    }

    public static void handlePacket(Packet230ModLoader packet230modloader, EntityPlayer entityplayermp)
    {
        if(packet230modloader.packetType == 0 && packet230modloader.dataInt.length == 4)
        {
            //Wreck it, Bukkit!
            List<net.minecraft.server.WorldServer> aworldserver = ModLoader.getMinecraftServerInstance().worlds;
            int i = aworldserver.size();
            int j = 0;
            do
            {
                if(j >= i)
                {
                    break;
                }
                net.minecraft.server.WorldServer worldserver = aworldserver.get(j);
                if(packet230modloader.dataInt[0] == ((World) (worldserver)).worldProvider.dimension)
                {
                    TileEntity tileentity = worldserver.getTileEntity(packet230modloader.dataInt[1], packet230modloader.dataInt[2], packet230modloader.dataInt[3]);
                    if(tileentity instanceof TileEntityBlock)
                    {
                        Iterator iterator = ((TileEntityBlock)tileentity).getNetworkedFields().iterator();
                        do
                        {
                            if(!iterator.hasNext())
                            {
                                break;
                            }
                            String s = (String)iterator.next();
                            fieldsToUpdateSet.add(new TileEntityField(tileentity, s, entityplayermp));
                            if(fieldsToUpdateSet.size() > 10000)
                            {
                                sendUpdatePacket();
                            }
                        } while(true);
                    }
                    break;
                }
                j++;
            } while(true);
        } else
        if(packet230modloader.packetType == 1 && packet230modloader.dataInt.length == 3)
        {
            Item item = Item.byId[packet230modloader.dataInt[0]];
            if(item instanceof INetworkItemEventListener)
            {
                ((INetworkItemEventListener)item).onNetworkEvent(packet230modloader.dataInt[1], entityplayermp, packet230modloader.dataInt[2]);
            }
        }
    }

    private static void sendUpdatePacket()
    {
        List<net.minecraft.server.WorldServer> aworldserver = ModLoader.getMinecraftServerInstance().worlds;
        int i = aworldserver.size();
label0:
        for(int j = 0; j < i; j++)
        {
            net.minecraft.server.WorldServer worldserver = aworldserver.get(j);
            Iterator iterator = ((World) (worldserver)).players.iterator();
            do
            {
                if(!iterator.hasNext())
                {
                    continue label0;
                }
                Object obj = iterator.next();
                EntityPlayer entityplayermp = (EntityPlayer)obj;
                Packet230ModLoader packet230modloader = new Packet230ModLoader();
                packet230modloader.modId = ModLoaderMp.GetModInstance(net.minecraft.server.mod_IC2.class).getId();
                packet230modloader.k = true;
                packet230modloader.packetType = 0;
                Vector vector = new Vector();
                Vector vector1 = new Vector();
                Vector vector2 = new Vector();
                vector1.add(Integer.valueOf(((World) (worldserver)).worldProvider.dimension));
                Iterator iterator1 = fieldsToUpdateSet.iterator();
                do
                {
                    if(!iterator1.hasNext())
                    {
                        break;
                    }
                    TileEntityField tileentityfield = (TileEntityField)iterator1.next();
                    if(!tileentityfield.te.g() && tileentityfield.te.world == worldserver && (tileentityfield.target == null || tileentityfield.target == entityplayermp))
                    {
                        int l = Math.min(Math.abs(tileentityfield.te.x - (int)entityplayermp.locX), Math.abs(tileentityfield.te.z - (int)entityplayermp.locZ));
                        if(l <= ModLoader.getMinecraftServerInstance().serverConfigurationManager.a())
                        {
                            vector1.add(Integer.valueOf(tileentityfield.te.x));
                            vector1.add(Integer.valueOf(tileentityfield.te.y));
                            vector1.add(Integer.valueOf(tileentityfield.te.z));
                            vector2.add(tileentityfield.field);
                            Field field = null;
                            try
                            {
                                Class class1 = tileentityfield.te.getClass();
                                do
                                {
                                    try
                                    {
                                        field = class1.getDeclaredField(tileentityfield.field);
                                    }
                                    catch(NoSuchFieldException nosuchfieldexception)
                                    {
                                        class1 = class1.getSuperclass();
                                    }
                                } while(field == null && class1 != null);
                                if(field == null)
                                {
                                    throw new NoSuchFieldException(tileentityfield.field);
                                }
                                field.setAccessible(true);
                                Class class2 = field.getType();
                                if(class2 == Float.TYPE)
                                {
                                    vector1.add(Integer.valueOf(0));
                                    vector.add(Float.valueOf(field.getFloat(tileentityfield.te)));
                                } else
                                if(class2 == Integer.TYPE)
                                {
                                    vector1.add(Integer.valueOf(1));
                                    vector1.add(Integer.valueOf(field.getInt(tileentityfield.te)));
                                } else
                                if(class2 == (java.lang.String.class))
                                {
                                    vector1.add(Integer.valueOf(2));
                                    vector2.add((String)field.get(tileentityfield.te));
                                } else
                                if(class2 == Boolean.TYPE)
                                {
                                    vector1.add(Integer.valueOf(3));
                                    vector1.add(Integer.valueOf(field.getBoolean(tileentityfield.te) ? 1 : 0));
                                } else
                                if(class2 == Byte.TYPE)
                                {
                                    vector1.add(Integer.valueOf(4));
                                    vector1.add(Integer.valueOf(field.getByte(tileentityfield.te)));
                                } else
                                if(class2 == Short.TYPE)
                                {
                                    vector1.add(Integer.valueOf(5));
                                    vector1.add(Integer.valueOf(field.getShort(tileentityfield.te)));
                                } else
                                {
                                    throw new RuntimeException((new StringBuilder()).append("Invalid field type: ").append(field.getType()).toString());
                                }
                            }
                            catch(Exception exception)
                            {
                                throw new RuntimeException(exception);
                            }
                        }
                    }
                } while(true);
                if(vector1.size() > 1)
                {
                    int ai[] = new int[vector1.size()];
                    int k = 0;
                    for(Iterator iterator2 = vector1.iterator(); iterator2.hasNext();)
                    {
                        Integer integer = (Integer)iterator2.next();
                        ai[k++] = integer.intValue();
                    }

                    float af[] = new float[vector.size()];
                    k = 0;
                    for(Iterator iterator3 = vector.iterator(); iterator3.hasNext();)
                    {
                        Float float1 = (Float)iterator3.next();
                        af[k++] = float1.floatValue();
                    }

                    packet230modloader.dataInt = ai;
                    packet230modloader.dataFloat = af;
                    packet230modloader.dataString = (String[])vector2.toArray(new String[0]);
                    ModLoaderMp.SendPacketTo(ModLoaderMp.GetModInstance(net.minecraft.server.mod_IC2.class), entityplayermp, packet230modloader);
                }
            } while(true);
        }

        fieldsToUpdateSet.clear();
    }

    private static final int updatePeriod = 2;
    private static Set fieldsToUpdateSet = new HashSet();
    private static int ticksLeftToUpdate = 2;

}
