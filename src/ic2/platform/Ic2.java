// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.platform;

import ic2.common.EntityDynamite;
import ic2.common.EntityMiningLaser;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.*;

// Referenced classes of package ic2.platform:
//            NetworkManager

public abstract class Ic2 extends BaseModMp
{

    public Ic2()
    {
        mod_IC2.initialize();
        ModLoader.SetInGameHook(this, true, true);
        ModLoaderMp.RegisterEntityTrackerEntry(ic2.common.EntityMiningLaser.class, true, 141);
        ModLoaderMp.RegisterEntityTracker(ic2.common.EntityMiningLaser.class, 160, 40);
        ModLoaderMp.RegisterEntityTrackerEntry(ic2.common.EntityDynamite.class, true, 142);
        ModLoaderMp.RegisterEntityTracker(ic2.common.EntityDynamite.class, 160, 5);
    }

    public int AddFuel(int i)
    {
        return mod_IC2.AddFuelCommon(i, 0);
    }

    public void HandlePacket(Packet230ModLoader packet230modloader, EntityPlayer entityplayermp)
    {
        NetworkManager.handlePacket(packet230modloader, entityplayermp);
    }

    public void OnTickInGame(MinecraftServer minecraftserver)
    {
        WorldServer worldserver = minecraftserver.getWorldServer(0);
        mod_IC2.OnTickInGame(worldserver.players, worldserver);
        NetworkManager.onTick();
    }
}
