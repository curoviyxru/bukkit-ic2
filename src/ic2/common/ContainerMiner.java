// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import java.util.List;
import net.minecraft.server.*;

// Referenced classes of package ic2.common:
//            ContainerIC2, TileEntityMiner

public class ContainerMiner extends ContainerIC2
{

    public ContainerMiner(InventoryPlayer inventoryplayer, TileEntityMiner tileentityminer)
    {
        miningTicker = 0;
        energy = 0;
        tileentity = tileentityminer;
        addSlot(new Slot(tileentityminer, 0, 81, 59));
        addSlot(new Slot(tileentityminer, 1, 117, 22));
        addSlot(new Slot(tileentityminer, 2, 81, 22));
        addSlot(new Slot(tileentityminer, 3, 45, 22));
        for(int i = 0; i < 3; i++)
        {
            for(int k = 0; k < 9; k++)
            {
                addSlot(new Slot(inventoryplayer, k + i * 9 + 9, 8 + k * 18, 84 + i * 18));
            }

        }

        for(int j = 0; j < 9; j++)
        {
            addSlot(new Slot(inventoryplayer, j, 8 + j * 18, 142));
        }

    }

    public void updateCraftingMatrix()
    {
        super.updateCraftingMatrix();
        for(int i = 0; i < slots.size(); i++)
        {
            ICrafting icrafting = (ICrafting)slots.get(i);
            if(miningTicker != tileentity.miningTicker)
            {
                icrafting.updateCraftingInventoryInfo(this, 0, tileentity.miningTicker);
            }
            if(energy != tileentity.energy)
            {
                icrafting.updateCraftingInventoryInfo(this, 1, tileentity.energy & 0xffff);
                icrafting.updateCraftingInventoryInfo(this, 2, tileentity.energy >>> 16);
            }
        }

        miningTicker = tileentity.miningTicker;
        energy = tileentity.energy;
    }

    public void updateProgressBar(int i, int j)
    {
        switch(i)
        {
        case 0: // '\0'
            tileentity.miningTicker = (short)j;
            break;

        case 1: // '\001'
            tileentity.energy = tileentity.energy & 0xffff0000 | j;
            break;

        case 2: // '\002'
            tileentity.energy = tileentity.energy & 0xffff | j << 16;
            break;
        }
    }

    public boolean isUsableByPlayer(EntityPlayer entityplayer)
    {
        return tileentity.canInteractWith(entityplayer);
    }

    public int guiInventorySize()
    {
        return 4;
    }

    public int getInput()
    {
        return 0;
    }

    public TileEntityMiner tileentity;
    public short miningTicker;
    public int energy;
}
