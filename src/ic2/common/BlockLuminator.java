// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import forge.ITextureProvider;
import ic2.platform.BlockContainerCommon;
import java.util.Random;
import net.minecraft.server.*;

// Referenced classes of package ic2.common:
//            BlockPoleFence, BlockCable, ItemBattery, TileEntityLuminator

public class BlockLuminator extends BlockContainerCommon
    implements ITextureProvider
{

    public BlockLuminator(int i, boolean flag)
    {
        super(i, 31, Material.glass);
        setStepSound(soundGlassFootstep);
        light = flag;
        setHardness(0.3F);
        setResistance(0.5F);
    }

    public void onBlockAdded(World world, int i, int j, int k)
    {
        TileEntity tileentityblock = getBlockEntity();
        world.setBlockTileEntity(i, j, k, tileentityblock);
    }

    public int quantityDropped(Random random)
    {
        return 0;
    }

    public void onBlockPlaced(World world, int i, int j, int k, int l)
    {
        world.setBlockMetadata(i, j, k, l);
        super.onBlockPlaced(world, i, j, k, l);
    }

    public boolean canPlaceBlockOnSide(World world, int i, int j, int k, int l)
    {
        if(world.getBlockId(i, j, k) != 0)
        {
            return false;
        }
        switch(l)
        {
        case 0: // '\0'
            j++;
            break;

        case 1: // '\001'
            j--;
            break;

        case 2: // '\002'
            k++;
            break;

        case 3: // '\003'
            k--;
            break;

        case 4: // '\004'
            i++;
            break;

        case 5: // '\005'
            i--;
            break;
        }
        return isSupportingBlock(world, i, j, k);
    }

    public static boolean isSupportingBlock(World world, int i, int j, int k)
    {
        if(world.getBlockId(i, j, k) == 0)
        {
            return false;
        }
        if(world.isBlockOpaqueCube(i, j, k))
        {
            return true;
        } else
        {
            return isSpecialSupporter(world, i, j, k);
        }
    }

    public static boolean isSpecialSupporter(IBlockAccess iblockaccess, int i, int j, int k)
    {
        Block block = Block.blocksList[iblockaccess.getBlockId(i, j, k)];
        if((block instanceof BlockFence) || (block instanceof BlockPoleFence) || (block instanceof BlockCable))
        {
            return true;
        }
        return block == mod_IC2.blockAlloyGlass || block == Block.glass;
    }

    public boolean canBlockStay(World world, int i, int j, int k)
    {
        int l = world.getBlockMetadata(i, j, k);
        switch(l)
        {
        case 0: // '\0'
            j++;
            break;

        case 1: // '\001'
            j--;
            break;

        case 2: // '\002'
            k++;
            break;

        case 3: // '\003'
            k--;
            break;

        case 4: // '\004'
            i++;
            break;

        case 5: // '\005'
            i--;
            break;
        }
        return isSupportingBlock(world, i, j, k);
    }

    public void onNeighborBlockChange(World world, int i, int j, int k, int l)
    {
        if(!canBlockStay(world, i, j, k))
        {
            world.setBlockWithNotify(i, j, k, 0);
        }
        super.onNeighborBlockChange(world, i, j, k, l);
    }

    public boolean renderAsNormalBlock()
    {
        return false;
    }

    public int getRenderType()
    {
        return mod_IC2.luminatorRenderId;
    }

    public AxisAlignedBB getCollisionBoundingBoxFromPool(World world, int i, int j, int k)
    {
        float af[] = getBoxOfLuminator(world, i, j, k);
        return AxisAlignedBB.getBoundingBoxFromPool(af[0] + (float)i, af[1] + (float)j, af[2] + (float)k, af[3] + (float)i, af[4] + (float)j, af[5] + (float)k);
    }

    public AxisAlignedBB getSelectedBoundingBoxFromPool(World world, int i, int j, int k)
    {
        float af[] = getBoxOfLuminator(world, i, j, k);
        return AxisAlignedBB.getBoundingBoxFromPool(af[0] + (float)i, af[1] + (float)j, af[2] + (float)k, af[3] + (float)i, af[4] + (float)j, af[5] + (float)k);
    }

    public static float[] getBoxOfLuminator(IBlockAccess iblockaccess, int i, int j, int k)
    {
        int l = iblockaccess.getBlockMetadata(i, j, k);
        float f = 0.0625F;
        switch(l)
        {
        case 0: // '\0'
            j++;
            break;

        case 1: // '\001'
            j--;
            break;

        case 2: // '\002'
            k++;
            break;

        case 3: // '\003'
            k--;
            break;

        case 4: // '\004'
            i++;
            break;

        case 5: // '\005'
            i--;
            break;
        }
        boolean flag = isSpecialSupporter(iblockaccess, i, j, k);
        switch(l)
        {
        case 1: // '\001'
            return (new float[] {
                0.0F, 0.0F, 0.0F, 1.0F, 1.0F * f, 1.0F
            });

        case 2: // '\002'
            if(flag)
            {
                return (new float[] {
                    0.0F, 0.0F, 15F * f, 1.0F, 1.0F, 1.0F
                });
            } else
            {
                return (new float[] {
                    6F * f, 3F * f, 14F * f, 1.0F - 6F * f, 1.0F - 3F * f, 1.0F
                });
            }

        case 3: // '\003'
            if(flag)
            {
                return (new float[] {
                    0.0F, 0.0F, 0.0F, 1.0F, 1.0F, 1.0F * f
                });
            } else
            {
                return (new float[] {
                    6F * f, 3F * f, 0.0F, 1.0F - 6F * f, 1.0F - 3F * f, 2.0F * f
                });
            }

        case 4: // '\004'
            if(flag)
            {
                return (new float[] {
                    15F * f, 0.0F, 0.0F, 1.0F, 1.0F, 1.0F
                });
            } else
            {
                return (new float[] {
                    14F * f, 3F * f, 6F * f, 1.0F, 1.0F - 3F * f, 1.0F - 6F * f
                });
            }

        case 5: // '\005'
            if(flag)
            {
                return (new float[] {
                    0.0F, 0.0F, 0.0F, 1.0F * f, 1.0F, 1.0F
                });
            } else
            {
                return (new float[] {
                    0.0F, 3F * f, 6F * f, 2.0F * f, 1.0F - 3F * f, 1.0F - 6F * f
                });
            }
        }
        if(flag)
        {
            return (new float[] {
                0.0F, 15F * f, 0.0F, 1.0F, 1.0F, 1.0F
            });
        } else
        {
            return (new float[] {
                4F * f, 13F * f, 4F * f, 1.0F - 4F * f, 1.0F, 1.0F - 4F * f
            });
        }
    }

    public boolean isOpaqueCube()
    {
        return false;
    }

    public boolean isBlockNormalCube(World world, int i, int j, int k)
    {
        return false;
    }

    public boolean blockActivated(World world, int i, int j, int k, EntityPlayer entityplayer)
    {
        ItemStack itemstack = entityplayer.getCurrentEquippedItem();
        if(itemstack == null || !(itemstack.getItem() instanceof ItemBattery))
        {
            return false;
        }
        ItemBattery itembattery = (ItemBattery)itemstack.getItem();
        TileEntityLuminator tileentityluminator = (TileEntityLuminator)world.getBlockTileEntity(i, j, k);
        int l = tileentityluminator.getMaxEnergy() - tileentityluminator.energy;
        if(l <= 0)
        {
            return false;
        }
        l = itembattery.getEnergyFrom(itemstack, l, 2);
        if(light)
        {
            tileentityluminator.energy += l;
        } else
        {
            world.setBlockAndMetadata(i, j, k, mod_IC2.blockLuminator.blockID, world.getBlockMetadata(i, j, k));
            TileEntityLuminator tileentityluminator1 = (TileEntityLuminator)world.getBlockTileEntity(i, j, k);
            tileentityluminator1.energy += l;
        }
        return true;
    }

    public void onEntityCollidedWithBlock(World world, int i, int j, int k, Entity entity)
    {
        if(light && (entity instanceof EntityMob))
        {
            boolean flag = (entity instanceof EntitySkeleton) || (entity instanceof EntityZombie);
            entity.fire++;
            if(flag)
            {
                entity.fire += 3;
            }
            if(entity.fire < 20)
            {
                entity.fire = 20;
            }
            if(entity.fire > 100 && !flag)
            {
                entity.fire = 100;
            }
        }
    }

    public TileEntity getBlockEntity()
    {
        return new TileEntityLuminator();
    }

    public String getTextureFile()
    {
        return "/ic2/sprites/block_0.png";
    }

    boolean light;
}
