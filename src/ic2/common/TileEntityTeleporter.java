// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import ic2.api.Direction;
import ic2.platform.*;
import java.util.*;

import ic2.platform.NetworkManager;
import net.minecraft.server.*;

// Referenced classes of package ic2.common:
//            TileEntityBlock, INetworkTileEntityEventListener, TileEntityElectricBlock, PositionSpec, 
//            AudioPosition

public class TileEntityTeleporter extends TileEntityBlock
    implements INetworkTileEntityEventListener
{

    public TileEntityTeleporter()
    {
        targetSet = false;
        audioSource = null;
    }

    public void readFromNBT(NBTTagCompound nbttagcompound)
    {
        super.readFromNBT(nbttagcompound);
        targetSet = nbttagcompound.getBoolean("targetSet");
        targetX = nbttagcompound.getInteger("targetX");
        targetY = nbttagcompound.getInteger("targetY");
        targetZ = nbttagcompound.getInteger("targetZ");
    }

    public void writeToNBT(NBTTagCompound nbttagcompound)
    {
        super.writeToNBT(nbttagcompound);
        nbttagcompound.setBoolean("targetSet", targetSet);
        nbttagcompound.setInteger("targetX", targetX);
        nbttagcompound.setInteger("targetY", targetY);
        nbttagcompound.setInteger("targetZ", targetZ);
    }

    public void updateEntity()
    {
        super.updateEntity();
        if(Platform.isSimulating())
        {
            if(worldObj.isBlockIndirectlyGettingPowered(xCoord, yCoord, zCoord) && targetSet)
            {
                Chunk chunk = Platform.getOrLoadChunk(worldObj, targetX >> 4, targetZ >> 4);
                if(chunk == null || chunk.getBlockID(targetX & 0xf, targetY, targetZ & 0xf) != mod_IC2.blockMachine2.blockID || chunk.getBlockMetadata(targetX & 0xf, targetY, targetZ & 0xf) != 0)
                {
                    targetSet = false;
                    setActive(false);
                } else
                {
                    setActive(true);
                    List list = worldObj.getEntitiesWithinAABB(net.minecraft.server.EntityLiving.class, AxisAlignedBB.getBoundingBox(xCoord - 1, yCoord, zCoord - 1, xCoord + 2, yCoord + 3, zCoord + 2));
                    if(!list.isEmpty())
                    {
                        double d = 1.7976931348623157E+308D;
                        EntityLiving entityliving = null;
                        Iterator iterator = list.iterator();
                        do
                        {
                            if(!iterator.hasNext())
                            {
                                break;
                            }
                            EntityLiving entityliving1 = (EntityLiving)iterator.next();
                            double d1 = ((double)xCoord - entityliving1.posX) * ((double)xCoord - entityliving1.posX) + ((double)(yCoord + 1) - entityliving1.posY) * ((double)(yCoord + 1) - entityliving1.posY) + ((double)zCoord - entityliving1.posZ) * ((double)zCoord - entityliving1.posZ);
                            if(d1 < d)
                            {
                                d = d1;
                                entityliving = entityliving1;
                            }
                        } while(true);
                        teleport(entityliving);
                    }
                }
            } else
            {
                setActive(false);
            }
        }
        if(Platform.isRendering() && getActive())
        {
            spawnBlueParticles(2, xCoord, yCoord, zCoord);
        }
    }

    public void invalidate()
    {
        if(Platform.isRendering() && audioSource != null)
        {
            AudioManager.removeSources(this);
            audioSource = null;
        }
        super.invalidate();
    }

    public void teleport(EntityLiving entityliving)
    {
        double d = Math.sqrt((xCoord - targetX) * (xCoord - targetX) + (yCoord - targetY) * (yCoord - targetY) + (zCoord - targetZ) * (zCoord - targetZ));
        int i = getWeightOf(entityliving);
        int j = (int)((double)i * Math.pow(d + 10D, 0.69999999999999996D) * 5D);
        if(j > getAvailableEnergy())
        {
            return;
        } else
        {
            consumeEnergy(j);
            Platform.teleportTo(entityliving, (double)targetX + 0.5D, (double)targetY + 1.5D + entityliving.getYOffset(), (double)targetZ + 0.5D, entityliving.rotationYaw, entityliving.rotationPitch);
            NetworkManager.initiateTileEntityEvent(this, 0, true);
            return;
        }
    }

    public void spawnBlueParticles(int i, int j, int k, int l)
    {
        for(int i1 = 0; i1 < i; i1++)
        {
            worldObj.spawnParticle("reddust", (float)j + worldObj.rand.nextFloat(), (float)(k + 1) + worldObj.rand.nextFloat(), (float)l + worldObj.rand.nextFloat(), -1D, 0.0D, 1.0D);
            worldObj.spawnParticle("reddust", (float)j + worldObj.rand.nextFloat(), (float)(k + 2) + worldObj.rand.nextFloat(), (float)l + worldObj.rand.nextFloat(), -1D, 0.0D, 1.0D);
        }

    }

    public void consumeEnergy(int i)
    {
        LinkedList linkedlist = new LinkedList();
        Direction adirection[] = Direction.values();
        int k = adirection.length;
        for(int l = 0; l < k; l++)
        {
            Direction direction = adirection[l];
            net.minecraft.server.TileEntity tileentity = direction.applyToTileEntity(this);
            if(!(tileentity instanceof TileEntityElectricBlock))
            {
                continue;
            }
            TileEntityElectricBlock tileentityelectricblock1 = (TileEntityElectricBlock)tileentity;
            if(tileentityelectricblock1.energy > 0)
            {
                linkedlist.add(tileentityelectricblock1);
            }
        }

        while(i > 0) 
        {
            int j = ((i + linkedlist.size()) - 1) / linkedlist.size();
            Iterator iterator = linkedlist.iterator();
            while(iterator.hasNext()) 
            {
                TileEntityElectricBlock tileentityelectricblock = (TileEntityElectricBlock)iterator.next();
                if(j > i)
                {
                    j = i;
                }
                if(tileentityelectricblock.energy <= j)
                {
                    i -= tileentityelectricblock.energy;
                    tileentityelectricblock.energy = 0;
                    iterator.remove();
                } else
                {
                    i -= j;
                    tileentityelectricblock.energy -= j;
                }
            }
        }
    }

    public int getAvailableEnergy()
    {
        int i = 0;
        Direction adirection[] = Direction.values();
        int j = adirection.length;
        for(int k = 0; k < j; k++)
        {
            Direction direction = adirection[k];
            net.minecraft.server.TileEntity tileentity = direction.applyToTileEntity(this);
            if(tileentity instanceof TileEntityElectricBlock)
            {
                i += ((TileEntityElectricBlock)tileentity).energy;
            }
        }

        return i;
    }

    public int getWeightOf(EntityLiving entityliving)
    {
        if(entityliving instanceof EntityAnimal)
        {
            return 100;
        }
        if(entityliving instanceof EntityPlayer)
        {
            int i = 1000;
            InventoryPlayer inventoryplayer = ((EntityPlayer)entityliving).inventory;
            for(int j = 0; j < inventoryplayer.mainInventory.length; j++)
            {
                if(inventoryplayer.mainInventory[j] != null)
                {
                    i += (100 * inventoryplayer.mainInventory[j].stackSize) / inventoryplayer.mainInventory[j].getMaxStackSize();
                }
            }

            for(int k = 0; k < inventoryplayer.armorInventory.length; k++)
            {
                if(inventoryplayer.armorInventory[k] != null)
                {
                    i += 100;
                }
            }

            return i;
        } else
        {
            return 500;
        }
    }

    public void setTarget(int i, int j, int k)
    {
        targetSet = true;
        targetX = i;
        targetY = j;
        targetZ = k;
        NetworkManager.updateTileEntityField(this, "targetX");
        NetworkManager.updateTileEntityField(this, "targetY");
        NetworkManager.updateTileEntityField(this, "targetZ");
    }

    public List getNetworkedFields()
    {
        Vector vector = new Vector(3);
        vector.add("targetX");
        vector.add("targetY");
        vector.add("targetZ");
        vector.addAll(super.getNetworkedFields());
        return vector;
    }

    public void onNetworkUpdate(String s)
    {
        if(s.equals("active") && prevActive != getActive())
        {
            if(audioSource == null)
            {
                audioSource = AudioManager.createSource(this, PositionSpec.Center, "Machines/Teleporter/TeleChargedLoop.ogg", true, false, AudioManager.defaultVolume);
            }
            if(getActive())
            {
                if(audioSource != null)
                {
                    audioSource.play();
                }
            } else
            if(audioSource != null)
            {
                audioSource.stop();
            }
        }
        super.onNetworkUpdate(s);
    }

    public void onNetworkEvent(int i)
    {
        switch(i)
        {
        case 0: // '\0'
            AudioManager.playOnce(this, PositionSpec.Center, "Machines/Teleporter/TeleUse.ogg", true, AudioManager.defaultVolume);
            AudioManager.playOnce(new AudioPosition((float)targetX + 0.5F, (float)targetY + 0.5F, (float)targetZ + 0.5F), PositionSpec.Center, "Machines/Teleporter/TeleUse.ogg", true, AudioManager.defaultVolume);
            spawnBlueParticles(20, xCoord, yCoord, zCoord);
            spawnBlueParticles(20, targetX, targetY, targetZ);
            break;

        default:
            throw new RuntimeException((new StringBuilder()).append("Unknown network event: ").append(i).toString());
        }
    }

    public boolean targetSet;
    public int targetX;
    public int targetY;
    public int targetZ;
    private AudioSource audioSource;
    private static final int EventTeleport = 0;
}
