// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import forge.ISpecialResistance;
import java.util.*;
import ic2.api.EnergyNet;
import net.minecraft.server.*;

public class ExplosionIC2
{
    static class DropData
    {

        public DropData add(int i, int j)
        {
            n += i;
            if(j > e)
            {
                e = j;
            }
            return this;
        }

        int n;
        int e;

        DropData(int i, int j)
        {
            n = i;
            e = j;
        }
    }

    static class ItemWithMeta
    {

        public boolean equals(Object obj)
        {
            if(obj instanceof ItemWithMeta)
            {
                ItemWithMeta itemwithmeta = (ItemWithMeta)obj;
                return itemwithmeta.itemId == itemId && itemwithmeta.metaData == metaData;
            } else
            {
                return false;
            }
        }

        public int hashCode()
        {
            return itemId * 31 ^ metaData;
        }

        int itemId;
        int metaData;

        ItemWithMeta(int i, int j)
        {
            itemId = i;
            metaData = j;
        }
    }

    static class XZposition
    {

        public boolean equals(Object obj)
        {
            if(obj instanceof XZposition)
            {
                XZposition xzposition = (XZposition)obj;
                return xzposition.x == x && xzposition.z == z;
            } else
            {
                return false;
            }
        }

        public int hashCode()
        {
            return x * 31 ^ z;
        }

        int x;
        int z;

        XZposition(int i, int j)
        {
            x = i;
            z = j;
        }
    }


    public ExplosionIC2(World world, Entity entity, double d, double d1, double d2, float f, float f1, float f2)
    {
        ExplosionRNG = new Random();
        destroyedBlockPositions = new HashMap();
        worldObj = world;
        exploder = entity;
        power = f;
        explosionDropRate = f1;
        explosionDamage = f2;
        explosionX = d;
        explosionY = d1;
        explosionZ = d2;
    }

    public void doExplosion()
    {
        if(power <= 0.0F)
        {
            return;
        }
        double d = (double)power / 0.40000000000000002D;
        entitiesInRange = worldObj.a(net.minecraft.server.EntityLiving.class, AxisAlignedBB.a(explosionX - d, explosionY - d, explosionZ - d, explosionX + d, explosionY + d, explosionZ + d));
        int i = (int)Math.ceil(3.1415926535897931D / Math.atan(1.0D / d));
        for(int j = 0; j < 2 * i; j++)
        {
            for(int k = 0; k < i; k++)
            {
                double d1 = (6.2831853071795862D / (double)i) * (double)j;
                double d2 = (3.1415926535897931D / (double)i) * (double)k;
                shootRay(explosionX, explosionY, explosionZ, d1, d2, power, j % 8 == 0 && k % 8 == 0);
            }

        }

        worldObj.makeSound(explosionX, explosionY, explosionZ, "random.explode", 4F, (1.0F + (worldObj.random.nextFloat() - worldObj.random.nextFloat()) * 0.2F) * 0.7F);
        HashMap hashmap = new HashMap();
        Iterator iterator = destroyedBlockPositions.entrySet().iterator();
        do
        {
            if(!iterator.hasNext())
            {
                break;
            }
            java.util.Map.Entry entry = (java.util.Map.Entry)iterator.next();
            int l = ((ChunkPosition)entry.getKey()).x;
            int i1 = ((ChunkPosition)entry.getKey()).y;
            int j1 = ((ChunkPosition)entry.getKey()).z;
            int k1 = worldObj.getTypeId(l, i1, j1);
            if(k1 != 0)
            {
                if(((Boolean)entry.getValue()).booleanValue())
                {
                    double d3 = (float)l + worldObj.random.nextFloat();
                    double d4 = (float)i1 + worldObj.random.nextFloat();
                    double d5 = (float)j1 + worldObj.random.nextFloat();
                    double d6 = d3 - explosionX;
                    double d7 = d4 - explosionY;
                    double d8 = d5 - explosionZ;
                    double d9 = MathHelper.a(d6 * d6 + d7 * d7 + d8 * d8);
                    d6 /= d9;
                    d7 /= d9;
                    d8 /= d9;
                    double d10 = 0.5D / (d9 / (double)power + 0.10000000000000001D);
                    d10 *= worldObj.random.nextFloat() * worldObj.random.nextFloat() + 0.3F;
                    d6 *= d10;
                    d7 *= d10;
                    d8 *= d10;
                    worldObj.a("explode", (d3 + explosionX) / 2D, (d4 + explosionY) / 2D, (d5 + explosionZ) / 2D, d6, d7, d8);
                    worldObj.a("smoke", d3, d4, d5, d6, d7, d8);
                    Block block = Block.byId[k1];
                    if(worldObj.random.nextFloat() <= explosionDropRate)
                    {
                        int j2 = worldObj.getData(l, i1, j1);
                        for(Iterator iterator3 = mod_IC2.getDrop(worldObj, block, j2).iterator(); iterator3.hasNext();)
                        {
                            ItemStack itemstack = (ItemStack)iterator3.next();
                            XZposition xzposition1 = new XZposition(l / 2, j1 / 2);
                            if(!hashmap.containsKey(xzposition1))
                            {
                                hashmap.put(xzposition1, new HashMap());
                            }
                            Map map = (Map)hashmap.get(xzposition1);
                            ItemWithMeta itemwithmeta1 = new ItemWithMeta(itemstack.id, itemstack.getData());
                            if(!map.containsKey(itemwithmeta1))
                            {
                                map.put(itemwithmeta1, new DropData(itemstack.count, i1));
                            } else
                            {
                                map.put(itemwithmeta1, ((DropData)map.get(itemwithmeta1)).add(itemstack.count, i1));
                            }
                        }

                    }
                }
                Block.byId[k1].d(worldObj, l, i1, j1);
                worldObj.setTypeId(l, i1, j1, 0);
            }
        } while(true);
        for(Iterator iterator1 = hashmap.entrySet().iterator(); iterator1.hasNext();)
        {
            java.util.Map.Entry entry1 = (java.util.Map.Entry)iterator1.next();
            XZposition xzposition = (XZposition)entry1.getKey();
            Iterator iterator2 = ((Map)entry1.getValue()).entrySet().iterator();
            while(iterator2.hasNext()) 
            {
                java.util.Map.Entry entry2 = (java.util.Map.Entry)iterator2.next();
                ItemWithMeta itemwithmeta = (ItemWithMeta)entry2.getKey();
                int l1 = ((DropData)entry2.getValue()).n;
                while(l1 > 0) 
                {
                    int i2 = Math.min(l1, 64);
                    EntityItem entityitem = new EntityItem(worldObj, (double)((float)xzposition.x + worldObj.random.nextFloat()) * 2D, (double)((DropData)entry2.getValue()).e + 0.5D, (double)((float)xzposition.z + worldObj.random.nextFloat()) * 2D, new ItemStack(itemwithmeta.itemId, i2, itemwithmeta.metaData));
                    entityitem.pickupDelay = 10;
                    worldObj.playerJoinedWorld(entityitem);
                    l1 -= i2;
                }
            }
        }

    }

    private void shootRay(double d, double d1, double d2, double d3, double d4, double d5, boolean flag)
    {
        double d6 = Math.sin(d4) * Math.cos(d3);
        double d7 = Math.cos(d4);
        double d8 = Math.sin(d4) * Math.sin(d3);
        do
        {
            int i = worldObj.getTypeId((int)d, (int)d1, (int)d2);
            double d9 = 0.5D;
            if(i > 0)
            {
                if(Block.byId[i] instanceof ISpecialResistance)
                {
                    ISpecialResistance ispecialresistance = (ISpecialResistance)Block.byId[i];
                    d9 += ((double)ispecialresistance.getSpecialExplosionResistance(worldObj, (int)d, (int)d1, (int)d2, explosionX, explosionY, explosionZ, exploder) + 4D) * 0.29999999999999999D;
                } else
                {
                    d9 += ((double)Block.byId[i].a(exploder) + 4D) * 0.29999999999999999D;
                }
            }
            if(d9 <= d5)
            {
                if(i > 0)
                {
                    ChunkPosition chunkposition = new ChunkPosition((int)d, (int)d1, (int)d2);
                    if(!destroyedBlockPositions.containsKey(chunkposition) || d5 > 8D && ((Boolean)destroyedBlockPositions.get(chunkposition)).booleanValue())
                    {
                        destroyedBlockPositions.put(chunkposition, Boolean.valueOf(d5 <= 8D));
                    }
                }
                if(flag)
                {
                    Iterator iterator = entitiesInRange.iterator();
                    do
                    {
                        if(!iterator.hasNext())
                        {
                            break;
                        }
                        EntityLiving entityliving = (EntityLiving)iterator.next();
                        if((entityliving.locX - d) * (entityliving.locX - d) + (entityliving.locY - d1) * (entityliving.locY - d1) + (entityliving.locZ - d2) * (entityliving.locZ - d2) <= 25D)
                        {
                            double d10 = entityliving.locX - explosionX;
                            double d11 = entityliving.locY - explosionY;
                            double d12 = entityliving.locZ - explosionZ;
                            double d13 = Math.sqrt(d10 * d10 + d11 * d11 + d12 * d12);
                            double d14 = d5 / 2D / (Math.pow(d13, 0.80000000000000004D) + 1.0D);
                            entityliving.damageEntity(null, (int)Math.pow(d14 * 3D, 2D)); //todo ???
                            d10 /= d13;
                            d11 /= d13;
                            d12 /= d13;
                            entityliving.motX += d10 * d14;
                            entityliving.motY += d11 * d14;
                            entityliving.motZ += d12 * d14;
                            iterator.remove();
                        }
                    } while(true);
                }
                if(d9 > 10D)
                {
                    for(int j = 0; j < 5; j++)
                    {
                        shootRay(d, d1, d2, ExplosionRNG.nextDouble() * 2D * 3.1415926535897931D, ExplosionRNG.nextDouble() * 3.1415926535897931D, d9 * 0.40000000000000002D, false);
                    }

                }
                d5 -= d9;
                d += d6;
                d1 += d7;
                d2 += d8;
            } else
            {
                return;
            }
        } while(true);
    }

    private Random ExplosionRNG;
    private World worldObj;
    public double explosionX;
    public double explosionY;
    public double explosionZ;
    public Entity exploder;
    public float power;
    public float explosionDropRate;
    public float explosionDamage;
    public List entitiesInRange;
    public Map destroyedBlockPositions;
    private final double dropPowerLimit = 8D;
    private final int secondaryRayCount = 5;
}
