// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import ic2.api.IPaintableBlock;
import ic2.platform.*;
import ic2.platform.NetworkManager;
import net.minecraft.server.*;

// Referenced classes of package ic2.common:
//            ItemIC2, PositionSpec

public class ItemToolPainter extends ItemIC2
{

    public ItemToolPainter(int i, int j)
    {
        super(i, 128);
        setMaxDamage(32);
        setMaxStackSize(1);
        color = j;
    }

    public int getIconFromDamage(int i)
    {
        return iconIndex + color;
    }

    public boolean onItemUse(ItemStack itemstack, EntityPlayer entityplayer, World world, int i, int j, int k, int l)
    {
        int i1 = world.getBlockId(i, j, k);
        if(i1 > 0 && (Block.blocksList[i1] instanceof IPaintableBlock) && ((IPaintableBlock)Block.blocksList[i1]).colorBlock(world, i, j, k, color))
        {
            if(Platform.isSimulating())
            {
                NetworkManager.announceBlockUpdate(world, i, j, k);
                if(itemstack.getData() >= 31)
                {
                    itemstack.setItemDamage(0);
                    itemstack.id = mod_IC2.itemToolPainter.shiftedIndex;
                } else
                {
                    itemstack.damageItem(1, null);
                }
            }
            if(Platform.isRendering())
            {
                AudioManager.playOnce(entityplayer, PositionSpec.Hand, "Tools/Painter.ogg", true, AudioManager.defaultVolume);
            }
            return true;
        } else
        {
            return false;
        }
    }

    public int color;
}
