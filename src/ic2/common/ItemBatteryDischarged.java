// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import net.minecraft.server.*;

// Referenced classes of package ic2.common:
//            ItemBattery

public class ItemBatteryDischarged extends ItemBattery
{

    public ItemBatteryDischarged(int i, int j, int k, int l, boolean flag, int i1)
    {
        super(i, j, k, l, flag, i1);
        setMaxDamage(0);
        setMaxStackSize(16);
    }

    public int getEnergyFrom(ItemStack itemstack, int i, int j)
    {
        return 0;
    }

    public int giveEnergyTo(ItemStack itemstack, int i, int j, boolean flag)
    {
        if(i <= 0)
        {
            return 0;
        }
        if(itemstack.stackSize != 1)
        {
            return 0;
        } else
        {
            itemstack.id = mod_IC2.itemBatRE.shiftedIndex;
            itemstack.setItemDamage(10001);
            return super.giveEnergyTo(itemstack, i, j, flag);
        }
    }

    public int getIconFromDamage(int i)
    {
        return iconIndex;
    }
}
