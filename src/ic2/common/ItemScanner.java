// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import ic2.platform.AudioManager;
import ic2.platform.Platform;
import java.util.Map;
import net.minecraft.server.*;

// Referenced classes of package ic2.common:
//            ElectricItem, PositionSpec

public class ItemScanner extends ElectricItem
{

    public ItemScanner(int i, int j, int k)
    {
        super(i, j, k, 50, 50);
        setMaxDamage(202);
        setMaxStackSize(1);
    }

    public ItemStack onItemRightClick(ItemStack itemstack, World world, EntityPlayer entityplayer)
    {
        if(tier == 1 && !use(itemstack, 1, entityplayer) || tier == 2 && !use(itemstack, 5, entityplayer))
        {
            return itemstack;
        }
        AudioManager.playOnce(entityplayer, PositionSpec.Hand, "Tools/ODScanner.ogg", true, AudioManager.defaultVolume);
        if(tier == 2)
        {
            int i = valueOfArea(world, (int)entityplayer.posX, (int)entityplayer.posY, (int)entityplayer.posZ, true);
            Platform.messagePlayer(entityplayer, (new StringBuilder()).append("SCAN RESULT: Ore value in this area is ").append(i).toString());
        } else
        {
            int j = valueOfArea(world, (int)entityplayer.posX, (int)entityplayer.posY, (int)entityplayer.posZ, false);
            Platform.messagePlayer(entityplayer, (new StringBuilder()).append("SCAN RESULT: Ore density in this area is ").append(j).toString());
        }
        return itemstack;
    }

    public static int valueOfArea(World world, int i, int j, int k, boolean flag)
    {
        int l = 0;
        int i1 = 0;
        byte byte0 = ((byte)(flag ? 4 : 2));
        for(int j1 = j; j1 > 0; j1--)
        {
            for(int k1 = i - byte0; k1 <= i + byte0; k1++)
            {
                for(int l1 = k - byte0; l1 <= k + byte0; l1++)
                {
                    int i2 = world.getBlockId(k1, j1, l1);
                    int j2 = world.getBlockMetadata(i, j, k);
                    if(flag)
                    {
                        l += valueOf(i2, j2);
                    } else
                    if(isValuable(i2, j2))
                    {
                        l++;
                    }
                    i1++;
                }

            }

        }

        return (i1 <= 0 ? null : Integer.valueOf((int)((1000D * (double)l) / (double)i1))).intValue();
    }

    public static boolean isValuable(int i, int j)
    {
        return valueOf(i, j) > 0;
    }

    public static int valueOf(int i, int j)
    {
        if(mod_IC2.valuableOres.containsKey(Integer.valueOf(i)))
        {
            Map map = (Map)mod_IC2.valuableOres.get(Integer.valueOf(i));
            if(map.containsKey(Integer.valueOf(-1)))
            {
                return ((Integer)map.get(Integer.valueOf(-1))).intValue();
            }
            if(map.containsKey(Integer.valueOf(j)))
            {
                return ((Integer)map.get(Integer.valueOf(j))).intValue();
            }
        }
        return 0;
    }

    public int startLayerScan(ItemStack itemstack)
    {
        return use(itemstack, 1, null) ? 2 : 0;
    }
}
