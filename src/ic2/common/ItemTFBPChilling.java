// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import net.minecraft.server.Block;
import net.minecraft.server.World;

// Referenced classes of package ic2.common:
//            ItemTFBP, TileEntityTerra

public class ItemTFBPChilling extends ItemTFBP
{

    public ItemTFBPChilling(int i, int j)
    {
        super(i, j);
    }

    public int getConsume()
    {
        return 2000;
    }

    public int getRange()
    {
        return 50;
    }

    public boolean terraform(World world, int i, int j, int k)
    {
        int l = TileEntityTerra.getFirstBlockFrom(world, i, j, k + 10);
        if(l == -1)
        {
            return false;
        }
        int i1 = world.getBlockId(i, l, j);
        if(i1 == Block.waterMoving.blockID || i1 == Block.waterStill.blockID)
        {
            world.setBlockWithNotify(i, l, j, Block.ice.blockID);
            return true;
        }
        if(i1 == Block.ice.blockID)
        {
            int j1 = world.getBlockId(i, l - 1, j);
            if(j1 == Block.waterMoving.blockID || j1 == Block.waterStill.blockID)
            {
                world.setBlockWithNotify(i, l - 1, j, Block.ice.blockID);
                return true;
            }
        }
        if(i1 == Block.snow.blockID && isSurroundedBySnow(world, i, l, j))
        {
            world.setBlockWithNotify(i, l, j, Block.blockSnow.blockID);
            return true;
        }
        if(Block.snow.canPlaceBlockAt(world, i, l + 1, j) || i1 == Block.ice.blockID)
        {
            world.setBlockWithNotify(i, l + 1, j, Block.snow.blockID);
        }
        return false;
    }

    public boolean isSurroundedBySnow(World world, int i, int j, int k)
    {
        return isSnowHere(world, i + 1, j, k) && isSnowHere(world, i - 1, j, k) && isSnowHere(world, i, j, k + 1) && isSnowHere(world, i, j, k - 1);
    }

    public boolean isSnowHere(World world, int i, int j, int k)
    {
        int l = j;
        j = TileEntityTerra.getFirstBlockFrom(world, i, k, j + 16);
        if(l > j)
        {
            return false;
        }
        int i1 = world.getBlockId(i, j, k);
        if(i1 == Block.snow.blockID || i1 == Block.blockSnow.blockID)
        {
            return true;
        }
        if(Block.snow.canPlaceBlockAt(world, i, j + 1, k) || i1 == Block.ice.blockID)
        {
            world.setBlockWithNotify(i, j + 1, k, Block.snow.blockID);
        }
        return false;
    }
}
