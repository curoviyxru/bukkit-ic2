// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import forge.ITextureProvider;
import net.minecraft.server.EnumToolMaterial;
import net.minecraft.server.ItemHoe;

public class ItemIC2Hoe extends ItemHoe
    implements ITextureProvider
{

    public ItemIC2Hoe(int i, int j, EnumToolMaterial enumtoolmaterial)
    {
        super(i, enumtoolmaterial);
        setIconIndex(j);
    }

    public String getTextureFile()
    {
        return "/ic2/sprites/item_0.png";
    }
}
