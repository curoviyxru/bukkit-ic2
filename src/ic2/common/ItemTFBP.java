// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import net.minecraft.server.*;

// Referenced classes of package ic2.common:
//            ItemIC2, ITerraformingBP, TileEntityTerra

public abstract class ItemTFBP extends ItemIC2
    implements ITerraformingBP
{

    public ItemTFBP(int i, int j)
    {
        super(i, j);
        setMaxStackSize(1);
    }

    public boolean onItemUse(ItemStack itemstack, EntityPlayer entityplayer, World world, int i, int j, int k, int l)
    {
        if(world.getBlockTileEntity(i, j, k) instanceof TileEntityTerra)
        {
            ((TileEntityTerra)world.getBlockTileEntity(i, j, k)).insertBlueprint(itemstack);
            entityplayer.inventory.mainInventory[entityplayer.inventory.currentItem] = null;
            return true;
        } else
        {
            return false;
        }
    }
}
