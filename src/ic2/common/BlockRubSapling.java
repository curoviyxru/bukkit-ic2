// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import forge.ITextureProvider;
import ic2.platform.Platform;
import java.util.Random;
import net.minecraft.server.*;

// Referenced classes of package ic2.common:
//            WorldGenRubTree

public class BlockRubSapling extends BlockSapling
    implements ITextureProvider
{

    public BlockRubSapling(int i, int j)
    {
        super(i, j);
    }

    public String getTextureFile()
    {
        return "/ic2/sprites/block_0.png";
    }

    public int getBlockTextureFromSideAndMetadata(int i, int j)
    {
        return blockIndexInTexture;
    }

    public void updateTick(World world, int i, int j, int k, Random random)
    {
        if(!Platform.isSimulating())
        {
            return;
        }
        if(!canBlockStay(world, i, j, k))
        {
            dropBlockAsItem(world, i, j, k, world.getBlockMetadata(i, j, k));
            world.setBlockWithNotify(i, j, k, 0);
            return;
        }
        if(world.getBlockLightValue(i, j + 1, k) >= 9 && random.nextInt(30) == 0)
        {
            growTree(world, i, j, k, random);
        }
    }

    public void growTree(World world, int i, int j, int k, Random random)
    {
        (new WorldGenRubTree()).grow(world, i, j, k, random);
    }

    protected int damageDropped(int i)
    {
        return 0;
    }

    public boolean blockActivated(World world, int i, int j, int k, EntityPlayer entityplayer)
    {
        if(!Platform.isSimulating())
        {
            return false;
        }
        ItemStack itemstack = entityplayer.getCurrentEquippedItem();
        if(itemstack == null)
        {
            return false;
        }
        if(itemstack.getItem() == Item.INK_SACK && itemstack.getData() == 15)
        {
            growTree(world, i, j, k, world.rand);
            itemstack.stackSize--;
        }
        return false;
    }
}
