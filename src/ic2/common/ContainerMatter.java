// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import java.util.List;
import net.minecraft.server.*;

// Referenced classes of package ic2.common:
//            ContainerIC2, TileEntityMatter

public class ContainerMatter extends ContainerIC2
{

    public ContainerMatter(InventoryPlayer inventoryplayer, TileEntityMatter tileentitymatter)
    {
        energy = 0;
        scrap = 0;
        tileentity = tileentitymatter;
        addSlot(new Slot(tileentitymatter, 0, 114, 54));
        addSlot(new SlotFurnace(inventoryplayer.player, tileentitymatter, 1, 114, 18));
        for(int i = 0; i < 3; i++)
        {
            for(int k = 0; k < 9; k++)
            {
                addSlot(new Slot(inventoryplayer, k + i * 9 + 9, 8 + k * 18, 84 + i * 18));
            }

        }

        for(int j = 0; j < 9; j++)
        {
            addSlot(new Slot(inventoryplayer, j, 8 + j * 18, 142));
        }

    }

    public void updateCraftingMatrix()
    {
        super.updateCraftingMatrix();
        for(int i = 0; i < slots.size(); i++)
        {
            ICrafting icrafting = (ICrafting)slots.get(i);
            if(energy != tileentity.energy)
            {
                icrafting.updateCraftingInventoryInfo(this, 0, tileentity.energy & 0xffff);
                icrafting.updateCraftingInventoryInfo(this, 1, tileentity.energy >>> 16);
            }
            if(scrap != tileentity.scrap)
            {
                icrafting.updateCraftingInventoryInfo(this, 2, tileentity.scrap);
            }
        }

        energy = tileentity.energy;
        scrap = tileentity.scrap;
    }

    public void updateProgressBar(int i, int j)
    {
        switch(i)
        {
        case 0: // '\0'
            tileentity.energy = tileentity.energy & 0xffff0000 | j;
            break;

        case 1: // '\001'
            tileentity.energy = tileentity.energy & 0xffff | j << 16;
            break;

        case 2: // '\002'
            tileentity.scrap = (short)j;
            break;
        }
    }

    public boolean isUsableByPlayer(EntityPlayer entityplayer)
    {
        return tileentity.canInteractWith(entityplayer);
    }

    public int guiInventorySize()
    {
        return 2;
    }

    public int getInput()
    {
        return 0;
    }

    public TileEntityMatter tileentity;
    public int energy;
    public short scrap;
}
