// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import net.minecraft.server.*;

// Referenced classes of package ic2.common:
//            ItemIC2, BlockCable

public class ItemCable extends ItemIC2
{

    public ItemCable(int i, int j)
    {
        super(i, j);
        setHasSubtypes(true);
    }

    public int getIconFromDamage(int i)
    {
        return iconIndex + i;
    }

    public String getItemNameIS(ItemStack itemstack)
    {
        int i = itemstack.getData();
        switch(i)
        {
        case 0: // '\0'
            return "itemCable";

        case 1: // '\001'
            return "itemCableO";

        case 2: // '\002'
            return "itemGoldCable";

        case 3: // '\003'
            return "itemGoldCableI";

        case 4: // '\004'
            return "itemGoldCableII";

        case 5: // '\005'
            return "itemIronCable";

        case 6: // '\006'
            return "itemIronCableI";

        case 7: // '\007'
            return "itemIronCableII";

        case 8: // '\b'
            return "itemIronCableIIII";

        case 9: // '\t'
            return "itemGlassCable";

        case 10: // '\n'
            return "itemTinCable";

        case 11: // '\013'
            return "itemDetectorCable";

        case 12: // '\f'
            return "itemSplitterCable";
        }
        return null;
    }

    public boolean onItemUse(ItemStack itemstack, EntityPlayer entityplayer, World world, int i, int j, int k, int l)
    {
        int i1 = world.getBlockId(i, j, k);
        if(i1 == Block.snow.blockID)
        {
            l = 0;
        } else
        if(!world.isAirBlock(i, j, k))
        {
            switch(l)
            {
            case 0: // '\0'
                j--;
                break;

            case 1: // '\001'
                j++;
                break;

            case 2: // '\002'
                k--;
                break;

            case 3: // '\003'
                k++;
                break;

            case 4: // '\004'
                i--;
                break;

            case 5: // '\005'
                i++;
                break;
            }
        }
        BlockCable blockcable = (BlockCable)mod_IC2.blockCable;
        if(world.canBlockBePlacedAt(i1, i, j, k, true, l) && world.checkIfAABBIsClear(blockcable.getCollisionBoundingBoxFromPool(world, i, j, k, itemstack.getData())) && world.setBlockAndMetadataWithNotify(i, j, k, blockcable.blockID, itemstack.getData()))
        {
            blockcable.onBlockPlaced(world, i, j, k, l);
            blockcable.onBlockPlacedBy(world, i, j, k, entityplayer);
            itemstack.stackSize--;
            return true;
        } else
        {
            return false;
        }
    }
}
