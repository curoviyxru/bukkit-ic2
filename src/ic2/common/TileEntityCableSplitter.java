// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import ic2.platform.Platform;
import net.minecraft.server.World;

// Referenced classes of package ic2.common:
//            TileEntityCable, EnergyNet

public class TileEntityCableSplitter extends TileEntityCable
{

    public TileEntityCableSplitter(short word0)
    {
        super(word0);
        ticksUntilNextUpdate = 0;
    }

    public TileEntityCableSplitter()
    {
        ticksUntilNextUpdate = 0;
    }

    public void updateEntity()
    {
        if(!created)
        {
            onCreated();
            created = true;
        }
        if(Platform.isSimulating())
        {
            if(ticksUntilNextUpdate == 0)
            {
                ticksUntilNextUpdate = 20;
                if(worldObj.isBlockGettingPowered(xCoord, yCoord, zCoord) == addedToEnergyNet)
                {
                    if(addedToEnergyNet)
                    {
                        EnergyNet.getForWorld(worldObj).removeTileEntity(this);
                        addedToEnergyNet = false;
                    } else
                    {
                        EnergyNet.getForWorld(worldObj).addTileEntity(this);
                        addedToEnergyNet = true;
                    }
                }
                setActive(addedToEnergyNet);
            }
            ticksUntilNextUpdate--;
        }
    }

    public static final int tickRate = 20;
    public int ticksUntilNextUpdate;
}
