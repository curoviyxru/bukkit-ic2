// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import ic2.platform.Platform;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.World;

// Referenced classes of package ic2.common:
//            TileEntityCable, EnergyNet

public class TileEntityCableDetector extends TileEntityCable
{

    public TileEntityCableDetector(short word0)
    {
        super(word0);
        lastValue = -1L;
        ticker = 0;
    }

    public TileEntityCableDetector()
    {
        lastValue = -1L;
        ticker = 0;
    }

    public void readFromNBT(NBTTagCompound nbttagcompound)
    {
        super.readFromNBT(nbttagcompound);
        setActiveWithoutNotify(nbttagcompound.getBoolean("active"));
    }

    public void writeToNBT(NBTTagCompound nbttagcompound)
    {
        super.writeToNBT(nbttagcompound);
        nbttagcompound.setBoolean("active", getActive());
    }

    public void updateEntity()
    {
        super.updateEntity();
        if(Platform.isSimulating() && ticker++ % tickRate == 0)
        {
            long l = EnergyNet.getForWorld(worldObj).getTotalEnergyConducted(this);
            if(lastValue != -1L)
            {
                if(l > lastValue)
                {
                    if(!getActive())
                    {
                        setActive(true);
                        worldObj.notifyBlocksOfNeighborChange(xCoord, yCoord, zCoord, worldObj.getBlockId(xCoord, yCoord, zCoord));
                    }
                } else
                if(getActive())
                {
                    setActive(false);
                    worldObj.notifyBlocksOfNeighborChange(xCoord, yCoord, zCoord, worldObj.getBlockId(xCoord, yCoord, zCoord));
                }
            }
            lastValue = l;
        }
    }

    public long lastValue;
    public static int tickRate = 20;
    public int ticker;

}
