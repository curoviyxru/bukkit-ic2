// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import java.util.Random;
import net.minecraft.server.*;

// Referenced classes of package ic2.common:
//            BlockMultiID, TileEntityTeleporter, TileEntityTesla, TileEntityBlock

public class BlockMachine2 extends BlockMultiID
{

    public BlockMachine2(int i)
    {
        super(i, Material.iron);
        setHardness(2.0F);
        setStepSound(soundMetalFootstep);
    }

    public String getTextureFile()
    {
        return "/ic2/sprites/block_machine2.png";
    }

    public int idDropped(int i, Random random)
    {
        switch(i)
        {
        default:
            return mod_IC2.blockMachine.blockID;
        }
    }

    protected int damageDropped(int i)
    {
        switch(i)
        {
        case 0: // '\0'
            return 12;
        }
        return 0;
    }

    public Integer getGui(World world, int i, int j, int k, EntityPlayer entityplayer)
    {
        switch(world.getBlockMetadata(i, j, k))
        {
        default:
            return null;
        }
    }

    public TileEntityBlock getBlockEntity(int i)
    {
        switch(i)
        {
        case 0: // '\0'
            return new TileEntityTeleporter();

        case 1: // '\001'
            return new TileEntityTesla();
        }
        return new TileEntityBlock();
    }

    public void randomDisplayTick(World world, int i, int j, int k, Random random)
    {
        int l = world.getBlockMetadata(i, j, k);
    }
}
