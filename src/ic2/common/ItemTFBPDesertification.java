// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import java.util.ArrayList;
import java.util.Random;
import net.minecraft.server.*;

// Referenced classes of package ic2.common:
//            ItemTFBP, TileEntityTerra, ItemTFBPCultivation

public class ItemTFBPDesertification extends ItemTFBP
{

    public ItemTFBPDesertification(int i, int j)
    {
        super(i, j);
    }

    public int getConsume()
    {
        return 2500;
    }

    public int getRange()
    {
        return 40;
    }

    public boolean terraform(World world, int i, int j, int k)
    {
        int l = TileEntityTerra.getFirstBlockFrom(world, i, j, k + 10);
        if(l == -1)
        {
            return false;
        }
        if(TileEntityTerra.switchGround(world, Block.dirt, Block.sand, i, l, j, false) || TileEntityTerra.switchGround(world, Block.grass, Block.sand, i, l, j, false) || TileEntityTerra.switchGround(world, Block.tilledField, Block.sand, i, l, j, false))
        {
            TileEntityTerra.switchGround(world, Block.dirt, Block.sand, i, l, j, false);
            return true;
        }
        int i1 = world.getBlockId(i, l, j);
        if(i1 == Block.waterMoving.blockID || i1 == Block.waterStill.blockID || i1 == Block.snow.blockID || i1 == Block.leaves.blockID || i1 == mod_IC2.blockRubLeaves.blockID || isPlant(i1))
        {
            world.setBlockWithNotify(i, l, j, 0);
            return true;
        }
        if(i1 == Block.ice.blockID || i1 == Block.blockSnow.blockID)
        {
            world.setBlockWithNotify(i, l, j, Block.waterMoving.blockID);
            return true;
        }
        if((i1 == Block.planks.blockID || i1 == Block.wood.blockID || i1 == mod_IC2.blockRubWood.blockID) && world.rand.nextInt(15) == 0)
        {
            world.setBlockWithNotify(i, l, j, Block.fire.blockID);
            return true;
        } else
        {
            return false;
        }
    }

    public boolean isPlant(int i)
    {
        for(int j = 0; j < ItemTFBPCultivation.plantIDs.size(); j++)
        {
            if(((Integer)ItemTFBPCultivation.plantIDs.get(j)).intValue() == i)
            {
                return true;
            }
        }

        return false;
    }
}
