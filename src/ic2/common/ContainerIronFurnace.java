// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import java.util.List;
import net.minecraft.server.*;

// Referenced classes of package ic2.common:
//            ContainerIC2, TileEntityIronFurnace

public class ContainerIronFurnace extends ContainerIC2
{

    public ContainerIronFurnace(InventoryPlayer inventoryplayer, TileEntityIronFurnace tileentityironfurnace)
    {
        progress = 0;
        fuel = 0;
        maxFuel = 0;
        tileentity = tileentityironfurnace;
        addSlot(new Slot(tileentityironfurnace, 0, 56, 17));
        addSlot(new Slot(tileentityironfurnace, 1, 56, 53));
        addSlot(new SlotFurnace(inventoryplayer.player, tileentityironfurnace, 2, 116, 35));
        for(int i = 0; i < 3; i++)
        {
            for(int k = 0; k < 9; k++)
            {
                addSlot(new Slot(inventoryplayer, k + i * 9 + 9, 8 + k * 18, 84 + i * 18));
            }

        }

        for(int j = 0; j < 9; j++)
        {
            addSlot(new Slot(inventoryplayer, j, 8 + j * 18, 142));
        }

    }

    public void updateCraftingMatrix()
    {
        super.updateCraftingMatrix();
        for(int i = 0; i < slots.size(); i++)
        {
            ICrafting icrafting = (ICrafting)slots.get(i);
            if(progress != tileentity.progress)
            {
                icrafting.updateCraftingInventoryInfo(this, 0, tileentity.progress);
            }
            if(fuel != tileentity.fuel)
            {
                icrafting.updateCraftingInventoryInfo(this, 1, tileentity.fuel);
            }
            if(maxFuel != tileentity.maxFuel)
            {
                icrafting.updateCraftingInventoryInfo(this, 2, tileentity.maxFuel);
            }
        }

        progress = tileentity.progress;
        fuel = tileentity.fuel;
        maxFuel = tileentity.maxFuel;
    }

    public void updateProgressBar(int i, int j)
    {
        if(i == 0)
        {
            tileentity.progress = (short)j;
        }
        if(i == 1)
        {
            tileentity.fuel = (short)j;
        }
        if(i == 2)
        {
            tileentity.maxFuel = (short)j;
        }
    }

    public boolean isUsableByPlayer(EntityPlayer entityplayer)
    {
        return tileentity.canInteractWith(entityplayer);
    }

    public int guiInventorySize()
    {
        return 3;
    }

    public int getInput()
    {
        return 0;
    }

    public TileEntityIronFurnace tileentity;
    public short progress;
    public short fuel;
    public short maxFuel;
}
