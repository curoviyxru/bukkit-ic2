// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import ic2.platform.Platform;
import java.io.*;
import java.lang.reflect.Constructor;
import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.util.*;
import net.minecraft.server.ItemStack;

// Referenced classes of package ic2.common:
//            IPersistentItemData

public class ItemData
{
    static class ItemMapValue
    {

        Object obj;
        long createTime;

        ItemMapValue(Object obj1, long l)
        {
            obj = obj1;
            createTime = l;
        }
    }


    public ItemData()
    {
    }

    public static short getUnusedDamageValue(int i, Class class1)
    {
        return getUnusedDamageValue((short)i, class1);
    }

    public static short getUnusedDamageValue(short word0, Class class1)
    {
        if(!loadedItems.containsKey(Short.valueOf(word0)))
        {
            loadedItems.put(Short.valueOf(word0), loadItemFromDisk(word0, class1));
        }
        Map map = (Map)loadedItems.get(Short.valueOf(word0));
        short word1 = 0;
        long l = 0x7fffffffffffffffL;
        short word2 = -32768;
        do
        {
            if(word2 != 0)
            {
                if(map.containsKey(Short.valueOf(word2)))
                {
                    if(((ItemMapValue)map.get(Short.valueOf(word2))).createTime < l)
                    {
                        word1 = word2;
                        l = ((ItemMapValue)map.get(Short.valueOf(word2))).createTime;
                    }
                } else
                {
                    return word2;
                }
                if(word2 == 32767)
                {
                    break;
                }
            }
            word2++;
        } while(true);
        map.remove(Short.valueOf(word1));
        return word1;
    }

    public static Object get(ItemStack itemstack, Class class1)
    {
        short word0 = (short)itemstack.id;
        short word1 = (short)itemstack.getData();
        if(!loadedItems.containsKey(Short.valueOf(word0)))
        {
            loadedItems.put(Short.valueOf(word0), loadItemFromDisk(word0, class1));
        }
        Map map = (Map)loadedItems.get(Short.valueOf(word0));
        if(!map.containsKey(Short.valueOf(word1)))
        {
            Object obj;
            try
            {
                obj = class1.getDeclaredConstructors()[0].newInstance(new Object[0]);
            }
            catch(Exception exception)
            {
                exception.printStackTrace();
                return null;
            }
            map.put(Short.valueOf(word1), new ItemMapValue(obj, System.currentTimeMillis()));
            return obj;
        } else
        {
            return ((ItemMapValue)map.get(Short.valueOf(word1))).obj;
        }
    }

    public static void remove(ItemStack itemstack)
    {
        if(loadedItems.containsKey(Integer.valueOf(itemstack.id)))
        {
            ((Map)loadedItems.get(Integer.valueOf(itemstack.id))).remove(Integer.valueOf(itemstack.getData()));
        }
    }

    public static void saveToDisk(int i)
    {
        saveToDisk((short)i);
    }

    public static void saveToDisk(short word0)
    {
        if(!loadedItems.containsKey(Short.valueOf(word0)))
        {
            return;
        }
        File file = new File(getItemDataDirectory(), (new StringBuilder()).append("item_").append(word0).append(".dat").toString());
        File file1 = new File(getItemDataDirectory(), (new StringBuilder()).append("item_").append(word0).append(".dat.tmp").toString());
        file.getParentFile().mkdirs();
        FileOutputStream fileoutputstream = null;
        try
        {
            fileoutputstream = new FileOutputStream(file1);
            FileChannel filechannel = fileoutputstream.getChannel();
            Map map = (Map)loadedItems.get(Short.valueOf(word0));
            Iterator iterator = map.entrySet().iterator();
            do
            {
                if(!iterator.hasNext())
                {
                    break;
                }
                java.util.Map.Entry entry = (java.util.Map.Entry)iterator.next();
                if(((ItemMapValue)entry.getValue()).obj instanceof IPersistentItemData)
                {
                    IPersistentItemData ipersistentitemdata = (IPersistentItemData)((ItemMapValue)entry.getValue()).obj;
                    ByteBuffer bytebuffer = ipersistentitemdata.saveToBuffer();
                    if(bytebuffer.position() > 0)
                    {
                        bytebuffer.flip();
                    }
                    ByteBuffer bytebuffer1 = ByteBuffer.allocate(16 + bytebuffer.limit());
                    bytebuffer1.putShort(((Short)entry.getKey()).shortValue());
                    bytebuffer1.putShort(ipersistentitemdata.getVersion());
                    bytebuffer1.putLong(((ItemMapValue)entry.getValue()).createTime);
                    bytebuffer1.putInt(bytebuffer.capacity());
                    bytebuffer1.put(bytebuffer);
                    bytebuffer1.flip();
                    while(bytebuffer1.hasRemaining()) 
                    {
                        filechannel.write(bytebuffer1);
                    }
                }
            } while(true);
        }
        catch(Exception exception)
        {
            exception.printStackTrace();
            return;
        }
        finally
        {
            try
            {
                if(fileoutputstream != null)
                {
                    fileoutputstream.close();
                }
            }
            catch(Exception exception2) { }
        }
        file.delete();
        file1.renameTo(file);
    }

    private static Map loadItemFromDisk(short word0, Class class1)
    {
        HashMap hashmap = new HashMap();
        File file = new File(getItemDataDirectory(), (new StringBuilder()).append("item_").append(word0).append(".dat").toString());
        if(!file.exists())
        {
            return hashmap;
        }
        FileInputStream fileinputstream = null;
        try
        {
            fileinputstream = new FileInputStream(file);
            FileChannel filechannel = fileinputstream.getChannel();
            MappedByteBuffer mappedbytebuffer = filechannel.map(java.nio.channels.FileChannel.MapMode.READ_ONLY, 0L, file.length());
            do
            {
                if(!mappedbytebuffer.hasRemaining())
                {
                    break;
                }
                Object obj;
                try
                {
                    obj = class1.getDeclaredConstructors()[0].newInstance(new Object[0]);
                }
                catch(Exception exception1)
                {
                    exception1.printStackTrace();
                    obj = null;
                }
                short word1 = mappedbytebuffer.getShort();
                short word2 = mappedbytebuffer.getShort();
                long l = mappedbytebuffer.getLong();
                int i = mappedbytebuffer.getInt();
                byte abyte0[] = new byte[i];
                mappedbytebuffer.get(abyte0, 0, i);
                hashmap.put(Short.valueOf(word1), new ItemMapValue(obj, l));
                if(obj instanceof IPersistentItemData)
                {
                    ((IPersistentItemData)obj).loadFromBuffer(word1, word2, ByteBuffer.wrap(abyte0));
                }
            } while(true);
        }
        catch(Exception exception)
        {
            exception.printStackTrace();
        }
        finally
        {
            try
            {
                if(fileinputstream != null)
                {
                    fileinputstream.close();
                }
            }
            catch(Exception exception3) { }
        }
        return hashmap;
    }

    private static File getItemDataDirectory()
    {
        return new File(Platform.getMinecraftDir(), "/industrialcraft/itemdata/");
    }

    private static Map loadedItems = new HashMap();

}
