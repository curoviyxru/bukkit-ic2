// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import java.util.Random;
import net.minecraft.server.*;

public class WorldGenRubTree extends WorldGenerator
{

    public WorldGenRubTree()
    {
    }

    public boolean a(World world, Random random, int i, int j, int k)
    {
        for(; j > 0; j--)
        {
            int l;
            for(l = 128; world.getTypeId(i, l - 1, k) == 0 && l > 0; l--) { }
            if(!grow(world, i, l, k, random))
            {
                j -= 3;
            }
            i += random.nextInt(15) - 7;
            k += random.nextInt(15) - 7;
        }

        return true;
    }

    public boolean grow(World world, int i, int j, int k, Random random)
    {
        int l = 25;
        int i1 = getGrowHeight(world, i, j, k);
        if(i1 < 2)
        {
            return false;
        }
        int j1 = i1 / 2;
        i1 -= i1 / 2;
        j1 += random.nextInt(i1 + 1);
label0:
        for(int k1 = 0; k1 < j1; k1++)
        {
            world.setTypeId(i, j + k1, k, mod_IC2.blockRubWood.id);
            if(random.nextInt(100) <= l)
            {
                l -= 10;
                world.setData(i, j + k1, k, random.nextInt(4) + 2);
            } else
            {
                world.setData(i, j + k1, k, 1);
            }
            if(j1 >= 4 && (j1 >= 7 || k1 <= 1) && k1 <= 2)
            {
                continue;
            }
            int i2 = i - 2;
            do
            {
                if(i2 > i + 2)
                {
                    continue label0;
                }
                for(int j2 = k - 2; j2 <= k + 2; j2++)
                {
                    int k2 = (k1 + 4) - j1;
                    if(k2 < 1)
                    {
                        k2 = 1;
                    }
                    boolean flag = i2 > i - 2 && i2 < i + 2 && j2 > k - 2 && j2 < k + 2 || i2 > i - 2 && i2 < i + 2 && random.nextInt(k2) == 0 || j2 > k - 2 && j2 < k + 2 && random.nextInt(k2) == 0;
                    if(flag && world.getTypeId(i2, j + k1, j2) == 0)
                    {
                        world.setTypeId(i2, j + k1, j2, mod_IC2.blockRubLeaves.id);
                    }
                }

                i2++;
            } while(true);
        }

        for(int l1 = 0; l1 <= j1 / 4 + random.nextInt(2); l1++)
        {
            if(world.getTypeId(i, j + j1 + l1, k) == 0)
            {
                world.setTypeId(i, j + j1 + l1, k, mod_IC2.blockRubLeaves.id);
            }
        }

        return true;
    }

    public int getGrowHeight(World world, int i, int j, int k)
    {
        if(world.getTypeId(i, j - 1, k) != Block.GRASS.id && world.getTypeId(i, j - 1, k) != Block.DIRT.id || world.getTypeId(i, j, k) != 0 && world.getTypeId(i, j, k) != mod_IC2.blockRubSapling.id)
        {
            return 0;
        }
        int l;
        for(l = 1; world.getTypeId(i, j + 1, k) == 0 && l < 8; j++)
        {
            l++;
        }

        return l;
    }

    public static final int maxHeight = 8;
}
