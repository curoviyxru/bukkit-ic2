// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import ic2.platform.AudioManager;
import ic2.platform.Platform;
import java.util.ArrayList;
import java.util.Random;
import net.minecraft.server.*;

// Referenced classes of package ic2.common:
//            ItemIC2, PositionSpec

public class ItemRemote extends ItemIC2
{

    public ItemRemote(int i, int j)
    {
        super(i, j);
        setMaxStackSize(1);
    }

    public boolean onItemUse(ItemStack itemstack, EntityPlayer entityplayer, World world, int i, int j, int k, int l)
    {
        if(!Platform.isSimulating())
        {
            return world.getBlockId(i, j, k) == mod_IC2.blockDynamite.blockID || world.getBlockId(i, j, k) == mod_IC2.blockDynamiteRemote.blockID;
        }
        if(itemstack.getData() == 0)
        {
            itemstack.setItemDamage(world.rand.nextInt(9001));
        }
        if(world.getBlockId(i, j, k) == mod_IC2.blockDynamite.blockID)
        {
            addRemote(i, j, k, itemstack.getData());
            world.setBlockWithNotify(i, j, k, mod_IC2.blockDynamiteRemote.blockID);
            return true;
        }
        if(world.getBlockId(i, j, k) == mod_IC2.blockDynamiteRemote.blockID)
        {
            removeRemote(i, j, k);
            addRemote(i, j, k, itemstack.getData());
            return true;
        } else
        {
            return true;
        }
    }

    public ItemStack onItemRightClick(ItemStack itemstack, World world, EntityPlayer entityplayer)
    {
        AudioManager.playOnce(entityplayer, PositionSpec.Hand, "Tools/dynamiteomote.ogg", true, AudioManager.defaultVolume);
        launchRemotes(world, itemstack.getData());
        return itemstack;
    }

    public static void addRemote(int i, int j, int k, int l)
    {
        remotes.add(new int[] {
            i, j, k, l
        });
    }

    public static void removeRemote(int i, int j, int k)
    {
        for(int l = 0; l < remotes.size(); l++)
        {
            if(((int[])remotes.get(l))[0] == i && ((int[])remotes.get(l))[1] == j && ((int[])remotes.get(l))[2] == k)
            {
                remotes.remove(l);
                return;
            }
        }

    }

    public static void launchRemotes(World world, int i)
    {
        for(int j = 0; j < remotes.size(); j++)
        {
            int ai[] = (int[])remotes.get(j);
            if(ai[3] == i)
            {
                int k = ai[0];
                int l = ai[1];
                int i1 = ai[2];
                world.setBlockWithNotify(k, l, i1, 0);
                Block.blocksList[mod_IC2.blockDynamiteRemote.blockID].onBlockDestroyedByExplosion(world, k, l, i1);
                remotes.remove(j);
                j--;
            }
        }

    }

    public static boolean isThereRemote(int i, int j, int k)
    {
        for(int l = 0; l < remotes.size(); l++)
        {
            if(((int[])remotes.get(l))[0] == i && ((int[])remotes.get(l))[1] == j && ((int[])remotes.get(l))[2] == k)
            {
                return true;
            }
        }

        return false;
    }

    public static ArrayList remotes = new ArrayList();

}
