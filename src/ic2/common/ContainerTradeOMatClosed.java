// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import java.util.List;
import net.minecraft.server.*;

// Referenced classes of package ic2.common:
//            ContainerIC2, TileEntityTradeOMat

public class ContainerTradeOMatClosed extends ContainerIC2
{

    public ContainerTradeOMatClosed(InventoryPlayer inventoryplayer, TileEntityTradeOMat tileentitytradeomat)
    {
        tileentity = tileentitytradeomat;
        addSlot(new Slot(tileentitytradeomat, 2, 143, 17));
        addSlot(new Slot(tileentitytradeomat, 3, 143, 53));
        for(int i = 0; i < 3; i++)
        {
            for(int k = 0; k < 9; k++)
            {
                addSlot(new Slot(inventoryplayer, k + i * 9 + 9, 8 + k * 18, 84 + i * 18));
            }

        }

        for(int j = 0; j < 9; j++)
        {
            addSlot(new Slot(inventoryplayer, j, 8 + j * 18, 142));
        }

    }

    public void updateCraftingMatrix()
    {
        super.updateCraftingMatrix();
        for(int i = 0; i < slots.size(); i++) { }
    }

    public void updateProgressBar(int i, int j)
    {
    }

    public boolean isUsableByPlayer(EntityPlayer entityplayer)
    {
        return tileentity.canInteractWith(entityplayer);
    }

    public int guiInventorySize()
    {
        return 2;
    }

    public int getInput()
    {
        return 0;
    }

    public TileEntityTradeOMat tileentity;
}
