// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import ic2.platform.Platform;
import java.util.Random;
import net.minecraft.server.*;

// Referenced classes of package ic2.common:
//            ItemElectricTool

public class ItemElectricToolDrill extends ItemElectricTool
{

    public ItemElectricToolDrill(int i, int j)
    {
        super(i, j, EnumToolMaterial.IRON, mineableBlocks, 1, 50, 100);
        soundTicker = 0;
        setMaxDamage(202);
        efficiencyOnProperMaterial = 8F;
    }

    public boolean onBlockDestroyed(ItemStack itemstack, int i, int j, int k, int l, EntityLiving entityliving)
    {
        use(itemstack, 1, (EntityPlayer)entityliving);
        return true;
    }

    public boolean canHarvestBlock(Block block)
    {
        if(block.blockMaterial == Material.rock || block.blockMaterial == Material.iron)
        {
            return true;
        } else
        {
            return super.canHarvestBlock(block);
        }
    }

    public float getStrVsBlock(ItemStack itemstack, Block block)
    {
        soundTicker++;
        if(soundTicker % 4 == 0)
        {
            Platform.playSoundSp(getRandomDrillSound(), 1.0F, 1.0F);
        }
        return super.getStrVsBlock(itemstack, block);
    }

    public float getStrVsBlock(ItemStack itemstack, Block block, int i)
    {
        soundTicker++;
        if(soundTicker % 4 == 0)
        {
            Platform.playSoundSp(getRandomDrillSound(), 1.0F, 1.0F);
        }
        return super.getStrVsBlock(itemstack, block, i);
    }

    public String getRandomDrillSound()
    {
        switch(mod_IC2.random.nextInt(4))
        {
        default:
            return "drill";

        case 1: // '\001'
            return "drillOne";

        case 2: // '\002'
            return "drillTwo";

        case 3: // '\003'
            return "drillThree";
        }
    }

    public static Block mineableBlocks[];
    public int soundTicker;

    static 
    {
        mineableBlocks = (new Block[] {
            Block.cobblestone, Block.stairDouble, Block.stairSingle, Block.stone, Block.sandStone, Block.cobblestoneMossy, Block.oreIron, Block.blockSteel, Block.oreCoal, Block.blockGold, 
            Block.oreGold, Block.oreDiamond, Block.blockDiamond, Block.ice, Block.netherrack, Block.oreLapis, Block.blockLapis, Block.oreRedstone, Block.oreRedstoneGlowing, Block.brick,
            Block.glowStone, Block.grass, Block.dirt, Block.sand, Block.gravel, Block.snow, Block.blockSnow, Block.blockClay, Block.tilledField
        });
    }
}
