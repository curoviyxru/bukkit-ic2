// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import ic2.platform.Platform;
import java.util.Random;
import net.minecraft.server.*;

// Referenced classes of package ic2.common:
//            PointExplosion

public class EntityDynamite extends Entity
{

    public EntityDynamite(World world, double d, double d1, double d2)
    {
        super(world);
        sticky = false;
        fuse = 100;
        inGround = false;
        ticksInAir = 0;
        setSize(0.5F, 0.5F);
        setPosition(d, d1, d2);
        yOffset = 0.0F;
    }

    public EntityDynamite(World world, EntityLiving entityliving, boolean flag)
    {
        super(world);
        sticky = false;
        fuse = 100;
        inGround = false;
        ticksInAir = 0;
        owner = entityliving;
        setSize(0.5F, 0.5F);
        setLocationAndAngles(entityliving.posX, entityliving.posY + (double)entityliving.getEyeHeight(), entityliving.posZ, entityliving.rotationYaw, entityliving.rotationPitch);
        posX -= MathHelper.cos((rotationYaw / 180F) * 3.141593F) * 0.16F;
        posY -= 0.10000000149011612D;
        posZ -= MathHelper.sin((rotationYaw / 180F) * 3.141593F) * 0.16F;
        setPosition(posX, posY, posZ);
        yOffset = 0.0F;
        motionX = -MathHelper.sin((rotationYaw / 180F) * 3.141593F) * MathHelper.cos((rotationPitch / 180F) * 3.141593F);
        motionZ = MathHelper.cos((rotationYaw / 180F) * 3.141593F) * MathHelper.cos((rotationPitch / 180F) * 3.141593F);
        motionY = -MathHelper.sin((rotationPitch / 180F) * 3.141593F);
        setDynamiteHeading(motionX, motionY, motionZ, 1.0F, 1.0F);
        sticky = flag;
    }

    protected void entityInit()
    {
    }

    public void setDynamiteHeading(double d, double d1, double d2, float f, 
            float f1)
    {
        float f2 = MathHelper.sqrt_double(d * d + d1 * d1 + d2 * d2);
        d /= f2;
        d1 /= f2;
        d2 /= f2;
        d += rand.nextGaussian() * 0.0074999998323619366D * (double)f1;
        d1 += rand.nextGaussian() * 0.0074999998323619366D * (double)f1;
        d2 += rand.nextGaussian() * 0.0074999998323619366D * (double)f1;
        d *= f;
        d1 *= f;
        d2 *= f;
        motionX = d;
        motionY = d1;
        motionZ = d2;
        float f3 = MathHelper.sqrt_double(d * d + d2 * d2);
        prevRotationYaw = rotationYaw = (float)((Math.atan2(d, d2) * 180D) / 3.1415927410125732D);
        prevRotationPitch = rotationPitch = (float)((Math.atan2(d1, f3) * 180D) / 3.1415927410125732D);
        ticksInGround = 0;
    }

    public void setVelocity(double d, double d1, double d2)
    {
        motionX = d;
        motionY = d1;
        motionZ = d2;
        if(prevRotationPitch == 0.0F && prevRotationYaw == 0.0F)
        {
            float f = MathHelper.sqrt_double(d * d + d2 * d2);
            prevRotationYaw = rotationYaw = (float)((Math.atan2(d, d2) * 180D) / 3.1415927410125732D);
            prevRotationPitch = rotationPitch = (float)((Math.atan2(d1, f) * 180D) / 3.1415927410125732D);
            prevRotationPitch = rotationPitch;
            prevRotationYaw = rotationYaw;
            setLocationAndAngles(posX, posY, posZ, rotationYaw, rotationPitch);
            ticksInGround = 0;
        }
    }

    public void onUpdate()
    {
        super.onUpdate();
        if(prevRotationPitch == 0.0F && prevRotationYaw == 0.0F)
        {
            float f = MathHelper.sqrt_double(motionX * motionX + motionZ * motionZ);
            prevRotationYaw = rotationYaw = (float)((Math.atan2(motionX, motionZ) * 180D) / 3.1415927410125732D);
            prevRotationPitch = rotationPitch = (float)((Math.atan2(motionY, f) * 180D) / 3.1415927410125732D);
        }
        if(fuse-- <= 0)
        {
            if(Platform.isSimulating())
            {
                setEntityDead();
                explode();
            } else
            {
                setEntityDead();
            }
        } else
        if(fuse < 100 && fuse % 2 == 0)
        {
            worldObj.spawnParticle("smoke", posX, posY + 0.5D, posZ, 0.0D, 0.0D, 0.0D);
        }
        if(inGround)
        {
            ticksInGround++;
            if(ticksInGround >= 200)
            {
                setEntityDead();
            }
            if(sticky)
            {
                fuse -= 3;
                motionX = 0.0D;
                motionY = 0.0D;
                motionZ = 0.0D;
                if(worldObj.getBlockId(stickX, stickY, stickZ) != 0)
                {
                    return;
                }
            }
        }
        ticksInAir++;
        Vec3D vec3d = Vec3D.createVector(posX, posY, posZ);
        Vec3D vec3d1 = Vec3D.createVector(posX + motionX, posY + motionY, posZ + motionZ);
        MovingObjectPosition movingobjectposition = worldObj.func_28105_a(vec3d, vec3d1, false, true);
        vec3d = Vec3D.createVector(posX, posY, posZ);
        vec3d1 = Vec3D.createVector(posX + motionX, posY + motionY, posZ + motionZ);
        if(movingobjectposition != null)
        {
            Vec3D vec3d2 = Vec3D.createVector(movingobjectposition.hitVec.xCoord, movingobjectposition.hitVec.yCoord, movingobjectposition.hitVec.zCoord);
            float f1 = (float)(movingobjectposition.hitVec.xCoord - posX);
            float f3 = (float)(movingobjectposition.hitVec.yCoord - posY);
            float f5 = (float)(movingobjectposition.hitVec.zCoord - posZ);
            float f7 = MathHelper.sqrt_double(f1 * f1 + f3 * f3 + f5 * f5);
            stickX = movingobjectposition.blockX;
            stickY = movingobjectposition.blockY;
            stickZ = movingobjectposition.blockZ;
            posX -= ((double)f1 / (double)f7) * 0.05000000074505806D;
            posY -= ((double)f3 / (double)f7) * 0.05000000074505806D;
            posZ -= ((double)f5 / (double)f7) * 0.05000000074505806D;
            posX += f1;
            posY += f3;
            posZ += f5;
            motionX *= 0.75F - rand.nextFloat();
            motionY *= -0.30000001192092896D;
            motionZ *= 0.75F - rand.nextFloat();
            inGround = true;
        } else
        {
            posX += motionX;
            posY += motionY;
            posZ += motionZ;
            inGround = false;
        }
        float f2 = MathHelper.sqrt_double(motionX * motionX + motionZ * motionZ);
        rotationYaw = (float)((Math.atan2(motionX, motionZ) * 180D) / 3.1415927410125732D);
        for(rotationPitch = (float)((Math.atan2(motionY, f2) * 180D) / 3.1415927410125732D); rotationPitch - prevRotationPitch < -180F; prevRotationPitch -= 360F) { }
        for(; rotationPitch - prevRotationPitch >= 180F; prevRotationPitch += 360F) { }
        for(; rotationYaw - prevRotationYaw < -180F; prevRotationYaw -= 360F) { }
        for(; rotationYaw - prevRotationYaw >= 180F; prevRotationYaw += 360F) { }
        rotationPitch = prevRotationPitch + (rotationPitch - prevRotationPitch) * 0.2F;
        rotationYaw = prevRotationYaw + (rotationYaw - prevRotationYaw) * 0.2F;
        float f4 = 0.98F;
        float f6 = 0.04F;
        if(isInWater())
        {
            fuse += 2000;
            for(int i = 0; i < 4; i++)
            {
                float f8 = 0.25F;
                worldObj.spawnParticle("bubble", posX - motionX * (double)f8, posY - motionY * (double)f8, posZ - motionZ * (double)f8, motionX, motionY, motionZ);
            }

            f4 = 0.75F;
        }
        motionX *= f4;
        motionY *= f4;
        motionZ *= f4;
        motionY -= f6;
        setPosition(posX, posY, posZ);
    }

    public void writeEntityToNBT(NBTTagCompound nbttagcompound)
    {
        nbttagcompound.setByte("inGround", (byte)(inGround ? 1 : 0));
    }

    public void readEntityFromNBT(NBTTagCompound nbttagcompound)
    {
        inGround = nbttagcompound.getByte("inGround") == 1;
    }

    public float getShadowSize()
    {
        return 0.0F;
    }

    public void explode()
    {
        PointExplosion pointexplosion = new PointExplosion(worldObj, null, (int)posX, (int)posY, (int)posZ, 1.0F, 1.0F, 0.8F);
        pointexplosion.doExplosionA(1, 1, 1, 1, 1, 1);
        pointexplosion.doExplosionB(true);
    }

    public boolean sticky;
    public static final int netId = 142;
    public int stickX;
    public int stickY;
    public int stickZ;
    public int fuse;
    private boolean inGround;
    public EntityLiving owner;
    private int ticksInGround;
    private int ticksInAir;
}
