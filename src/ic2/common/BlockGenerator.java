// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import ic2.platform.Platform;
import java.util.Random;
import net.minecraft.server.*;

// Referenced classes of package ic2.common:
//            BlockMultiID, TileEntityNuclearReactor, TileEntityGenerator, TileEntityGeoGenerator, 
//            TileEntityWaterGenerator, TileEntitySolarGenerator, TileEntityWindGenerator, TileEntityBlock

public class BlockGenerator extends BlockMultiID
{

    public BlockGenerator(int i)
    {
        super(i, Material.iron);
        setHardness(3F);
        setStepSound(soundMetalFootstep);
    }

    public String getTextureFile()
    {
        return "/ic2/sprites/block_generator.png";
    }

    public int idDropped(int i, Random random)
    {
        switch(i)
        {
        default:
            return blockID;
        }
    }

    protected int damageDropped(int i)
    {
        switch(i)
        {
        case 2: // '\002'
            return 2;
        }
        return 0;
    }

    public int quantityDropped(Random random)
    {
        return 1;
    }

    public Integer getGui(World world, int i, int j, int k, EntityPlayer entityplayer)
    {
        TileEntity tileentity = world.getBlockTileEntity(i, j, k);
        switch(world.getBlockMetadata(i, j, k))
        {
        case 1: // '\001'
            return Integer.valueOf(mod_IC2.guiIdGeoGenerator);

        case 2: // '\002'
            return Integer.valueOf(mod_IC2.guiIdWaterGenerator);

        case 3: // '\003'
            return Integer.valueOf(mod_IC2.guiIdSolarGenerator);

        case 4: // '\004'
            return Integer.valueOf(mod_IC2.guiIdWindGenerator);

        case 5: // '\005'
            TileEntityNuclearReactor tileentitynuclearreactor = (TileEntityNuclearReactor)tileentity;
            switch(tileentitynuclearreactor.getReactorSize())
            {
            case 3: // '\003'
                return Integer.valueOf(mod_IC2.guiIdNuclearReactor6x3);

            case 4: // '\004'
                return Integer.valueOf(mod_IC2.guiIdNuclearReactor6x4);

            case 5: // '\005'
                return Integer.valueOf(mod_IC2.guiIdNuclearReactor6x5);

            case 6: // '\006'
                return Integer.valueOf(mod_IC2.guiIdNuclearReactor6x6);

            case 7: // '\007'
                return Integer.valueOf(mod_IC2.guiIdNuclearReactor6x7);

            case 8: // '\b'
                return Integer.valueOf(mod_IC2.guiIdNuclearReactor6x8);

            case 9: // '\t'
                return Integer.valueOf(mod_IC2.guiIdNuclearReactor6x9);
            }
            return null;
        }
        return Integer.valueOf(mod_IC2.guiIdGenerator);
    }

    public TileEntityBlock getBlockEntity(int i)
    {
        switch(i)
        {
        case 0: // '\0'
            return new TileEntityGenerator();

        case 1: // '\001'
            return new TileEntityGeoGenerator();

        case 2: // '\002'
            return new TileEntityWaterGenerator();

        case 3: // '\003'
            return new TileEntitySolarGenerator();

        case 4: // '\004'
            return new TileEntityWindGenerator();

        case 5: // '\005'
            return new TileEntityNuclearReactor();
        }
        return null;
    }

    public void randomDisplayTick(World world, int i, int j, int k, Random random)
    {
        if(!Platform.isRendering())
        {
            return;
        }
        int l = world.getBlockMetadata(i, j, k);
        if(l == 0 && isActive(world, i, j, k))
        {
            TileEntityBlock tileentityblock = (TileEntityBlock)world.getBlockTileEntity(i, j, k);
            short word0 = tileentityblock.getFacing();
            float f = (float)i + 0.5F;
            float f1 = (float)j + 0.0F + (random.nextFloat() * 6F) / 16F;
            float f2 = (float)k + 0.5F;
            float f3 = 0.52F;
            float f4 = random.nextFloat() * 0.6F - 0.3F;
            switch(word0)
            {
            case 4: // '\004'
                world.spawnParticle("smoke", f - f3, f1, f2 + f4, 0.0D, 0.0D, 0.0D);
                world.spawnParticle("flame", f - f3, f1, f2 + f4, 0.0D, 0.0D, 0.0D);
                break;

            case 5: // '\005'
                world.spawnParticle("smoke", f + f3, f1, f2 + f4, 0.0D, 0.0D, 0.0D);
                world.spawnParticle("flame", f + f3, f1, f2 + f4, 0.0D, 0.0D, 0.0D);
                break;

            case 2: // '\002'
                world.spawnParticle("smoke", f + f4, f1, f2 - f3, 0.0D, 0.0D, 0.0D);
                world.spawnParticle("flame", f + f4, f1, f2 - f3, 0.0D, 0.0D, 0.0D);
                break;

            case 3: // '\003'
                world.spawnParticle("smoke", f + f4, f1, f2 + f3, 0.0D, 0.0D, 0.0D);
                world.spawnParticle("flame", f + f4, f1, f2 + f3, 0.0D, 0.0D, 0.0D);
                break;
            }
        } else
        if(l == 5)
        {
            int i1 = ((TileEntityNuclearReactor)world.getBlockTileEntity(i, j, k)).heat / 1000;
            if(i1 <= 0)
            {
                return;
            }
            i1 = world.rand.nextInt(i1);
            for(int j1 = 0; j1 < i1; j1++)
            {
                world.spawnParticle("smoke", (float)i + random.nextFloat(), (float)j + 0.95F, (float)k + random.nextFloat(), 0.0D, 0.0D, 0.0D);
            }

            i1 -= world.rand.nextInt(4) + 3;
            for(int k1 = 0; k1 < i1; k1++)
            {
                world.spawnParticle("flame", (float)i + random.nextFloat(), (float)j + 1.0F, (float)k + random.nextFloat(), 0.0D, 0.0D, 0.0D);
            }

        }
    }

    public boolean blockActivated(World world, int i, int j, int k, EntityPlayer entityplayer)
    {
        if(entityplayer.getCurrentEquippedItem() != null && entityplayer.getCurrentEquippedItem().itemID == mod_IC2.blockReactorChamber.blockID)
        {
            return false;
        } else
        {
            return super.blockActivated(world, i, j, k, entityplayer);
        }
    }
}
