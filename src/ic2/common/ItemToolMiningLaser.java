// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import ic2.platform.*;
import java.util.HashMap;
import java.util.Map;

import ic2.platform.NetworkManager;
import net.minecraft.server.*;

// Referenced classes of package ic2.common:
//            ItemElectricTool, INetworkItemEventListener, EntityMiningLaser, PositionSpec

public class ItemToolMiningLaser extends ItemElectricTool
    implements INetworkItemEventListener
{
    static class ObjData
    {

        int setting;
        boolean laserKeyDown;

        ObjData()
        {
            setting = 0;
            laserKeyDown = false;
        }
    }


    public ItemToolMiningLaser(int i, int j)
    {
        super(i, j, EnumToolMaterial.WOOD, new Block[] {
            null
        }, 2, 10, 40);
        setMaxDamage(10002);
    }

    public ItemStack onItemRightClick(ItemStack itemstack, World world, EntityPlayer entityplayer)
    {
        if(!Platform.isSimulating())
        {
            return itemstack;
        }
        if(!playerToObjDataMap.containsKey(entityplayer.username))
        {
            playerToObjDataMap.put(entityplayer.username, new ObjData());
        }
        ObjData objdata = (ObjData)playerToObjDataMap.get(entityplayer.username);
        if(objdata.laserKeyDown)
        {
            objdata.setting = (objdata.setting + 1) % 5;
            String s = (new String[] {
                "Mining", "Low-Focus", "Long-Range", "Scatter", "Explosive"
            })[objdata.setting];
            Platform.messagePlayer(entityplayer, (new StringBuilder()).append("Laser Mode: ").append(s).toString());
        } else
        {
            int i = (new int[] {
                125, 10, 500, 1000, 500
            })[objdata.setting];
            if(!use(itemstack, i, entityplayer))
            {
                return itemstack;
            }
            switch(objdata.setting)
            {
            default:
                break;

            case 0: // '\0'
                world.entityJoinedWorld(new EntityMiningLaser(world, entityplayer, 8F, false));
                NetworkManager.initiateItemEvent(entityplayer, itemstack, 2, true);
                break;

            case 1: // '\001'
                world.entityJoinedWorld(new EntityMiningLaser(world, entityplayer, 1.5F, false));
                NetworkManager.initiateItemEvent(entityplayer, itemstack, 3, true);
                break;

            case 2: // '\002'
                world.entityJoinedWorld(new EntityMiningLaser(world, entityplayer, 32F, false));
                NetworkManager.initiateItemEvent(entityplayer, itemstack, 4, true);
                break;

            case 3: // '\003'
                for(int j = -2; j <= 2; j++)
                {
                    for(int k = -2; k <= 2; k++)
                    {
                        world.entityJoinedWorld(new EntityMiningLaser(world, entityplayer, 6F, false, entityplayer.rotationYaw + 20F * (float)j, entityplayer.rotationPitch + 20F * (float)k));
                    }

                }

                NetworkManager.initiateItemEvent(entityplayer, itemstack, 5, true);
                break;

            case 4: // '\004'
                world.entityJoinedWorld(new EntityMiningLaser(world, entityplayer, 12F, true));
                NetworkManager.initiateItemEvent(entityplayer, itemstack, 6, true);
                break;
            }
        }
        return itemstack;
    }

    public void onUpdate(ItemStack itemstack, World world, Entity entity, int i, boolean flag)
    {
        if(Platform.isRendering() && flag && (entity instanceof EntityPlayer) && entity == Platform.getPlayerInstance())
        {
            EntityPlayer entityplayer = (EntityPlayer)entity;
            boolean flag1 = Platform.isKeyDownLaserMode(entityplayer);
            if(lastLaserKeyDown != flag1)
            {
                NetworkManager.initiateClientItemEvent(itemstack, flag1 ? 0 : 1);
                lastLaserKeyDown = flag1;
            }
        }
    }

    public void onNetworkEvent(int i, EntityPlayer entityplayer, int j)
    {
        switch(j)
        {
        default:
            break;

        case 0: // '\0'
        case 1: // '\001'
            if(!playerToObjDataMap.containsKey(entityplayer.username))
            {
                playerToObjDataMap.put(entityplayer.username, new ObjData());
            }
            ((ObjData)playerToObjDataMap.get(entityplayer.username)).laserKeyDown = j == 0;
            break;

        case 2: // '\002'
            AudioManager.playOnce(entityplayer, PositionSpec.Hand, "Tools/MiningLaser/MiningLaser.ogg", true, AudioManager.defaultVolume);
            break;

        case 3: // '\003'
            AudioManager.playOnce(entityplayer, PositionSpec.Hand, "Tools/MiningLaser/MiningLaserLowFocus.ogg", true, AudioManager.defaultVolume);
            break;

        case 4: // '\004'
            AudioManager.playOnce(entityplayer, PositionSpec.Hand, "Tools/MiningLaser/MiningLaserLongRange.ogg", true, AudioManager.defaultVolume);
            break;

        case 5: // '\005'
            AudioManager.playOnce(entityplayer, PositionSpec.Hand, "Tools/MiningLaser/MiningLaserScatter.ogg", true, AudioManager.defaultVolume);
            break;

        case 6: // '\006'
            AudioManager.playOnce(entityplayer, PositionSpec.Hand, "Tools/MiningLaser/MiningLaserExplosive.ogg", true, AudioManager.defaultVolume);
            break;
        }
    }

    public static Map playerToObjDataMap = new HashMap();
    private static boolean lastLaserKeyDown = false;
    private static final int EventLaserKeyDown = 0;
    private static final int EventLaserKeyUp = 1;
    private static final int EventShotMining = 2;
    private static final int EventShotLowFocus = 3;
    private static final int EventShotLongRange = 4;
    private static final int EventShotScatter = 5;
    private static final int EventShotExplosive = 6;

}
