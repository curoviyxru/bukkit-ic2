// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import ic2.platform.Platform;
import java.util.Random;
import net.minecraft.server.*;

// Referenced classes of package ic2.common:
//            BlockTex, ItemRemote, EntityDynamite

public class BlockDynamite extends BlockTex
{

    public BlockDynamite(int i, int j)
    {
        super(i, j, Material.tnt);
        setTickOnLoad(true);
    }

    public AxisAlignedBB getCollisionBoundingBoxFromPool(World world, int i, int j, int k)
    {
        return null;
    }

    public boolean isOpaqueCube()
    {
        return false;
    }

    public boolean renderAsNormalBlock()
    {
        return false;
    }

    public int getRenderType()
    {
        return 2;
    }

    public boolean canPlaceBlockAt(World world, int i, int j, int k)
    {
        if(world.isBlockNormalCube(i - 1, j, k))
        {
            return true;
        }
        if(world.isBlockNormalCube(i + 1, j, k))
        {
            return true;
        }
        if(world.isBlockNormalCube(i, j, k - 1))
        {
            return true;
        }
        if(world.isBlockNormalCube(i, j, k + 1))
        {
            return true;
        } else
        {
            return world.isBlockNormalCube(i, j - 1, k);
        }
    }

    public void onBlockPlaced(World world, int i, int j, int k, int l)
    {
        int i1 = world.getBlockMetadata(i, j, k);
        if(l == 1 && world.isBlockNormalCube(i, j - 1, k))
        {
            i1 = 5;
        }
        if(l == 2 && world.isBlockNormalCube(i, j, k + 1))
        {
            i1 = 4;
        }
        if(l == 3 && world.isBlockNormalCube(i, j, k - 1))
        {
            i1 = 3;
        }
        if(l == 4 && world.isBlockNormalCube(i + 1, j, k))
        {
            i1 = 2;
        }
        if(l == 5 && world.isBlockNormalCube(i - 1, j, k))
        {
            i1 = 1;
        }
        world.setBlockMetadataWithNotify(i, j, k, i1);
    }

    public void updateTick(World world, int i, int j, int k, Random random)
    {
        if(blockID == mod_IC2.blockDynamiteRemote.blockID && !ItemRemote.isThereRemote(i, j, k))
        {
            world.setBlockWithNotify(i, j, k, mod_IC2.blockDynamite.blockID);
        }
        super.updateTick(world, i, j, k, random);
        if(world.getBlockMetadata(i, j, k) == 0)
        {
            onBlockAdded(world, i, j, k);
        }
    }

    public void onBlockAdded(World world, int i, int j, int k)
    {
        if(world.isBlockIndirectlyGettingPowered(i, j, k))
        {
            onBlockDestroyedByPlayer(world, i, j, k, 1);
            world.setBlockWithNotify(i, j, k, 0);
            return;
        }
        if(world.isBlockNormalCube(i, j - 1, k))
        {
            world.setBlockMetadataWithNotify(i, j, k, 5);
        } else
        if(world.isBlockNormalCube(i - 1, j, k))
        {
            world.setBlockMetadataWithNotify(i, j, k, 1);
        } else
        if(world.isBlockNormalCube(i + 1, j, k))
        {
            world.setBlockMetadataWithNotify(i, j, k, 2);
        } else
        if(world.isBlockNormalCube(i, j, k - 1))
        {
            world.setBlockMetadataWithNotify(i, j, k, 3);
        } else
        if(world.isBlockNormalCube(i, j, k + 1))
        {
            world.setBlockMetadataWithNotify(i, j, k, 4);
        }
        dropBlockIfCantStay(world, i, j, k);
    }

    public int quantityDropped(Random random)
    {
        return 0;
    }

    public int idDropped(int i, Random random)
    {
        return mod_IC2.itemDynamite.shiftedIndex;
    }

    public void onBlockDestroyedByExplosion(World world, int i, int j, int k)
    {
        EntityDynamite entitydynamite = new EntityDynamite(world, (float)i + 0.5F, (float)j + 0.5F, (float)k + 0.5F);
        entitydynamite.fuse = 5;
        world.entityJoinedWorld(entitydynamite);
    }

    public void onBlockDestroyedByPlayer(World world, int i, int j, int k, int l)
    {
        if(!Platform.isSimulating())
        {
            return;
        } else
        {
            EntityDynamite entitydynamite = new EntityDynamite(world, (float)i + 0.5F, (float)j + 0.5F, (float)k + 0.5F);
            entitydynamite.fuse = 40;
            world.entityJoinedWorld(entitydynamite);
            world.playSoundAtEntity(entitydynamite, "random.fuse", 1.0F, 1.0F);
            return;
        }
    }

    public void onNeighborBlockChange(World world, int i, int j, int k, int l)
    {
        if(l > 0 && Block.blocksList[l].canProvidePower() && world.isBlockIndirectlyGettingPowered(i, j, k))
        {
            onBlockDestroyedByPlayer(world, i, j, k, 1);
            world.setBlockWithNotify(i, j, k, 0);
            return;
        }
        if(dropBlockIfCantStay(world, i, j, k))
        {
            int i1 = world.getBlockMetadata(i, j, k);
            boolean flag = false;
            if(!world.isBlockNormalCube(i - 1, j, k) && i1 == 1)
            {
                flag = true;
            }
            if(!world.isBlockNormalCube(i + 1, j, k) && i1 == 2)
            {
                flag = true;
            }
            if(!world.isBlockNormalCube(i, j, k - 1) && i1 == 3)
            {
                flag = true;
            }
            if(!world.isBlockNormalCube(i, j, k + 1) && i1 == 4)
            {
                flag = true;
            }
            if(!world.isBlockNormalCube(i, j - 1, k) && i1 == 5)
            {
                flag = true;
            }
            if(flag)
            {
                dropBlockAsItem(world, i, j, k, world.getBlockMetadata(i, j, k));
                world.setBlockWithNotify(i, j, k, 0);
            }
        }
    }

    public boolean dropBlockIfCantStay(World world, int i, int j, int k)
    {
        if(!canPlaceBlockAt(world, i, j, k))
        {
            onBlockDestroyedByExplosion(world, i, j, k);
            world.setBlockWithNotify(i, j, k, 0);
            return false;
        } else
        {
            return true;
        }
    }

    public MovingObjectPosition collisionRayTrace(World world, int i, int j, int k, Vec3D vec3d, Vec3D vec3d1)
    {
        int l = world.getBlockMetadata(i, j, k) & 7;
        float f = 0.15F;
        if(l == 1)
        {
            setBlockBounds(0.0F, 0.2F, 0.5F - f, f * 2.0F, 0.8F, 0.5F + f);
        } else
        if(l == 2)
        {
            setBlockBounds(1.0F - f * 2.0F, 0.2F, 0.5F - f, 1.0F, 0.8F, 0.5F + f);
        } else
        if(l == 3)
        {
            setBlockBounds(0.5F - f, 0.2F, 0.0F, 0.5F + f, 0.8F, f * 2.0F);
        } else
        if(l == 4)
        {
            setBlockBounds(0.5F - f, 0.2F, 1.0F - f * 2.0F, 0.5F + f, 0.8F, 1.0F);
        } else
        {
            float f1 = 0.1F;
            setBlockBounds(0.5F - f1, 0.0F, 0.5F - f1, 0.5F + f1, 0.6F, 0.5F + f1);
        }
        return super.collisionRayTrace(world, i, j, k, vec3d, vec3d1);
    }
}
