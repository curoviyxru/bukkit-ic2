// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import ic2.platform.ItemBlockCommon;
import net.minecraft.server.ItemStack;

public class ItemPersonalBlock extends ItemBlockCommon
{

    public ItemPersonalBlock(int i)
    {
        super(i);
        setMaxDamage(0);
        setHasSubtypes(true);
    }

    public int getPlacedBlockMetadata(int i)
    {
        return i;
    }

    public String getItemNameIS(ItemStack itemstack)
    {
        int i = itemstack.getData();
        switch(i)
        {
        case 0: // '\0'
            return "blockPersonalChest";

        case 1: // '\001'
            return "blockPersonalTrader";
        }
        return null;
    }
}
