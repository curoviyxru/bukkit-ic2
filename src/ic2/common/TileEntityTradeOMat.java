// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import forge.ISidedInventory;
import ic2.api.Direction;
import ic2.platform.*;
import java.util.Random;

import ic2.platform.NetworkManager;
import net.minecraft.server.*;

// Referenced classes of package ic2.common:
//            TileEntityMachine, IPersonalBlock, IHasGuiContainer, INetworkTileEntityEventListener, 
//            TileEntityPersonalChest, StackUtil, ContainerTradeOMatOpen, ContainerTradeOMatClosed, 
//            PositionSpec

public class TileEntityTradeOMat extends TileEntityMachine
    implements IPersonalBlock, IHasGuiContainer, ISidedInventory, INetworkTileEntityEventListener
{

    public TileEntityTradeOMat()
    {
        super(4);
        owner = "null";
        totalTradeCount = 0;
    }

    public void readFromNBT(NBTTagCompound nbttagcompound)
    {
        super.readFromNBT(nbttagcompound);
        owner = nbttagcompound.getString("owner");
        totalTradeCount = nbttagcompound.getInteger("totalTradeCount");
    }

    public void writeToNBT(NBTTagCompound nbttagcompound)
    {
        super.writeToNBT(nbttagcompound);
        nbttagcompound.setString("owner", owner);
        nbttagcompound.setInteger("totalTradeCount", totalTradeCount);
    }

    public void updateEntity()
    {
        super.updateEntity();
        if(Platform.isSimulating() && inventory[0] != null && inventory[1] != null && inventory[2] != null && inventory[0].isItemEqual(inventory[2]) && inventory[2].stackSize >= inventory[0].stackSize && (inventory[3] == null || inventory[3].isItemEqual(inventory[1]) && inventory[3].stackSize + inventory[1].stackSize <= inventory[3].getMaxStackSize()))
        {
            Direction adirection[] = Direction.values();
            int i = adirection.length;
            for(int j = 0; j < i; j++)
            {
                Direction direction = adirection[j];
                TileEntity tileentity = direction.applyToTileEntity(this);
                if(!(tileentity instanceof IInventory) || (tileentity instanceof TileEntityPersonalChest) && !((TileEntityPersonalChest)tileentity).owner.equals(owner))
                {
                    continue;
                }
                IInventory iinventory = (IInventory)tileentity;
                if(iinventory.getSizeInventory() < 18)
                {
                    continue;
                }
                int k = 0;
                int l = 0;
                for(int i1 = 0; i1 < iinventory.getSizeInventory(); i1++)
                {
                    ItemStack itemstack = iinventory.getStackInSlot(i1);
                    if(itemstack == null)
                    {
                        k += inventory[0].getMaxStackSize();
                        continue;
                    }
                    if(itemstack.isItemEqual(inventory[0]))
                    {
                        k += itemstack.getMaxStackSize() - itemstack.stackSize;
                    }
                    if(itemstack.isItemEqual(inventory[1]))
                    {
                        l += itemstack.stackSize;
                    }
                }

                int j1 = inventory[3] != null ? inventory[3].getMaxStackSize() - inventory[3].stackSize : inventory[1].getMaxStackSize();
                int k1 = Math.min(Math.min(Math.min(inventory[2].stackSize / inventory[0].stackSize, k / inventory[0].stackSize), j1 / inventory[1].stackSize), l / inventory[1].stackSize);
                if(k1 <= 0)
                {
                    continue;
                }
                int l1 = inventory[0].stackSize * k1;
                int i2 = inventory[1].stackSize * k1;
                inventory[2].stackSize -= l1;
                if(inventory[2].stackSize == 0)
                {
                    inventory[2] = null;
                }
                if(inventory[3] == null)
                {
                    inventory[3] = inventory[1].copy();
                    inventory[3].stackSize = i2;
                } else
                {
                    inventory[3].stackSize += i2;
                }
                StackUtil.putInInventory(iinventory, new ItemStack(inventory[0].itemID, l1, inventory[0].getItemDamage()));
                StackUtil.getFromInventory(iinventory, new ItemStack(inventory[1].itemID, i2, inventory[1].getItemDamage()));
                totalTradeCount += k1;
                NetworkManager.initiateTileEntityEvent(this, 0, true);
                onInventoryChanged();
                break;
            }

        }
    }

    public String getWantAsString()
    {
        if(inventory[0] == null)
        {
            return "";
        } else
        {
            return itemStackToString(inventory[0]);
        }
    }

    public String getOfferAsString()
    {
        if(inventory[1] == null)
        {
            return "";
        } else
        {
            return itemStackToString(inventory[1]);
        }
    }

    public String itemStackToString(ItemStack itemstack)
    {
        return (new StringBuilder()).append("").append(itemstack.stackSize).append(" ").append(Platform.getItemNameIS(itemstack)).toString();
    }

    public boolean wrenchRemove(EntityPlayer entityplayer)
    {
        return canAccess(entityplayer);
    }

    public boolean canAccess(EntityPlayer entityplayer)
    {
        if(owner.equals("null"))
        {
            owner = entityplayer.username;
            return true;
        } else
        {
            return owner.equalsIgnoreCase(entityplayer.username);
        }
    }

    public String getInvName()
    {
        return "Trade-O-Mat";
    }

    public Container getGuiContainer(InventoryPlayer inventoryplayer)
    {
        if(canAccess(inventoryplayer.player))
        {
            return new ContainerTradeOMatOpen(inventoryplayer, this);
        } else
        {
            return new ContainerTradeOMatClosed(inventoryplayer, this);
        }
    }

    public int getStartInventorySide(int i)
    {
        switch(i)
        {
        case 0: // '\0'
            return 3;

        case 1: // '\001'
        default:
            return 2;
        }
    }

    public int getSizeInventorySide(int i)
    {
        return 1;
    }

    public void onNetworkEvent(int i)
    {
        switch(i)
        {
        case 0: // '\0'
            AudioManager.playOnce(this, PositionSpec.Center, "Machines/o-mat.ogg", true, AudioManager.defaultVolume);
            break;

        default:
            throw new RuntimeException((new StringBuilder()).append("Unknown network event: ").append(i).toString());
        }
    }

    public static Random randomizer = new Random();
    public String owner;
    public int totalTradeCount;
    private static final int EventTrade = 0;

}
