// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import java.io.PrintStream;
import net.minecraft.server.*;

// Referenced classes of package ic2.common:
//            ItemIC2, IChargeableItem

public class ItemBatterySU extends ItemIC2
{

    public ItemBatterySU(int i, int j, int k, int l)
    {
        super(i, j);
        soundTicker = 0;
        ratio = k;
        tier = l;
        setMaxDamage(1002);
    }

    public ItemStack onItemRightClick(ItemStack itemstack, World world, EntityPlayer entityplayer)
    {
        if(itemstack.id != mod_IC2.itemBatSU.shiftedIndex)
        {
            return itemstack;
        }
        soundTicker++;
        if(soundTicker % 32 == 0)
        {
            world.playSoundAtEntity(entityplayer, "battery", 1.0F, 1.0F);
        }
        int i = ratio * 1000;
        for(int j = 0; j < 9; j++)
        {
            ItemStack itemstack1 = entityplayer.inventory.mainInventory[j];
            if(itemstack1 == null || !(Item.itemsList[itemstack1.itemID] instanceof IChargeableItem) || itemstack1 == itemstack)
            {
                continue;
            }
            int k = i;
            do
            {
                k = i;
                i -= ((IChargeableItem)Item.itemsList[itemstack1.itemID]).giveEnergyTo(itemstack1, i, 1, false);
                System.err.println(itemstack1.getItemDamage());
            } while(i > 0 && i != k);
            if(i <= 0)
            {
                break;
            }
        }

        if(i < ratio * 1000)
        {
            itemstack.stackSize--;
        }
        return itemstack;
    }

    public int ratio;
    public int tier;
    public int soundTicker;
}
