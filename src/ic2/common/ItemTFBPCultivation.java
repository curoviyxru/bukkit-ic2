// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import java.util.ArrayList;
import java.util.Random;
import net.minecraft.server.*;

// Referenced classes of package ic2.common:
//            ItemTFBP, TileEntityTerra

public class ItemTFBPCultivation extends ItemTFBP
{

    public ItemTFBPCultivation(int i, int j)
    {
        super(i, j);
    }

    public int getConsume()
    {
        return 4000;
    }

    public int getRange()
    {
        return 40;
    }

    public boolean terraform(World world, int i, int j, int k)
    {
        int l = TileEntityTerra.getFirstSolidBlockFrom(world, i, j, k + 10);
        if(l == -1)
        {
            return false;
        }
        if(TileEntityTerra.switchGround(world, Block.sand, Block.dirt, i, l, j, true))
        {
            return true;
        }
        int i1 = world.getBlockId(i, l, j);
        if(i1 == Block.dirt.blockID)
        {
            world.setBlockWithNotify(i, l, j, Block.grass.blockID);
            return true;
        }
        if(i1 == Block.grass.blockID)
        {
            return growPlantsOn(world, i, l + 1, j);
        } else
        {
            return false;
        }
    }

    public boolean growPlantsOn(World world, int i, int j, int k)
    {
        int l = world.getBlockId(i, j, k);
        if(l == 0 || l == Block.tallGrass.blockID && world.rand.nextInt(4) == 0)
        {
            int i1 = pickRandomPlantId(world.rand);
            if(i1 == Block.crops.blockID)
            {
                world.setBlockWithNotify(i, j - 1, k, Block.tilledField.blockID);
            }
            if(i1 == Block.tallGrass.blockID)
            {
                world.setBlockAndMetadataWithNotify(i, j, k, i1, 1);
                return true;
            } else
            {
                world.setBlockWithNotify(i, j, k, i1);
                return true;
            }
        } else
        {
            return false;
        }
    }

    public int pickRandomPlantId(Random random)
    {
        for(int i = 0; i < plantIDs.size(); i++)
        {
            if(random.nextInt(5) <= 1)
            {
                return ((Integer)plantIDs.get(i)).intValue();
            }
        }

        return Block.tallGrass.blockID;
    }

    public static void loadPlants()
    {
        plantIDs.add(Integer.valueOf(Block.tallGrass.blockID));
        plantIDs.add(Integer.valueOf(Block.plantRed.blockID));
        plantIDs.add(Integer.valueOf(Block.plantYellow.blockID));
        plantIDs.add(Integer.valueOf(Block.sapling.blockID));
        plantIDs.add(Integer.valueOf(Block.crops.blockID));
        plantIDs.add(Integer.valueOf(Block.mushroomRed.blockID));
        plantIDs.add(Integer.valueOf(Block.mushroomBrown.blockID));
        plantIDs.add(Integer.valueOf(mod_IC2.blockRubSapling.blockID));
        plantIDs.add(Integer.valueOf(Block.pumpkin.blockID));
    }

    public static ArrayList plantIDs = new ArrayList();

}
