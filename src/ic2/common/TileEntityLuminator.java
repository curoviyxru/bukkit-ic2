// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import ic2.api.Direction;
import ic2.api.IEnergySink;
import ic2.platform.Platform;
import net.minecraft.server.*;

// Referenced classes of package ic2.common:
//            EnergyNet, TileEntityCable, ExplosionIC2

public class TileEntityLuminator extends TileEntity
    implements IEnergySink
{

    public TileEntityLuminator()
    {
        energy = 0;
        ticker = -1;
        addedToEnergyNet = false;
    }

    public void readFromNBT(NBTTagCompound nbttagcompound)
    {
        super.readFromNBT(nbttagcompound);
        energy = nbttagcompound.getShort("energy");
    }

    public void writeToNBT(NBTTagCompound nbttagcompound)
    {
        super.writeToNBT(nbttagcompound);
        nbttagcompound.setShort("energy", (short)energy);
    }

    public void invalidate()
    {
        if(Platform.isSimulating() && addedToEnergyNet)
        {
            EnergyNet.getForWorld(worldObj).removeTileEntity(this);
            addedToEnergyNet = false;
        }
        super.invalidate();
    }

    public void updateEntity()
    {
        if(Platform.isSimulating())
        {
            if(!addedToEnergyNet)
            {
                EnergyNet.getForWorld(worldObj).addTileEntity(this);
                addedToEnergyNet = true;
            }
            ticker++;
            if(ticker % 4 == 0)
            {
                energy--;
                if(energy <= 0)
                {
                    worldObj.setBlockAndMetadataWithNotify(xCoord, yCoord, zCoord, mod_IC2.blockLuminatorDark.blockID, worldObj.getBlockMetadata(xCoord, yCoord, zCoord));
                }
            }
        }
    }

    public boolean isAddedToEnergyNet()
    {
        return addedToEnergyNet;
    }

    public boolean acceptsEnergyFrom(TileEntity tileentity, Direction direction)
    {
        return tileentity instanceof TileEntityCable;
    }

    public boolean demandsEnergy()
    {
        return energy < getMaxEnergy();
    }

    public int injectEnergy(Direction direction, int i)
    {
        if(i > 32)
        {
            poof();
            return 0;
        }
        if(i <= 0)
        {
            return 0;
        }
        if(worldObj.getBlockId(xCoord, yCoord, zCoord) == mod_IC2.blockLuminatorDark.blockID)
        {
            worldObj.setBlockAndMetadataWithNotify(xCoord, yCoord, zCoord, mod_IC2.blockLuminator.blockID, worldObj.getBlockMetadata(xCoord, yCoord, zCoord));
            worldObj.setBlockTileEntity(xCoord, yCoord, zCoord, this);
            return injectEnergy(direction, i);
        }
        energy += i;
        int j = 0;
        if(energy > getMaxEnergy() + 32)
        {
            j = energy - (getMaxEnergy() + 32);
            energy = getMaxEnergy() + 32;
        }
        return j;
    }

    public int getMaxEnergy()
    {
        return 10000;
    }

    public void poof()
    {
        worldObj.setBlockWithNotify(xCoord, yCoord, zCoord, 0);
        ExplosionIC2 explosionic2 = new ExplosionIC2(worldObj, null, 0.5D + (double)xCoord, 0.5D + (double)yCoord, 0.5D + (double)zCoord, 0.5F, 0.85F, 2.0F);
        explosionic2.doExplosion();
    }

    public boolean canCableConnectFrom(int i, int j, int k)
    {
        int l = worldObj.getBlockMetadata(xCoord, yCoord, zCoord);
        switch(l)
        {
        case 0: // '\0'
            return i == xCoord && j == yCoord + 1 && k == zCoord;

        case 1: // '\001'
            return i == xCoord && j == yCoord - 1 && k == zCoord;

        case 2: // '\002'
            return i == xCoord && j == yCoord && k == zCoord + 1;

        case 3: // '\003'
            return i == xCoord && j == yCoord && k == zCoord - 1;

        case 4: // '\004'
            return i == xCoord + 1 && j == yCoord && k == zCoord;

        case 5: // '\005'
            return i == xCoord - 1 && j == yCoord && k == zCoord;
        }
        return false;
    }

    public int energy;
    public int ticker;
    public boolean addedToEnergyNet;
}
