// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import ic2.platform.Platform;
import java.util.HashMap;
import java.util.Map;
import net.minecraft.server.*;

// Referenced classes of package ic2.common:
//            ItemArmorNanoSuit

public class ItemArmorQuantumSuit extends ItemArmorNanoSuit
{

    public ItemArmorQuantumSuit(int i, int j, int k, int l)
    {
        super(i, j, k, l);
        setMaxDamage(1002);
        tier = 3;
        ratio = 1000;
        transfer = 1000;
    }

    public static void performQuantum(EntityPlayer entityplayer)
    {
        ItemStack aitemstack[] = entityplayer.inventory.armorInventory;
        if(aitemstack[3] != null && (aitemstack[3].getItem() instanceof ItemArmorQuantumSuit))
        {
            boolean flag = false;
            if(entityplayer.air < 100 && aitemstack[3].getItemDamage() < aitemstack[3].getMaxDamage() - 1)
            {
                entityplayer.air += 200;
                aitemstack[3].damageItem(1, null);
            }
            if(Platform.givePlayerOneFood(entityplayer) && aitemstack[3].getItemDamage() < aitemstack[3].getMaxDamage() - 5)
            {
                aitemstack[3].damageItem(10, null);
            }
            Platform.removePoisonFrom(entityplayer);
        }
        if(aitemstack[2] != null && (aitemstack[2].getItem() instanceof ItemArmorQuantumSuit))
        {
            entityplayer.fire = 0;
        }
        if(aitemstack[1] != null && (aitemstack[1].getItem() instanceof ItemArmorQuantumSuit) && (entityplayer.onGround && Math.abs(entityplayer.motionX) + Math.abs(entityplayer.motionZ) > 0.10000000149011612D || entityplayer.isInWater()) && mod_IC2.shallApplyQuantumSpeed(entityplayer) && aitemstack[1].getItemDamage() < aitemstack[1].getMaxDamage() - 1)
        {
            int i = speedTickerMap.containsKey(entityplayer) ? ((Integer)speedTickerMap.get(entityplayer)).intValue() : 0;
            if(++i >= 10)
            {
                i = 0;
                aitemstack[1].damageItem(1, null);
            }
            speedTickerMap.put(entityplayer, Integer.valueOf(i));
            float f1 = 0.22F;
            if(entityplayer.isInWater())
            {
                f1 = 0.1F;
                if(mod_IC2.getIsJumpingOfEntityLiving(entityplayer))
                {
                    entityplayer.motionY += 0.10000000149011612D;
                }
            }
            if(f1 > 0.0F)
            {
                entityplayer.moveFlying(0.0F, 1.0F, f1);
            }
        }
        if(aitemstack[0] != null && (aitemstack[0].getItem() instanceof ItemArmorQuantumSuit))
        {
            float f = jumpChargeMap.containsKey(entityplayer) ? ((Float)jumpChargeMap.get(entityplayer)).floatValue() : 1.0F;
            if(entityplayer.onGround && f < 1.0F && aitemstack[0].getItemDamage() < aitemstack[0].getMaxDamage() - 1)
            {
                f = 1.0F;
                aitemstack[0].damageItem(1, null);
            }
            if(entityplayer.motionY >= 0.0D && f > 0.0F && !entityplayer.isInWater())
            {
                if(mod_IC2.getIsJumpingOfEntityLiving(entityplayer) && Platform.isKeyDownSuitActivate(entityplayer))
                {
                    if(f == 1.0F)
                    {
                        entityplayer.motionX *= 3.5D;
                        entityplayer.motionZ *= 3.5D;
                    }
                    entityplayer.motionY += f * 0.3F;
                    f = (float)((double)f * 0.75D);
                } else
                if(f < 1.0F)
                {
                    f = 0.0F;
                }
            }
            jumpChargeMap.put(entityplayer, Float.valueOf(f));
            if(entityplayer.motionX > (double)speedCap)
            {
                entityplayer.motionX = speedCap;
            }
            if(entityplayer.motionZ > (double)speedCap)
            {
                entityplayer.motionZ = speedCap;
            }
            if(aitemstack[0].getItemDamage() < aitemstack[0].getMaxDamage() - 1)
            {
                absorbFalling(entityplayer);
            } else
            {
                fallStorageMap.put(entityplayer, Float.valueOf(0.0F));
            }
        }
    }

    public static void absorbFalling(EntityPlayer entityplayer)
    {
        float f = fallStorageMap.containsKey(entityplayer) ? ((Float)fallStorageMap.get(entityplayer)).floatValue() : 0.0F;
        float f1 = mod_IC2.getFallDistanceOfEntity(entityplayer);
        if(f1 < 1.0F && f == 0.0F)
        {
            return;
        }
        if(f > 0.0F && entityplayer.isInWater())
        {
            f = 0.0F;
        }
        if(f1 >= 1.0F)
        {
            f1--;
            mod_IC2.setFallDistanceOfEntity(entityplayer, f1);
            f++;
        }
        if(entityplayer.onGround)
        {
            if(f < 3F)
            {
                f = 0.0F;
            } else
            {
                int i = (int)Math.ceil(f - 3F);
                if(i > 8)
                {
                    entityplayer.inventory.armorInventory[0].damageItem(1, null);
                }
                f = 0.0F;
            }
        }
        fallStorageMap.put(entityplayer, Float.valueOf(f));
    }

    public static float speedCap = 6F;
    public static Map fallStorageMap = new HashMap();
    public static Map speedTickerMap = new HashMap();
    public static Map jumpChargeMap = new HashMap();

}
