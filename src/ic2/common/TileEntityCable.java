// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import ic2.api.*;
import ic2.platform.NetworkManager;
import ic2.platform.Platform;
import java.util.*;
import net.minecraft.server.*;

// Referenced classes of package ic2.common:
//            TileEntityBlock, INetworkTileEntityEventListener, EnergyNet, TileEntityLuminator

public class TileEntityCable extends TileEntityBlock
    implements IEnergyConductor, INetworkTileEntityEventListener
{

    public TileEntityCable(short word0)
    {
        cableType = 0;
        color = 0;
        addedToEnergyNet = false;
        cableType = word0;
    }

    public TileEntityCable()
    {
        cableType = 0;
        color = 0;
        addedToEnergyNet = false;
    }

    public void readFromNBT(NBTTagCompound nbttagcompound)
    {
        super.readFromNBT(nbttagcompound);
        cableType = nbttagcompound.getShort("cableType");
        color = nbttagcompound.getShort("color");
    }

    public void writeToNBT(NBTTagCompound nbttagcompound)
    {
        super.writeToNBT(nbttagcompound);
        nbttagcompound.setShort("cableType", cableType);
        nbttagcompound.setShort("color", color);
    }

    public void updateEntity()
    {
        if(Platform.isSimulating() && !addedToEnergyNet)
        {
            EnergyNet.getForWorld(worldObj).addTileEntity(this);
            addedToEnergyNet = true;
        }
        super.updateEntity();
    }

    public void invalidate()
    {
        if(Platform.isSimulating() && addedToEnergyNet)
        {
            EnergyNet.getForWorld(worldObj).removeTileEntity(this);
            addedToEnergyNet = false;
        }
        super.invalidate();
    }

    public boolean changeColor(int i)
    {
        if(color == i || cableType == 1 || cableType == 2 || cableType == 5 || cableType == 10 || cableType == 11 || cableType == 12)
        {
            return false;
        }
        if(Platform.isSimulating())
        {
            EnergyNet.getForWorld(worldObj).removeTileEntity(this);
            addedToEnergyNet = false;
            color = (short)i;
            EnergyNet.getForWorld(worldObj).addTileEntity(this);
            addedToEnergyNet = true;
            NetworkManager.updateTileEntityField(this, "color");
        }
        return true;
    }

    public boolean tryAddInsulation()
    {
        short word0;
        switch(cableType)
        {
        case 1: // '\001'
            word0 = 0;
            break;

        case 2: // '\002'
            word0 = 3;
            break;

        case 3: // '\003'
            word0 = 4;
            break;

        case 5: // '\005'
            word0 = 6;
            break;

        case 6: // '\006'
            word0 = 7;
            break;

        case 7: // '\007'
            word0 = 8;
            break;

        case 4: // '\004'
        default:
            word0 = cableType;
            break;
        }
        if(word0 != cableType)
        {
            if(Platform.isSimulating())
            {
                changeType(word0);
            }
            return true;
        } else
        {
            return false;
        }
    }

    public boolean tryRemoveInsulation()
    {
        short word0;
        switch(cableType)
        {
        case 0: // '\0'
            word0 = 1;
            break;

        case 3: // '\003'
            word0 = 2;
            break;

        case 4: // '\004'
            word0 = 3;
            break;

        case 6: // '\006'
            word0 = 5;
            break;

        case 7: // '\007'
            word0 = 6;
            break;

        case 8: // '\b'
            word0 = 7;
            break;

        case 1: // '\001'
        case 2: // '\002'
        case 5: // '\005'
        default:
            word0 = cableType;
            break;
        }
        if(word0 != cableType)
        {
            if(Platform.isSimulating())
            {
                changeType(word0);
            }
            return true;
        } else
        {
            return false;
        }
    }

    public void changeType(short word0)
    {
        worldObj.setBlockMetadata(xCoord, yCoord, zCoord, word0);
        EnergyNet.getForWorld(worldObj).removeTileEntity(this);
        addedToEnergyNet = false;
        cableType = word0;
        EnergyNet.getForWorld(worldObj).addTileEntity(this);
        addedToEnergyNet = true;
        NetworkManager.updateTileEntityField(this, "cableType");
    }

    public boolean wrenchSetFacing(EntityPlayer entityplayer, int i)
    {
        return false;
    }

    public boolean wrenchRemove(EntityPlayer entityplayer)
    {
        return false;
    }

    public boolean isAddedToEnergyNet()
    {
        return addedToEnergyNet;
    }

    public boolean acceptsEnergyFrom(TileEntity tileentity, Direction direction)
    {
        return !(tileentity instanceof TileEntityCable) || canInteractWithCable((TileEntityCable)tileentity);
    }

    public boolean emitsEnergyTo(TileEntity tileentity, Direction direction)
    {
        if((tileentity instanceof TileEntityCable) && !canInteractWithCable((TileEntityCable)tileentity))
        {
            return false;
        }
        if(tileentity instanceof TileEntityLuminator)
        {
            return ((TileEntityLuminator)tileentity).canCableConnectFrom(xCoord, yCoord, zCoord);
        } else
        {
            return true;
        }
    }

    public boolean canInteractWith(TileEntity tileentity)
    {
        if(tileentity instanceof TileEntityCable)
        {
            return canInteractWithCable((TileEntityCable)tileentity);
        }
        if(tileentity instanceof TileEntityLuminator)
        {
            return ((TileEntityLuminator)tileentity).canCableConnectFrom(xCoord, yCoord, zCoord);
        }
        return (tileentity instanceof IEnergySink) || (tileentity instanceof IEnergySource) || (tileentity instanceof IEnergyConductor);
    }

    public boolean canInteractWithCable(TileEntityCable tileentitycable)
    {
        return color == 0 || tileentitycable.color == 0 || color == tileentitycable.color;
    }

    public float getCableThickness()
    {
        return getCableThickness(cableType);
    }

    public static float getCableThickness(int i)
    {
        float f = 1.0F;
        switch(i)
        {
        case 0: // '\0'
            f = 6F;
            break;

        case 1: // '\001'
            f = 4F;
            break;

        case 2: // '\002'
            f = 3F;
            break;

        case 3: // '\003'
            f = 5F;
            break;

        case 4: // '\004'
            f = 6F;
            break;

        case 5: // '\005'
            f = 6F;
            break;

        case 6: // '\006'
            f = 8F;
            break;

        case 7: // '\007'
            f = 10F;
            break;

        case 8: // '\b'
            f = 12F;
            break;

        case 9: // '\t'
            f = 4F;
            break;

        case 10: // '\n'
            f = 5F;
            break;

        case 11: // '\013'
            f = 8F;
            break;

        case 12: // '\f'
            f = 8F;
            break;
        }
        return f / 16F;
    }

    public double getConductionLoss()
    {
        switch(cableType)
        {
        case 0: // '\0'
            return 0.20000000000000001D;

        case 1: // '\001'
            return 0.29999999999999999D;

        case 2: // '\002'
            return 0.5D;

        case 3: // '\003'
            return 0.45000000000000001D;

        case 4: // '\004'
            return 0.40000000000000002D;

        case 5: // '\005'
            return 1.0D;

        case 6: // '\006'
            return 0.94999999999999996D;

        case 7: // '\007'
            return 0.90000000000000002D;

        case 8: // '\b'
            return 0.80000000000000004D;

        case 9: // '\t'
            return 0.025000000000000001D;

        case 10: // '\n'
            return 0.025000000000000001D;

        case 11: // '\013'
            return 0.025000000000000001D;

        case 12: // '\f'
            return 0.025000000000000001D;
        }
        return 0.025000000000000001D;
    }

    public int getInsulationEnergyAbsorption()
    {
        switch(cableType)
        {
        case 0: // '\0'
            return 32;

        case 1: // '\001'
            return 8;

        case 2: // '\002'
            return 8;

        case 3: // '\003'
            return 32;

        case 4: // '\004'
            return 128;

        case 5: // '\005'
            return 0;

        case 6: // '\006'
            return 128;

        case 7: // '\007'
            return 512;

        case 8: // '\b'
            return 9001;

        case 9: // '\t'
            return 9001;

        case 10: // '\n'
            return 3;

        case 11: // '\013'
            return 9001;

        case 12: // '\f'
            return 9001;
        }
        return 0;
    }

    public int getInsulationBreakdownEnergy()
    {
        return 9001;
    }

    public int getConductorBreakdownEnergy()
    {
        switch(cableType)
        {
        case 0: // '\0'
            return 33;

        case 1: // '\001'
            return 33;

        case 2: // '\002'
            return 129;

        case 3: // '\003'
            return 129;

        case 4: // '\004'
            return 129;

        case 5: // '\005'
            return 2049;

        case 6: // '\006'
            return 2049;

        case 7: // '\007'
            return 2049;

        case 8: // '\b'
            return 2049;

        case 9: // '\t'
            return 513;

        case 10: // '\n'
            return 6;

        case 11: // '\013'
            return 2049;

        case 12: // '\f'
            return 2049;
        }
        return 0;
    }

    public void removeInsulation()
    {
    }

    public void removeConductor()
    {
        worldObj.setBlockWithNotify(xCoord, yCoord, zCoord, 0);
        NetworkManager.initiateTileEntityEvent(this, 0, true);
    }

    public List getNetworkedFields()
    {
        Vector vector = new Vector(1);
        vector.add("color");
        vector.addAll(super.getNetworkedFields());
        return vector;
    }

    public void onNetworkUpdate(String s)
    {
        if(s.equals("cableType") || s.equals("color"))
        {
            worldObj.markBlockNeedsUpdate(xCoord, yCoord, zCoord);
        }
        super.onNetworkUpdate(s);
    }

    public void onNetworkEvent(int i)
    {
        switch(i)
        {
        case 0: // '\0'
            worldObj.playSoundEffect((float)xCoord + 0.5F, (float)yCoord + 0.5F, (float)zCoord + 0.5F, "random.fizz", 0.5F, 2.6F + (worldObj.rand.nextFloat() - worldObj.rand.nextFloat()) * 0.8F);
            for(int j = 0; j < 8; j++)
            {
                worldObj.spawnParticle("largesmoke", (double)xCoord + Math.random(), (double)yCoord + 1.2D, (double)zCoord + Math.random(), 0.0D, 0.0D, 0.0D);
            }

            break;

        default:
            throw new RuntimeException((new StringBuilder()).append("Unknown network event: ").append(i).toString());
        }
    }

    public float getWrenchDropRate()
    {
        return 0.0F;
    }

    public short cableType;
    public short color;
    public boolean addedToEnergyNet;
    private static final int EventRemoveConductor = 0;
}
