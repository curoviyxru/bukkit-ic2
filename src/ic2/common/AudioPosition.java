// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import net.minecraft.server.Entity;
import net.minecraft.server.TileEntity;

// Referenced classes of package ic2.common:
//            PositionSpec

public class AudioPosition
{

    public static AudioPosition getFrom(Object obj, PositionSpec positionspec)
    {
        if(obj instanceof AudioPosition)
        {
            return (AudioPosition)obj;
        }
        if(obj instanceof Entity)
        {
            Entity entity = (Entity)obj;
            return new AudioPosition((float)entity.locX, (float)entity.locY, (float)entity.locZ);
        }
        if(obj instanceof TileEntity)
        {
            TileEntity tileentity = (TileEntity)obj;
            return new AudioPosition((float)tileentity.x + 0.5F, (float)tileentity.y + 0.5F, (float)tileentity.z + 0.5F);
        } else
        {
            return null;
        }
    }

    public AudioPosition(float f, float f1, float f2)
    {
        x = f;
        y = f1;
        z = f2;
    }

    public float x;
    public float y;
    public float z;
}
