// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import forge.ISpecialResistance;
import ic2.platform.Platform;
import java.util.List;
import java.util.Random;
import net.minecraft.server.*;

// Referenced classes of package ic2.common:
//            ExplosionIC2

public class EntityMiningLaser extends Entity
{

    public EntityMiningLaser(World world, double d, double d1, double d2)
    {
        super(world);
        range = 0.0F;
        explosive = false;
        startPosX = 0.0D;
        startPosY = 0.0D;
        startPosZ = 0.0D;
        headingSet = false;
        ticksInAir = 0;
        setSize(0.8F, 0.8F);
        yOffset = 0.0F;
        setPosition(d, d1, d2);
    }

    public EntityMiningLaser(World world, EntityLiving entityliving, float f, boolean flag)
    {
        this(world, entityliving, f, flag, entityliving.rotationYaw, entityliving.rotationPitch);
    }

    public EntityMiningLaser(World world, EntityLiving entityliving, float f, boolean flag, float f1, float f2)
    {
        super(world);
        range = 0.0F;
        explosive = false;
        startPosX = 0.0D;
        startPosY = 0.0D;
        startPosZ = 0.0D;
        headingSet = false;
        ticksInAir = 0;
        owner = entityliving;
        setSize(0.8F, 0.8F);
        yOffset = 0.0F;
        double d = Math.toRadians(f1);
        double d1 = Math.toRadians(f2);
        double d2 = -Math.sin(d) * Math.cos(d1);
        double d3 = -Math.sin(d1);
        double d4 = Math.cos(d) * Math.cos(d1);
        startPosX = entityliving.posX - Math.cos(d) * 0.16D;
        startPosY = (entityliving.posY + (double)entityliving.getEyeHeight()) - 0.10000000000000001D;
        startPosZ = entityliving.posZ - Math.sin(d) * 0.16D;
        setPosition(startPosX, startPosY, startPosZ);
        setLaserHeading(d2, d3, d4, 1.0D);
        range = f;
        explosive = flag;
    }

    protected void entityInit()
    {
    }

    public void setLaserHeading(double d, double d1, double d2, double d3)
    {
        double d4 = MathHelper.sqrt_double(d * d + d1 * d1 + d2 * d2);
        motionX = (d / d4) * d3;
        motionY = (d1 / d4) * d3;
        motionZ = (d2 / d4) * d3;
        prevRotationYaw = rotationYaw = (float)Math.toDegrees(Math.atan2(d, d2));
        prevRotationPitch = rotationPitch = (float)Math.toDegrees(Math.atan2(d1, MathHelper.sqrt_double(d * d + d2 * d2)));
        headingSet = true;
    }

    public void setVelocity(double d, double d1, double d2)
    {
        setLaserHeading(d, d1, d2, 1.0D);
    }

    public void onUpdate()
    {
        super.onUpdate();
        if(Platform.isSimulating() && getDistance(startPosX, startPosY, startPosZ) > (double)range)
        {
            if(explosive)
            {
                explode();
            }
            setEntityDead();
            return;
        }
        ticksInAir++;
        Vec3D vec3d = Vec3D.createVector(posX, posY, posZ);
        Vec3D vec3d1 = Vec3D.createVector(posX + motionX, posY + motionY, posZ + motionZ);
        MovingObjectPosition movingobjectposition = worldObj.func_28105_a(vec3d, vec3d1, false, true);
        vec3d = Vec3D.createVector(posX, posY, posZ);
        vec3d1 = Vec3D.createVector(posX + motionX, posY + motionY, posZ + motionZ);
        if(movingobjectposition != null)
        {
            vec3d1 = Vec3D.createVector(movingobjectposition.hitVec.xCoord, movingobjectposition.hitVec.yCoord, movingobjectposition.hitVec.zCoord);
        }
        Entity entity = null;
        List list = worldObj.getEntitiesWithinAABBExcludingEntity(this, boundingBox.addCoord(motionX, motionY, motionZ).expand(1.0D, 1.0D, 1.0D));
        double d = 0.0D;
        for(int i = 0; i < list.size(); i++)
        {
            Entity entity1 = (Entity)list.get(i);
            if(!entity1.canBeCollidedWith() || entity1 == owner && ticksInAir < 5)
            {
                continue;
            }
            float f = 0.3F;
            AxisAlignedBB axisalignedbb = entity1.boundingBox.expand(f, f, f);
            MovingObjectPosition movingobjectposition1 = Platform.axisalignedbbUntranslatedFunction1(axisalignedbb, vec3d, vec3d1);
            if(movingobjectposition1 == null)
            {
                continue;
            }
            double d1 = vec3d.distanceTo(movingobjectposition1.hitVec);
            if(d1 < d || d == 0.0D)
            {
                entity = entity1;
                d = d1;
            }
        }

        if(entity != null)
        {
            movingobjectposition = new MovingObjectPosition(entity);
        }
        if(movingobjectposition != null)
        {
            if(movingobjectposition.entityHit != null)
            {
                if(explosive)
                {
                    explode();
                    setEntityDead();
                    return;
                }
                if(Platform.isSimulating())
                {
                    int j = (int)((double)range - getDistance(startPosX, startPosY, startPosZ));
                    if(j < 1)
                    {
                        j = 0;
                    }

                    if(movingobjectposition.entityHit.attackEntityFrom(null, j))
                    {
                        movingobjectposition.entityHit.fire += 10 * j;
                    }
                }
                setEntityDead();
            } else
            {
                if(explosive)
                {
                    explode();
                    setEntityDead();
                    return;
                }
                int k = movingobjectposition.blockX;
                int l = movingobjectposition.blockY;
                int i1 = movingobjectposition.blockZ;
                int j1 = worldObj.getBlockId(k, l, i1);
                if(!canMine(j1))
                {
                    setEntityDead();
                } else
                if(Platform.isSimulating())
                {
                    float f1 = 0.0F;
                    if(Block.blocksList[j1] instanceof ISpecialResistance)
                    {
                        ISpecialResistance ispecialresistance = (ISpecialResistance)Block.blocksList[j1];
                        f1 = ispecialresistance.getSpecialExplosionResistance(worldObj, k, l, i1, posX, posY, posZ, this) + 0.3F;
                    } else
                    {
                        f1 = Block.blocksList[j1].getExplosionResistance(this) + 0.3F;
                    }
                    range -= f1 / 10F;
                    if(range > 0.0F)
                    {
                        Block.blocksList[j1].dropBlockAsItemWithChance(worldObj, k, l, i1, worldObj.getBlockMetadata(k, l, i1), 0.9F);
                        worldObj.setBlockWithNotify(k, l, i1, 0);
                        if(worldObj.rand.nextInt(10) == 0 && (Block.blocksList[j1].blockMaterial == Material.wood || Block.blocksList[j1].blockMaterial == Material.cloth || Block.blocksList[j1].blockMaterial == Material.leaves))
                        {
                            worldObj.setBlockWithNotify(k, l, i1, Block.fire.blockID);
                        }
                    }
                }
            }
        }
        setPosition(posX + motionX, posY + motionY, posZ + motionZ);
        if(isInWater())
        {
            setEntityDead();
        }
    }

    public void writeEntityToNBT(NBTTagCompound nbttagcompound)
    {
    }

    public void readEntityFromNBT(NBTTagCompound nbttagcompound)
    {
    }

    public float getShadowSize()
    {
        return 0.0F;
    }

    public void explode()
    {
        if(Platform.isSimulating())
        {
            ExplosionIC2 explosionic2 = new ExplosionIC2(worldObj, null, posX, posY, posZ, 5F, 0.85F, 0.55F);
            explosionic2.doExplosion();
        }
    }

    public boolean canMine(int i)
    {
        for(int j = 0; j < unmineableBlocks.length; j++)
        {
            if(i == unmineableBlocks[j].blockID)
            {
                return false;
            }
        }

        return true;
    }

    public float range;
    public boolean explosive;
    public double startPosX;
    public double startPosY;
    public double startPosZ;
    public static Block unmineableBlocks[];
    public static final int netId = 141;
    public static final double laserSpeed = 1D;
    public EntityLiving owner;
    public boolean headingSet;
    private int ticksInAir;

    static 
    {
        unmineableBlocks = (new Block[] {
            Block.brick, Block.obsidian, Block.lavaMoving, Block.lavaStill, Block.waterMoving, Block.waterStill, Block.bedrock, Block.glass
        });
    }
}
