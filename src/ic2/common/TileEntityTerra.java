// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import ic2.api.Direction;
import ic2.platform.*;
import java.util.Random;
import net.minecraft.server.*;

// Referenced classes of package ic2.common:
//            TileEntityElecMachine, ITerraformingBP, StackUtil, PositionSpec

public class TileEntityTerra extends TileEntityElecMachine
{

    public TileEntityTerra()
    {
        super(1, 0, 0x186a0, 512);
        failedAttempts = 0;
        lastX = -1;
        lastY = -1;
        lastZ = -1;
        inactiveTicks = 0;
    }

    public String getInvName()
    {
        return "Terraformer";
    }

    public void updateEntity()
    {
        super.updateEntity();
        if(Platform.isSimulating())
        {
            boolean flag = false;
            if(inventory[0] != null && (inventory[0].getItem() instanceof ITerraformingBP))
            {
                ITerraformingBP iterraformingbp = (ITerraformingBP)inventory[0].getItem();
                if(energy >= iterraformingbp.getConsume())
                {
                    flag = true;
                    int i = xCoord;
                    int j = zCoord;
                    boolean flag1 = true;
                    if(lastY > -1)
                    {
                        int k = iterraformingbp.getRange() / 10;
                        i = (lastX - worldObj.rand.nextInt(k + 1)) + worldObj.rand.nextInt(k + 1);
                        j = (lastZ - worldObj.rand.nextInt(k + 1)) + worldObj.rand.nextInt(k + 1);
                    } else
                    {
                        if(failedAttempts > 4)
                        {
                            failedAttempts = 4;
                        }
                        int l = (iterraformingbp.getRange() * (failedAttempts + 1)) / 5;
                        i = (i - worldObj.rand.nextInt(l + 1)) + worldObj.rand.nextInt(l + 1);
                        j = (j - worldObj.rand.nextInt(l + 1)) + worldObj.rand.nextInt(l + 1);
                    }
                    if(iterraformingbp.terraform(worldObj, i, j, yCoord))
                    {
                        energy -= iterraformingbp.getConsume();
                        failedAttempts = 0;
                        lastX = i;
                        lastZ = j;
                        lastY = yCoord;
                    } else
                    {
                        energy -= iterraformingbp.getConsume() / 10;
                        failedAttempts++;
                        lastY = -1;
                    }
                }
            }
            if(flag)
            {
                inactiveTicks = 0;
                setActive(true);
            } else
            if(!flag && getActive() && inactiveTicks++ > 30)
            {
                setActive(false);
            }
        }
    }

    public void invalidate()
    {
        if(Platform.isRendering() && audioSource != null)
        {
            AudioManager.removeSources(this);
            audioSource = null;
        }
        super.invalidate();
    }

    public int injectEnergy(Direction direction, int i)
    {
        if(i > 512)
        {
            mod_IC2.explodeMachineAt(worldObj, xCoord, yCoord, zCoord);
            return 0;
        }
        if(energy + i > maxEnergy)
        {
            int j = (energy + i) - maxEnergy;
            energy = maxEnergy;
            return j;
        } else
        {
            energy += i;
            return 0;
        }
    }

    public boolean ejectBlueprint()
    {
        if(inventory[0] == null)
        {
            return false;
        }
        if(Platform.isSimulating())
        {
            StackUtil.dropAsEntity(worldObj, xCoord, yCoord, zCoord, inventory[0]);
            inventory[0] = null;
        }
        return true;
    }

    public void insertBlueprint(ItemStack itemstack)
    {
        ejectBlueprint();
        inventory[0] = itemstack;
    }

    public static int getFirstSolidBlockFrom(World world, int i, int j, int k)
    {
        for(; k > 0; k--)
        {
            if(world.isBlockOpaqueCube(i, k, j))
            {
                return k;
            }
        }

        return -1;
    }

    public static int getFirstBlockFrom(World world, int i, int j, int k)
    {
        for(; k > 0; k--)
        {
            if(world.getBlockId(i, k, j) != 0)
            {
                return k;
            }
        }

        return -1;
    }

    public static boolean switchGround(World world, Block block, Block block1, int i, int j, int k, boolean flag)
    {
        if(flag)
        {
            int l = ++j;
            do
            {
                int k1 = world.getBlockId(i, j - 1, k);
                if(k1 == 0 || Block.blocksList[k1] != block)
                {
                    break;
                }
                j--;
            } while(true);
            if(l == j)
            {
                return false;
            } else
            {
                world.setBlockWithNotify(i, j, k, block1.blockID);
                return true;
            }
        }
        do
        {
            int i1 = world.getBlockId(i, j, k);
            if(i1 == 0 || Block.blocksList[i1] != block1)
            {
                break;
            }
            j--;
        } while(true);
        int j1 = world.getBlockId(i, j, k);
        if(j1 == 0 || Block.blocksList[j1] != block)
        {
            return false;
        } else
        {
            world.setBlockWithNotify(i, j, k, block1.blockID);
            return true;
        }
    }

    public void onNetworkUpdate(String s)
    {
        if(s.equals("active") && prevActive != getActive())
        {
            if(audioSource == null)
            {
                audioSource = AudioManager.createSource(this, PositionSpec.Center, "Terraformers/TerraformerGenericloop.ogg", true, false, AudioManager.defaultVolume);
            }
            if(getActive())
            {
                if(audioSource != null)
                {
                    audioSource.play();
                }
            } else
            if(audioSource != null)
            {
                audioSource.stop();
            }
        }
        super.onNetworkUpdate(s);
    }

    public int failedAttempts;
    public int lastX;
    public int lastY;
    public int lastZ;
    public AudioSource audioSource;
    public int inactiveTicks;
}
