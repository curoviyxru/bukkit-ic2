// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import forge.ISidedInventory;
import java.util.Random;
import net.minecraft.server.*;

// Referenced classes of package ic2.common:
//            TileEntityBaseGenerator, ContainerSolarGenerator

public class TileEntitySolarGenerator extends TileEntityBaseGenerator
    implements ISidedInventory
{

    public TileEntitySolarGenerator()
    {
        super(1);
        initialized = false;
        sunIsVisible = false;
        production = 1;
        ticker = randomizer.nextInt(tickRate());
    }

    public short getMaximumStorage()
    {
        return 100;
    }

    public int gaugeFuelScaled(int i)
    {
        return i;
    }

    public boolean gainFuel()
    {
        if(ticker++ % tickRate() == 0 || !initialized)
        {
            updateSunVisibility();
            initialized = true;
        }
        if(sunIsVisible)
        {
            if(mod_IC2.energyGeneratorSolar == 100 || worldObj.rand.nextInt(100) < mod_IC2.energyGeneratorSolar)
            {
                fuel++;
            }
            return true;
        } else
        {
            return false;
        }
    }

    public void updateSunVisibility()
    {
        if(!worldObj.isDaytime() || worldObj.worldProvider.hasNoSky || !worldObj.canBlockSeeTheSky(xCoord, yCoord + 1, zCoord) || !(worldObj.getWorldChunkManager().getBiomeGenAt(xCoord, zCoord) instanceof BiomeGenDesert) && (worldObj.getWorldInfo().getIsRaining() || worldObj.getWorldInfo().getIsThundering()))
        {
            fuel = 0;
            sunIsVisible = false;
        } else
        {
            sunIsVisible = true;
        }
    }

    public boolean needsFuel()
    {
        return true;
    }

    public String getInvName()
    {
        return "Solar Panel";
    }

    public int tickRate()
    {
        return 128;
    }

    public boolean delayActiveUpdate()
    {
        return true;
    }

    public Container getGuiContainer(InventoryPlayer inventoryplayer)
    {
        return new ContainerSolarGenerator(inventoryplayer, this);
    }

    public int getStartInventorySide(int i)
    {
        return 0;
    }

    public int getSizeInventorySide(int i)
    {
        return 1;
    }

    public static Random randomizer = new Random();
    public int ticker;
    public boolean initialized;
    public boolean sunIsVisible;

}
