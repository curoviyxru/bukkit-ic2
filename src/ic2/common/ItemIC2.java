// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import forge.ITextureProvider;
import ic2.platform.ItemCommon;

public class ItemIC2 extends ItemCommon
    implements ITextureProvider
{

    public ItemIC2(int i, int j)
    {
        super(i);
        setIconIndex(j);
    }

    public String getTextureFile()
    {
        return "/ic2/sprites/item_0.png";
    }
}
