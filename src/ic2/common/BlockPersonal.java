// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import java.util.Random;
import net.minecraft.server.*;

// Referenced classes of package ic2.common:
//            BlockMultiID, IPersonalBlock, TileEntityPersonalChest, TileEntityTradeOMat, 
//            TileEntityBlock

public class BlockPersonal extends BlockMultiID
{

    public BlockPersonal(int i)
    {
        super(i, Material.iron);
        setBlockUnbreakable();
        setStepSound(soundMetalFootstep);
    }

    public String getTextureFile()
    {
        return "/ic2/sprites/block_personal.png";
    }

    public int idDropped(int i, Random random)
    {
        return blockID;
    }

    protected int damageDropped(int i)
    {
        return i;
    }

    public Integer getGui(World world, int i, int j, int k, EntityPlayer entityplayer)
    {
        if(world.getBlockMetadata(i, j, k) == 1)
        {
            TileEntity tileentity = world.getBlockTileEntity(i, j, k);
            if(((IPersonalBlock)tileentity).canAccess(entityplayer))
            {
                return Integer.valueOf(mod_IC2.guiIdTradeOMatOpen);
            } else
            {
                return Integer.valueOf(mod_IC2.guiIdTradeOMatClosed);
            }
        }
        switch(world.getBlockMetadata(i, j, k))
        {
        case 0: // '\0'
            return Integer.valueOf(0);
        }
        return null;
    }

    public TileEntityBlock getBlockEntity(int i)
    {
        switch(i)
        {
        case 0: // '\0'
            return new TileEntityPersonalChest();

        case 1: // '\001'
            return new TileEntityTradeOMat();
        }
        return new TileEntityBlock();
    }

    public boolean blockActivated(World world, int i, int j, int k, EntityPlayer entityplayer)
    {
        if(entityplayer.isSneaking())
        {
            return false;
        }
        int l = world.getBlockMetadata(i, j, k);
        TileEntity tileentity = world.getBlockTileEntity(i, j, k);
        if(l != 1 && (tileentity instanceof IPersonalBlock) && !((IPersonalBlock)tileentity).canAccess(entityplayer))
        {
            return false;
        } else
        {
            return super.blockActivated(world, i, j, k, entityplayer);
        }
    }
}
