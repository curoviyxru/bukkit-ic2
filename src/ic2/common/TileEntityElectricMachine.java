// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import forge.ISidedInventory;
import ic2.platform.*;
import ic2.platform.NetworkManager;
import net.minecraft.server.*;

// Referenced classes of package ic2.common:
//            TileEntityElecMachine, IHasGuiContainer, INetworkTileEntityEventListener

public abstract class TileEntityElectricMachine extends TileEntityElecMachine
    implements IHasGuiContainer, INetworkTileEntityEventListener, ISidedInventory
{

    public TileEntityElectricMachine(int i, int j, int k, int l)
    {
        super(i, 1, (j * k + l) - 1, l);
        progress = 0;
        energyconsume = j;
        operationLength = k;
    }

    public void readFromNBT(NBTTagCompound nbttagcompound)
    {
        super.readFromNBT(nbttagcompound);
        progress = nbttagcompound.getShort("progress");
    }

    public void writeToNBT(NBTTagCompound nbttagcompound)
    {
        super.writeToNBT(nbttagcompound);
        nbttagcompound.setShort("progress", progress);
    }

    public int gaugeProgressScaled(int i)
    {
        return (progress * i) / operationLength;
    }

    public int gaugeFuelScaled(int i)
    {
        if(energy <= 0)
        {
            return 0;
        }
        int j = (energy * i) / (operationLength * energyconsume);
        if(j > i)
        {
            return i;
        } else
        {
            return j;
        }
    }

    public void updateEntity()
    {
        super.updateEntity();
        if(Platform.isSimulating())
        {
            boolean flag = false;
            if(energy <= energyconsume * operationLength && canOperate())
            {
                flag = provideEnergy();
            }
            boolean flag1 = getActive();
            if(progress >= operationLength)
            {
                operate();
                flag = true;
                progress = 0;
                flag1 = false;
                NetworkManager.initiateTileEntityEvent(this, 2, true);
            }
            boolean flag2 = canOperate();
            if(!flag1 || progress == 0)
            {
                if(flag2)
                {
                    if(energy >= energyconsume)
                    {
                        flag1 = true;
                        NetworkManager.initiateTileEntityEvent(this, 0, true);
                    }
                } else
                {
                    progress = 0;
                }
            } else
            if(!flag2 || energy < energyconsume)
            {
                if(!flag2)
                {
                    progress = 0;
                }
                flag1 = false;
                NetworkManager.initiateTileEntityEvent(this, 1, true);
            }
            if(flag1)
            {
                progress++;
                energy -= energyconsume;
            }
            if(flag)
            {
                onInventoryChanged();
            }
            if(flag1 != getActive())
            {
                setActive(flag1);
            }
        }
    }

    public void invalidate()
    {
        super.invalidate();
        if(Platform.isRendering() && audioSource != null)
        {
            AudioManager.removeSources(this);
            audioSource = null;
        }
    }

    public void operate()
    {
        if(!canOperate())
        {
            return;
        }
        ItemStack itemstack = getResultFor(inventory[0]);
        if(inventory[2] == null)
        {
            inventory[2] = itemstack.copy();
        } else
        {
            inventory[2].stackSize += itemstack.stackSize;
        }
        if(inventory[0].getItem().hasContainerItem())
        {
            inventory[0] = new ItemStack(inventory[0].getItem().getContainerItem());
        } else
        {
            inventory[0].stackSize--;
        }
        if(inventory[0].stackSize <= 0)
        {
            inventory[0] = null;
        }
    }

    public boolean canOperate()
    {
        if(inventory[0] == null)
        {
            return false;
        }
        ItemStack itemstack = getResultFor(inventory[0]);
        if(itemstack == null)
        {
            return false;
        }
        if(inventory[2] == null)
        {
            return true;
        }
        if(!inventory[2].isItemEqual(itemstack))
        {
            return false;
        } else
        {
            return inventory[2].stackSize + itemstack.stackSize <= inventory[2].getMaxStackSize();
        }
    }

    public abstract ItemStack getResultFor(ItemStack itemstack);

    public abstract String getInvName();

    public String getStartSoundFile()
    {
        return null;
    }

    public String getInterruptSoundFile()
    {
        return null;
    }

    public void onNetworkEvent(int i)
    {
        if(audioSource == null && getStartSoundFile() != null)
        {
            audioSource = AudioManager.createSource(this, getStartSoundFile());
        }
        switch(i)
        {
        default:
            break;

        case 0: // '\0'
            if(audioSource != null)
            {
                audioSource.play();
            }
            break;

        case 1: // '\001'
            if(audioSource == null)
            {
                break;
            }
            audioSource.stop();
            if(getInterruptSoundFile() != null)
            {
                AudioManager.playOnce(this, getInterruptSoundFile());
            }
            break;

        case 2: // '\002'
            if(audioSource != null)
            {
                audioSource.stop();
            }
            break;
        }
    }

    public int getStartInventorySide(int i)
    {
        switch(i)
        {
        case 0: // '\0'
            return 1;

        case 1: // '\001'
            return 0;
        }
        return 2;
    }

    public int getSizeInventorySide(int i)
    {
        return 1;
    }

    public short progress;
    public int energyconsume;
    public int operationLength;
    public AudioSource audioSource;
    private static final int EventStart = 0;
    private static final int EventInterrupt = 1;
    private static final int EventStop = 2;
}
