// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import forge.ITextureProvider;
import net.minecraft.server.Block;
import net.minecraft.server.Material;

public class BlockMetal extends Block
    implements ITextureProvider
{

    public BlockMetal(int i, Material material)
    {
        super(i, material);
    }

    protected int damageDropped(int i)
    {
        return i;
    }

    public int getBlockTextureFromSideAndMetadata(int i, int j)
    {
        switch(j)
        {
        case 0: // '\0'
            return 93;

        case 1: // '\001'
            return 94;

        case 2: // '\002'
            return 78;

        case 3: // '\003'
            return i >= 2 ? 95 : 79;
        }
        return 0;
    }

    public String getTextureFile()
    {
        return "/ic2/sprites/block_0.png";
    }
}
