// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import forge.ITextureProvider;
import ic2.platform.BlockCommon;
import java.util.Random;
import net.minecraft.server.*;

public class BlockTex extends BlockCommon
    implements ITextureProvider
{

    public BlockTex(int i, int j, Material material)
    {
        super(i, j, material);
    }

    public String getTextureFile()
    {
        return "/ic2/sprites/block_0.png";
    }

    public int idDropped(int i, Random random)
    {
        if(blockID == mod_IC2.blockOreUran.blockID)
        {
            return mod_IC2.itemOreUran.shiftedIndex;
        } else
        {
            return blockID;
        }
    }
}
