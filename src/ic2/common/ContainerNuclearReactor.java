// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import java.util.List;
import net.minecraft.server.*;

// Referenced classes of package ic2.common:
//            ContainerIC2, TileEntityNuclearReactor

public class ContainerNuclearReactor extends ContainerIC2
{

    public ContainerNuclearReactor(InventoryPlayer inventoryplayer, TileEntityNuclearReactor tileentitynuclearreactor, short word0)
    {
        output = 0;
        heat = 0;
        tileentity = tileentitynuclearreactor;
        size = word0;
        int i = 89 - 9 * word0;
        byte byte0 = 18;
        int j = 0;
        int k = 0;
        for(int l = 0; l < 54; l++)
        {
            if(j < word0)
            {
                addSlot(new Slot(tileentity, l, i + 18 * j, byte0 + 18 * k));
            }
            if(++j >= 9)
            {
                k++;
                j = 0;
            }
        }

        for(int i1 = 0; i1 < 3; i1++)
        {
            for(int k1 = 0; k1 < 9; k1++)
            {
                addSlot(new Slot(inventoryplayer, k1 + i1 * 9 + 9, 8 + k1 * 18, 140 + i1 * 18));
            }

        }

        for(int j1 = 0; j1 < 9; j1++)
        {
            addSlot(new Slot(inventoryplayer, j1, 8 + j1 * 18, 198));
        }

    }

    public void updateCraftingMatrix()
    {
        super.updateCraftingMatrix();
        for(int i = 0; i < slots.size(); i++)
        {
            ICrafting icrafting = (ICrafting)slots.get(i);
            if(output != tileentity.output)
            {
                icrafting.updateCraftingInventoryInfo(this, 0, tileentity.output);
            }
            if(heat != tileentity.heat)
            {
                icrafting.updateCraftingInventoryInfo(this, 1, tileentity.heat);
            }
        }

        output = tileentity.output;
        heat = tileentity.heat;
    }

    public void updateProgressBar(int i, int j)
    {
        if(i == 0)
        {
            tileentity.output = (short)j;
        }
        if(i == 1)
        {
            tileentity.heat = (short)j;
        }
    }

    public boolean isUsableByPlayer(EntityPlayer entityplayer)
    {
        return tileentity.canInteractWith(entityplayer);
    }

    public int guiInventorySize()
    {
        return 6 * size;
    }

    public int getInput()
    {
        return -1;
    }

    public TileEntityNuclearReactor tileentity;
    public short output;
    public short heat;
    public short size;
}
