// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import forge.ISidedInventory;
import ic2.platform.Platform;
import net.minecraft.server.*;

// Referenced classes of package ic2.common:
//            TileEntityMachine, IHasGuiContainer, ContainerIronFurnace

public class TileEntityIronFurnace extends TileEntityMachine
    implements IHasGuiContainer, ISidedInventory
{

    public TileEntityIronFurnace()
    {
        super(3);
        fuel = 0;
        maxFuel = 0;
        progress = 0;
    }

    public void readFromNBT(NBTTagCompound nbttagcompound)
    {
        super.readFromNBT(nbttagcompound);
        fuel = nbttagcompound.getShort("fuel");
        maxFuel = nbttagcompound.getShort("maxFuel");
        progress = nbttagcompound.getShort("progress");
    }

    public void writeToNBT(NBTTagCompound nbttagcompound)
    {
        super.writeToNBT(nbttagcompound);
        nbttagcompound.setShort("fuel", fuel);
        nbttagcompound.setShort("maxFuel", maxFuel);
        nbttagcompound.setShort("progress", progress);
    }

    public int gaugeProgressScaled(int i)
    {
        return (progress * i) / 160;
    }

    public int gaugeFuelScaled(int i)
    {
        if(maxFuel == 0)
        {
            maxFuel = fuel;
            if(maxFuel == 0)
            {
                maxFuel = 160;
            }
        }
        return (fuel * i) / maxFuel;
    }

    public void updateEntity()
    {
        super.updateEntity();
        boolean flag = isBurning();
        boolean flag1 = false;
        if(!Platform.isSimulating())
        {
            return;
        }
        if(fuel <= 0 && canOperate())
        {
            fuel = maxFuel = getFuelValueFor(inventory[1]);
            if(fuel > 0)
            {
                if(inventory[1].getItem().hasContainerItem())
                {
                    inventory[1] = new ItemStack(inventory[1].getItem().getContainerItem());
                } else
                {
                    inventory[1].stackSize--;
                }
                if(inventory[1].stackSize <= 0)
                {
                    inventory[1] = null;
                }
                flag1 = true;
            }
        }
        if(isBurning() && canOperate())
        {
            progress++;
            if(progress >= 160)
            {
                progress = 0;
                operate();
                flag1 = true;
            }
        } else
        {
            progress = 0;
        }
        if(fuel > 0)
        {
            fuel--;
        }
        if(flag != isBurning())
        {
            setActive(isBurning());
            flag1 = true;
        }
        if(flag1)
        {
            onInventoryChanged();
        }
    }

    public void operate()
    {
        if(!canOperate())
        {
            return;
        }
        ItemStack itemstack = getResultFor(inventory[0]);
        if(inventory[2] == null)
        {
            inventory[2] = itemstack.copy();
        } else
        {
            inventory[2].stackSize += itemstack.stackSize;
        }
        if(inventory[0].getItem().hasContainerItem())
        {
            inventory[0] = new ItemStack(inventory[0].getItem().getContainerItem());
        } else
        {
            inventory[0].stackSize--;
        }
        if(inventory[0].stackSize <= 0)
        {
            inventory[0] = null;
        }
    }

    public boolean isBurning()
    {
        return fuel > 0;
    }

    public boolean canOperate()
    {
        if(inventory[0] == null)
        {
            return false;
        }
        ItemStack itemstack = getResultFor(inventory[0]);
        if(itemstack == null)
        {
            return false;
        }
        if(inventory[2] == null)
        {
            return true;
        }
        if(!inventory[2].isItemEqual(itemstack))
        {
            return false;
        } else
        {
            return inventory[2].stackSize + itemstack.stackSize <= inventory[2].getMaxStackSize();
        }
    }

    public static short getFuelValueFor(ItemStack itemstack)
    {
        if(itemstack == null)
        {
            return 0;
        }
        int i = itemstack.getItem().shiftedIndex;
        if(i < 256 && Block.blocksList[i].blockMaterial == Material.wood)
        {
            return 300;
        }
        if(i == Item.stick.shiftedIndex)
        {
            return 100;
        }
        if(i == Item.coal.shiftedIndex)
        {
            return 1600;
        }
        if(i == Item.bucketLava.shiftedIndex)
        {
            return 2000;
        }
        if(i == Block.sapling.blockID)
        {
            return 100;
        }
        if(i == mod_IC2.itemFuelCan.shiftedIndex)
        {
            return (short)(itemstack.getData() * 2);
        } else
        {
            return (short)Platform.AddAllFuel(i, itemstack.getData());
        }
    }

    public ItemStack getResultFor(ItemStack itemstack)
    {
        return FurnaceRecipes.smelting().getSmeltingResult(itemstack.id);
    }

    public String getInvName()
    {
        return "Iron Furnace";
    }

    public Container getGuiContainer(InventoryPlayer inventoryplayer)
    {
        return new ContainerIronFurnace(inventoryplayer, this);
    }

    public int getStartInventorySide(int i)
    {
        switch(i)
        {
        case 0: // '\0'
            return 1;

        case 1: // '\001'
            return 0;
        }
        return 2;
    }

    public int getSizeInventorySide(int i)
    {
        return 1;
    }

    public short fuel;
    public short maxFuel;
    public short progress;
    public final short operationLength = 160;
}
