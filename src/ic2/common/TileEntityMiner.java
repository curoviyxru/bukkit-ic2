// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import forge.ForgeHooks;
import forge.ISidedInventory;
import ic2.platform.*;
import java.io.Serializable;
import java.util.*;
import net.minecraft.server.*;

// Referenced classes of package ic2.common:
//            TileEntityElecMachine, IHasGuiContainer, ItemScanner, IChargeableItem, 
//            ItemElectricToolDrill, ItemElectricToolDDrill, ItemElectricTool, StackUtil, 
//            TileEntityPump, ContainerMiner, PositionSpec

public class TileEntityMiner extends TileEntityElecMachine
    implements IHasGuiContainer, ISidedInventory
{

    public TileEntityMiner()
    {
        super(4, 0, 1000, 32);
        targetX = 0;
        targetY = -1;
        targetZ = 0;
        miningTicker = 0;
        stuckOn = null;
    }

    public void updateEntity()
    {
        super.updateEntity();
        if(Platform.isSimulating())
        {
            boolean flag = isOperating();
            boolean flag1 = false;
            if(isOperating())
            {
                energy--;
                if(inventory[1] != null && (Item.itemsList[inventory[1].itemID] instanceof ItemScanner))
                {
                    energy -= ((IChargeableItem)Item.itemsList[inventory[1].itemID]).giveEnergyTo(inventory[1], energy, 2, false);
                }
                if(inventory[3] != null && ((Item.itemsList[inventory[3].itemID] instanceof ItemElectricToolDrill) || (Item.itemsList[inventory[3].itemID] instanceof ItemElectricToolDDrill)))
                {
                    energy -= ((IChargeableItem)Item.itemsList[inventory[3].itemID]).giveEnergyTo(inventory[3], energy, 1, false);
                }
            }
            if(energy <= maxEnergy)
            {
                flag1 = provideEnergy();
            }
            if(flag)
            {
                flag1 = mine();
            } else
            if(inventory[3] == null)
            {
                if(energy >= 2 && canWithdraw())
                {
                    targetY = -1;
                    miningTicker++;
                    energy -= 2;
                    if(miningTicker >= 20)
                    {
                        miningTicker = 0;
                        flag1 = withdrawPipe();
                    }
                } else
                if(isStuck())
                {
                    miningTicker = 0;
                }
            }
            setActive(isOperating());
            if(flag != isOperating())
            {
                flag1 = true;
            }
            if(flag1)
            {
                onInventoryChanged();
            }
        }
    }

    public void invalidate()
    {
        if(Platform.isRendering() && audioSource != null)
        {
            AudioManager.removeSources(this);
            audioSource = null;
        }
        super.invalidate();
    }

    public void readFromNBT(NBTTagCompound nbttagcompound)
    {
        super.readFromNBT(nbttagcompound);
        targetX = nbttagcompound.getInteger("targetX");
        targetY = nbttagcompound.getInteger("targetY");
        targetZ = nbttagcompound.getInteger("targetZ");
        miningTicker = nbttagcompound.getShort("miningTicker");
    }

    public void writeToNBT(NBTTagCompound nbttagcompound)
    {
        super.writeToNBT(nbttagcompound);
        nbttagcompound.setInteger("targetX", (short)targetX);
        nbttagcompound.setInteger("targetY", (short)targetY);
        nbttagcompound.setInteger("targetZ", (short)targetZ);
        nbttagcompound.setShort("miningTicker", miningTicker);
    }

    public boolean mine()
    {
        if(targetY < 0)
        {
            aquireTarget();
            return false;
        }
        if(!canReachTarget(targetX, targetY, targetZ, true))
        {
            int i = targetX - xCoord;
            int k = targetZ - zCoord;
            if(Math.abs(i) > Math.abs(k))
            {
                if(i > 0)
                {
                    targetX--;
                } else
                {
                    targetX++;
                }
            } else
            if(k > 0)
            {
                targetZ--;
            } else
            {
                targetZ++;
            }
            return false;
        }
        if(!canMine(worldObj.getBlockId(targetX, targetY, targetZ), worldObj.getBlockMetadata(targetX, targetY, targetZ)))
        {
            int j = worldObj.getBlockId(targetX, targetY, targetZ);
            if((j == Block.waterMoving.blockID || j == Block.waterStill.blockID || j == Block.lavaMoving.blockID || j == Block.lavaStill.blockID) && isAnyPumpConnected())
            {
                return false;
            } else
            {
                miningTicker = -1;
                stuckOn = Platform.translateBlockName(Block.blocksList[j]);
                return false;
            }
        }
        stuckOn = null;
        miningTicker++;
        energy--;
        if(inventory[3].itemID == mod_IC2.itemToolDDrill.shiftedIndex)
        {
            miningTicker += 3;
            energy -= 14;
        }
        if(miningTicker >= 200)
        {
            miningTicker = 0;
            mineBlock();
            return true;
        } else
        {
            return false;
        }
    }

    public void mineBlock()
    {
        ItemElectricTool.use(inventory[3], 1, null);
        int i = worldObj.getBlockId(targetX, targetY, targetZ);
        int j = worldObj.getBlockMetadata(targetX, targetY, targetZ);
        boolean flag = false;
        if(i == Block.waterMoving.blockID || i == Block.waterStill.blockID || i == Block.lavaMoving.blockID || i == Block.lavaStill.blockID)
        {
            flag = true;
            if(j != 0)
            {
                i = 0;
            }
        }
        if(i != 0)
        {
            if(!flag)
            {
                Block block = Block.blocksList[i];
                StackUtil.distributeDrop(this, mod_IC2.getDrop(worldObj, block, j));
            } else
            {
                if(i == Block.waterMoving.blockID || i == Block.waterStill.blockID)
                {
                    usePump(Block.waterStill.blockID);
                }
                if(i == Block.lavaMoving.blockID || i == Block.lavaStill.blockID)
                {
                    usePump(Block.lavaStill.blockID);
                }
            }
            worldObj.setBlockWithNotify(targetX, targetY, targetZ, 0);
            energy -= 2 * (yCoord - targetY);
        }
        if(targetX == xCoord && targetZ == zCoord)
        {
            worldObj.setBlock(targetX, targetY, targetZ, mod_IC2.blockMiningPipe.blockID);
            inventory[2].stackSize--;
            if(inventory[2].stackSize == 0)
            {
                inventory[2] = null;
            }
            energy -= 10;
        }
        updateMineTip(targetY);
        targetY = -1;
    }

    public boolean withdrawPipe()
    {
        ArrayList arraylist = new ArrayList();
        arraylist.add(new ItemStack(mod_IC2.blockMiningPipe));
        StackUtil.distributeDrop(this, arraylist);
        int i = getPipeTip();
        worldObj.setBlockWithNotify(xCoord, i, zCoord, 0);
        if(inventory[2] != null && inventory[2].itemID != mod_IC2.blockMiningPipe.blockID && inventory[2].itemID < 256)
        {
            worldObj.setBlockAndMetadataWithNotify(xCoord, i, zCoord, inventory[2].itemID, inventory[2].getItemDamage());
            inventory[2].stackSize--;
            if(inventory[2].stackSize == 0)
            {
                inventory[2] = null;
            }
            updateMineTip(i + 1);
            return true;
        } else
        {
            updateMineTip(i + 1);
            return false;
        }
    }

    public void updateMineTip(int i)
    {
        if(i == yCoord)
        {
            return;
        }
        int j = xCoord;
        int k = yCoord - 1;
        int l = zCoord;
        for(; k > i; k--)
        {
            if(worldObj.getBlockId(j, k, l) != mod_IC2.blockMiningPipe.blockID)
            {
                worldObj.setBlockWithNotify(j, k, l, mod_IC2.blockMiningPipe.blockID);
            }
        }

        worldObj.setBlockWithNotify(j, i, l, mod_IC2.blockMiningTip.blockID);
    }

    public boolean canReachTarget(int i, int j, int k, boolean flag)
    {
        if(xCoord == i && zCoord == k)
        {
            return true;
        }
        if(!flag && !canPass(worldObj.getBlockId(i, j, k)))
        {
            return false;
        }
        int l = i - xCoord;
        int i1 = k - zCoord;
        if(Math.abs(l) > Math.abs(i1))
        {
            if(l > 0)
            {
                i--;
            } else
            {
                i++;
            }
        } else
        if(i1 > 0)
        {
            k--;
        } else
        {
            k++;
        }
        return canReachTarget(i, j, k, false);
    }

    public void aquireTarget()
    {
        int i = getPipeTip();
        if(i >= yCoord || inventory[1] == null || !(inventory[1].getItem() instanceof ItemScanner))
        {
            setTarget(xCoord, i - 1, zCoord);
            return;
        }
        int j = ((ItemScanner)inventory[1].getItem()).startLayerScan(inventory[1]);
        if(j > 0)
        {
            for(int k = xCoord - j; k <= xCoord + j; k++)
            {
                for(int l = zCoord - j; l <= zCoord + j; l++)
                {
                    int i1 = worldObj.getBlockId(k, i, l);
                    int j1 = worldObj.getBlockMetadata(k, i, l);
                    if(ItemScanner.isValuable(i1, j1) && canMine(i1, j1) || isAnyPumpConnected() && worldObj.getBlockMetadata(k, i, l) == 0 && (i1 == Block.lavaMoving.blockID || i1 == Block.lavaStill.blockID))
                    {
                        setTarget(k, i, l);
                        return;
                    }
                }

            }

        }
        setTarget(xCoord, i - 1, zCoord);
    }

    public void setTarget(int i, int j, int k)
    {
        targetX = i;
        targetY = j;
        targetZ = k;
    }

    public int getPipeTip()
    {
        int i;
        for(i = yCoord; worldObj.getBlockId(xCoord, i - 1, zCoord) == mod_IC2.blockMiningPipe.blockID || worldObj.getBlockId(xCoord, i - 1, zCoord) == mod_IC2.blockMiningTip.blockID; i--) { }
        return i;
    }

    public boolean canPass(int i)
    {
        return i == 0 || i == Block.waterMoving.blockID || i == Block.waterStill.blockID || i == Block.lavaMoving.blockID || i == Block.lavaStill.blockID || i == mod_IC2.blockMachine.blockID || i == mod_IC2.blockMiningPipe.blockID || i == mod_IC2.blockMiningTip.blockID;
    }

    public boolean isOperating()
    {
        return energy > 100 && canOperate();
    }

    public boolean canOperate()
    {
        if(inventory[2] == null || inventory[3] == null)
        {
            return false;
        }
        if(inventory[2].itemID != mod_IC2.blockMiningPipe.blockID)
        {
            return false;
        }
        if(inventory[3].itemID != mod_IC2.itemToolDrill.shiftedIndex && inventory[3].itemID != mod_IC2.itemToolDDrill.shiftedIndex)
        {
            return false;
        } else
        {
            return !isStuck();
        }
    }

    public boolean isStuck()
    {
        return miningTicker < 0;
    }

    public String getStuckOn()
    {
        return stuckOn;
    }

    public boolean canMine(int i, int j)
    {
        if(i == 0)
        {
            return true;
        }
        if(i == mod_IC2.blockMiningPipe.blockID || i == mod_IC2.blockMiningTip.blockID)
        {
            return false;
        }
        if((i == Block.waterMoving.blockID || i == Block.waterStill.blockID || i == Block.lavaMoving.blockID || i == Block.lavaStill.blockID) && isPumpConnected())
        {
            return true;
        }
        Block block = Block.blocksList[i];
        if(block.getHardness(j) < 0.0F)
        {
            return false;
        }
        if(block.canCollideCheck(j, false) && block.blockMaterial.getIsHarvestable())
        {
            return true;
        }
        if(i == Block.web.blockID)
        {
            return true;
        }
        if(inventory[3] != null && (inventory[3].itemID != mod_IC2.itemToolDrill.shiftedIndex || inventory[3].itemID != mod_IC2.itemToolDDrill.shiftedIndex))
        {
            try
            {
                HashMap hashmap = (HashMap)ModLoader.getPrivateValue(forge.ForgeHooks.class, null, "toolClasses");
                List list = (List)hashmap.get(Integer.valueOf(inventory[3].itemID));
                if(list == null)
                {
                    return inventory[3].canHarvestBlock(block);
                }
                Object aobj[] = list.toArray();
                String s = (String)aobj[0];
                int k = ((Integer)aobj[1]).intValue();
                HashMap hashmap1 = (HashMap)ModLoader.getPrivateValue(forge.ForgeHooks.class, null, "toolHarvestLevels");
                Integer integer = (Integer)hashmap1.get(Arrays.asList(new Serializable[] {
                    Integer.valueOf(block.blockID), Integer.valueOf(j), s
                }));
                if(integer == null)
                {
                    return inventory[3].canHarvestBlock(block);
                }
                if(integer.intValue() > k)
                {
                    return false;
                } else
                {
                    return inventory[3].canHarvestBlock(block);
                }
            }
            catch(NoSuchFieldException nosuchfieldexception)
            {
                return false;
            }
        } else
        {
            return false;
        }
    }

    public boolean canWithdraw()
    {
        return worldObj.getBlockId(xCoord, yCoord - 1, zCoord) == mod_IC2.blockMiningPipe.blockID || worldObj.getBlockId(xCoord, yCoord - 1, zCoord) == mod_IC2.blockMiningTip.blockID;
    }

    public boolean isPumpConnected()
    {
        if((worldObj.getBlockTileEntity(xCoord, yCoord + 1, zCoord) instanceof TileEntityPump) && ((TileEntityPump)worldObj.getBlockTileEntity(xCoord, yCoord + 1, zCoord)).canHarvest())
        {
            return true;
        }
        if((worldObj.getBlockTileEntity(xCoord, yCoord - 1, zCoord) instanceof TileEntityPump) && ((TileEntityPump)worldObj.getBlockTileEntity(xCoord, yCoord - 1, zCoord)).canHarvest())
        {
            return true;
        }
        if((worldObj.getBlockTileEntity(xCoord + 1, yCoord, zCoord) instanceof TileEntityPump) && ((TileEntityPump)worldObj.getBlockTileEntity(xCoord + 1, yCoord, zCoord)).canHarvest())
        {
            return true;
        }
        if((worldObj.getBlockTileEntity(xCoord - 1, yCoord, zCoord) instanceof TileEntityPump) && ((TileEntityPump)worldObj.getBlockTileEntity(xCoord - 1, yCoord, zCoord)).canHarvest())
        {
            return true;
        }
        if((worldObj.getBlockTileEntity(xCoord, yCoord, zCoord + 1) instanceof TileEntityPump) && ((TileEntityPump)worldObj.getBlockTileEntity(xCoord, yCoord, zCoord + 1)).canHarvest())
        {
            return true;
        }
        return (worldObj.getBlockTileEntity(xCoord, yCoord, zCoord - 1) instanceof TileEntityPump) && ((TileEntityPump)worldObj.getBlockTileEntity(xCoord, yCoord, zCoord - 1)).canHarvest();
    }

    public boolean isAnyPumpConnected()
    {
        if(worldObj.getBlockTileEntity(xCoord, yCoord + 1, zCoord) instanceof TileEntityPump)
        {
            return true;
        }
        if(worldObj.getBlockTileEntity(xCoord, yCoord - 1, zCoord) instanceof TileEntityPump)
        {
            return true;
        }
        if(worldObj.getBlockTileEntity(xCoord + 1, yCoord, zCoord) instanceof TileEntityPump)
        {
            return true;
        }
        if(worldObj.getBlockTileEntity(xCoord - 1, yCoord, zCoord) instanceof TileEntityPump)
        {
            return true;
        }
        if(worldObj.getBlockTileEntity(xCoord, yCoord, zCoord + 1) instanceof TileEntityPump)
        {
            return true;
        }
        return worldObj.getBlockTileEntity(xCoord, yCoord, zCoord - 1) instanceof TileEntityPump;
    }

    public void usePump(int i)
    {
        if((worldObj.getBlockTileEntity(xCoord, yCoord + 1, zCoord) instanceof TileEntityPump) && ((TileEntityPump)worldObj.getBlockTileEntity(xCoord, yCoord + 1, zCoord)).canHarvest())
        {
            ((TileEntityPump)worldObj.getBlockTileEntity(xCoord, yCoord + 1, zCoord)).pumpThis(i);
            return;
        }
        if((worldObj.getBlockTileEntity(xCoord, yCoord - 1, zCoord) instanceof TileEntityPump) && ((TileEntityPump)worldObj.getBlockTileEntity(xCoord, yCoord - 1, zCoord)).canHarvest())
        {
            ((TileEntityPump)worldObj.getBlockTileEntity(xCoord, yCoord - 1, zCoord)).pumpThis(i);
            return;
        }
        if((worldObj.getBlockTileEntity(xCoord + 1, yCoord, zCoord) instanceof TileEntityPump) && ((TileEntityPump)worldObj.getBlockTileEntity(xCoord + 1, yCoord, zCoord)).canHarvest())
        {
            ((TileEntityPump)worldObj.getBlockTileEntity(xCoord + 1, yCoord, zCoord)).pumpThis(i);
            return;
        }
        if((worldObj.getBlockTileEntity(xCoord - 1, yCoord, zCoord) instanceof TileEntityPump) && ((TileEntityPump)worldObj.getBlockTileEntity(xCoord - 1, yCoord, zCoord)).canHarvest())
        {
            ((TileEntityPump)worldObj.getBlockTileEntity(xCoord - 1, yCoord, zCoord)).pumpThis(i);
            return;
        }
        if((worldObj.getBlockTileEntity(xCoord, yCoord, zCoord + 1) instanceof TileEntityPump) && ((TileEntityPump)worldObj.getBlockTileEntity(xCoord, yCoord, zCoord + 1)).canHarvest())
        {
            ((TileEntityPump)worldObj.getBlockTileEntity(xCoord, yCoord, zCoord + 1)).pumpThis(i);
            return;
        }
        if((worldObj.getBlockTileEntity(xCoord, yCoord, zCoord - 1) instanceof TileEntityPump) && ((TileEntityPump)worldObj.getBlockTileEntity(xCoord, yCoord, zCoord - 1)).canHarvest())
        {
            ((TileEntityPump)worldObj.getBlockTileEntity(xCoord, yCoord, zCoord - 1)).pumpThis(i);
            return;
        } else
        {
            return;
        }
    }

    public String getInvName()
    {
        return "Miner";
    }

    public int gaugeEnergyScaled(int i)
    {
        if(energy <= 0)
        {
            return 0;
        }
        int j = (energy * i) / 1000;
        if(j > i)
        {
            j = i;
        }
        return j;
    }

    public Container getGuiContainer(InventoryPlayer inventoryplayer)
    {
        return new ContainerMiner(inventoryplayer, this);
    }

    public void onNetworkUpdate(String s)
    {
        if(s.equals("active") && prevActive != getActive())
        {
            if(audioSource == null)
            {
                audioSource = AudioManager.createSource(this, PositionSpec.Center, "Machines/MinerOp.ogg", true, false, AudioManager.defaultVolume);
            }
            if(getActive())
            {
                if(audioSource != null)
                {
                    audioSource.play();
                }
            } else
            if(audioSource != null)
            {
                audioSource.stop();
            }
        }
        super.onNetworkUpdate(s);
    }

    public int getStartInventorySide(int i)
    {
        byte byte0;
        switch(getFacing())
        {
        case 2: // '\002'
            byte0 = 4;
            break;

        case 3: // '\003'
            byte0 = 5;
            break;

        case 4: // '\004'
            byte0 = 3;
            break;

        case 5: // '\005'
            byte0 = 2;
            break;

        default:
            byte0 = 2;
            break;
        }
        if(i == byte0)
        {
            return 3;
        }
        switch(i)
        {
        case 0: // '\0'
            return 0;

        case 1: // '\001'
            return 2;
        }
        return 1;
    }

    public int getSizeInventorySide(int i)
    {
        return 1;
    }

    public int targetX;
    public int targetY;
    public int targetZ;
    public short miningTicker;
    public String stuckOn;
    private AudioSource audioSource;
}
