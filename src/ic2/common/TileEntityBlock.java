// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import ic2.api.IWrenchable;
import ic2.platform.NetworkManager;
import ic2.platform.Platform;
import java.util.List;
import java.util.Vector;
import net.minecraft.server.*;

// Referenced classes of package ic2.common:
//            INetworkUpdateListener

public class TileEntityBlock extends TileEntity
    implements INetworkUpdateListener, IWrenchable
{

    public TileEntityBlock()
    {
        created = false;
        active = false;
        facing = 0;
        prevActive = false;
        prevFacing = 0;
    }

    public void onCreated()
    {
        if(!Platform.isSimulating())
        {
            NetworkManager.requestInitialTileEntityData(worldObj, xCoord, yCoord, zCoord);
        }
    }

    public void readFromNBT(NBTTagCompound nbttagcompound)
    {
        super.readFromNBT(nbttagcompound);
        prevFacing = facing = nbttagcompound.getShort("facing");
    }

    public void writeToNBT(NBTTagCompound nbttagcompound)
    {
        super.writeToNBT(nbttagcompound);
        nbttagcompound.setShort("facing", facing);
    }

    public void updateEntity()
    {
        if(!created)
        {
            onCreated();
            created = true;
        }
    }

    public boolean getActive()
    {
        return active;
    }

    public void setActive(boolean flag)
    {
        active = flag;
        if(prevActive != flag)
        {
            NetworkManager.updateTileEntityField(this, "active");
        }
        prevActive = flag;
    }

    public void setActiveWithoutNotify(boolean flag)
    {
        active = flag;
        prevActive = flag;
    }

    public short getFacing()
    {
        return facing;
    }

    public List getNetworkedFields()
    {
        Vector vector = new Vector(2);
        vector.add("active");
        vector.add("facing");
        return vector;
    }

    public void onNetworkUpdate(String s)
    {
        if(s.equals("active") && prevActive != active || s.equals("facing") && prevFacing != facing)
        {
            worldObj.markBlockNeedsUpdate(xCoord, yCoord, zCoord);
            prevActive = active;
            prevFacing = facing;
        }
    }

    public boolean wrenchSetFacing(EntityPlayer entityplayer, int i)
    {
        return false;
    }

    public void setFacing(short word0)
    {
        facing = word0;
        if(prevFacing != word0)
        {
            NetworkManager.updateTileEntityField(this, "facing");
        }
        prevFacing = word0;
    }

    public boolean wrenchRemove(EntityPlayer entityplayer)
    {
        return true;
    }

    public float getWrenchDropRate()
    {
        return 1.0F;
    }

    protected boolean created;
    private boolean active;
    private short facing;
    public boolean prevActive;
    public short prevFacing;
}
