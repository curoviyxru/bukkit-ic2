// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import ic2.platform.Platform;
import java.util.logging.Level;
import net.minecraft.server.*;

// Referenced classes of package ic2.common:
//            BlockIC2Explosive, EntityIC2Explosive

public class BlockITNT extends BlockIC2Explosive
{

    public BlockITNT(int i, int j, boolean flag)
    {
        super(i, j, flag);
        isITNT = flag;
    }

    public EntityIC2Explosive getExplosionEntity(World world, float f, float f1, float f2)
    {
        if(isITNT)
        {
            return new EntityIC2Explosive(world, f, f1, f2, 60, 5.5F, 0.9F, 0.3F, mod_IC2.blockITNT);
        } else
        {
            return new EntityIC2Explosive(world, f, f1, f2, 300, mod_IC2.explosionPowerNuke, 0.05F, 1.5F, mod_IC2.blockNuke);
        }
    }

    public void onBlockPlacedBy(World world, int i, int j, int k, EntityLiving entityliving)
    {
        if(!Platform.isRendering() && !isITNT && (entityliving instanceof EntityPlayer))
        {
            Platform.log(Level.INFO, (new StringBuilder()).append("Player ").append(((EntityPlayer)entityliving).username).append(" placed a nuke at ").append(i).append("/").append(j).append("/").append(k).toString());
        }
    }

    public boolean isITNT;
}
