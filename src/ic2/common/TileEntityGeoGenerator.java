// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import forge.ISidedInventory;
import ic2.platform.Platform;
import net.minecraft.server.Container;
import net.minecraft.server.InventoryPlayer;
import net.minecraft.server.Item;
import net.minecraft.server.mod_IC2;

// Referenced classes of package ic2.common:
//            TileEntityBaseGenerator, ContainerBaseGenerator

public class TileEntityGeoGenerator extends TileEntityBaseGenerator
    implements ISidedInventory
{

    public TileEntityGeoGenerator()
    {
        super(2);
        maxLava = 24000;
        production = mod_IC2.energyGeneratorGeo;
    }

    public short getMaximumStorage()
    {
        return 100;
    }

    public int gaugeFuelScaled(int i)
    {
        if(fuel <= 0)
        {
            return 0;
        } else
        {
            return (fuel * i) / maxLava;
        }
    }

    public boolean gainFuel()
    {
        if(inventory[1] == null || maxLava - fuel < 1000)
        {
            return false;
        }
        if(inventory[1].itemID == Item.bucketLava.shiftedIndex)
        {
            fuel += 1000;
            inventory[1].itemID = Item.bucketEmpty.shiftedIndex;
            return true;
        }
        if(inventory[1].itemID == mod_IC2.itemCellLava.shiftedIndex)
        {
            fuel += 1000;
            inventory[1].stackSize--;
            if(inventory[1].stackSize <= 0)
            {
                inventory[1] = null;
            }
            return true;
        } else
        {
            return false;
        }
    }

    public boolean needsFuel()
    {
        return fuel <= maxLava;
    }

    public int distributeLava(int i)
    {
        int j = maxLava - fuel;
        if(j > i)
        {
            j = i;
        }
        i -= j;
        fuel += j / 2;
        return i;
    }

    public String getInvName()
    {
        if(Platform.isRendering())
        {
            return "Geothermal Generator";
        } else
        {
            return "Geoth. Generator";
        }
    }

    public String getOperationSoundFile()
    {
        return "Generators/GeothermalLoop.ogg";
    }

    public Container getGuiContainer(InventoryPlayer inventoryplayer)
    {
        return new ContainerBaseGenerator(inventoryplayer, this);
    }

    public int getStartInventorySide(int i)
    {
        switch(i)
        {
        case 0: // '\0'
            return 1;

        case 1: // '\001'
        default:
            return 0;
        }
    }

    public int getSizeInventorySide(int i)
    {
        return 1;
    }

    public int maxLava;
}
