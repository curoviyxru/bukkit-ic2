// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import forge.ISidedInventory;
import ic2.api.Direction;
import ic2.platform.Platform;
import java.util.Random;
import net.minecraft.server.*;

// Referenced classes of package ic2.common:
//            TileEntityElecMachine, IHasGuiContainer, ContainerInduction

public class TileEntityInduction extends TileEntityElecMachine
    implements IHasGuiContainer, ISidedInventory
{

    public TileEntityInduction()
    {
        super(5, 2, maxHeat, 128);
        heat = 0;
        progress = 0;
        soundTicker = mod_IC2.random.nextInt(64);
    }

    public String getInvName()
    {
        if(Platform.isRendering())
        {
            return "Induction Furnace";
        } else
        {
            return "InductionFurnace";
        }
    }

    public void readFromNBT(NBTTagCompound nbttagcompound)
    {
        super.readFromNBT(nbttagcompound);
        heat = nbttagcompound.getShort("heat");
        progress = nbttagcompound.getShort("progress");
    }

    public void writeToNBT(NBTTagCompound nbttagcompound)
    {
        super.writeToNBT(nbttagcompound);
        nbttagcompound.setShort("heat", heat);
        nbttagcompound.setShort("progress", progress);
    }

    public String getHeat()
    {
        return (new StringBuilder()).append("").append((heat * 100) / maxHeat).append("%").toString();
    }

    public int gaugeProgressScaled(int i)
    {
        return (i * progress) / 4000;
    }

    public int gaugeFuelScaled(int i)
    {
        return (i * energy) / maxEnergy;
    }

    public void updateEntity()
    {
        super.updateEntity();
        if(Platform.isSimulating())
        {
            boolean flag = false;
            if(energy <= maxEnergy)
            {
                flag = provideEnergy();
            }
            boolean flag1 = getActive();
            if(progress >= 4000)
            {
                operate();
                flag = true;
                progress = 0;
                flag1 = false;
            }
            boolean flag2 = canOperate();
            if(energy > 0 && (flag2 || isRedstonePowered()))
            {
                energy--;
                if(heat < maxHeat)
                {
                    heat++;
                }
                flag1 = true;
            } else
            {
                heat -= Math.min(heat, 4);
            }
            if(!flag1 || progress == 0)
            {
                if(flag2)
                {
                    if(energy >= 15)
                    {
                        flag1 = true;
                    }
                } else
                {
                    progress = 0;
                }
            } else
            if(!flag2 || energy < 15)
            {
                if(!flag2)
                {
                    progress = 0;
                }
                flag1 = false;
            }
            if(flag1 && flag2)
            {
                progress += heat / 30;
                energy -= 15;
            }
            if(flag)
            {
                onInventoryChanged();
            }
            if(flag1 != getActive())
            {
                setActive(flag1);
            }
        }
    }

    public void operate()
    {
        operate(0, 3);
        operate(1, 4);
    }

    public void operate(int i, int j)
    {
        if(!canOperate(i, j))
        {
            return;
        }
        ItemStack itemstack = getResultFor(inventory[i]);
        if(inventory[j] == null)
        {
            inventory[j] = itemstack.copy();
        } else
        {
            inventory[j].stackSize += itemstack.stackSize;
        }
        if(inventory[i].getItem().hasContainerItem())
        {
            inventory[i] = new ItemStack(inventory[i].getItem().getContainerItem());
        } else
        {
            inventory[i].stackSize--;
        }
        if(inventory[i].stackSize <= 0)
        {
            inventory[i] = null;
        }
    }

    public boolean canOperate()
    {
        return canOperate(0, 3) || canOperate(1, 4);
    }

    public boolean canOperate(int i, int j)
    {
        if(inventory[i] == null)
        {
            return false;
        }
        ItemStack itemstack = getResultFor(inventory[i]);
        if(itemstack == null)
        {
            return false;
        }
        return inventory[j] == null || inventory[j].isItemEqual(itemstack) && inventory[j].stackSize + itemstack.stackSize <= itemstack.getMaxStackSize();
    }

    public ItemStack getResultFor(ItemStack itemstack)
    {
        return FurnaceRecipes.smelting().getSmeltingResult(itemstack.id);
    }

    public int injectEnergy(Direction direction, int i)
    {
        if(i > 128)
        {
            mod_IC2.explodeMachineAt(worldObj, xCoord, yCoord, zCoord);
            return 0;
        }
        energy += i;
        int j = 0;
        if(energy > maxEnergy)
        {
            j = energy - maxEnergy;
            energy = maxEnergy;
        }
        return j;
    }

    public Container getGuiContainer(InventoryPlayer inventoryplayer)
    {
        return new ContainerInduction(inventoryplayer, this);
    }

    public int getStartInventorySide(int i)
    {
        switch(i)
        {
        case 0: // '\0'
            return 2;

        case 1: // '\001'
            return 0;
        }
        return 3;
    }

    public int getSizeInventorySide(int i)
    {
        switch(i)
        {
        case 0: // '\0'
            return 1;
        }
        return 2;
    }

    public float getWrenchDropRate()
    {
        return 0.8F;
    }

    public int soundTicker;
    public static short maxHeat = 10000;
    public short heat;
    public short progress;
    private static final int inputSlot = 0;
    private static final int fuelSlot = 2;
    private static final int outputSlot = 3;

}
