// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import net.minecraft.server.Item;
import net.minecraft.server.ItemStack;

// Referenced classes of package ic2.common:
//            ItemArmorBackpack, IChargeableItem

public class ItemArmorBatpack extends ItemArmorBackpack
    implements IChargeableItem
{

    public ItemArmorBatpack(int i, int j, int k)
    {
        super(i, j, k);
        tier = 1;
        sendTier = 1;
        ratio = 2;
        transfer = 100;
        setMaxDamage(30002);
    }

    public void useBatpackOn(ItemStack itemstack, ItemStack itemstack1)
    {
        if(!(Item.itemsList[itemstack.id] instanceof IChargeableItem))
        {
            return;
        }
        IChargeableItem ichargeableitem = (IChargeableItem)Item.itemsList[itemstack.id];
        int i = (itemstack1.getMaxDamage() - itemstack1.getItemDamage() - 1) * ratio;
        if(i <= 0)
        {
            return;
        } else
        {
            i -= ichargeableitem.giveEnergyTo(itemstack, i, sendTier, true);
            itemstack1.setItemDamage(itemstack1.getMaxDamage() - 1 - i / ratio);
            return;
        }
    }

    public int giveEnergyTo(ItemStack itemstack, int i, int j, boolean flag)
    {
        if(j < tier || itemstack.getData() == 1)
        {
            return 0;
        }
        int k = (itemstack.getData() - 1) * ratio;
        if(!flag && transfer != 0 && i > transfer)
        {
            i = transfer;
        }
        if(k < i)
        {
            i = k;
        }
        for(; i % ratio != 0; i--) { }
        itemstack.setItemDamage(itemstack.getData() - i / ratio);
        return i;
    }

    public int tier;
    public int sendTier;
    public int ratio;
    public int transfer;
}
