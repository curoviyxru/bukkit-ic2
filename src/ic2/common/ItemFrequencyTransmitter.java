// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import forge.IUseItemFirst;
import ic2.platform.Platform;
import java.nio.ByteBuffer;
import net.minecraft.server.*;

// Referenced classes of package ic2.common:
//            ItemIC2, ItemData, TileEntityTeleporter, IPersistentItemData

public class ItemFrequencyTransmitter extends ItemIC2
    implements IUseItemFirst
{
    static class ObjData
        implements IPersistentItemData
    {

        public void loadFromBuffer(short word0, short word1, ByteBuffer bytebuffer)
        {
            targetSet = bytebuffer.get() != 0;
            targetX = bytebuffer.getInt();
            targetY = bytebuffer.getInt();
            targetZ = bytebuffer.getInt();
        }

        public ByteBuffer saveToBuffer()
        {
            ByteBuffer bytebuffer = ByteBuffer.allocate(13);
            bytebuffer.put((byte)(targetSet ? 1 : 0));
            bytebuffer.putInt(targetX);
            bytebuffer.putInt(targetY);
            bytebuffer.putInt(targetZ);
            return bytebuffer;
        }

        public short getVersion()
        {
            return 0;
        }

        boolean targetSet;
        int targetX;
        int targetY;
        int targetZ;

        ObjData()
        {
            targetSet = false;
        }
    }


    public ItemFrequencyTransmitter(int i, int j)
    {
        super(i, j);
        maxStackSize = 1;
        setMaxDamage(0);
    }

    public ItemStack onItemRightClick(ItemStack itemstack, World world, EntityPlayer entityplayer)
    {
        if(Platform.isSimulating())
        {
            ObjData objdata = (ObjData)ItemData.get(itemstack, ic2.common.ItemFrequencyTransmitter.ObjData.class);
            if(objdata.targetSet)
            {
                ItemData.remove(itemstack);
                ItemData.saveToDisk(itemstack.id);
                itemstack.setItemDamage(0);
                Platform.messagePlayer(entityplayer, "Frequency Transmitter unlinked");
            }
        }
        return itemstack;
    }

    public boolean onItemUseFirst(ItemStack itemstack, EntityPlayer entityplayer, World world, int i, int j, int k, int l)
    {
        TileEntity tileentity = world.getBlockTileEntity(i, j, k);
        if(tileentity instanceof TileEntityTeleporter)
        {
            if(Platform.isSimulating())
            {
                if(itemstack.getData() == 0)
                {
                    itemstack.setItemDamage(ItemData.getUnusedDamageValue(itemstack.id, ic2.common.ItemFrequencyTransmitter.ObjData.class));
                }
                ObjData objdata = (ObjData)ItemData.get(itemstack, ic2.common.ItemFrequencyTransmitter.ObjData.class);
                TileEntityTeleporter tileentityteleporter = (TileEntityTeleporter)tileentity;
                if(objdata.targetSet)
                {
                    Chunk chunk = Platform.getOrLoadChunk(world, objdata.targetX >> 4, objdata.targetZ >> 4);
                    if(chunk == null || chunk.getBlockID(objdata.targetX & 0xf, objdata.targetY, objdata.targetZ & 0xf) != mod_IC2.blockMachine2.blockID || chunk.getBlockMetadata(objdata.targetX & 0xf, objdata.targetY, objdata.targetZ & 0xf) != 0)
                    {
                        objdata.targetSet = false;
                    }
                }
                if(!objdata.targetSet)
                {
                    objdata.targetSet = true;
                    objdata.targetX = tileentityteleporter.xCoord;
                    objdata.targetY = tileentityteleporter.yCoord;
                    objdata.targetZ = tileentityteleporter.zCoord;
                    Platform.messagePlayer(entityplayer, "Frequency Transmitter linked to Teleporter.");
                } else
                if(tileentityteleporter.xCoord == objdata.targetX && tileentityteleporter.yCoord == objdata.targetY && tileentityteleporter.zCoord == objdata.targetZ)
                {
                    Platform.messagePlayer(entityplayer, "Can't link Teleporter to itself.");
                } else
                if(tileentityteleporter.targetSet && tileentityteleporter.targetX == objdata.targetX && tileentityteleporter.targetY == objdata.targetY && tileentityteleporter.targetZ == objdata.targetZ)
                {
                    Platform.messagePlayer(entityplayer, "Teleportation link unchanged.");
                } else
                {
                    tileentityteleporter.setTarget(objdata.targetX, objdata.targetY, objdata.targetZ);
                    TileEntity tileentity1 = world.getBlockTileEntity(objdata.targetX, objdata.targetY, objdata.targetZ);
                    if(tileentity1 instanceof TileEntityTeleporter)
                    {
                        TileEntityTeleporter tileentityteleporter1 = (TileEntityTeleporter)tileentity1;
                        if(!tileentityteleporter1.targetSet)
                        {
                            tileentityteleporter1.setTarget(tileentityteleporter.xCoord, tileentityteleporter.yCoord, tileentityteleporter.zCoord);
                        }
                    }
                    Platform.messagePlayer(entityplayer, "Teleportation link established.");
                }
                ItemData.saveToDisk(itemstack.id);
            }
            return true;
        } else
        {
            return false;
        }
    }
}
