// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import ic2.api.*;
import ic2.platform.Platform;
import net.minecraft.server.*;

// Referenced classes of package ic2.common:
//            TileEntityBlock, EnergyNet

public abstract class TileEntityTransformer extends TileEntityBlock
    implements IEnergySink, IEnergySource
{

    public TileEntityTransformer(int i, int j, int k)
    {
        energy = 0;
        redstone = false;
        addedToEnergyNet = false;
        lowOutput = i;
        highOutput = j;
        maxStorage = k;
    }

    public void readFromNBT(NBTTagCompound nbttagcompound)
    {
        super.readFromNBT(nbttagcompound);
        energy = nbttagcompound.getInteger("energy");
    }

    public void writeToNBT(NBTTagCompound nbttagcompound)
    {
        super.writeToNBT(nbttagcompound);
        nbttagcompound.setInteger("energy", energy);
    }

    public void updateEntity()
    {
        super.updateEntity();
        if(Platform.isSimulating())
        {
            if(!addedToEnergyNet)
            {
                EnergyNet.getForWorld(worldObj).addTileEntity(this);
                addedToEnergyNet = true;
            }
            updateRedstone();
            if(redstone)
            {
                if(energy >= highOutput)
                {
                    int i = highOutput;
                    energy -= i;
                    i = EnergyNet.getForWorld(worldObj).emitEnergyFrom(this, i);
                    energy += i;
                }
            } else
            {
                for(int j = 0; j < 4 && energy >= lowOutput; j++)
                {
                    int k = lowOutput;
                    energy -= k;
                    k = EnergyNet.getForWorld(worldObj).emitEnergyFrom(this, k);
                    energy += k;
                }

            }
        }
    }

    public void invalidate()
    {
        if(Platform.isSimulating() && addedToEnergyNet)
        {
            EnergyNet.getForWorld(worldObj).removeTileEntity(this);
            addedToEnergyNet = false;
        }
        super.invalidate();
    }

    public void updateRedstone()
    {
        boolean flag = worldObj.isBlockIndirectlyGettingPowered(xCoord, yCoord, zCoord);
        if(flag != redstone)
        {
            EnergyNet.getForWorld(worldObj).removeTileEntity(this);
            addedToEnergyNet = false;
            redstone = flag;
            EnergyNet.getForWorld(worldObj).addTileEntity(this);
            addedToEnergyNet = true;
            setActive(redstone);
        }
    }

    public boolean isAddedToEnergyNet()
    {
        return addedToEnergyNet;
    }

    public boolean acceptsEnergyFrom(TileEntity tileentity, Direction direction)
    {
        if(redstone)
        {
            return !facingMatchesDirection(direction);
        } else
        {
            return facingMatchesDirection(direction);
        }
    }

    public boolean emitsEnergyTo(TileEntity tileentity, Direction direction)
    {
        if(redstone)
        {
            return facingMatchesDirection(direction);
        } else
        {
            return !facingMatchesDirection(direction);
        }
    }

    public boolean facingMatchesDirection(Direction direction)
    {
        return direction.toSideValue() == getFacing();
    }

    public int getMaxEnergyOutput()
    {
        if(redstone)
        {
            return highOutput;
        } else
        {
            return lowOutput;
        }
    }

    public boolean demandsEnergy()
    {
        return energy < maxStorage;
    }

    public int injectEnergy(Direction direction, int i)
    {
        if(redstone && i > lowOutput || !redstone && i > highOutput)
        {
            mod_IC2.explodeMachineAt(worldObj, xCoord, yCoord, zCoord);
            return 0;
        }
        int j = i;
        if(energy + i > maxStorage)
        {
            j = maxStorage - energy;
        }
        energy += j;
        return i - j;
    }

    public boolean wrenchSetFacing(EntityPlayer entityplayer, int i)
    {
        return getFacing() != i;
    }

    public void setFacing(short word0)
    {
        if(addedToEnergyNet)
        {
            EnergyNet.getForWorld(worldObj).removeTileEntity(this);
        }
        addedToEnergyNet = false;
        super.setFacing(word0);
        EnergyNet.getForWorld(worldObj).addTileEntity(this);
        addedToEnergyNet = true;
    }

    public int lowOutput;
    public int highOutput;
    public int maxStorage;
    public int energy;
    public boolean redstone;
    public boolean addedToEnergyNet;
}
