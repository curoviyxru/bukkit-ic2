// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import forge.ITextureProvider;
import net.minecraft.server.EnumToolMaterial;
import net.minecraft.server.ItemPickaxe;

public class ItemIC2Pickaxe extends ItemPickaxe
    implements ITextureProvider
{

    public ItemIC2Pickaxe(int i, int j, EnumToolMaterial enumtoolmaterial, float f)
    {
        super(i, enumtoolmaterial);
        efficiencyOnProperMaterial = f;
        setIconIndex(j);
    }

    public String getTextureFile()
    {
        return "/ic2/sprites/item_0.png";
    }

    public float efficiencyOnProperMaterial;
}
