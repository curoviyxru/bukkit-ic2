// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import net.minecraft.server.*;

// Referenced classes of package ic2.common:
//            TileEntityElectricMachine, ContainerElectricMachine

public class TileEntityElecFurnace extends TileEntityElectricMachine
{

    public TileEntityElecFurnace()
    {
        super(3, 3, 100, 32);
    }

    public ItemStack getResultFor(ItemStack itemstack)
    {
        return FurnaceRecipes.smelting().getSmeltingResult(itemstack.id);
    }

    public String getInvName()
    {
        return "Electric Furnace";
    }

    public String getStartSoundFile()
    {
        return "Machines/Electro Furnace/ElectroFurnaceLoop.ogg";
    }

    public String getInterruptSoundFile()
    {
        return null;
    }

    public Container getGuiContainer(InventoryPlayer inventoryplayer)
    {
        return new ContainerElectricMachine(inventoryplayer, this);
    }
}
