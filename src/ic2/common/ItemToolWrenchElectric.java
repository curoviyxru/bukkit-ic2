// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import net.minecraft.server.EntityPlayer;
import net.minecraft.server.ItemStack;

// Referenced classes of package ic2.common:
//            ItemToolWrench, IChargeableItem, ItemElectricTool

public class ItemToolWrenchElectric extends ItemToolWrench
    implements IChargeableItem
{

    public ItemToolWrenchElectric(int i, int j)
    {
        super(i, j);
        setMaxDamage(202);
    }

    public boolean canTakeDamage(ItemStack itemstack, int i)
    {
        return i < getMaxDamage() - itemstack.getData();
    }

    public int giveEnergyTo(ItemStack itemstack, int i, int j, boolean flag)
    {
        if(j < 1)
        {
            return 0;
        }
        int k = itemstack.getData() - 1;
        if(k > 5)
        {
            k = 5;
        }
        int l = i / 50;
        if(l > k)
        {
            l = k;
        }
        itemstack.setItemDamage(itemstack.getData() - l);
        return l * 50;
    }

    public void damage(ItemStack itemstack, int i, EntityPlayer entityplayer)
    {
        ItemElectricTool.use(itemstack, i, entityplayer);
    }
}
