// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import ic2.api.Direction;
import ic2.api.IEnergySource;
import ic2.platform.*;
import java.util.List;
import java.util.Random;

import ic2.platform.NetworkManager;
import net.minecraft.server.*;

// Referenced classes of package ic2.common:
//            TileEntityMachine, IHasGuiContainer, EnergyNet, ExplosionIC2, 
//            TileEntityReactorChamber, ContainerNuclearReactor, PositionSpec

public class TileEntityNuclearReactor extends TileEntityMachine
    implements IEnergySource, IHasGuiContainer
{

    public TileEntityNuclearReactor()
    {
        super(54);
        output = 0;
        heat = 0;
        addedToEnergyNet = false;
        lastOutput = 0;
        updateTicker = randomizer.nextInt(tickRate());
    }

    public void invalidate()
    {
        if(Platform.isSimulating() && addedToEnergyNet)
        {
            EnergyNet.getForWorld(worldObj).removeTileEntity(this);
            addedToEnergyNet = false;
        }
        if(Platform.isRendering())
        {
            AudioManager.removeSources(this);
            audioSourceMain = null;
            audioSourceGeiger = null;
        }
        super.invalidate();
    }

    public String getInvName()
    {
        return "Nuclear Reactor";
    }

    public void readFromNBT(NBTTagCompound nbttagcompound)
    {
        super.readFromNBT(nbttagcompound);
        heat = nbttagcompound.getShort("heat");
        output = nbttagcompound.getShort("output");
    }

    public void writeToNBT(NBTTagCompound nbttagcompound)
    {
        super.writeToNBT(nbttagcompound);
        nbttagcompound.setShort("heat", heat);
        nbttagcompound.setShort("output", output);
    }

    public void updateEntity()
    {
        super.updateEntity();
        if(Platform.isSimulating())
        {
            if(!addedToEnergyNet)
            {
                EnergyNet.getForWorld(worldObj).addTileEntity(this);
                addedToEnergyNet = true;
            }
            sendEnergy(output);
            if(updateTicker++ % tickRate() != 0)
            {
                return;
            }
            if(!worldObj.doChunksNearChunkExist(xCoord, yCoord, zCoord, 2))
            {
                output = 0;
            } else
            {
                dropAllUnfittingStuff();
                if(heat > 0)
                {
                    heat -= coolReactorFromOutside();
                    if(heat <= 0)
                    {
                        heat = 0;
                    } else
                    if(calculateHeatEffects())
                    {
                        return;
                    }
                }
                output = 0;
                processChambers();
                setActive(heat >= 1000 || output > 0);
                onInventoryChanged();
            }
            NetworkManager.updateTileEntityField(this, "output");
        }
    }

    public void dropAllUnfittingStuff()
    {
        short word0 = getReactorSize();
        for(int i = 0; i < 9; i++)
        {
            for(int j = 0; j < 6; j++)
            {
                ItemStack itemstack = getMatrixCoord(i, j);
                if(itemstack == null)
                {
                    continue;
                }
                if(itemstack.stackSize <= 0)
                {
                    setMatrixCoord(i, j, null);
                    continue;
                }
                if(i >= word0 || !isUsefulItem(itemstack))
                {
                    eject(itemstack);
                    setMatrixCoord(i, j, null);
                }
            }

        }

    }

    public void eject(ItemStack itemstack)
    {
        if(!Platform.isSimulating() || itemstack == null)
        {
            return;
        } else
        {
            float f = 0.7F;
            double d = (double)(worldObj.rand.nextFloat() * f) + (double)(1.0F - f) * 0.5D;
            double d1 = (double)(worldObj.rand.nextFloat() * f) + (double)(1.0F - f) * 0.5D;
            double d2 = (double)(worldObj.rand.nextFloat() * f) + (double)(1.0F - f) * 0.5D;
            EntityItem entityitem = new EntityItem(worldObj, (double)xCoord + d, (double)yCoord + d1, (double)zCoord + d2, itemstack);
            entityitem.delayBeforeCanPickup = 10;
            worldObj.entityJoinedWorld(entityitem);
            return;
        }
    }

    public boolean isUsefulItem(ItemStack itemstack)
    {
        if(itemstack == null)
        {
            return false;
        }
        int i = itemstack.id;
        return i == mod_IC2.itemCellUran.shiftedIndex || i == mod_IC2.itemCellCoolant.shiftedIndex || i == mod_IC2.itemReactorPlating.shiftedIndex || i == Block.ice.blockID || i == Item.bucketWater.shiftedIndex || i == Item.bucketLava.shiftedIndex || i == Item.bucketEmpty.shiftedIndex || i == mod_IC2.itemReactorCooler.shiftedIndex || i == mod_IC2.itemCellUranDepleted.shiftedIndex || i == mod_IC2.itemCellUranEnriched.shiftedIndex || i == mod_IC2.itemCellUranEmpty.shiftedIndex;
    }

    public int coolReactorFromOutside()
    {
        int i = 1;
        int j = 0;
        for(int k = xCoord - 1; k <= xCoord + 1; k++)
        {
            for(int l = yCoord - 1; l <= yCoord + 1; l++)
            {
                for(int i1 = zCoord - 1; i1 <= zCoord + 1; i1++)
                {
                    if(worldObj.getBlockId(k, l, i1) == mod_IC2.blockReactorChamber.blockID)
                    {
                        i += 2;
                    }
                    if(worldObj.getBlockMaterial(k, l, i1) == Material.water)
                    {
                        i++;
                    }
                    if(worldObj.getBlockId(k, l, i1) == 0)
                    {
                        j++;
                    }
                    if(worldObj.getBlockId(k, l, i1) == Block.fire.blockID)
                    {
                        j -= 2;
                    }
                    if(worldObj.getBlockMaterial(k, l, i1) == Material.lava)
                    {
                        i -= 3;
                    }
                }

            }

        }

        i += j / 4;
        if(i < 0)
        {
            return 0;
        } else
        {
            return i;
        }
    }

    public boolean calculateHeatEffects()
    {
        if(heat < 4000 || !Platform.isSimulating())
        {
            return false;
        }
        short word0 = getReactorSize();
        int i = 10000;
        i += 1000 * (word0 - 3);
        for(int j = 0; j < 6; j++)
        {
            for(int k = 0; k < word0; k++)
            {
                if(getMatrixCoord(k, j) != null && getMatrixCoord(k, j).itemID == mod_IC2.itemReactorPlating.shiftedIndex)
                {
                    i += 100;
                }
            }

        }

        float f = (float)heat / (float)i;
        if(f >= 1.0F)
        {
            float f1 = 10F;
            for(int l = 0; l < 6; l++)
            {
                for(int i2 = 0; i2 < word0; i2++)
                {
                    if(getMatrixCoord(i2, l) != null && getMatrixCoord(i2, l).itemID == mod_IC2.itemCellUran.shiftedIndex)
                    {
                        f1 += 3F;
                        continue;
                    }
                    if(getMatrixCoord(i2, l) != null && getMatrixCoord(i2, l).itemID == mod_IC2.itemReactorPlating.shiftedIndex)
                    {
                        f1--;
                    }
                }

            }

            if(f1 > mod_IC2.explosionPowerReactorMax)
            {
                f1 = mod_IC2.explosionPowerReactorMax;
            }
            worldObj.setBlockWithNotify(xCoord, yCoord, zCoord, 0);
            ExplosionIC2 explosionic2 = new ExplosionIC2(worldObj, null, xCoord, yCoord, zCoord, f1, 0.01F, 1.5F);
            explosionic2.doExplosion();
            return true;
        }
        if(f >= 0.85F && worldObj.rand.nextFloat() <= 4F * (f - 0.7F))
        {
            int ai[] = getRandCoord(2);
            if(ai != null)
            {
                int i1 = worldObj.getBlockId(ai[0], ai[1], ai[2]);
                if(i1 == 0)
                {
                    worldObj.setBlockWithNotify(ai[0], ai[1], ai[2], Block.fire.blockID);
                } else
                {
                    Material material = Block.blocksList[i1].blockMaterial;
                    if(i1 != Block.bedrock.blockID)
                    {
                        if(material == Material.rock || material == Material.iron || material == Material.lava || material == Material.ground || material == Material.clay)
                        {
                            worldObj.setBlockAndMetadataWithNotify(ai[0], ai[1], ai[2], Block.lavaMoving.blockID, 15);
                        } else
                        {
                            worldObj.setBlockWithNotify(ai[0], ai[1], ai[2], Block.fire.blockID);
                        }
                    }
                }
            }
        }
        if(f >= 0.7F)
        {
            List list = worldObj.getEntitiesWithinAABB(net.minecraft.server.EntityLiving.class, AxisAlignedBB.getBoundingBox(xCoord - 3, yCoord - 3, zCoord - 3, xCoord + 4, yCoord + 4, zCoord + 4));
            for(int j1 = 0; j1 < list.size(); j1++)
            {
                Entity entity = (Entity)list.get(j1);
                entity.attackEntityFrom(null, 1);
            }

        }
        if(f >= 0.5F)
        {
            int ai1[] = getRandCoord(2);
            if(ai1 != null)
            {
                int k1 = worldObj.getBlockId(ai1[0], ai1[1], ai1[2]);
                if(k1 > 0 && Block.blocksList[k1].blockMaterial == Material.water)
                {
                    worldObj.setBlockWithNotify(ai1[0], ai1[1], ai1[2], 0);
                }
            }
        }
        if(f >= 0.4F && worldObj.rand.nextFloat() <= 1.5F * (f - 0.4F))
        {
            int ai2[] = getRandCoord(2);
            if(ai2 != null)
            {
                int l1 = worldObj.getBlockId(ai2[0], ai2[1], ai2[2]);
                if(l1 > 0)
                {
                    Material material1 = Block.blocksList[l1].blockMaterial;
                    if(material1 == Material.wood || material1 == Material.leaves || material1 == Material.cloth)
                    {
                        worldObj.setBlockWithNotify(ai2[0], ai2[1], ai2[2], Block.fire.blockID);
                    }
                }
            }
        }
        return false;
    }

    public int[] getRandCoord(int i)
    {
        if(i <= 0)
        {
            return null;
        }
        int ai[] = new int[3];
        ai[0] = (xCoord + worldObj.rand.nextInt(2 * i + 1)) - i;
        ai[1] = (yCoord + worldObj.rand.nextInt(2 * i + 1)) - i;
        ai[2] = (zCoord + worldObj.rand.nextInt(2 * i + 1)) - i;
        if(ai[0] == xCoord && ai[1] == yCoord && ai[2] == zCoord)
        {
            return null;
        } else
        {
            return ai;
        }
    }

    public void processChambers()
    {
        short word0 = getReactorSize();
        for(int i = 0; i < 6; i++)
        {
            for(int j = 0; j < word0; j++)
            {
                processChamber(j, i);
            }

        }

    }

    public void processChamber(int i, int j)
    {
        if(getMatrixCoord(i, j) == null)
        {
            return;
        }
        int k = getMatrixCoord(i, j).itemID;
        if(k == mod_IC2.itemCellCoolant.shiftedIndex && getMatrixCoord(i, j).getItemDamage() > 0)
        {
            getMatrixCoord(i, j).setItemDamage(getMatrixCoord(i, j).getItemDamage() - 1);
        }
        if(k == mod_IC2.itemReactorPlating.shiftedIndex && getMatrixCoord(i, j).getItemDamage() > 0 && worldObj.rand.nextInt(10) == 0)
        {
            getMatrixCoord(i, j).setItemDamage(getMatrixCoord(i, j).getItemDamage() - 1);
        }
        if(k == mod_IC2.itemCellUranEmpty.shiftedIndex || k == mod_IC2.itemCellUranDepleted.shiftedIndex || k == mod_IC2.itemCellUranEnriched.shiftedIndex)
        {
            heat++;
        }
        if(k == Item.bucketWater.shiftedIndex && heat > 4000)
        {
            heat -= 250;
            getMatrixCoord(i, j).itemID = Item.bucketEmpty.shiftedIndex;
        }
        if(k == Item.bucketLava.shiftedIndex)
        {
            heat += 2000;
            getMatrixCoord(i, j).itemID = Item.bucketEmpty.shiftedIndex;
        }
        if(k == Block.ice.blockID && heat > 300)
        {
            heat -= 300;
            getMatrixCoord(i, j).stackSize--;
            if(getMatrixCoord(i, j).stackSize <= 0)
            {
                setMatrixCoord(i, j, null);
            }
        }
        if(k == mod_IC2.itemReactorCooler.shiftedIndex)
        {
            disperseHeat(i, j);
        }
        if(k == mod_IC2.itemCellUran.shiftedIndex && produceEnergy())
        {
            generateEnergy(i, j);
        }
    }

    public void disperseHeat(int i, int j)
    {
        switchHeat(i, j, i - 1, j);
        switchHeat(i, j, i + 1, j);
        switchHeat(i, j, i, j - 1);
        switchHeat(i, j, i, j + 1);
        int k = ((getMatrixCoord(i, j).getItemDamage() - heat) + 1) / 2;
        if(k > 0)
        {
            if(k > 25)
            {
                k = 25;
            }
            heat += k;
            getMatrixCoord(i, j).setItemDamage(getMatrixCoord(i, j).getItemDamage() - k);
        } else
        {
            k *= -1;
            if(k > 25)
            {
                k = 25;
            }
            heat -= k;
            getMatrixCoord(i, j).damageItem(k, null);
        }
    }

    public void switchHeat(int i, int j, int k, int l)
    {
        if(getMatrixCoord(k, l) == null)
        {
            return;
        }
        int i1 = getMatrixCoord(k, l).itemID;
        if(i1 != mod_IC2.itemCellCoolant.shiftedIndex && i1 != mod_IC2.itemReactorPlating.shiftedIndex)
        {
            return;
        }
        int j1 = getMatrixCoord(i, j).getItemDamage();
        int k1 = getMatrixCoord(k, l).getItemDamage();
        int l1 = (j1 - k1) / 2;
        if(l1 > 0)
        {
            if(l1 > 6)
            {
                l1 = 6;
            }
            getMatrixCoord(i, j).setItemDamage(j1 - l1);
            if(i1 == mod_IC2.itemCellCoolant.shiftedIndex)
            {
                getMatrixCoord(k, l).damageItem(l1, null);
            } else
            {
                spreadHeat(k, l, l1, false);
            }
        } else
        {
            l1 *= -1;
            if(l1 > 6)
            {
                l1 = 6;
            }
            getMatrixCoord(i, j).damageItem(l1, null);
            getMatrixCoord(k, l).setItemDamage(k1 - l1);
        }
    }

    public void generateEnergy(int i, int j)
    {
        int k = 1 + isUranium(i + 1, j) + isUranium(i - 1, j) + isUranium(i, j + 1) + isUranium(i, j - 1);
        output += k * pulsePower();
        for(k += enrichDepleted(i + 1, j) + enrichDepleted(i - 1, j) + enrichDepleted(i, j + 1) + enrichDepleted(i, j - 1); k > 0; k--)
        {
            int l = canTakeHeat(i + 1, j, true, true) + canTakeHeat(i - 1, j, true, true) + canTakeHeat(i, j + 1, true, true) + canTakeHeat(i, j - 1, true, true);
            byte byte0;
            switch(l)
            {
            case 2: // '\002'
                byte0 = 4;
                break;

            case 3: // '\003'
                byte0 = 2;
                break;

            case 4: // '\004'
                byte0 = 1;
                break;

            default:
                byte0 = 10;
                break;
            }
            if(l == 0)
            {
                heat += byte0;
            } else
            {
                giveHeatTo(i + 1, j, byte0);
                giveHeatTo(i - 1, j, byte0);
                giveHeatTo(i, j + 1, byte0);
                giveHeatTo(i, j - 1, byte0);
            }
        }

        if(getMatrixCoord(i, j).getItemDamage() == 9999 && worldObj.rand.nextInt(3) == 0)
        {
            setMatrixCoord(i, j, new ItemStack(mod_IC2.itemCellUranEmpty));
        } else
        {
            getMatrixCoord(i, j).damageItem(1, null);
        }
    }

    public int isUranium(int i, int j)
    {
        return getMatrixCoord(i, j) == null || getMatrixCoord(i, j).itemID != mod_IC2.itemCellUran.shiftedIndex ? 0 : 1;
    }

    public int enrichDepleted(int i, int j)
    {
        if(getMatrixCoord(i, j) == null || getMatrixCoord(i, j).itemID != mod_IC2.itemCellUranDepleted.shiftedIndex)
        {
            return 0;
        }
        byte byte0 = 8;
        if(heat >= 3000)
        {
            byte0 = 4;
        }
        if(heat >= 6000)
        {
            byte0 = 2;
        }
        if(heat >= 9000)
        {
            byte0 = 1;
        }
        if(worldObj.rand.nextInt(byte0) != 0)
        {
            return 1;
        }
        if(getMatrixCoord(i, j).getItemDamage() <= 0)
        {
            setMatrixCoord(i, j, new ItemStack(mod_IC2.itemCellUranEnriched));
        } else
        {
            getMatrixCoord(i, j).setItemDamage(getMatrixCoord(i, j).getItemDamage() - 2);
        }
        return 1;
    }

    public int canTakeHeat(int i, int j, boolean flag, boolean flag1)
    {
        if(getMatrixCoord(i, j) == null)
        {
            return 0;
        }
        int k = getMatrixCoord(i, j).itemID;
        return k != mod_IC2.itemCellCoolant.shiftedIndex && (k != mod_IC2.itemReactorPlating.shiftedIndex || !flag) && (k != mod_IC2.itemReactorCooler.shiftedIndex || !flag1) ? 0 : 1;
    }

    public void giveHeatTo(int i, int j, int k)
    {
        if(canTakeHeat(i, j, true, true) == 0)
        {
            return;
        }
        if(getMatrixCoord(i, j).itemID == mod_IC2.itemReactorPlating.shiftedIndex)
        {
            spreadHeat(i, j, k, true);
        } else
        {
            getMatrixCoord(i, j).damageItem(k, null);
        }
    }

    public void spreadHeat(int i, int j, int k, boolean flag)
    {
        int l = canTakeHeat(i + 1, j, flag, false) + canTakeHeat(i - 1, j, flag, false) + canTakeHeat(i, j + 1, flag, false) + canTakeHeat(i, j - 1, flag, false);
        if(l == 0)
        {
            getMatrixCoord(i, j).damageItem(k, null);
            return;
        }
        for(; k % l != 0 && getMatrixCoord(i, j).getItemDamage() > 0; getMatrixCoord(i, j).setItemDamage(getMatrixCoord(i, j).getItemDamage() - 1))
        {
            k++;
        }

        int i1 = k / l;
        k -= i1 * l;
        if(k > 0)
        {
            getMatrixCoord(i, j).damageItem(k, null);
        }
        spreadHeatTo(i - 1, j, i1, flag);
        spreadHeatTo(i + 1, j, i1, flag);
        spreadHeatTo(i, j - 1, i1, flag);
        spreadHeatTo(i, j + 1, i1, flag);
    }

    public void spreadHeatTo(int i, int j, int k, boolean flag)
    {
        if(canTakeHeat(i, j, flag, false) == 0)
        {
            return;
        }
        if(getMatrixCoord(i, j).itemID == mod_IC2.itemReactorPlating.shiftedIndex && flag)
        {
            spreadHeat(i, j, k, false);
        } else
        {
            getMatrixCoord(i, j).damageItem(k, null);
        }
    }

    public boolean produceEnergy()
    {
        return !worldObj.isBlockIndirectlyGettingPowered(xCoord, yCoord, zCoord);
    }

    public ItemStack getMatrixCoord(int i, int j)
    {
        if(i < 0 || i >= 9 || j < 0 || j >= 6)
        {
            return null;
        } else
        {
            return super.getStackInSlot(i + j * 9);
        }
    }

    public ItemStack getStackInSlot(int i)
    {
        int j = i % 9;
        short word0 = getReactorSize();
        if(j >= word0)
        {
            return getMatrixCoord(word0 - 1, i / 9);
        } else
        {
            return super.getStackInSlot(i);
        }
    }

    public void setMatrixCoord(int i, int j, ItemStack itemstack)
    {
        if(i < 0 || i >= 9 || j < 0 || j >= 6)
        {
            return;
        } else
        {
            super.setInventorySlotContents(i + j * 9, itemstack);
            return;
        }
    }

    public void setInventorySlotContents(int i, ItemStack itemstack)
    {
        int j = i % 9;
        short word0 = getReactorSize();
        if(j >= word0)
        {
            setMatrixCoord(word0 - 1, i / 9, itemstack);
        } else
        {
            super.setInventorySlotContents(i, itemstack);
        }
    }

    public short getReactorSize()
    {
        if(worldObj == null)
        {
            return 9;
        }
        short word0 = 3;
        Direction adirection[] = Direction.values();
        int i = adirection.length;
        for(int j = 0; j < i; j++)
        {
            Direction direction = adirection[j];
            TileEntity tileentity = direction.applyToTileEntity(this);
            if(tileentity instanceof TileEntityReactorChamber)
            {
                word0++;
            }
        }

        return word0;
    }

    public int tickRate()
    {
        return 20;
    }

    public static int pulsePower()
    {
        return mod_IC2.energyGeneratorNuclear;
    }

    public boolean isAddedToEnergyNet()
    {
        return addedToEnergyNet;
    }

    public boolean emitsEnergyTo(TileEntity tileentity, Direction direction)
    {
        return true;
    }

    public int getMaxEnergyOutput()
    {
        return 240 * pulsePower();
    }

    public int sendEnergy(int i)
    {
        i = EnergyNet.getForWorld(worldObj).emitEnergyFrom(this, i);
        if(i > 0 && (worldObj.getBlockTileEntity(xCoord, yCoord + 1, zCoord) instanceof TileEntityReactorChamber))
        {
            i = ((TileEntityReactorChamber)worldObj.getBlockTileEntity(xCoord, yCoord + 1, zCoord)).sendEnergy(i);
        }
        if(i > 0 && (worldObj.getBlockTileEntity(xCoord, yCoord - 1, zCoord) instanceof TileEntityReactorChamber))
        {
            i = ((TileEntityReactorChamber)worldObj.getBlockTileEntity(xCoord, yCoord - 1, zCoord)).sendEnergy(i);
        }
        if(i > 0 && (worldObj.getBlockTileEntity(xCoord + 1, yCoord, zCoord) instanceof TileEntityReactorChamber))
        {
            i = ((TileEntityReactorChamber)worldObj.getBlockTileEntity(xCoord + 1, yCoord, zCoord)).sendEnergy(i);
        }
        if(i > 0 && (worldObj.getBlockTileEntity(xCoord - 1, yCoord, zCoord) instanceof TileEntityReactorChamber))
        {
            i = ((TileEntityReactorChamber)worldObj.getBlockTileEntity(xCoord - 1, yCoord, zCoord)).sendEnergy(i);
        }
        if(i > 0 && (worldObj.getBlockTileEntity(xCoord, yCoord, zCoord + 1) instanceof TileEntityReactorChamber))
        {
            i = ((TileEntityReactorChamber)worldObj.getBlockTileEntity(xCoord, yCoord, zCoord + 1)).sendEnergy(i);
        }
        if(i > 0 && (worldObj.getBlockTileEntity(xCoord, yCoord, zCoord - 1) instanceof TileEntityReactorChamber))
        {
            i = ((TileEntityReactorChamber)worldObj.getBlockTileEntity(xCoord, yCoord, zCoord - 1)).sendEnergy(i);
        }
        return i;
    }

    public Container getGuiContainer(InventoryPlayer inventoryplayer)
    {
        return new ContainerNuclearReactor(inventoryplayer, this, getReactorSize());
    }

    public void onNetworkUpdate(String s)
    {
        if(s.equals("output"))
        {
            if(output > 0)
            {
                if(lastOutput <= 0)
                {
                    if(audioSourceMain == null)
                    {
                        audioSourceMain = AudioManager.createSource(this, PositionSpec.Center, "Generators/NuclearReactor/NuclearReactorLoop.ogg", true, false, AudioManager.defaultVolume);
                    }
                    if(audioSourceMain != null)
                    {
                        audioSourceMain.play();
                    }
                }
                if(output < 40)
                {
                    if(lastOutput <= 0 || lastOutput >= 40)
                    {
                        if(audioSourceGeiger != null)
                        {
                            audioSourceGeiger.remove();
                        }
                        audioSourceGeiger = AudioManager.createSource(this, PositionSpec.Center, "Generators/NuclearReactor/GeigerLowEU.ogg", true, false, AudioManager.defaultVolume);
                        if(audioSourceGeiger != null)
                        {
                            audioSourceGeiger.play();
                        }
                    }
                } else
                if(output < 80)
                {
                    if(lastOutput < 40 || lastOutput >= 80)
                    {
                        if(audioSourceGeiger != null)
                        {
                            audioSourceGeiger.remove();
                        }
                        audioSourceGeiger = AudioManager.createSource(this, PositionSpec.Center, "Generators/NuclearReactor/GeigerMedEU.ogg", true, false, AudioManager.defaultVolume);
                        if(audioSourceGeiger != null)
                        {
                            audioSourceGeiger.play();
                        }
                    }
                } else
                if(output >= 80 && lastOutput < 80)
                {
                    if(audioSourceGeiger != null)
                    {
                        audioSourceGeiger.remove();
                    }
                    audioSourceGeiger = AudioManager.createSource(this, PositionSpec.Center, "Generators/NuclearReactor/GeigerHighEU.ogg", true, false, AudioManager.defaultVolume);
                    if(audioSourceGeiger != null)
                    {
                        audioSourceGeiger.play();
                    }
                }
            } else
            if(lastOutput > 0)
            {
                if(audioSourceMain != null)
                {
                    audioSourceMain.stop();
                }
                if(audioSourceGeiger != null)
                {
                    audioSourceGeiger.stop();
                }
            }
            lastOutput = output;
        }
        super.onNetworkUpdate(s);
    }

    public float getWrenchDropRate()
    {
        return 0.8F;
    }

    public static Random randomizer = new Random();
    public short output;
    public int updateTicker;
    public short heat;
    public boolean addedToEnergyNet;
    public AudioSource audioSourceMain;
    public AudioSource audioSourceGeiger;
    private short lastOutput;

}
