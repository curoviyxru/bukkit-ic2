// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import ic2.platform.Platform;
import java.util.Random;
import net.minecraft.server.*;

// Referenced classes of package ic2.common:
//            BlockMultiID, TileEntityElectricBatBox, TileEntityElectricMFE, TileEntityElectricMFSU, 
//            TileEntityTransformerLV, TileEntityTransformerMV, TileEntityTransformerHV, TileEntityBlock

public class BlockElectric extends BlockMultiID
{

    public BlockElectric(int i)
    {
        super(i, Material.iron);
        setHardness(1.5F);
        setStepSound(soundMetalFootstep);
    }

    public String getTextureFile()
    {
        return "/ic2/sprites/block_electric.png";
    }

    public int idDropped(int i, Random random)
    {
        switch(i)
        {
        case 0: // '\0'
            return blockID;

        case 3: // '\003'
            return blockID;
        }
        return mod_IC2.blockMachine.blockID;
    }

    protected int damageDropped(int i)
    {
        switch(i)
        {
        case 0: // '\0'
            return i;

        case 3: // '\003'
            return i;
        }
        return 0;
    }

    public int quantityDropped(Random random)
    {
        return 1;
    }

    public boolean canProvidePower()
    {
        return true;
    }

    public Integer getGui(World world, int i, int j, int k, EntityPlayer entityplayer)
    {
        switch(world.getBlockMetadata(i, j, k))
        {
        case 0: // '\0'
            return Integer.valueOf(mod_IC2.guiIdElectricBatBox);

        case 1: // '\001'
            return Integer.valueOf(mod_IC2.guiIdElectricMFE);

        case 2: // '\002'
            return Integer.valueOf(mod_IC2.guiIdElectricMFSU);
        }
        return null;
    }

    public TileEntityBlock getBlockEntity(int i)
    {
        switch(i)
        {
        case 0: // '\0'
            return new TileEntityElectricBatBox();

        case 1: // '\001'
            return new TileEntityElectricMFE();

        case 2: // '\002'
            return new TileEntityElectricMFSU();

        case 3: // '\003'
            return new TileEntityTransformerLV();

        case 4: // '\004'
            return new TileEntityTransformerMV();

        case 5: // '\005'
            return new TileEntityTransformerHV();
        }
        return null;
    }

    public void onBlockPlacedBy(World world, int i, int j, int k, EntityLiving entityliving)
    {
        if(!Platform.isSimulating())
        {
            return;
        }
        TileEntityBlock tileentityblock = (TileEntityBlock)world.getBlockTileEntity(i, j, k);
        if(entityliving == null)
        {
            tileentityblock.setFacing((short)1);
        } else
        {
            int l = MathHelper.floor_double((double)((entityliving.rotationYaw * 4F) / 360F) + 0.5D) & 3;
            int i1 = Math.round(entityliving.rotationPitch);
            if(i1 >= 65)
            {
                tileentityblock.setFacing((short)1);
            } else
            if(i1 <= -65)
            {
                tileentityblock.setFacing((short)0);
            } else
            {
                switch(l)
                {
                case 0: // '\0'
                    tileentityblock.setFacing((short)2);
                    break;

                case 1: // '\001'
                    tileentityblock.setFacing((short)5);
                    break;

                case 2: // '\002'
                    tileentityblock.setFacing((short)3);
                    break;

                case 3: // '\003'
                    tileentityblock.setFacing((short)4);
                    break;
                }
            }
        }
    }
}
