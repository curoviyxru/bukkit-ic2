// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import forge.ISpecialResistance;
import ic2.platform.Platform;
import java.util.*;
import net.minecraft.server.*;

public class PointExplosion
{

    public PointExplosion(World world, Entity entity, int i, int j, int k, float f, float f1, 
            float f2)
    {
        ExplosionRNG = new Random();
        destroyedBlockPositions = new HashSet();
        worldObj = world;
        exploder = entity;
        explosionSize = f;
        explosionDropRate = f1;
        explosionDamage = f2;
        explosionX = i;
        explosionY = j;
        explosionZ = k;
        if(explosionX < 0)
        {
            explosionX--;
        }
        if(explosionZ < 0)
        {
            explosionZ--;
        }
    }

    public void doExplosionA(int i, int j, int k, int l, int i1, int j1)
    {
        for(int k1 = explosionX - i; k1 <= explosionX + l; k1++)
        {
            for(int i2 = explosionY - j; i2 <= explosionY + i1; i2++)
            {
                for(int k2 = explosionZ - k; k2 <= explosionZ + j1; k2++)
                {
                    int i3 = worldObj.getTypeId(k1, i2, k2);
                    float f = 0.0F;
                    if(i3 > 0)
                    {
                        if(Block.byId[i3] instanceof ISpecialResistance)
                        {
                            ISpecialResistance ispecialresistance = (ISpecialResistance)Block.byId[i3];
                            f = ispecialresistance.getSpecialExplosionResistance(worldObj, k1, i2, k2, explosionX, explosionY, explosionZ, exploder);
                        } else
                        {
                            f = Block.byId[i3].getExplosionResistance(exploder);
                        }
                    }
                    if(explosionSize >= f / 10F)
                    {
                        destroyedBlockPositions.add(new ChunkPosition(k1, i2, k2));
                    }
                }

            }

        }

        explosionSize *= 2.0F;
        int l1 = MathHelper.floor((double)explosionX - (double)explosionSize - 1.0D);
        int j2 = MathHelper.floor((double)explosionX + (double)explosionSize + 1.0D);
        int l2 = MathHelper.floor((double)explosionY - (double)explosionSize - 1.0D);
        int j3 = MathHelper.floor((double)explosionY + (double)explosionSize + 1.0D);
        int k3 = MathHelper.floor((double)explosionZ - (double)explosionSize - 1.0D);
        int l3 = MathHelper.floor((double)explosionZ + (double)explosionSize + 1.0D);
        List list = worldObj.getEntitiesWithinAABBExcludingEntity(exploder, AxisAlignedBB.getBoundingBoxFromPool(l1, l2, k3, j2, j3, l3));
        Vec3D vec3d = Vec3D.createVector(explosionX, explosionY, explosionZ);
        for(int i4 = 0; i4 < list.size(); i4++)
        {
            Entity entity = (Entity)list.get(i4);
            double d = entity.getDistance(explosionX, explosionY, explosionZ) / (double)explosionSize;
            if(d <= 1.0D)
            {
                double d1 = entity.locX - (double)explosionX;
                double d2 = entity.locY - (double)explosionY;
                double d3 = entity.locZ - (double)explosionZ;
                double d4 = MathHelper.a(d1 * d1 + d2 * d2 + d3 * d3);
                d1 /= d4;
                d2 /= d4;
                d3 /= d4;
                double d5 = Platform.worldUntranslatedFunction2(worldObj, vec3d, entity.boundingBox);
                double d6 = (1.0D - d) * d5;
                entity.damageEntity(exploder, (int)((((d6 * d6 + d6) / 2D) * 8D * (double)explosionSize + 1.0D) * (double)explosionDamage));
                double d7 = d6;
                entity.motX += d1 * d7;
                entity.motY += d2 * d7;
                entity.motZ += d3 * d7;
            }
        }

    }

    public void doExplosionB(boolean flag)
    {
        worldObj.playSoundEffect(explosionX, explosionY, explosionZ, "random.explode", 4F, (1.0F + (worldObj.random.nextFloat() - worldObj.random.nextFloat()) * 0.2F) * 0.7F);
        ArrayList arraylist = new ArrayList();
        arraylist.addAll(destroyedBlockPositions);
        for(int i = arraylist.size() - 1; i >= 0; i--)
        {
            ChunkPosition chunkposition = (ChunkPosition)arraylist.get(i);
            int j = chunkposition.x;
            int k = chunkposition.y;
            int l = chunkposition.z;
            int i1 = worldObj.getTypeId(j, k, l);
            if(flag)
            {
                double d = (float)j + worldObj.random.nextFloat();
                double d1 = (float)k + worldObj.random.nextFloat();
                double d2 = (float)l + worldObj.random.nextFloat();
                double d3 = d - (double)explosionX;
                double d4 = d1 - (double)explosionY;
                double d5 = d2 - (double)explosionZ;
                double d6 = MathHelper.sqrt_double(d3 * d3 + d4 * d4 + d5 * d5);
                d3 /= d6;
                d4 /= d6;
                d5 /= d6;
                double d7 = 0.5D / (d6 / (double)explosionSize + 0.10000000000000001D);
                d7 *= worldObj.random.nextFloat() * worldObj.random.nextFloat() + 0.3F;
                d3 *= d7;
                d4 *= d7;
                d5 *= d7;
                worldObj.a("explode", (d + (double)explosionX * 1.0D) / 2D, (d1 + (double)explosionY * 1.0D) / 2D, (d2 + (double)explosionZ * 1.0D) / 2D, d3, d4, d5);
                worldObj.a("smoke", d, d1, d2, d3, d4, d5);
            }
            if(i1 > 0)
            {
                Block.byId[i1].dropBlockAsItemWithChance(worldObj, j, k, l, worldObj.getBlockMetadata(j, k, l), explosionDropRate);
                worldObj.setTypeId(j, k, l, 0);
                Block.byId[i1].onBlockDestroyedByExplosion(worldObj, j, k, l);
            }
        }

    }

    private Random ExplosionRNG;
    private World worldObj;
    public int explosionX;
    public int explosionY;
    public int explosionZ;
    public Entity exploder;
    public float explosionSize;
    public float explosionDropRate;
    public float explosionDamage;
    public Set destroyedBlockPositions;
}
