// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import java.util.*;
import net.minecraft.server.*;

// Referenced classes of package ic2.common:
//            ItemIC2

public class ItemScrapbox extends ItemIC2
{
    class Drop
    {

        ItemStack itemStack;
        float upperChanceBound;

        Drop(ItemStack itemstack, float f)
        {
            itemStack = itemstack;
            if(dropList.isEmpty())
            {
                upperChanceBound = f;
            } else
            {
                upperChanceBound = ((Drop)dropList.get(dropList.size() - 1)).upperChanceBound + f;
            }
        }
    }


    public ItemScrapbox(int i, int j)
    {
        super(i, j);
        dropList = new Vector();
        if(mod_IC2.suddenlyHoes)
        {
            addDrop(Item.hoeWood, 9001F);
        } else
        {
            addDrop(Item.hoeWood, 5.01F);
        }
        addDrop(Block.dirt, 5F);
        addDrop(Item.stick, 4F);
        addDrop(Block.grass, 3F);
        addDrop(Block.gravel, 3F);
        addDrop(Block.netherrack, 2.0F);
        addDrop(Item.appleRed, 1.5F);
        addDrop(Item.bread, 1.5F);
        addDrop(mod_IC2.itemTinCanFilled, 1.5F);
        addDrop(Item.swordWood);
        addDrop(Item.shovelWood);
        addDrop(Item.pickaxeWood);
        addDrop(Block.slowSand);
        addDrop(Item.sign);
        addDrop(Item.leather);
        addDrop(Item.feather);
        addDrop(Item.porkCooked, 0.9F);
        addDrop(Block.pumpkin, 0.9F);
        addDrop(Item.minecartEmpty, 0.9F);
        addDrop(Item.redstone, 0.9F);
        addDrop(mod_IC2.itemRubber, 0.8F);
        addDrop(Item.GLOWSTONE_DUST, 0.8F);
        addDrop(mod_IC2.itemDustCoal, 0.8F);
        addDrop(mod_IC2.itemDustCopper, 0.8F);
        addDrop(mod_IC2.itemDustTin, 0.8F);
        addDrop(mod_IC2.blockOreCopper, 0.7F);
        addDrop(mod_IC2.blockOreTin, 0.7F);
        addDrop(mod_IC2.itemFuelPlantBall, 0.7F);
        addDrop(mod_IC2.itemBatSU, 0.7F);
        addDrop(mod_IC2.itemDustIron, 0.7F);
        addDrop(mod_IC2.itemDustGold, 0.7F);
        addDrop(Item.slimeBall, 0.6F);
        addDrop(Block.oreIron, 0.5F);
        addDrop(Item.helmetGold, 0.5F);
        addDrop(Block.oreGold, 0.5F);
        addDrop(Item.cake, 0.5F);
        addDrop(Item.diamond, 0.1F);
    }

    public ItemStack onItemRightClick(ItemStack itemstack, World world, EntityPlayer entityplayer)
    {
        itemstack.stackSize--;
        ItemStack itemstack1 = getDrop(world);
        if(itemstack1 != null)
        {
            entityplayer.dropPlayerItem(itemstack1);
        }
        return itemstack;
    }

    public void addDrop(Item item)
    {
        addDrop(new ItemStack(item), 1.0F);
    }

    public void addDrop(Item item, float f)
    {
        addDrop(new ItemStack(item), f);
    }

    public void addDrop(Block block)
    {
        addDrop(new ItemStack(block), 1.0F);
    }

    public void addDrop(Block block, float f)
    {
        addDrop(new ItemStack(block), f);
    }

    public void addDrop(ItemStack itemstack)
    {
        addDrop(itemstack, 1.0F);
    }

    public void addDrop(ItemStack itemstack, float f)
    {
        dropList.add(new Drop(itemstack, f));
    }

    public ItemStack getDrop(World world)
    {
        label0:
        {
            if(dropList.isEmpty())
            {
                break label0;
            }
            float f = world.rand.nextFloat() * ((Drop)dropList.get(dropList.size() - 1)).upperChanceBound;
            Iterator iterator = dropList.iterator();
            Drop drop;
            do
            {
                if(!iterator.hasNext())
                {
                    break label0;
                }
                drop = (Drop)iterator.next();
            } while(drop.upperChanceBound < f);
            return drop.itemStack.copy();
        }
        return null;
    }

    public List dropList;
}
