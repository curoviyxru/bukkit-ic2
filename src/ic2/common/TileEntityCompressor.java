// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import java.util.HashMap;
import net.minecraft.server.*;

// Referenced classes of package ic2.common:
//            TileEntityElectricMachine, RecipeInput, TileEntityPump, ContainerElectricMachine

public class TileEntityCompressor extends TileEntityElectricMachine
{

    public TileEntityCompressor()
    {
        super(3, 2, 300, 32);
    }

    public ItemStack getResultFor(ItemStack itemstack)
    {
        if(itemstack == null)
        {
            return null;
        } else
        {
            return (ItemStack)recipes.get(new RecipeInput(itemstack.id, itemstack.getData()));
        }
    }

    public static void initRecipes()
    {
        addRecipe(new RecipeInput(mod_IC2.itemFuelPlantBall.shiftedIndex, 0), new ItemStack(mod_IC2.itemFuelPlantCmpr));
        addRecipe(new RecipeInput(mod_IC2.itemFuelCoalDust.shiftedIndex, 0), new ItemStack(mod_IC2.itemFuelCoalCmpr));
        addRecipe(new RecipeInput(Block.sand.blockID, 0), new ItemStack(Block.sandStone));
        addRecipe(new RecipeInput(Item.snowball.shiftedIndex, 0), new ItemStack(Block.ice));
        addRecipe(new RecipeInput(mod_IC2.itemCellWater.shiftedIndex, 0), new ItemStack(Item.snowball));
        addRecipe(new RecipeInput(mod_IC2.itemIngotAlloy.shiftedIndex, 0), new ItemStack(mod_IC2.itemPartAlloy));
        addRecipe(new RecipeInput(mod_IC2.itemPartCarbonMesh.shiftedIndex, 0), new ItemStack(mod_IC2.itemPartCarbonPlate));
        addRecipe(new RecipeInput(mod_IC2.itemPartCoalBall.shiftedIndex, 0), new ItemStack(mod_IC2.itemPartCoalBlock));
        addRecipe(new RecipeInput(mod_IC2.itemPartCoalChunk.shiftedIndex, 0), new ItemStack(mod_IC2.itemPartIndustrialDiamond));
        addRecipe(new RecipeInput(mod_IC2.itemOreUran.shiftedIndex, 0), new ItemStack(mod_IC2.itemIngotUran));
        addRecipe(new RecipeInput(mod_IC2.blockFoam.blockID, 0), new ItemStack(mod_IC2.itemPartPellet));
    }

    public boolean canOperate()
    {
        if(getValidPump() != null)
        {
            return inventory[2] == null || inventory[2].isItemEqual(new ItemStack(Item.snowball)) && inventory[2].stackSize < Item.snowball.getItemStackLimit();
        } else
        {
            return super.canOperate();
        }
    }

    public void operate()
    {
        if(!canOperate())
        {
            return;
        }
        ItemStack itemstack = getResultFor(inventory[0]);
        if(itemstack == null)
        {
            TileEntityPump tileentitypump = getValidPump();
            if(tileentitypump == null)
            {
                return;
            }
            tileentitypump.pumpCharge = 0;
            worldObj.setBlockWithNotify(tileentitypump.xCoord, tileentitypump.yCoord - 1, tileentitypump.zCoord, 0);
            itemstack = new ItemStack(Item.snowball);
        } else
        {
            if(inventory[0].getItem().hasContainerItem())
            {
                inventory[0] = new ItemStack(inventory[0].getItem().getContainerItem());
            } else
            {
                inventory[0].stackSize--;
            }
            if(inventory[0].stackSize <= 0)
            {
                inventory[0] = null;
            }
        }
        if(inventory[2] == null)
        {
            inventory[2] = itemstack.copy();
        } else
        {
            inventory[2].stackSize += itemstack.stackSize;
        }
    }

    public TileEntityPump getValidPump()
    {
        if(validPump != null && validPump.isPumpReady() && validPump.isWaterBelow())
        {
            return validPump;
        }
        if(worldObj.getBlockTileEntity(xCoord, yCoord - 1, zCoord) instanceof TileEntityPump)
        {
            TileEntityPump tileentitypump = (TileEntityPump)worldObj.getBlockTileEntity(xCoord, yCoord - 1, zCoord);
            if(tileentitypump.isPumpReady() && tileentitypump.isWaterBelow())
            {
                return validPump = tileentitypump;
            }
        }
        if(worldObj.getBlockTileEntity(xCoord + 1, yCoord, zCoord) instanceof TileEntityPump)
        {
            TileEntityPump tileentitypump1 = (TileEntityPump)worldObj.getBlockTileEntity(xCoord + 1, yCoord, zCoord);
            if(tileentitypump1.isPumpReady() && tileentitypump1.isWaterBelow())
            {
                return validPump = tileentitypump1;
            }
        }
        if(worldObj.getBlockTileEntity(xCoord - 1, yCoord, zCoord) instanceof TileEntityPump)
        {
            TileEntityPump tileentitypump2 = (TileEntityPump)worldObj.getBlockTileEntity(xCoord - 1, yCoord, zCoord);
            if(tileentitypump2.isPumpReady() && tileentitypump2.isWaterBelow())
            {
                return validPump = tileentitypump2;
            }
        }
        if(worldObj.getBlockTileEntity(xCoord, yCoord, zCoord + 1) instanceof TileEntityPump)
        {
            TileEntityPump tileentitypump3 = (TileEntityPump)worldObj.getBlockTileEntity(xCoord, yCoord, zCoord + 1);
            if(tileentitypump3.isPumpReady() && tileentitypump3.isWaterBelow())
            {
                return validPump = tileentitypump3;
            }
        }
        if(worldObj.getBlockTileEntity(xCoord, yCoord, zCoord - 1) instanceof TileEntityPump)
        {
            TileEntityPump tileentitypump4 = (TileEntityPump)worldObj.getBlockTileEntity(xCoord, yCoord, zCoord - 1);
            if(tileentitypump4.isPumpReady() && tileentitypump4.isWaterBelow())
            {
                return validPump = tileentitypump4;
            }
        }
        return null;
    }

    public static void addRecipe(RecipeInput recipeinput, ItemStack itemstack)
    {
        recipes.put(recipeinput, itemstack);
    }

    public String getInvName()
    {
        return "Compressor";
    }

    public String getStartSoundFile()
    {
        return "Machines/CompressorOp.ogg";
    }

    public String getInterruptSoundFile()
    {
        return "Machines/InterruptOne.ogg";
    }

    public Container getGuiContainer(InventoryPlayer inventoryplayer)
    {
        return new ContainerElectricMachine(inventoryplayer, this);
    }

    public float getWrenchDropRate()
    {
        return 0.85F;
    }

    public TileEntityPump validPump;
    public static HashMap recipes = new HashMap();

}
