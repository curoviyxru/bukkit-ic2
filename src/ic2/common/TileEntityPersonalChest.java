// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.common;

import forge.ISidedInventory;
import ic2.platform.NetworkManager;
import ic2.platform.Platform;
import java.util.List;
import java.util.Vector;
import net.minecraft.server.EntityPlayer;
import net.minecraft.server.NBTTagCompound;

// Referenced classes of package ic2.common:
//            TileEntityMachine, IPersonalBlock

public class TileEntityPersonalChest extends TileEntityMachine
    implements IPersonalBlock, ISidedInventory
{

    public TileEntityPersonalChest()
    {
        super(54);
        owner = "null";
    }

    public void readFromNBT(NBTTagCompound nbttagcompound)
    {
        super.readFromNBT(nbttagcompound);
        owner = nbttagcompound.getString("owner");
    }

    public void writeToNBT(NBTTagCompound nbttagcompound)
    {
        super.writeToNBT(nbttagcompound);
        nbttagcompound.setString("owner", owner);
    }

    public String getInvName()
    {
        return "Personal Safe";
    }

    public void updateEntity()
    {
        super.updateEntity();
    }

    public List getNetworkedFields()
    {
        Vector vector = new Vector(1);
        vector.add("owner");
        vector.addAll(super.getNetworkedFields());
        return vector;
    }

    public boolean wrenchRemove(EntityPlayer entityplayer)
    {
        if(!canAccess(entityplayer))
        {
            return false;
        }
        for(int i = 0; i < inventory.length; i++)
        {
            if(inventory[i] != null)
            {
                Platform.messagePlayer(entityplayer, "Can't wrench non-empty safe");
                return false;
            }
        }

        return true;
    }

    public boolean canAccess(EntityPlayer entityplayer)
    {
        if(owner.equals("null"))
        {
            owner = entityplayer.username;
            NetworkManager.updateTileEntityField(this, "owner");
            return true;
        }
        if(Platform.isSimulating() && Platform.isPlayerOp(entityplayer))
        {
            return true;
        }
        if(owner.equalsIgnoreCase(entityplayer.username))
        {
            return true;
        }
        if(Platform.isSimulating())
        {
            Platform.messagePlayer(entityplayer, (new StringBuilder()).append("This safe is owned by ").append(owner).toString());
        }
        return false;
    }

    public int getStartInventorySide(int i)
    {
        return 0;
    }

    public int getSizeInventorySide(int i)
    {
        if(Platform.isSimulating() && Platform.isRendering())
        {
            return inventory.length;
        } else
        {
            return 0;
        }
    }

    public String owner;
}
