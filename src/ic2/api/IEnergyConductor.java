// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package ic2.api;


// Referenced classes of package ic2.api:
//            IEnergyAcceptor, IEnergyEmitter

public interface IEnergyConductor
    extends IEnergyAcceptor, IEnergyEmitter
{

    public abstract double getConductionLoss();

    public abstract int getInsulationEnergyAbsorption();

    public abstract int getInsulationBreakdownEnergy();

    public abstract int getConductorBreakdownEnergy();

    public abstract void removeInsulation();

    public abstract void removeConductor();
}
