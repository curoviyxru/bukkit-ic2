// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package net.minecraft.server;

import forge.*;
import ic2.common.*;
import ic2.platform.*;
import java.io.File;
import java.util.*;

// Referenced classes of package net.minecraft.server:
//            Block, Item, ModLoader, ItemStack, 
//            BlockFire, World, WorldChunkManager, BiomeGenBase, 
//            WorldGenMinable, EntityPlayer, InventoryPlayer, Entity, 
//            EntityLiving, EntityItem, Material, EnumToolMaterial

public class mod_IC2 extends Ic2
{

    //TODO: убрать или переделать вещи крашащие клиент и которые работают с эффектами, улучшить джетпак, пофиксить бег нанокостюма и его отключение, кастомное отравление, унифицировать код хавки

    public static ArrayList getDrop(World world, Block bl, int l) {
        //System.out.println(bl == null);
        ArrayList arraylist = new ArrayList();
        int i1 = bl.a(world.random);
        for(int j1 = 0; j1 < i1; j1++)
        {
            int k1 = bl.a(l, world.random);
            if(k1 > 0)
            {
                arraylist.add(new ItemStack(k1, 1, bl.a_(l)));
            }
        }

        return arraylist;
    }

    static class OreHandler
        implements IOreHandler
    {

        public void registerOre(String s, ItemStack itemstack)
        {
            if(s.equals("dyeBlue"))
            {
                mod_IC2.registerBlueDyeCraftingRecipes(itemstack);
            } else
            if(s.equals("ingotBronze"))
            {
                mod_IC2.registerBronzeIngotCraftingRecipes(itemstack);
            } else
            if(s.equals("ingotCopper"))
            {
                mod_IC2.registerCopperIngotCraftingRecipes(itemstack);
                TileEntityMacerator.addRecipe(new RecipeInput(itemstack.id, itemstack.getData()), new ItemStack(mod_IC2.itemDustCopper, 1));
            } else
            if(s.equals("ingotRefinedIron"))
            {
                mod_IC2.registerRefinedIronCraftingRecipes(itemstack);
                TileEntityMacerator.addRecipe(new RecipeInput(itemstack.id, itemstack.getData()), new ItemStack(mod_IC2.itemDustIron, 1));
            } else
            if(s.equals("ingotSilver"))
            {
                mod_IC2.registerSilverIngotCraftingRecipes(itemstack);
                TileEntityMacerator.addRecipe(new RecipeInput(itemstack.id, itemstack.getData()), new ItemStack(mod_IC2.itemDustSilver, 1));
                if(!mod_IC2.silverDustSmeltingRegistered)
                {
                    ModLoader.AddSmelting(mod_IC2.itemDustSilver.id, new ItemStack(itemstack.id, 1, itemstack.getData()));
                    mod_IC2.silverDustSmeltingRegistered = true;
                }
            } else
            if(s.equals("ingotTin"))
            {
                mod_IC2.registerTinIngotCraftingRecipes(itemstack);
                TileEntityMacerator.addRecipe(new RecipeInput(itemstack.id, itemstack.getData()), new ItemStack(mod_IC2.itemDustTin, 1));
            } else
            if(s.equals("ingotUranium"))
            {
                mod_IC2.registerUraniumIngotCraftingRecipes(itemstack);
            } else
            if(s.equals("itemOreUran"))
            {
                TileEntityCompressor.addRecipe(new RecipeInput(itemstack.id, itemstack.getData()), new ItemStack(mod_IC2.itemIngotUran));
            } else
            if(s.equals("oreCopper"))
            {
                TileEntityMacerator.addRecipe(new RecipeInput(itemstack.id, itemstack.getData()), new ItemStack(mod_IC2.itemDustCopper, 2));
            } else
            if(s.equals("oreSilver"))
            {
                TileEntityMacerator.addRecipe(new RecipeInput(itemstack.id, itemstack.getData()), new ItemStack(mod_IC2.itemDustSilver, 2));
            } else
            if(s.equals("oreTin"))
            {
                TileEntityMacerator.addRecipe(new RecipeInput(itemstack.id, itemstack.getData()), new ItemStack(mod_IC2.itemDustTin, 2));
            } else
            if(s.equals("woodRubber"))
            {
                TileEntityExtractor.addRecipe(new RecipeInput(itemstack.id, itemstack.getData()), new ItemStack(mod_IC2.itemRubber, 1));
            }
        }
    }

    public static void initialize()
    {
        MinecraftForge.versionDetect("IndustrialCraft 2", 1, 1, 2);
        AudioManager.initialize();
        try
        {
            config = new Configuration(new File(Platform.getMinecraftDir(), "/config/IC2.cfg"));
            config.load();
        }
        catch(Exception exception)
        {
            System.out.println((new StringBuilder()).append("[IndustrialCraft] Error while trying to access configuration!").toString());
            config = null;
        }
        if(config != null)
        {
            enableCraftingBucket = Boolean.parseBoolean(config.getOrCreateBooleanProperty("enableCraftingBucket", 0, enableCraftingBucket).value);
            enableCraftingCoin = Boolean.parseBoolean(config.getOrCreateBooleanProperty("enableCraftingCoin", 0, enableCraftingCoin).value);
            enableCraftingGlowstoneDust = Boolean.parseBoolean(config.getOrCreateBooleanProperty("enableCraftingGlowstoneDust", 0, enableCraftingGlowstoneDust).value);
            enableCraftingGunpowder = Boolean.parseBoolean(config.getOrCreateBooleanProperty("enableCraftingGunpowder", 0, enableCraftingGunpowder).value);
            enableCraftingNuke = Boolean.parseBoolean(config.getOrCreateBooleanProperty("enableCraftingNuke", 0, enableCraftingNuke).value);
            enableCraftingRail = Boolean.parseBoolean(config.getOrCreateBooleanProperty("enableCraftingRail", 0, enableCraftingRail).value);
            enableQuantumSpeedOnSprint = Boolean.parseBoolean(config.getOrCreateBooleanProperty("enableQuantumSpeedOnSprint", 0, enableQuantumSpeedOnSprint).value);
            if(!Platform.isRendering())
            {
                enableLoggingWrench = Boolean.parseBoolean(config.getOrCreateBooleanProperty("enableLoggingWrench", 0, enableLoggingWrench).value);
            }
            explosionPowerNuke = Float.parseFloat(config.getOrCreateProperty("explosionPowerNuke", 0, Float.toString(explosionPowerNuke)).value);
            explosionPowerReactorMax = Float.parseFloat(config.getOrCreateProperty("explosionPowerReactorMax", 0, Float.toString(explosionPowerReactorMax)).value);
            energyGeneratorBase = Integer.parseInt(config.getOrCreateIntProperty("energyGeneratorBase", 0, energyGeneratorBase).value);
            setValuableOreFromString(config.getOrCreateProperty("valuableOres", 0, getValuableOreString()).value);
        }
        addValuableOre(Block.COAL_ORE.id, 1);
        addValuableOre(blockOreTin.id, 2);
        addValuableOre(blockOreCopper.id, 2);
        addValuableOre(Block.GOLD_ORE.id, 3);
        addValuableOre(Block.REDSTONE_ORE.id, 3);
        addValuableOre(Block.LAPIS_ORE.id, 3);
        addValuableOre(Block.IRON_ORE.id, 4);
        addValuableOre(blockOreUran.id, 4);
        addValuableOre(Block.DIAMOND_ORE.id, 5);
        Block.OBSIDIAN.b(60F);
        Block.WATER.b(30F);
        Block.STATIONARY_WATER.b(30F);
        Block.STATIONARY_LAVA.b(30F);
        ((BlockIC2Door)blockDoorAlloy).setItemDropped(itemDoorAlloy.id);
        ModLoader.RegisterBlock(blockMachine, ic2.common.ItemMachine.class);
        ModLoader.RegisterBlock(blockOreCopper);
        ModLoader.RegisterBlock(blockOreTin);
        ModLoader.RegisterBlock(blockOreUran);
        ModLoader.RegisterBlock(blockGenerator, ic2.common.ItemGenerator.class);
        ModLoader.RegisterBlock(blockMiningPipe);
        ModLoader.RegisterBlock(blockMiningTip);
        ModLoader.RegisterBlock(blockRubWood);
        ModLoader.RegisterBlock(blockRubLeaves);
        ModLoader.RegisterBlock(blockRubSapling);
        ModLoader.RegisterBlock(blockITNT);
        ModLoader.RegisterBlock(blockNuke);
        ModLoader.RegisterBlock(blockDynamite);
        ModLoader.RegisterBlock(blockDynamiteRemote);
        ModLoader.RegisterBlock(blockRubber);
        ModLoader.RegisterBlock(blockReactorChamber);
        ModLoader.RegisterBlock(blockFenceIron);
        ModLoader.RegisterBlock(blockAlloy);
        ModLoader.RegisterBlock(blockAlloyGlass);
        ModLoader.RegisterBlock(blockDoorAlloy);
        ModLoader.RegisterBlock(blockCable);
        ModLoader.RegisterBlock(blockElectric, ic2.common.ItemElectricBlock.class);
        ModLoader.RegisterBlock(blockLuminator);
        ModLoader.RegisterBlock(blockPersonal, ic2.common.ItemPersonalBlock.class);
        ModLoader.RegisterBlock(blockMetal, ic2.common.ItemBlockMetal.class);
        ModLoader.RegisterBlock(blockMachine2, ic2.common.ItemMachine2.class);
        ModLoader.RegisterBlock(blockFoam);
        ModLoader.RegisterBlock(blockWall);
        ModLoader.RegisterBlock(blockScaffold);
        ModLoader.RegisterBlock(blockLuminatorDark);
        ModLoader.RegisterTileEntity(ic2.common.TileEntityBlock.class, "Empty Management TileEntity");
        ModLoader.RegisterTileEntity(ic2.common.TileEntityIronFurnace.class, "Iron Furnace");
        ModLoader.RegisterTileEntity(ic2.common.TileEntityElecFurnace.class, "Electric Furnace");
        ModLoader.RegisterTileEntity(ic2.common.TileEntityMacerator.class, "Macerator");
        ModLoader.RegisterTileEntity(ic2.common.TileEntityExtractor.class, "Extractor");
        ModLoader.RegisterTileEntity(ic2.common.TileEntityCompressor.class, "Compressor");
        ModLoader.RegisterTileEntity(ic2.common.TileEntityGenerator.class, "Generator");
        ModLoader.RegisterTileEntity(ic2.common.TileEntityGeoGenerator.class, "Geothermal Generator");
        ModLoader.RegisterTileEntity(ic2.common.TileEntityWaterGenerator.class, "Water Mill");
        ModLoader.RegisterTileEntity(ic2.common.TileEntitySolarGenerator.class, "Solar Panel");
        ModLoader.RegisterTileEntity(ic2.common.TileEntityWindGenerator.class, "Wind Mill");
        ModLoader.RegisterTileEntity(ic2.common.TileEntityCanner.class, "Canning Machine");
        ModLoader.RegisterTileEntity(ic2.common.TileEntityMiner.class, "Miner");
        ModLoader.RegisterTileEntity(ic2.common.TileEntityPump.class, "Pump");
        ModLoader.RegisterTileEntity(ic2.common.TileEntityNuclearReactor.class, "Nuclear Reactor");
        ModLoader.RegisterTileEntity(ic2.common.TileEntityReactorChamber.class, "Reactor Chamber");
        ModLoader.RegisterTileEntity(ic2.common.TileEntityMagnetizer.class, "Magnetizer");
        ModLoader.RegisterTileEntity(ic2.common.TileEntityCable.class, "Cable");
        ModLoader.RegisterTileEntity(ic2.common.TileEntityElectricBatBox.class, "BatBox");
        ModLoader.RegisterTileEntity(ic2.common.TileEntityElectricMFE.class, "MFE");
        ModLoader.RegisterTileEntity(ic2.common.TileEntityElectricMFSU.class, "MFSU");
        ModLoader.RegisterTileEntity(ic2.common.TileEntityTransformerLV.class, "LV-Transformer");
        ModLoader.RegisterTileEntity(ic2.common.TileEntityTransformerMV.class, "MV-Transformer");
        ModLoader.RegisterTileEntity(ic2.common.TileEntityTransformerHV.class, "HV-Transformer");
        ModLoader.RegisterTileEntity(ic2.common.TileEntityLuminator.class, "Luminator");
        ModLoader.RegisterTileEntity(ic2.common.TileEntityElectrolyzer.class, "Electrolyzer");
        ModLoader.RegisterTileEntity(ic2.common.TileEntityPersonalChest.class, "Personal Safe");
        ModLoader.RegisterTileEntity(ic2.common.TileEntityTradeOMat.class, "Trade-O-Mat");
        ModLoader.RegisterTileEntity(ic2.common.TileEntityRecycler.class, "Recycler");
        ModLoader.RegisterTileEntity(ic2.common.TileEntityInduction.class, "Induction Furnace");
        ModLoader.RegisterTileEntity(ic2.common.TileEntityMatter.class, "Mass Fabricator");
        ModLoader.RegisterTileEntity(ic2.common.TileEntityTerra.class, "Terraformer");
        ModLoader.RegisterTileEntity(ic2.common.TileEntityTeleporter.class, "Teleporter");
        ModLoader.RegisterTileEntity(ic2.common.TileEntityTesla.class, "Tesla Coil");
        ModLoader.RegisterTileEntity(ic2.common.TileEntityCableDetector.class, "Detector Cable");
        ModLoader.RegisterTileEntity(ic2.common.TileEntityCableSplitter.class, "SplitterCable");
        ModLoader.AddSmelting(Item.IRON_INGOT.id, new ItemStack(itemIngotAdvIron, 1));
        ModLoader.AddSmelting(blockOreTin.id, new ItemStack(itemIngotTin, 1));
        ModLoader.AddSmelting(blockOreCopper.id, new ItemStack(itemIngotCopper, 1));
        ModLoader.AddSmelting(itemDustIron.id, new ItemStack(Item.IRON_INGOT, 1));
        ModLoader.AddSmelting(itemDustGold.id, new ItemStack(Item.GOLD_INGOT, 1));
        ModLoader.AddSmelting(itemDustTin.id, new ItemStack(itemIngotTin, 1));
        ModLoader.AddSmelting(itemDustCopper.id, new ItemStack(itemIngotCopper, 1));
        ModLoader.AddSmelting(itemFuelCoalDust.id, new ItemStack(itemDustCoal, 1));
        ModLoader.AddSmelting(itemDustBronze.id, new ItemStack(itemIngotBronze, 1));
        ModLoader.AddSmelting(blockRubWood.id, new ItemStack(Block.LOG, 1));
        ModLoader.AddSmelting(itemHarz.id, new ItemStack(itemRubber, 1));
        TileEntityMacerator.initRecipes();
        TileEntityExtractor.initRecipes();
        TileEntityCompressor.initRecipes();
        ItemTFBPCultivation.loadPlants();
        ItemTFBPFlatification.loadRemoveables();
        MinecraftForge.setToolClass(itemToolBronzePickaxe, "pickaxe", 2);
        MinecraftForge.setToolClass(itemToolBronzeAxe, "axe", 2);
        MinecraftForge.setToolClass(itemToolBronzeSpade, "shovel", 2);
        MinecraftForge.setToolClass(itemToolChainsaw, "axe", 2);
        MinecraftForge.setToolClass(itemToolDrill, "pickaxe", 2);
        MinecraftForge.setToolClass(itemToolDDrill, "pickaxe", 3);
        MinecraftForge.setBlockHarvestLevel(blockOreCopper, "pickaxe", 1);
        MinecraftForge.setBlockHarvestLevel(blockOreTin, "pickaxe", 1);
        MinecraftForge.setBlockHarvestLevel(blockOreUran, "pickaxe", 2);
        MinecraftForge.setBlockHarvestLevel(blockAlloy, "pickaxe", 2);
        MinecraftForge.setBlockHarvestLevel(blockDoorAlloy, "pickaxe", 2);
        MinecraftForge.setBlockHarvestLevel(blockRubWood, "axe", 0);
        MinecraftForge.setBlockHarvestLevel(blockCable, "axe", 0);
        MinecraftForge.setBlockHarvestLevel(blockWall, "pickaxe", 1);
        windStrength = 10 + random.nextInt(10);
        globalTicker = 0;
        addBurnAble(blockRubLeaves.id, 30);
        addBurnAble(blockRubWood.id, 4);
        addBurnAble(blockScaffold.id, 8);
        registerGenericCraftingRecipes();
        MinecraftForge.registerOreHandler(oreHandler);
        MinecraftForge.registerOre("ingotBronze", new ItemStack(itemIngotBronze, 1));
        MinecraftForge.registerOre("ingotCopper", new ItemStack(itemIngotCopper, 1));
        MinecraftForge.registerOre("ingotRefinedIron", new ItemStack(itemIngotAdvIron, 1));
        MinecraftForge.registerOre("ingotTin", new ItemStack(itemIngotTin, 1));
        MinecraftForge.registerOre("ingotUranium", new ItemStack(itemIngotUran, 1));
        MinecraftForge.registerOre("itemDropUranium", new ItemStack(itemOreUran, 1));
        MinecraftForge.registerOre("oreCopper", new ItemStack(blockOreCopper, 1));
        MinecraftForge.registerOre("oreTin", new ItemStack(blockOreTin, 1));
        MinecraftForge.registerOre("oreUranium", new ItemStack(blockOreUran, 1));
        MinecraftForge.registerOre("woodRubber", new ItemStack(blockRubWood, 1));
        if(config != null)
        {
            config.save();
        }
    }

    public static void addBurnAble(int i, int j)
    {
        try
        {
            ((int[])ModLoader.getPrivateValue(net.minecraft.server.BlockFire.class, Block.FIRE, 0))[i] = j;
        }
        catch(SecurityException securityexception)
        {
            System.out.println("Security exception thrown.");
            securityexception.printStackTrace();
        }
        catch(IllegalArgumentException illegalargumentexception)
        {
            System.out.println("Illegal argument thrown.");
            illegalargumentexception.printStackTrace();
        }
        catch(NoSuchFieldException nosuchfieldexception)
        {
            System.out.println("No such field thrown.");
            nosuchfieldexception.printStackTrace();
        }
    }

    public static int AddFuelCommon(int i, int j)
    {
        if(i == blockRubSapling.id)
        {
            return 80;
        }
        if(i == Item.SUGAR_CANE.id)
        {
            return 50;
        }
        if(i == Block.CACTUS.id)
        {
            return 50;
        }
        return i != itemScrap.id ? 0 : 350;
    }

    private static void registerGenericCraftingRecipes()
    {
        ModLoader.AddRecipe(new ItemStack(blockMachine, 1, 1), new Object[] {
            "III", "I I", "III", Character.valueOf('I'), Item.IRON_INGOT
        });
        ModLoader.AddRecipe(new ItemStack(blockMachine, 1, 1), new Object[] {
            " I ", "I I", "IFI", Character.valueOf('I'), Item.IRON_INGOT, Character.valueOf('F'), Block.FURNACE
        });
        ModLoader.AddRecipe(new ItemStack(Block.WOOD, 3), new Object[] {
            "W", Character.valueOf('W'), blockRubWood
        });
        ModLoader.AddRecipe(new ItemStack(itemTreetap, 1), new Object[] {
            " P ", "PPP", "P  ", Character.valueOf('P'), Block.WOOD
        });
        ModLoader.AddShapelessRecipe(new ItemStack(itemCable, 1, 0), new Object[] {
            itemRubber, new ItemStack(itemCable, 1, 1)
        });
        ModLoader.AddRecipe(new ItemStack(itemCable, 12, 2), new Object[] {
            "GGG", Character.valueOf('G'), Item.GOLD_INGOT
        });
        ModLoader.AddRecipe(new ItemStack(itemCable, 4, 3), new Object[] {
            " R ", "RGR", " R ", Character.valueOf('G'), Item.GOLD_INGOT, Character.valueOf('R'), itemRubber
        });
        ModLoader.AddShapelessRecipe(new ItemStack(itemCable, 1, 3), new Object[] {
            itemRubber, new ItemStack(itemCable, 1, 2)
        });
        ModLoader.AddShapelessRecipe(new ItemStack(itemCable, 1, 4), new Object[] {
            itemRubber, new ItemStack(itemCable, 1, 3)
        });
        ModLoader.AddShapelessRecipe(new ItemStack(itemCable, 1, 4), new Object[] {
            itemRubber, itemRubber, new ItemStack(itemCable, 1, 2)
        });
        ModLoader.AddShapelessRecipe(new ItemStack(itemCable, 1, 6), new Object[] {
            itemRubber, new ItemStack(itemCable, 1, 5)
        });
        ModLoader.AddShapelessRecipe(new ItemStack(itemCable, 1, 7), new Object[] {
            itemRubber, new ItemStack(itemCable, 1, 6)
        });
        ModLoader.AddShapelessRecipe(new ItemStack(itemCable, 1, 8), new Object[] {
            itemRubber, new ItemStack(itemCable, 1, 7)
        });
        ModLoader.AddShapelessRecipe(new ItemStack(itemCable, 1, 8), new Object[] {
            itemRubber, itemRubber, new ItemStack(itemCable, 1, 6)
        });
        ModLoader.AddShapelessRecipe(new ItemStack(itemCable, 1, 7), new Object[] {
            itemRubber, itemRubber, new ItemStack(itemCable, 1, 5)
        });
        ModLoader.AddShapelessRecipe(new ItemStack(itemCable, 1, 8), new Object[] {
            itemRubber, itemRubber, itemRubber, new ItemStack(itemCable, 1, 5)
        });
        ModLoader.AddRecipe(new ItemStack(itemCable, 4, 9), new Object[] {
            "GGG", "RDR", "GGG", Character.valueOf('G'), Block.GLASS, Character.valueOf('R'), Item.REDSTONE, Character.valueOf('D'), Item.DIAMOND
        });
        ModLoader.AddRecipe(new ItemStack(itemCable, 4, 9), new Object[] {
            "GGG", "RDR", "GGG", Character.valueOf('G'), Block.GLASS, Character.valueOf('R'), Item.REDSTONE, Character.valueOf('D'), itemPartIndustrialDiamond
        });
        ModLoader.AddRecipe(new ItemStack(itemCable, 1, 11), new Object[] {
            " C ", "RIR", " R ", Character.valueOf('R'), Item.REDSTONE, Character.valueOf('I'), new ItemStack(itemCable, 1, 8), Character.valueOf('C'), itemPartCircuit
        });
        ModLoader.AddRecipe(new ItemStack(itemCable, 1, 12), new Object[] {
            " R ", "ILI", " R ", Character.valueOf('R'), Item.REDSTONE, Character.valueOf('I'), new ItemStack(itemCable, 1, 8), Character.valueOf('L'), Block.LEVER
        });
        ModLoader.AddRecipe(new ItemStack(itemToolPainter, 1), new Object[] {
            " CC", " IC", "I  ", Character.valueOf('C'), Block.WOOL, Character.valueOf('I'), Item.IRON_INGOT
        });
        ModLoader.AddShapelessRecipe(new ItemStack(itemToolPainterBlack, 1), new Object[] {
            itemToolPainter, new ItemStack(Item.INK_SACK, 1, 0)
        });
        ModLoader.AddShapelessRecipe(new ItemStack(itemToolPainterRed, 1), new Object[] {
            itemToolPainter, new ItemStack(Item.INK_SACK, 1, 1)
        });
        ModLoader.AddShapelessRecipe(new ItemStack(itemToolPainterGreen, 1), new Object[] {
            itemToolPainter, new ItemStack(Item.INK_SACK, 1, 2)
        });
        ModLoader.AddShapelessRecipe(new ItemStack(itemToolPainterBrown, 1), new Object[] {
            itemToolPainter, new ItemStack(Item.INK_SACK, 1, 3)
        });
        ModLoader.AddShapelessRecipe(new ItemStack(itemToolPainterBlue, 1), new Object[] {
            itemToolPainter, new ItemStack(Item.INK_SACK, 1, 4)
        });
        ModLoader.AddShapelessRecipe(new ItemStack(itemToolPainterPurple, 1), new Object[] {
            itemToolPainter, new ItemStack(Item.INK_SACK, 1, 5)
        });
        ModLoader.AddShapelessRecipe(new ItemStack(itemToolPainterCyan, 1), new Object[] {
            itemToolPainter, new ItemStack(Item.INK_SACK, 1, 6)
        });
        ModLoader.AddShapelessRecipe(new ItemStack(itemToolPainterLightGrey, 1), new Object[] {
            itemToolPainter, new ItemStack(Item.INK_SACK, 1, 7)
        });
        ModLoader.AddShapelessRecipe(new ItemStack(itemToolPainterDarkGrey, 1), new Object[] {
            itemToolPainter, new ItemStack(Item.INK_SACK, 1, 8)
        });
        ModLoader.AddShapelessRecipe(new ItemStack(itemToolPainterPink, 1), new Object[] {
            itemToolPainter, new ItemStack(Item.INK_SACK, 1, 9)
        });
        ModLoader.AddShapelessRecipe(new ItemStack(itemToolPainterLime, 1), new Object[] {
            itemToolPainter, new ItemStack(Item.INK_SACK, 1, 10)
        });
        ModLoader.AddShapelessRecipe(new ItemStack(itemToolPainterYellow, 1), new Object[] {
            itemToolPainter, new ItemStack(Item.INK_SACK, 1, 11)
        });
        ModLoader.AddShapelessRecipe(new ItemStack(itemToolPainterCloud, 1), new Object[] {
            itemToolPainter, new ItemStack(Item.INK_SACK, 1, 12)
        });
        ModLoader.AddShapelessRecipe(new ItemStack(itemToolPainterMagenta, 1), new Object[] {
            itemToolPainter, new ItemStack(Item.INK_SACK, 1, 13)
        });
        ModLoader.AddShapelessRecipe(new ItemStack(itemToolPainterOrange, 1), new Object[] {
            itemToolPainter, new ItemStack(Item.INK_SACK, 1, 14)
        });
        ModLoader.AddShapelessRecipe(new ItemStack(itemToolPainterWhite, 1), new Object[] {
            itemToolPainter, new ItemStack(Item.INK_SACK, 1, 15)
        });
        ModLoader.AddRecipe(new ItemStack(itemPartCircuitAdv, 1), new Object[] {
            "RGR", "LCL", "RGR", Character.valueOf('L'), new ItemStack(Item.INK_SACK, 1, 4), Character.valueOf('G'), Item.GLOWSTONE_DUST, Character.valueOf('R'), Item.REDSTONE, Character.valueOf('C'), 
            itemPartCircuit
        });
        ModLoader.AddRecipe(new ItemStack(itemPartCircuitAdv, 1), new Object[] {
            "RLR", "GCG", "RLR", Character.valueOf('L'), new ItemStack(Item.INK_SACK, 1, 4), Character.valueOf('G'), Item.GLOWSTONE_DUST, Character.valueOf('R'), Item.REDSTONE, Character.valueOf('C'), 
            itemPartCircuit
        });
        ModLoader.AddRecipe(new ItemStack(itemIngotAdvIron, 8), new Object[] {
            "M", Character.valueOf('M'), new ItemStack(blockMachine, 1, 0)
        });
        ModLoader.AddRecipe(new ItemStack(blockPersonal, 1, 0), new Object[] {
            "c", "M", "C", Character.valueOf('c'), itemPartCircuit, Character.valueOf('C'), Block.CHEST, Character.valueOf('M'), new ItemStack(blockMachine, 1, 0)
        });
        ModLoader.AddRecipe(new ItemStack(blockPersonal, 1, 1), new Object[] {
            "RRR", "CMC", Character.valueOf('R'), Item.REDSTONE, Character.valueOf('C'), Block.CHEST, Character.valueOf('M'), new ItemStack(blockMachine, 1, 0)
        });
        ModLoader.AddRecipe(new ItemStack(blockMachine, 1, 2), new Object[] {
            " C ", "RFR", Character.valueOf('C'), itemPartCircuit, Character.valueOf('R'), Item.REDSTONE, Character.valueOf('F'), new ItemStack(blockMachine, 1, 1)
        });
        ModLoader.AddRecipe(new ItemStack(blockMachine, 1, 3), new Object[] {
            "FFF", "SMS", " C ", Character.valueOf('F'), Item.FLINT, Character.valueOf('S'), Block.COBBLESTONE, Character.valueOf('M'), new ItemStack(blockMachine, 1, 0), Character.valueOf('C'), 
            itemPartCircuit
        });
        ModLoader.AddRecipe(new ItemStack(blockMachine, 1, 4), new Object[] {
            "TMT", "TCT", Character.valueOf('T'), itemTreetap, Character.valueOf('M'), new ItemStack(blockMachine, 1, 0), Character.valueOf('C'), itemPartCircuit
        });
        ModLoader.AddRecipe(new ItemStack(blockMachine, 1, 5), new Object[] {
            "S S", "SMS", "SCS", Character.valueOf('S'), Block.STONE, Character.valueOf('M'), new ItemStack(blockMachine, 1, 0), Character.valueOf('C'), itemPartCircuit
        });
        ModLoader.AddRecipe(new ItemStack(itemBatSU, 5, 0), new Object[] {
            "C", "R", "D", Character.valueOf('D'), itemDustCoal, Character.valueOf('R'), Item.REDSTONE, Character.valueOf('C'), new ItemStack(itemCable, 1, 0)
        });
        ModLoader.AddRecipe(new ItemStack(itemBatSU, 5, 0), new Object[] {
            "C", "D", "R", Character.valueOf('D'), itemDustCoal, Character.valueOf('R'), Item.REDSTONE, Character.valueOf('C'), new ItemStack(itemCable, 1, 0)
        });
        ModLoader.AddRecipe(new ItemStack(itemBatCrystal, 1, 10001), new Object[] {
            "RRR", "RDR", "RRR", Character.valueOf('D'), Item.DIAMOND, Character.valueOf('R'), Item.REDSTONE
        });
        ModLoader.AddRecipe(new ItemStack(itemBatCrystal, 1, 10001), new Object[] {
            "RRR", "RDR", "RRR", Character.valueOf('D'), itemPartIndustrialDiamond, Character.valueOf('R'), Item.REDSTONE
        });
        ModLoader.AddRecipe(new ItemStack(itemBatLamaCrystal, 1, 10001), new Object[] {
            "LCL", "LDL", "LCL", Character.valueOf('D'), new ItemStack(itemBatCrystal, 1, 10001), Character.valueOf('C'), itemPartCircuit, Character.valueOf('L'), new ItemStack(Item.INK_SACK, 1, 4)
        });
        ModLoader.AddRecipe(new ItemStack(itemBatLamaCrystal, 1, 10001), new Object[] {
            "LCL", "LDL", "LCL", Character.valueOf('D'), new ItemStack(itemBatCrystal, 1, 1), Character.valueOf('C'), itemPartCircuit, Character.valueOf('L'), new ItemStack(Item.INK_SACK, 1, 4)
        });
        ModLoader.AddRecipe(new ItemStack(itemToolDDrill, 1, 121), new Object[] {
            " D ", "DdD", Character.valueOf('D'), Item.DIAMOND, Character.valueOf('d'), new ItemStack(itemToolDrill, 1, 201)
        });
        ModLoader.AddRecipe(new ItemStack(itemToolDDrill, 1, 121), new Object[] {
            " D ", "DdD", Character.valueOf('D'), itemPartIndustrialDiamond, Character.valueOf('d'), new ItemStack(itemToolDrill, 1, 201)
        });
        ModLoader.AddRecipe(new ItemStack(itemToolDDrill, 1, 1), new Object[] {
            " D ", "DdD", Character.valueOf('D'), Item.DIAMOND, Character.valueOf('d'), new ItemStack(itemToolDrill, 1, 1)
        });
        ModLoader.AddRecipe(new ItemStack(itemToolDDrill, 1, 1), new Object[] {
            " D ", "DdD", Character.valueOf('D'), itemPartIndustrialDiamond, Character.valueOf('d'), new ItemStack(itemToolDrill, 1, 1)
        });
        ModLoader.AddRecipe(new ItemStack(itemScanner, 1, 201), new Object[] {
            " G ", "CBC", "ccc", Character.valueOf('B'), itemBatREDischarged, Character.valueOf('c'), new ItemStack(itemCable, 1, 0), Character.valueOf('G'), Item.GLOWSTONE_DUST, Character.valueOf('C'), 
            itemPartCircuit
        });
        ModLoader.AddRecipe(new ItemStack(itemScanner, 1, 1), new Object[] {
            " G ", "CBC", "ccc", Character.valueOf('B'), new ItemStack(itemBatRE, 1, 1), Character.valueOf('c'), new ItemStack(itemCable, 1, 0), Character.valueOf('G'), Item.GLOWSTONE_DUST, Character.valueOf('C'), 
            itemPartCircuit
        });
        ModLoader.AddRecipe(new ItemStack(itemScannerAdv, 1, 201), new Object[] {
            " G ", "GCG", "cSc", Character.valueOf('S'), new ItemStack(itemScanner, 1, 201), Character.valueOf('c'), new ItemStack(itemCable, 1, 0), Character.valueOf('G'), Item.GLOWSTONE_DUST, Character.valueOf('C'), 
            itemPartCircuitAdv
        });
        ModLoader.AddRecipe(new ItemStack(itemScannerAdv, 1, 1), new Object[] {
            " G ", "GCG", "cSc", Character.valueOf('S'), new ItemStack(itemScanner, 1, 1), Character.valueOf('c'), new ItemStack(itemCable, 1, 0), Character.valueOf('G'), Item.GLOWSTONE_DUST, Character.valueOf('C'), 
            itemPartCircuitAdv
        });
        ModLoader.AddRecipe(new ItemStack(blockMachine, 1, 7), new Object[] {
            "CMC", " P ", " P ", Character.valueOf('P'), blockMiningPipe, Character.valueOf('M'), new ItemStack(blockMachine, 1, 0), Character.valueOf('C'), itemPartCircuit
        });
        ModLoader.AddShapelessRecipe(new ItemStack(itemCellWater, 1), new Object[] {
            itemCellEmpty, Item.WATER_BUCKET
        });
        ModLoader.AddShapelessRecipe(new ItemStack(itemCellLava, 1), new Object[] {
            itemCellEmpty, Item.LAVA_BUCKET
        });
        ModLoader.AddShapelessRecipe(new ItemStack(Block.OBSIDIAN, 1), new Object[] {
            itemCellWater, itemCellWater, itemCellLava, itemCellLava
        });
        ModLoader.AddShapelessRecipe(new ItemStack(itemFuelCoalDust, 1), new Object[] {
            itemDustCoal, Item.WATER_BUCKET
        });
        ModLoader.AddShapelessRecipe(new ItemStack(itemFuelCoalDust, 1), new Object[] {
            itemDustCoal, itemCellWater
        });
        ModLoader.AddRecipe(new ItemStack(itemFuelCoalDust, 8), new Object[] {
            "CCC", "CWC", "CCC", Character.valueOf('C'), itemDustCoal, Character.valueOf('W'), Item.WATER_BUCKET
        });
        ModLoader.AddRecipe(new ItemStack(itemFuelCoalDust, 8), new Object[] {
            "CCC", "CWC", "CCC", Character.valueOf('C'), itemDustCoal, Character.valueOf('W'), itemCellWater
        });
        ModLoader.AddShapelessRecipe(new ItemStack(itemCellCoal, 1), new Object[] {
            itemCellEmpty, itemFuelCoalCmpr
        });
        ModLoader.AddRecipe(new ItemStack(itemFuelPlantBall, 2), new Object[] {
            "PPP", "P P", "PPP", Character.valueOf('P'), Block.SAPLING
        });
        ModLoader.AddRecipe(new ItemStack(itemFuelPlantBall, 2), new Object[] {
            "PPP", "P P", "PPP", Character.valueOf('P'), blockRubSapling
        });
        ModLoader.AddRecipe(new ItemStack(itemFuelPlantBall, 1), new Object[] {
            "PPP", "P P", "PPP", Character.valueOf('P'), Item.WHEAT
        });
        ModLoader.AddRecipe(new ItemStack(itemFuelPlantBall, 1), new Object[] {
            "PPP", "P P", "PPP", Character.valueOf('P'), Item.SUGAR_CANE
        });
        ModLoader.AddRecipe(new ItemStack(itemFuelPlantBall, 1), new Object[] {
            "PPP", "P P", "PPP", Character.valueOf('P'), Block.CACTUS
        });
        ModLoader.AddShapelessRecipe(new ItemStack(itemCellBio, 1), new Object[] {
            itemCellEmpty, itemFuelPlantCmpr
        });
        ModLoader.AddRecipe(new ItemStack(blockMachine, 1, 8), new Object[] {
            "cCc", "cMc", "PTP", Character.valueOf('c'), itemCellEmpty, Character.valueOf('T'), itemTreetap, Character.valueOf('P'), blockMiningPipe, Character.valueOf('M'), 
            new ItemStack(blockMachine, 1, 0), Character.valueOf('C'), itemPartCircuit
        });
        ModLoader.AddRecipe(new ItemStack(blockMachine, 1, 9), new Object[] {
            "RFR", "RMR", "RFR", Character.valueOf('R'), Item.REDSTONE, Character.valueOf('F'), blockFenceIron, Character.valueOf('M'), new ItemStack(blockMachine, 1, 0)
        });
        ModLoader.AddRecipe(new ItemStack(blockMachine, 1, 10), new Object[] {
            "c c", "cCc", "EME", Character.valueOf('E'), itemCellEmpty, Character.valueOf('c'), new ItemStack(itemCable, 1, 0), Character.valueOf('M'), new ItemStack(blockMachine, 1, 0), Character.valueOf('C'), 
            itemPartCircuit
        });
        ModLoader.AddRecipe(new ItemStack(blockGenerator, 1, 0), new Object[] {
            "B", "M", "F", Character.valueOf('B'), itemBatREDischarged, Character.valueOf('F'), Block.FURNACE, Character.valueOf('M'), new ItemStack(blockMachine, 1, 0)
        });
        ModLoader.AddRecipe(new ItemStack(blockGenerator, 1, 0), new Object[] {
            "B", "M", "F", Character.valueOf('B'), new ItemStack(itemBatRE, 1, 1), Character.valueOf('F'), Block.FURNACE, Character.valueOf('M'), new ItemStack(blockMachine, 1, 0)
        });
        if(energyGeneratorWater > 0)
        {
            ModLoader.AddRecipe(new ItemStack(blockGenerator, 2, 2), new Object[] {
                "SPS", "PGP", "SPS", Character.valueOf('S'), Item.STICK, Character.valueOf('P'), Block.WOOD, Character.valueOf('G'), new ItemStack(blockGenerator, 1, 0)
            });
        }
        if(energyGeneratorSolar > 0)
        {
            ModLoader.AddRecipe(new ItemStack(blockGenerator, 1, 3), new Object[] {
                "CgC", "gCg", "cGc", Character.valueOf('G'), new ItemStack(blockGenerator, 1, 0), Character.valueOf('C'), itemDustCoal, Character.valueOf('g'), Block.GLASS, Character.valueOf('c'), 
                itemPartCircuit
            });
        }
        if(energyGeneratorWind > 0)
        {
            ModLoader.AddRecipe(new ItemStack(blockGenerator, 1, 4), new Object[] {
                "I I", " G ", "I I", Character.valueOf('I'), Item.IRON_INGOT, Character.valueOf('G'), new ItemStack(blockGenerator, 1, 0)
            });
        }
        ModLoader.AddShapelessRecipe(new ItemStack(itemDustBronze, 2), new Object[] {
            itemDustTin, itemDustCopper, itemDustCopper, itemDustCopper
        });
        ModLoader.AddRecipe(new ItemStack(itemToolWrenchElectric, 1, 201), new Object[] {
            "  W", " C ", "B  ", Character.valueOf('W'), itemToolWrench, Character.valueOf('B'), itemBatREDischarged, Character.valueOf('C'), itemPartCircuit
        });
        ModLoader.AddRecipe(new ItemStack(itemToolWrenchElectric, 1, 1), new Object[] {
            "  W", " C ", "B  ", Character.valueOf('W'), itemToolWrench, Character.valueOf('B'), new ItemStack(itemBatRE, 1, 1), Character.valueOf('C'), itemPartCircuit
        });
        ModLoader.AddRecipe(new ItemStack(blockAlloy, 8), new Object[] {
            "SSS", "SAS", "SSS", Character.valueOf('S'), Block.STONE, Character.valueOf('A'), itemPartAlloy
        });
        ModLoader.AddRecipe(new ItemStack(blockAlloyGlass, 7), new Object[] {
            "GAG", "GGG", "GAG", Character.valueOf('G'), Block.GLASS, Character.valueOf('A'), itemPartAlloy
        });
        ModLoader.AddRecipe(new ItemStack(blockAlloyGlass, 7), new Object[] {
            "GGG", "AGA", "GGG", Character.valueOf('G'), Block.GLASS, Character.valueOf('A'), itemPartAlloy
        });
        ModLoader.AddRecipe(new ItemStack(itemDoorAlloy, 1), new Object[] {
            "SS", "SS", "SS", Character.valueOf('S'), blockAlloy
        });
        ModLoader.AddRecipe(new ItemStack(itemArmorAlloyChestplate), new Object[] {
            "A A", "ALA", "AIA", Character.valueOf('L'), Item.LEATHER_CHESTPLATE, Character.valueOf('I'), Item.IRON_CHESTPLATE, Character.valueOf('A'), itemPartAlloy
        });
        ModLoader.AddRecipe(new ItemStack(itemArmorAlloyChestplate), new Object[] {
            "A A", "AIA", "ALA", Character.valueOf('L'), Item.LEATHER_CHESTPLATE, Character.valueOf('I'), Item.IRON_CHESTPLATE, Character.valueOf('A'), itemPartAlloy
        });
        ModLoader.AddRecipe(new ItemStack(blockReactorChamber), new Object[] {
            "ACA", "PMP", "APA", Character.valueOf('A'), itemPartAlloy, Character.valueOf('C'), itemReactorCooler, Character.valueOf('P'), itemReactorPlating, Character.valueOf('M'), 
            new ItemStack(blockMachine, 1, 0)
        });
        ModLoader.AddShapelessRecipe(new ItemStack(itemCellUran, 1), new Object[] {
            new ItemStack(itemCellUranEnriched), itemDustCoal
        });
        ModLoader.AddShapelessRecipe(new ItemStack(itemCellUranDepleted, 1, 9999), new Object[] {
            new ItemStack(itemCellUranEmpty), itemDustCoal
        });
        if(energyGeneratorNuclear > 0)
        {
            ModLoader.AddRecipe(new ItemStack(blockGenerator, 1, 5), new Object[] {
                "AcA", "CGC", "AcA", Character.valueOf('A'), itemPartAlloy, Character.valueOf('C'), blockReactorChamber, Character.valueOf('c'), itemPartCircuitAdv, Character.valueOf('G'), 
                new ItemStack(blockGenerator, 1, 0)
            });
        }
        ModLoader.AddRecipe(new ItemStack(itemPartCarbonFibre, 1), new Object[] {
            "CC", "CC", Character.valueOf('C'), itemDustCoal
        });
        ModLoader.AddShapelessRecipe(new ItemStack(itemPartCarbonMesh, 1), new Object[] {
            itemPartCarbonFibre, itemPartCarbonFibre
        });
        ModLoader.AddRecipe(new ItemStack(itemArmorNanoHelmet, 1, 251), new Object[] {
            "CcC", "CGC", Character.valueOf('C'), itemPartCarbonPlate, Character.valueOf('c'), new ItemStack(itemBatCrystal, 1, 10001), Character.valueOf('G'), Block.GLASS
        });
        ModLoader.AddRecipe(new ItemStack(itemArmorNanoChestplate, 1, 251), new Object[] {
            "C C", "CcC", "CCC", Character.valueOf('C'), itemPartCarbonPlate, Character.valueOf('c'), new ItemStack(itemBatCrystal, 1, 10001)
        });
        ModLoader.AddRecipe(new ItemStack(itemArmorNanoLegs, 1, 251), new Object[] {
            "CcC", "C C", "C C", Character.valueOf('C'), itemPartCarbonPlate, Character.valueOf('c'), new ItemStack(itemBatCrystal, 1, 10001)
        });
        ModLoader.AddRecipe(new ItemStack(itemArmorNanoBoots, 1, 251), new Object[] {
            "C C", "CcC", Character.valueOf('C'), itemPartCarbonPlate, Character.valueOf('c'), new ItemStack(itemBatCrystal, 1, 10001)
        });
        ModLoader.AddRecipe(new ItemStack(itemNanoSaber, 1, 5001), new Object[] {
            "GA ", "GA ", "CcC", Character.valueOf('C'), itemPartCarbonPlate, Character.valueOf('c'), new ItemStack(itemBatCrystal, 1, 10001), Character.valueOf('G'), Item.GLOWSTONE_DUST, Character.valueOf('A'), 
            itemPartAlloy
        });
        ModLoader.AddRecipe(new ItemStack(itemArmorNanoHelmet, 1, 1), new Object[] {
            "CcC", "CGC", Character.valueOf('C'), itemPartCarbonPlate, Character.valueOf('c'), new ItemStack(itemBatCrystal, 1, 1), Character.valueOf('G'), Block.GLASS
        });
        ModLoader.AddRecipe(new ItemStack(itemArmorNanoChestplate, 1, 1), new Object[] {
            "C C", "CcC", "CCC", Character.valueOf('C'), itemPartCarbonPlate, Character.valueOf('c'), new ItemStack(itemBatCrystal, 1, 1)
        });
        ModLoader.AddRecipe(new ItemStack(itemArmorNanoLegs, 1, 1), new Object[] {
            "CcC", "C C", "C C", Character.valueOf('C'), itemPartCarbonPlate, Character.valueOf('c'), new ItemStack(itemBatCrystal, 1, 1)
        });
        ModLoader.AddRecipe(new ItemStack(itemArmorNanoBoots, 1, 1), new Object[] {
            "C C", "CcC", Character.valueOf('C'), itemPartCarbonPlate, Character.valueOf('c'), new ItemStack(itemBatCrystal, 1, 1)
        });
        ModLoader.AddRecipe(new ItemStack(itemNanoSaber, 1, 1), new Object[] {
            "GA ", "GA ", "CcC", Character.valueOf('C'), itemPartCarbonPlate, Character.valueOf('c'), new ItemStack(itemBatCrystal, 1, 1), Character.valueOf('G'), Item.GLOWSTONE_DUST, Character.valueOf('A'), 
            itemPartAlloy
        });
        ModLoader.AddRecipe(new ItemStack(blockMachine, 1, 12), new Object[] {
            " A ", "CMC", " A ", Character.valueOf('A'), itemPartAlloy, Character.valueOf('C'), itemPartCarbonPlate, Character.valueOf('M'), new ItemStack(blockMachine, 1, 0)
        });
        ModLoader.AddRecipe(new ItemStack(blockMachine, 1, 12), new Object[] {
            " C ", "AMA", " C ", Character.valueOf('A'), itemPartAlloy, Character.valueOf('C'), itemPartCarbonPlate, Character.valueOf('M'), new ItemStack(blockMachine, 1, 0)
        });
        ModLoader.AddRecipe(new ItemStack(blockITNT, 4), new Object[] {
            "FFF", "TTT", "FFF", Character.valueOf('F'), Item.FLINT, Character.valueOf('T'), Block.TNT
        });
        ModLoader.AddRecipe(new ItemStack(blockITNT, 4), new Object[] {
            "FTF", "FTF", "FTF", Character.valueOf('F'), Item.FLINT, Character.valueOf('T'), Block.TNT
        });
        ModLoader.AddRecipe(new ItemStack(itemDynamite, 8), new Object[] {
            "S", "T", Character.valueOf('S'), Item.STRING, Character.valueOf('T'), blockITNT
        });
        ModLoader.AddRecipe(new ItemStack(itemDynamiteSticky, 8), new Object[] {
            "DDD", "DRD", "DDD", Character.valueOf('D'), itemDynamite, Character.valueOf('R'), itemHarz
        });
        ModLoader.AddRecipe(new ItemStack(blockITNT, 4), new Object[] {
            " c ", "GCG", "TTT", Character.valueOf('c'), new ItemStack(itemCable, 1, 0), Character.valueOf('G'), Item.GLOWSTONE_DUST, Character.valueOf('C'), itemPartCircuit, Character.valueOf('T'), 
            Block.TNT
        });
        ModLoader.AddRecipe(new ItemStack(blockRubber, 3), new Object[] {
            "RRR", "RRR", Character.valueOf('R'), itemRubber
        });
        ModLoader.AddRecipe(new ItemStack(itemArmorRubBoots, 1), new Object[] {
            "R R", "R R", "RCR", Character.valueOf('R'), itemRubber, Character.valueOf('C'), Block.WOOL
        });
        if(enableCraftingGlowstoneDust)
        {
            ModLoader.AddRecipe(new ItemStack(Item.GLOWSTONE_DUST, 1), new Object[] {
                "RGR", "GRG", "RGR", Character.valueOf('R'), Item.REDSTONE, Character.valueOf('G'), itemDustGold
            });
        }
        if(enableCraftingGunpowder)
        {
            ModLoader.AddRecipe(new ItemStack(Item.SULPHUR, 3), new Object[] {
                "RCR", "CRC", "RCR", Character.valueOf('R'), Item.REDSTONE, Character.valueOf('C'), itemDustCoal
            });
        }
        ModLoader.AddRecipe(new ItemStack(itemBatSU, 8), new Object[] {
            "c", "C", "R", Character.valueOf('R'), Item.REDSTONE, Character.valueOf('C'), itemFuelCoalDust, Character.valueOf('c'), new ItemStack(itemCable, 1, 0)
        });
        ModLoader.AddRecipe(new ItemStack(itemBatSU, 8), new Object[] {
            "c", "R", "C", Character.valueOf('R'), Item.REDSTONE, Character.valueOf('C'), itemFuelCoalDust, Character.valueOf('c'), new ItemStack(itemCable, 1, 0)
        });
        ModLoader.AddRecipe(new ItemStack(blockElectric, 1, 0), new Object[] {
            "PCP", "BBB", "PPP", Character.valueOf('P'), Block.WOOD, Character.valueOf('C'), new ItemStack(itemCable, 1, 0), Character.valueOf('B'), itemBatREDischarged
        });
        ModLoader.AddRecipe(new ItemStack(blockElectric, 1, 1), new Object[] {
            "cCc", "CMC", "cCc", Character.valueOf('M'), new ItemStack(blockMachine, 1, 0), Character.valueOf('c'), new ItemStack(itemCable, 1, 0), Character.valueOf('C'), new ItemStack(itemBatCrystal, 1, 10001)
        });
        ModLoader.AddRecipe(new ItemStack(blockElectric, 1, 2), new Object[] {
            "LCL", "LML", "LAL", Character.valueOf('M'), new ItemStack(blockElectric, 1, 1), Character.valueOf('A'), new ItemStack(blockMachine, 1, 12), Character.valueOf('C'), itemPartCircuitAdv, Character.valueOf('L'), 
            new ItemStack(itemBatLamaCrystal, 1, 10001)
        });
        ModLoader.AddRecipe(new ItemStack(blockElectric, 1, 4), new Object[] {
            " C ", " M ", " C ", Character.valueOf('M'), new ItemStack(blockMachine, 1, 0), Character.valueOf('C'), new ItemStack(itemCable, 1, 0)
        });
        ModLoader.AddRecipe(new ItemStack(blockElectric, 1, 5), new Object[] {
            " c ", "CED", " c ", Character.valueOf('E'), new ItemStack(blockElectric, 1, 4), Character.valueOf('c'), new ItemStack(itemCable, 1, 0), Character.valueOf('D'), new ItemStack(itemBatCrystal, 1, 10001), Character.valueOf('C'), 
            itemPartCircuit
        });
        ModLoader.AddRecipe(new ItemStack(itemToolMeter), new Object[] {
            " G ", "cCc", "c c", Character.valueOf('G'), Item.GLOWSTONE_DUST, Character.valueOf('c'), new ItemStack(itemCable, 1, 0), Character.valueOf('C'), itemPartCircuit
        });
        ModLoader.AddRecipe(new ItemStack(itemToolMiningLaser, 1, 8001), new Object[] {
            "Rcc", "AAC", " AA", Character.valueOf('A'), itemPartAlloy, Character.valueOf('C'), itemPartCircuitAdv, Character.valueOf('c'), new ItemStack(itemBatCrystal, 1, 10001), Character.valueOf('R'), 
            Item.REDSTONE
        });
        ModLoader.AddRecipe(new ItemStack(itemToolMiningLaser, 1, 1), new Object[] {
            "Rcc", "AAC", " AA", Character.valueOf('A'), itemPartAlloy, Character.valueOf('C'), itemPartCircuitAdv, Character.valueOf('c'), new ItemStack(itemBatCrystal, 1, 1), Character.valueOf('R'), 
            Item.REDSTONE
        });
        ModLoader.AddShapelessRecipe(new ItemStack(Block.PISTON_STICKY, 1), new Object[] {
            Block.PISTON, itemHarz
        });
        ModLoader.AddRecipe(new ItemStack(Block.TORCH, 4), new Object[] {
            "R", "I", Character.valueOf('I'), Item.STICK, Character.valueOf('R'), itemHarz
        });
        ModLoader.AddShapelessRecipe(new ItemStack(blockFoam, 3), new Object[] {
            itemDustClay, Item.WATER_BUCKET, Item.REDSTONE, itemDustCoal
        });
        ModLoader.AddShapelessRecipe(new ItemStack(blockFoam, 3), new Object[] {
            itemDustClay, itemCellWater, Item.REDSTONE, itemDustCoal
        });
        ModLoader.AddRecipe(new ItemStack(itemFoamSprayer, 1, 1601), new Object[] {
            "SS ", "Ss ", "  S", Character.valueOf('S'), Block.COBBLESTONE, Character.valueOf('s'), Item.STICK
        });
        ModLoader.AddRecipe(new ItemStack(itemFoamSprayer, 1, 1), new Object[] {
            "PPP", "PSP", "PPP", Character.valueOf('P'), itemPartPellet, Character.valueOf('S'), new ItemStack(itemFoamSprayer, 1, 1601)
        });
        ModLoader.AddShapelessRecipe(new ItemStack(itemFoamSprayer, 1, 1401), new Object[] {
            new ItemStack(itemFoamSprayer, 1, 1601), itemPartPellet
        });
        ModLoader.AddShapelessRecipe(new ItemStack(itemFoamSprayer, 1, 1301), new Object[] {
            new ItemStack(itemFoamSprayer, 1, 1501), itemPartPellet
        });
        ModLoader.AddShapelessRecipe(new ItemStack(itemFoamSprayer, 1, 1201), new Object[] {
            new ItemStack(itemFoamSprayer, 1, 1401), itemPartPellet
        });
        ModLoader.AddShapelessRecipe(new ItemStack(itemFoamSprayer, 1, 1101), new Object[] {
            new ItemStack(itemFoamSprayer, 1, 1301), itemPartPellet
        });
        ModLoader.AddShapelessRecipe(new ItemStack(itemFoamSprayer, 1, 1001), new Object[] {
            new ItemStack(itemFoamSprayer, 1, 1201), itemPartPellet
        });
        ModLoader.AddShapelessRecipe(new ItemStack(itemFoamSprayer, 1, 901), new Object[] {
            new ItemStack(itemFoamSprayer, 1, 1101), itemPartPellet
        });
        ModLoader.AddShapelessRecipe(new ItemStack(itemFoamSprayer, 1, 801), new Object[] {
            new ItemStack(itemFoamSprayer, 1, 1001), itemPartPellet
        });
        ModLoader.AddShapelessRecipe(new ItemStack(itemFoamSprayer, 1, 701), new Object[] {
            new ItemStack(itemFoamSprayer, 1, 901), itemPartPellet
        });
        ModLoader.AddShapelessRecipe(new ItemStack(itemFoamSprayer, 1, 601), new Object[] {
            new ItemStack(itemFoamSprayer, 1, 801), itemPartPellet
        });
        ModLoader.AddShapelessRecipe(new ItemStack(itemFoamSprayer, 1, 501), new Object[] {
            new ItemStack(itemFoamSprayer, 1, 701), itemPartPellet
        });
        ModLoader.AddShapelessRecipe(new ItemStack(itemFoamSprayer, 1, 401), new Object[] {
            new ItemStack(itemFoamSprayer, 1, 601), itemPartPellet
        });
        ModLoader.AddShapelessRecipe(new ItemStack(itemFoamSprayer, 1, 301), new Object[] {
            new ItemStack(itemFoamSprayer, 1, 501), itemPartPellet
        });
        ModLoader.AddShapelessRecipe(new ItemStack(itemFoamSprayer, 1, 201), new Object[] {
            new ItemStack(itemFoamSprayer, 1, 401), itemPartPellet
        });
        ModLoader.AddShapelessRecipe(new ItemStack(itemFoamSprayer, 1, 101), new Object[] {
            new ItemStack(itemFoamSprayer, 1, 301), itemPartPellet
        });
        ModLoader.AddShapelessRecipe(new ItemStack(itemFoamSprayer, 1, 1), new Object[] {
            new ItemStack(itemFoamSprayer, 1, 201), itemPartPellet
        });
        ModLoader.AddRecipe(new ItemStack(blockScaffold, 16), new Object[] {
            "PPP", " s ", "s s", Character.valueOf('P'), Block.WOOD, Character.valueOf('s'), Item.STICK
        });
        ModLoader.AddRecipe(new ItemStack(blockMachine, 1, 14), new Object[] {
            "GCG", "ALA", "GCG", Character.valueOf('A'), new ItemStack(blockMachine, 1, 12), Character.valueOf('L'), new ItemStack(itemBatLamaCrystal, 1, 10001), Character.valueOf('G'), Item.GLOWSTONE_DUST, Character.valueOf('C'), 
            itemPartCircuitAdv
        });
        ModLoader.AddRecipe(new ItemStack(blockMachine, 1, 14), new Object[] {
            "GCG", "ALA", "GCG", Character.valueOf('A'), new ItemStack(blockMachine, 1, 12), Character.valueOf('L'), new ItemStack(itemBatLamaCrystal, 1, 1), Character.valueOf('G'), Item.GLOWSTONE_DUST, Character.valueOf('C'), 
            itemPartCircuitAdv
        });
        ModLoader.AddRecipe(new ItemStack(Block.STONE, 16), new Object[] {
            "   ", " M ", "   ", Character.valueOf('M'), itemMatter
        });
        ModLoader.AddRecipe(new ItemStack(Block.GLASS, 32), new Object[] {
            " M ", "M M", " M ", Character.valueOf('M'), itemMatter
        });
        ModLoader.AddRecipe(new ItemStack(Block.GRASS, 16), new Object[] {
            "   ", "M  ", "M  ", Character.valueOf('M'), itemMatter
        });
        ModLoader.AddRecipe(new ItemStack(Block.MOSSY_COBBLESTONE, 16), new Object[] {
            "   ", " M ", "M M", Character.valueOf('M'), itemMatter
        });
        ModLoader.AddRecipe(new ItemStack(Block.SANDSTONE, 16), new Object[] {
            "   ", "  M", " M ", Character.valueOf('M'), itemMatter
        });
        ModLoader.AddRecipe(new ItemStack(Block.SNOW_BLOCK, 4), new Object[] {
            "M M", "   ", "   ", Character.valueOf('M'), itemMatter
        });
        ModLoader.AddRecipe(new ItemStack(Block.STATIONARY_WATER, 1), new Object[] {
            "   ", " M ", " M ", Character.valueOf('M'), itemMatter
        });
        ModLoader.AddRecipe(new ItemStack(Block.STATIONARY_LAVA, 1), new Object[] {
            " M ", " M ", " M ", Character.valueOf('M'), itemMatter
        });
        ModLoader.AddRecipe(new ItemStack(Item.COAL, 2), new Object[] {
            "  M", "M  ", "  M", Character.valueOf('M'), itemMatter
        });
        ModLoader.AddRecipe(new ItemStack(Block.IRON_ORE, 2), new Object[] {
            "M M", " M ", "M M", Character.valueOf('M'), itemMatter
        });
        ModLoader.AddRecipe(new ItemStack(Block.GOLD_ORE, 2), new Object[] {
            " M ", "MMM", " M ", Character.valueOf('M'), itemMatter
        });
        ModLoader.AddRecipe(new ItemStack(Item.DIAMOND, 1), new Object[] {
            "MMM", "MMM", "MMM", Character.valueOf('M'), itemMatter
        });
        ModLoader.AddRecipe(new ItemStack(Item.REDSTONE, 24), new Object[] {
            "   ", " M ", "MMM", Character.valueOf('M'), itemMatter
        });
        ModLoader.AddRecipe(new ItemStack(blockOreCopper, 5), new Object[] {
            "  M", "M M", "   ", Character.valueOf('M'), itemMatter
        });
        ModLoader.AddRecipe(new ItemStack(blockOreTin, 5), new Object[] {
            "   ", "M M", "  M", Character.valueOf('M'), itemMatter
        });
        ModLoader.AddRecipe(new ItemStack(Block.OBSIDIAN, 12), new Object[] {
            "M M", "M M", "   ", Character.valueOf('M'), itemMatter
        });
        ModLoader.AddRecipe(new ItemStack(Block.NETHERRACK, 16), new Object[] {
            "  M", " M ", "M  ", Character.valueOf('M'), itemMatter
        });
        ModLoader.AddRecipe(new ItemStack(Block.GLOWSTONE, 8), new Object[] {
            " M ", "M M", "MMM", Character.valueOf('M'), itemMatter
        });
        ModLoader.AddRecipe(new ItemStack(Block.LOG, 8), new Object[] {
            " M ", "   ", "   ", Character.valueOf('M'), itemMatter
        });
        ModLoader.AddRecipe(new ItemStack(Item.INK_SACK, 9, 4), new Object[] {
            " M ", " M ", " MM", Character.valueOf('M'), itemMatter
        });
        ModLoader.AddRecipe(new ItemStack(Item.FEATHER, 32), new Object[] {
            " M ", " M ", "M M", Character.valueOf('M'), itemMatter
        });
        ModLoader.AddRecipe(new ItemStack(Item.SNOW_BALL, 16), new Object[] {
            "   ", "   ", "MMM", Character.valueOf('M'), itemMatter
        });
        ModLoader.AddRecipe(new ItemStack(Item.SULPHUR, 15), new Object[] {
            "MMM", "M  ", "MMM", Character.valueOf('M'), itemMatter
        });
        ModLoader.AddRecipe(new ItemStack(itemOreIridium, 1), new Object[] {
            "MMM", " M ", "MMM", Character.valueOf('M'), itemMatter
        });
        ModLoader.AddRecipe(new ItemStack(Item.CLAY_BALL, 48), new Object[] {
            "MM ", "M  ", "MM ", Character.valueOf('M'), itemMatter
        });
        ModLoader.AddRecipe(new ItemStack(Item.INK_SACK, 32, 3), new Object[] {
            "MM ", "  M", "MM ", Character.valueOf('M'), itemMatter
        });
        ModLoader.AddRecipe(new ItemStack(Item.INK_SACK, 48, 0), new Object[] {
            " MM", " MM", " M ", Character.valueOf('M'), itemMatter
        });
        ModLoader.AddRecipe(new ItemStack(itemHarz, 21, 0), new Object[] {
            "M M", "   ", "M M", Character.valueOf('M'), itemMatter
        });
        ModLoader.AddRecipe(new ItemStack(Block.CACTUS, 48), new Object[] {
            " M ", "MMM", "M M", Character.valueOf('M'), itemMatter
        });
        ModLoader.AddRecipe(new ItemStack(Block.SUGAR_CANE_BLOCK, 48), new Object[] {
            "M M", "M M", "M M", Character.valueOf('M'), itemMatter
        });
        ModLoader.AddRecipe(new ItemStack(Item.FLINT, 32), new Object[] {
            " M ", "MM ", "MM ", Character.valueOf('M'), itemMatter
        });
        ModLoader.AddRecipe(new ItemStack(Block.WOOL, 12), new Object[] {
            "M M", "   ", " M ", Character.valueOf('M'), itemMatter
        });
        ModLoader.AddRecipe(new ItemStack(itemPartIridium, 1), new Object[] {
            "IAI", "ADA", "IAI", Character.valueOf('I'), itemOreIridium, Character.valueOf('A'), itemPartAlloy, Character.valueOf('D'), Item.DIAMOND
        });
        ModLoader.AddRecipe(new ItemStack(itemPartIridium, 1), new Object[] {
            "IAI", "ADA", "IAI", Character.valueOf('I'), itemOreIridium, Character.valueOf('A'), itemPartAlloy, Character.valueOf('D'), itemPartIndustrialDiamond
        });
        ModLoader.AddRecipe(new ItemStack(itemArmorQuantumHelmet, 1, 1001), new Object[] {
            "ILI", "CGC", Character.valueOf('I'), itemPartIridium, Character.valueOf('L'), new ItemStack(itemBatLamaCrystal, 1, 10001), Character.valueOf('G'), blockAlloyGlass, Character.valueOf('C'), itemPartCircuitAdv
        });
        ModLoader.AddRecipe(new ItemStack(itemArmorQuantumChestplate, 1, 1001), new Object[] {
            "A A", "ILI", "IAI", Character.valueOf('I'), itemPartIridium, Character.valueOf('L'), new ItemStack(itemBatLamaCrystal, 1, 10001), Character.valueOf('A'), itemPartAlloy
        });
        ModLoader.AddRecipe(new ItemStack(itemArmorQuantumLegs, 1, 1001), new Object[] {
            "MLM", "I I", "G G", Character.valueOf('I'), itemPartIridium, Character.valueOf('L'), new ItemStack(itemBatLamaCrystal, 1, 10001), Character.valueOf('G'), Item.GLOWSTONE_DUST, Character.valueOf('M'), 
            new ItemStack(blockMachine, 1, 0)
        });
        ModLoader.AddRecipe(new ItemStack(itemArmorQuantumBoots, 1, 1001), new Object[] {
            "I I", "RLR", Character.valueOf('I'), itemPartIridium, Character.valueOf('L'), new ItemStack(itemBatLamaCrystal, 1, 10001), Character.valueOf('R'), itemArmorRubBoots
        });
        ModLoader.AddRecipe(new ItemStack(itemArmorQuantumHelmet, 1, 1), new Object[] {
            "ILI", "CGC", Character.valueOf('I'), itemPartIridium, Character.valueOf('L'), new ItemStack(itemBatLamaCrystal, 1, 1), Character.valueOf('G'), blockAlloyGlass, Character.valueOf('C'), itemPartCircuitAdv
        });
        ModLoader.AddRecipe(new ItemStack(itemArmorQuantumChestplate, 1, 1), new Object[] {
            "A A", "ILI", "IAI", Character.valueOf('I'), itemPartIridium, Character.valueOf('L'), new ItemStack(itemBatLamaCrystal, 1, 1), Character.valueOf('A'), itemPartAlloy
        });
        ModLoader.AddRecipe(new ItemStack(itemArmorQuantumLegs, 1, 1), new Object[] {
            "MLM", "I I", "G G", Character.valueOf('I'), itemPartIridium, Character.valueOf('L'), new ItemStack(itemBatLamaCrystal, 1, 1), Character.valueOf('G'), Item.GLOWSTONE_DUST, Character.valueOf('M'), 
            new ItemStack(blockMachine, 1, 0)
        });
        ModLoader.AddRecipe(new ItemStack(itemArmorQuantumBoots, 1, 1), new Object[] {
            "I I", "RLR", Character.valueOf('I'), itemPartIridium, Character.valueOf('L'), new ItemStack(itemBatLamaCrystal, 1, 1), Character.valueOf('R'), itemArmorRubBoots
        });
        ModLoader.AddRecipe(new ItemStack(itemIngotCopper, 9), new Object[] {
            "B", Character.valueOf('B'), new ItemStack(blockMetal, 1, 0)
        });
        ModLoader.AddRecipe(new ItemStack(itemIngotTin, 9), new Object[] {
            "B", Character.valueOf('B'), new ItemStack(blockMetal, 1, 1)
        });
        ModLoader.AddRecipe(new ItemStack(itemIngotBronze, 9), new Object[] {
            "B", Character.valueOf('B'), new ItemStack(blockMetal, 1, 2)
        });
        ModLoader.AddRecipe(new ItemStack(itemIngotUran, 9), new Object[] {
            "B", Character.valueOf('B'), new ItemStack(blockMetal, 1, 3)
        });
        ModLoader.AddRecipe(new ItemStack(itemScrapBox, 1), new Object[] {
            "SSS", "SSS", "SSS", Character.valueOf('S'), itemScrap
        });
        ModLoader.AddRecipe(new ItemStack(itemPartCoalBall, 1), new Object[] {
            "CCC", "CFC", "CCC", Character.valueOf('C'), itemDustCoal, Character.valueOf('F'), Item.FLINT
        });
        ModLoader.AddRecipe(new ItemStack(itemPartCoalChunk, 1), new Object[] {
            "###", "#O#", "###", Character.valueOf('#'), itemPartCoalBlock, Character.valueOf('O'), Block.OBSIDIAN
        });
        ModLoader.AddRecipe(new ItemStack(itemPartCoalChunk, 1), new Object[] {
            "###", "#O#", "###", Character.valueOf('#'), itemPartCoalBlock, Character.valueOf('O'), Block.IRON_BLOCK
        });
        ModLoader.AddRecipe(new ItemStack(itemPartCoalChunk, 1), new Object[] {
            "###", "#O#", "###", Character.valueOf('#'), itemPartCoalBlock, Character.valueOf('O'), Block.BRICK
        });
        ModLoader.AddRecipe(new ItemStack(Item.DIAMOND_PICKAXE, 1), new Object[] {
            "DDD", " S ", " S ", Character.valueOf('S'), Item.STICK, Character.valueOf('D'), itemPartIndustrialDiamond
        });
        ModLoader.AddRecipe(new ItemStack(Item.DIAMOND_HOE, 1), new Object[] {
            "DD ", " S ", " S ", Character.valueOf('S'), Item.STICK, Character.valueOf('D'), itemPartIndustrialDiamond
        });
        ModLoader.AddRecipe(new ItemStack(Item.DIAMOND_SPADE, 1), new Object[] {
            "D", "S", "S", Character.valueOf('S'), Item.STICK, Character.valueOf('D'), itemPartIndustrialDiamond
        });
        ModLoader.AddRecipe(new ItemStack(Item.DIAMOND_AXE, 1), new Object[] {
            "DD ", "DS ", " S ", Character.valueOf('S'), Item.STICK, Character.valueOf('D'), itemPartIndustrialDiamond
        });
        ModLoader.AddRecipe(new ItemStack(Item.DIAMOND_SWORD, 1), new Object[] {
            "D", "D", "S", Character.valueOf('S'), Item.STICK, Character.valueOf('D'), itemPartIndustrialDiamond
        });
        ModLoader.AddRecipe(new ItemStack(itemTFBP, 1), new Object[] {
            " C ", " A ", "R R", Character.valueOf('C'), itemPartCircuit, Character.valueOf('A'), itemPartCircuitAdv, Character.valueOf('R'), Item.REDSTONE
        });
        ModLoader.AddRecipe(new ItemStack(blockMachine, 1, 15), new Object[] {
            "GTG", "DMD", "GDG", Character.valueOf('T'), itemTFBP, Character.valueOf('G'), Item.GLOWSTONE_DUST, Character.valueOf('D'), Block.DIRT, Character.valueOf('M'), 
            new ItemStack(blockMachine, 1, 12)
        });
        ModLoader.AddRecipe(new ItemStack(itemTFBPCultivation, 1), new Object[] {
            " S ", "S#S", " S ", Character.valueOf('#'), itemTFBP, Character.valueOf('S'), Item.SEEDS
        });
        ModLoader.AddRecipe(new ItemStack(itemTFBPDesertification, 1), new Object[] {
            " S ", "S#S", " S ", Character.valueOf('#'), itemTFBP, Character.valueOf('S'), Block.SAND
        });
        ModLoader.AddRecipe(new ItemStack(itemTFBPIrrigation, 1), new Object[] {
            " W ", "W#W", " W ", Character.valueOf('#'), itemTFBP, Character.valueOf('W'), Item.WATER_BUCKET
        });
        ModLoader.AddRecipe(new ItemStack(itemTFBPChilling, 1), new Object[] {
            " S ", "S#S", " S ", Character.valueOf('#'), itemTFBP, Character.valueOf('S'), Item.SNOW_BALL
        });
        ModLoader.AddRecipe(new ItemStack(itemTFBPFlatification, 1), new Object[] {
            " D ", "D#D", " D ", Character.valueOf('#'), itemTFBP, Character.valueOf('D'), Block.DIRT
        });
        ModLoader.AddRecipe(new ItemStack(itemFreq, 1, 0), new Object[] {
            "c", "C", "C", Character.valueOf('C'), itemPartCircuit, Character.valueOf('c'), itemCable
        });
        ModLoader.AddRecipe(new ItemStack(blockMachine2, 1, 0), new Object[] {
            "GFG", "CMC", "GDG", Character.valueOf('M'), new ItemStack(blockMachine, 1, 12), Character.valueOf('C'), itemCable, Character.valueOf('F'), itemFreq, Character.valueOf('G'), 
            Item.GLOWSTONE_DUST, Character.valueOf('D'), itemPartIndustrialDiamond
        });
        ModLoader.AddRecipe(new ItemStack(blockMachine2, 1, 0), new Object[] {
            "GFG", "CMC", "GDG", Character.valueOf('M'), new ItemStack(blockMachine, 1, 12), Character.valueOf('C'), itemCable, Character.valueOf('F'), itemFreq, Character.valueOf('G'), 
            Item.GLOWSTONE_DUST, Character.valueOf('D'), Item.DIAMOND
        });
        ModLoader.AddRecipe(new ItemStack(itemDustIronSmall, 1), new Object[] {
            "CTC", "TCT", "CTC", Character.valueOf('C'), itemDustCopper, Character.valueOf('T'), itemDustTin
        });
        ModLoader.AddRecipe(new ItemStack(itemDustIronSmall, 1), new Object[] {
            "TCT", "CTC", "TCT", Character.valueOf('C'), itemDustCopper, Character.valueOf('T'), itemDustTin
        });
        ModLoader.AddRecipe(new ItemStack(itemArmorLappack, 1, 24001), new Object[] {
            "LAL", "LBL", "L L", Character.valueOf('L'), Block.LAPIS_BLOCK, Character.valueOf('A'), itemPartCircuitAdv, Character.valueOf('B'), new ItemStack(itemArmorBatpack, 1, 1)
        });
        ModLoader.AddRecipe(new ItemStack(itemArmorLappack, 1, 30001), new Object[] {
            "LAL", "LBL", "L L", Character.valueOf('L'), Block.LAPIS_BLOCK, Character.valueOf('A'), itemPartCircuitAdv, Character.valueOf('B'), new ItemStack(itemArmorBatpack, 1, 30001)
        });
        ModLoader.AddShapelessRecipe(new ItemStack(itemDustIron, 1), new Object[] {
            itemDustIronSmall, itemDustIronSmall
        });
        if(enableCraftingCoin)
        {
            ModLoader.AddRecipe(new ItemStack(itemIngotAdvIron, 1), new Object[] {
                "III", "III", "III", Character.valueOf('I'), itemCoin
            });
        }
        ModLoader.AddShapelessRecipe(new ItemStack(itemTFBP, 1), new Object[] {
            itemTFBPCultivation
        });
        ModLoader.AddShapelessRecipe(new ItemStack(itemTFBP, 1), new Object[] {
            itemTFBPDesertification
        });
        ModLoader.AddShapelessRecipe(new ItemStack(itemTFBP, 1), new Object[] {
            itemTFBPIrrigation
        });
        ModLoader.AddShapelessRecipe(new ItemStack(itemTFBP, 1), new Object[] {
            itemTFBPChilling
        });
        ModLoader.AddShapelessRecipe(new ItemStack(itemTFBP, 1), new Object[] {
            itemTFBPFlatification
        });
    }

    private static void registerBlueDyeCraftingRecipes(ItemStack itemstack)
    {
        ModLoader.AddShapelessRecipe(new ItemStack(itemToolPainterBlue, 1), new Object[] {
            itemToolPainter, itemstack
        });
    }

    private static void registerBronzeIngotCraftingRecipes(ItemStack itemstack)
    {
        ModLoader.AddRecipe(new ItemStack(itemToolBronzePickaxe, 1), new Object[] {
            "BBB", " S ", " S ", Character.valueOf('B'), itemstack, Character.valueOf('S'), Item.STICK
        });
        ModLoader.AddRecipe(new ItemStack(itemToolBronzeAxe, 1), new Object[] {
            "BB", "SB", "S ", Character.valueOf('B'), itemstack, Character.valueOf('S'), Item.STICK
        });
        ModLoader.AddRecipe(new ItemStack(itemToolBronzeHoe, 1), new Object[] {
            "BB", "S ", "S ", Character.valueOf('B'), itemstack, Character.valueOf('S'), Item.STICK
        });
        ModLoader.AddRecipe(new ItemStack(itemToolBronzeSword, 1), new Object[] {
            "B", "B", "S", Character.valueOf('B'), itemstack, Character.valueOf('S'), Item.STICK
        });
        ModLoader.AddRecipe(new ItemStack(itemToolBronzeSpade, 1), new Object[] {
            " B ", " S ", " S ", Character.valueOf('B'), itemstack, Character.valueOf('S'), Item.STICK
        });
        ModLoader.AddRecipe(new ItemStack(itemArmorBronzeHelmet, 1), new Object[] {
            "BBB", "B B", Character.valueOf('B'), itemstack
        });
        ModLoader.AddRecipe(new ItemStack(itemArmorBronzeChestplate, 1), new Object[] {
            "B B", "BBB", "BBB", Character.valueOf('B'), itemstack
        });
        ModLoader.AddRecipe(new ItemStack(itemArmorBronzeLegs, 1), new Object[] {
            "BBB", "B B", "B B", Character.valueOf('B'), itemstack
        });
        ModLoader.AddRecipe(new ItemStack(itemArmorBronzeBoots, 1), new Object[] {
            "B B", "B B", Character.valueOf('B'), itemstack
        });
        ModLoader.AddRecipe(new ItemStack(itemToolWrench, 1), new Object[] {
            "B B", "BBB", " B ", Character.valueOf('B'), itemstack
        });
        ModLoader.AddRecipe(new ItemStack(blockMetal, 1, 2), new Object[] {
            "MMM", "MMM", "MMM", Character.valueOf('M'), itemstack
        });
        if(enableCraftingRail)
        {
            ModLoader.AddRecipe(new ItemStack(Block.RAILS, 8), new Object[] {
                "B B", "BsB", "B B", Character.valueOf('B'), itemstack, Character.valueOf('s'), Item.STICK
            });
        }
        Iterator iterator = MinecraftForge.generateRecipes(new Object[] {
            "III", "BBB", "TTT", Character.valueOf('I'), MinecraftForge.getOreClass("ingotRefinedIron"), Character.valueOf('B'), itemstack, Character.valueOf('T'), MinecraftForge.getOreClass("ingotTin")
        }).iterator();
        do
        {
            if(!iterator.hasNext())
            {
                break;
            }
            Object aobj[] = (Object[])iterator.next();
            if(aobj[4] == null || aobj[8] == null)
            {
                break;
            }
            ModLoader.AddRecipe(new ItemStack(itemIngotAlloy, 2), aobj);
        } while(true);
    }

    private static void registerCopperIngotCraftingRecipes(ItemStack itemstack)
    {
        ModLoader.AddRecipe(new ItemStack(blockElectric, 1, 3), new Object[] {
            "PCP", "ccc", "PCP", Character.valueOf('P'), Block.WOOD, Character.valueOf('C'), new ItemStack(itemCable, 1, 0), Character.valueOf('c'), itemstack
        });
        ModLoader.AddRecipe(new ItemStack(blockMachine, 1, 13), new Object[] {
            "CCC", "CFC", "CMC", Character.valueOf('C'), itemstack, Character.valueOf('F'), new ItemStack(blockMachine, 1, 2), Character.valueOf('M'), new ItemStack(blockMachine, 1, 12)
        });
        ModLoader.AddRecipe(new ItemStack(blockMetal, 1, 0), new Object[] {
            "MMM", "MMM", "MMM", Character.valueOf('M'), itemstack
        });
        ModLoader.AddRecipe(new ItemStack(itemCable, 6, 0), new Object[] {
            "RRR", "CCC", "RRR", Character.valueOf('C'), itemstack, Character.valueOf('R'), itemRubber
        });
        ModLoader.AddRecipe(new ItemStack(itemCable, 6, 0), new Object[] {
            "RCR", "RCR", "RCR", Character.valueOf('C'), itemstack, Character.valueOf('R'), itemRubber
        });
        ModLoader.AddRecipe(new ItemStack(itemCable, 6, 1), new Object[] {
            "CCC", Character.valueOf('C'), itemstack
        });
        ModLoader.AddRecipe(new ItemStack(itemReactorCooler, 1), new Object[] {
            "ici", "CPC", Character.valueOf('C'), itemCellCoolant, Character.valueOf('c'), itemPartCircuitAdv, Character.valueOf('i'), itemstack, Character.valueOf('P'), itemReactorPlating
        });
        ModLoader.AddRecipe(new ItemStack(itemReactorPlating, 1), new Object[] {
            " C ", "CAC", " C ", Character.valueOf('C'), itemstack, Character.valueOf('A'), itemPartAlloy
        });
    }

    private static void registerRefinedIronCraftingRecipes(ItemStack itemstack)
    {
        ModLoader.AddRecipe(new ItemStack(itemCable, 12, 5), new Object[] {
            "III", Character.valueOf('I'), itemstack
        });
        ModLoader.AddRecipe(new ItemStack(itemCable, 4, 6), new Object[] {
            " R ", "RIR", " R ", Character.valueOf('I'), itemstack, Character.valueOf('R'), itemRubber
        });
        ModLoader.AddRecipe(new ItemStack(itemToolCutter, 1), new Object[] {
            "A A", " A ", "I I", Character.valueOf('A'), itemstack, Character.valueOf('I'), Item.IRON_INGOT
        });
        ModLoader.AddRecipe(new ItemStack(itemPartCircuit, 1), new Object[] {
            "CCC", "RIR", "CCC", Character.valueOf('I'), itemstack, Character.valueOf('R'), Item.REDSTONE, Character.valueOf('C'), new ItemStack(itemCable, 1, 0)
        });
        ModLoader.AddRecipe(new ItemStack(itemPartCircuit, 1), new Object[] {
            "CRC", "CIC", "CRC", Character.valueOf('I'), itemstack, Character.valueOf('R'), Item.REDSTONE, Character.valueOf('C'), new ItemStack(itemCable, 1, 0)
        });
        ModLoader.AddRecipe(new ItemStack(blockMachine, 1, 0), new Object[] {
            "III", "I I", "III", Character.valueOf('I'), itemstack
        });
        ModLoader.AddRecipe(new ItemStack(blockMachine, 1, 11), new Object[] {
            " G ", "DMD", "IDI", Character.valueOf('D'), Block.DIRT, Character.valueOf('G'), Item.GLOWSTONE_DUST, Character.valueOf('M'), new ItemStack(blockMachine, 1, 5), Character.valueOf('I'), 
            itemstack
        });
        ModLoader.AddRecipe(new ItemStack(itemToolDrill, 1, 201), new Object[] {
            " I ", "ICI", "IBI", Character.valueOf('I'), itemstack, Character.valueOf('B'), itemBatREDischarged, Character.valueOf('C'), itemPartCircuit
        });
        ModLoader.AddRecipe(new ItemStack(itemToolDrill, 1, 1), new Object[] {
            " I ", "ICI", "IBI", Character.valueOf('I'), itemstack, Character.valueOf('B'), new ItemStack(itemBatRE, 1, 1), Character.valueOf('C'), itemPartCircuit
        });
        ModLoader.AddRecipe(new ItemStack(itemToolChainsaw, 1, 201), new Object[] {
            " II", "ICI", "BI ", Character.valueOf('I'), itemstack, Character.valueOf('B'), itemBatREDischarged, Character.valueOf('C'), itemPartCircuit
        });
        ModLoader.AddRecipe(new ItemStack(itemToolChainsaw, 1, 1), new Object[] {
            " II", "ICI", "BI ", Character.valueOf('I'), itemstack, Character.valueOf('B'), new ItemStack(itemBatRE, 1, 1), Character.valueOf('C'), itemPartCircuit
        });
        ModLoader.AddRecipe(new ItemStack(blockMiningPipe, 8), new Object[] {
            "I I", "I I", "ITI", Character.valueOf('I'), itemstack, Character.valueOf('T'), itemTreetap
        });
        ModLoader.AddRecipe(new ItemStack(blockFenceIron, 12), new Object[] {
            "III", "III", Character.valueOf('I'), itemstack
        });
        ModLoader.AddRecipe(new ItemStack(blockGenerator, 1, 0), new Object[] {
            " B ", "III", " F ", Character.valueOf('B'), itemBatREDischarged, Character.valueOf('F'), new ItemStack(blockMachine, 1, 1), Character.valueOf('I'), itemstack
        });
        ModLoader.AddRecipe(new ItemStack(blockGenerator, 1, 0), new Object[] {
            " B ", "III", " F ", Character.valueOf('B'), new ItemStack(itemBatRE, 1, 1), Character.valueOf('F'), new ItemStack(blockMachine, 1, 1), Character.valueOf('I'), itemstack
        });
        if(energyGeneratorGeo > 0)
        {
            ModLoader.AddRecipe(new ItemStack(blockGenerator, 1, 1), new Object[] {
                "gCg", "gCg", "IGI", Character.valueOf('G'), new ItemStack(blockGenerator, 1, 0), Character.valueOf('C'), itemCellEmpty, Character.valueOf('g'), Block.GLASS, Character.valueOf('I'), 
                itemstack
            });
        }
        ModLoader.AddRecipe(new ItemStack(itemArmorJetpack, 1, 18001), new Object[] {
            "ICI", "IFI", "R R", Character.valueOf('I'), itemstack, Character.valueOf('C'), itemPartCircuit, Character.valueOf('F'), itemFuelCanEmpty, Character.valueOf('R'), 
            Item.REDSTONE
        });
        ModLoader.AddRecipe(new ItemStack(itemArmorJetpackElectric, 1, 10001), new Object[] {
            "ICI", "IBI", "G G", Character.valueOf('I'), itemstack, Character.valueOf('C'), itemPartCircuitAdv, Character.valueOf('B'), new ItemStack(blockElectric, 1, 0), Character.valueOf('G'), 
            Item.GLOWSTONE_DUST
        });
        if(enableCraftingCoin)
        {
            ModLoader.AddRecipe(new ItemStack(itemCoin, 16), new Object[] {
                "II", "II", Character.valueOf('I'), itemstack
            });
        }
        ModLoader.AddRecipe(new ItemStack(blockMachine2, 1, 1), new Object[] {
            "RRR", "RMR", "ICI", Character.valueOf('M'), new ItemStack(blockElectric, 1, 4), Character.valueOf('R'), Item.REDSTONE, Character.valueOf('C'), itemPartCircuit, Character.valueOf('I'), 
            itemstack
        });
        ModLoader.AddRecipe(new ItemStack(blockLuminatorDark, 8), new Object[] {
            "ICI", "GTG", "GGG", Character.valueOf('G'), Block.GLASS, Character.valueOf('I'), itemstack, Character.valueOf('T'), new ItemStack(itemCable, 1, 10), Character.valueOf('C'), 
            itemCable
        });
        Iterator iterator = MinecraftForge.generateRecipes(new Object[] {
            "III", "BBB", "TTT", Character.valueOf('I'), itemstack, Character.valueOf('B'), MinecraftForge.getOreClass("ingotBronze"), Character.valueOf('T'), MinecraftForge.getOreClass("ingotTin")
        }).iterator();
        do
        {
            if(!iterator.hasNext())
            {
                break;
            }
            Object aobj[] = (Object[])iterator.next();
            if(aobj[6] == null || aobj[8] == null)
            {
                break;
            }
            ModLoader.AddRecipe(new ItemStack(itemIngotAlloy, 2), aobj);
        } while(true);
    }

    private static void registerSilverIngotCraftingRecipes(ItemStack itemstack)
    {
        ModLoader.AddRecipe(new ItemStack(itemCable, 6, 9), new Object[] {
            "GGG", "SDS", "GGG", Character.valueOf('G'), Block.GLASS, Character.valueOf('S'), itemstack, Character.valueOf('R'), Item.REDSTONE, Character.valueOf('D'), 
            Item.DIAMOND
        });
        ModLoader.AddRecipe(new ItemStack(itemCable, 6, 9), new Object[] {
            "GGG", "SDS", "GGG", Character.valueOf('G'), Block.GLASS, Character.valueOf('S'), itemstack, Character.valueOf('R'), Item.REDSTONE, Character.valueOf('D'), 
            itemPartIndustrialDiamond
        });
    }

    private static void registerTinIngotCraftingRecipes(ItemStack itemstack)
    {
        ModLoader.AddRecipe(new ItemStack(blockMachine, 1, 6), new Object[] {
            "TCT", "TMT", "TTT", Character.valueOf('T'), itemstack, Character.valueOf('M'), new ItemStack(blockMachine, 1, 0), Character.valueOf('C'), itemPartCircuit
        });
        ModLoader.AddRecipe(new ItemStack(blockMetal, 1, 1), new Object[] {
            "MMM", "MMM", "MMM", Character.valueOf('M'), itemstack
        });
        ModLoader.AddRecipe(new ItemStack(itemArmorBatpack, 1, 1), new Object[] {
            "BCB", "BTB", "B B", Character.valueOf('T'), itemstack, Character.valueOf('C'), itemPartCircuit, Character.valueOf('B'), new ItemStack(itemBatRE, 1, 1)
        });
        ModLoader.AddRecipe(new ItemStack(itemArmorBatpack, 1, 30001), new Object[] {
            "BCB", "BTB", "B B", Character.valueOf('T'), itemstack, Character.valueOf('C'), itemPartCircuit, Character.valueOf('B'), itemBatREDischarged
        });
        ModLoader.AddRecipe(new ItemStack(itemBatREDischarged, 1, 0), new Object[] {
            " C ", "TRT", "TRT", Character.valueOf('T'), itemstack, Character.valueOf('R'), Item.REDSTONE, Character.valueOf('C'), new ItemStack(itemCable, 1, 0)
        });
        if(enableCraftingBucket)
        {
            ModLoader.AddRecipe(new ItemStack(Item.BUCKET, 1), new Object[] {
                "T T", " T ", Character.valueOf('T'), itemstack
            });
        }
        ModLoader.AddRecipe(new ItemStack(itemCable, 9, 10), new Object[] {
            "TTT", Character.valueOf('T'), itemstack
        });
        ModLoader.AddRecipe(new ItemStack(itemCellEmpty, 16), new Object[] {
            " T ", "T T", " T ", Character.valueOf('T'), itemstack
        });
        ModLoader.AddRecipe(new ItemStack(itemFuelCanEmpty, 1), new Object[] {
            " TT", "T T", "TTT", Character.valueOf('T'), itemstack
        });
        ModLoader.AddRecipe(new ItemStack(itemRemote, 1), new Object[] {
            " C ", "TLT", " F ", Character.valueOf('C'), new ItemStack(itemCable, 1, 0), Character.valueOf('F'), itemFreq, Character.valueOf('L'), new ItemStack(Item.INK_SACK, 1, 4), Character.valueOf('T'), 
            itemstack
        });
        ModLoader.AddRecipe(new ItemStack(itemTinCan, 4), new Object[] {
            "T T", "TTT", Character.valueOf('T'), itemstack
        });
        ModLoader.AddRecipe(new ItemStack(itemArmorCFPack, 1, 129), new Object[] {
            "SCS", "FTF", "F F", Character.valueOf('T'), itemstack, Character.valueOf('C'), itemPartCircuit, Character.valueOf('F'), new ItemStack(itemFuelCanEmpty), Character.valueOf('S'), 
            itemFoamSprayer
        });
        Iterator iterator = MinecraftForge.generateRecipes(new Object[] {
            "III", "BBB", "TTT", Character.valueOf('I'), MinecraftForge.getOreClass("ingotRefinedIron"), Character.valueOf('B'), MinecraftForge.getOreClass("ingotBronze"), Character.valueOf('T'), itemstack
        }).iterator();
        do
        {
            if(!iterator.hasNext())
            {
                break;
            }
            Object aobj[] = (Object[])iterator.next();
            if(aobj[4] == null || aobj[6] == null)
            {
                break;
            }
            ModLoader.AddRecipe(new ItemStack(itemIngotAlloy, 2), aobj);
        } while(true);
    }

    public static void registerUraniumIngotCraftingRecipes(ItemStack itemstack)
    {
        ModLoader.AddRecipe(new ItemStack(itemCellUranEmpty, 8), new Object[] {
            "CCC", "CUC", "CCC", Character.valueOf('C'), itemCellEmpty, Character.valueOf('U'), itemstack
        });
        ModLoader.AddShapelessRecipe(new ItemStack(itemCellUran, 1), new Object[] {
            new ItemStack(itemCellEmpty), itemstack
        });
        ModLoader.AddRecipe(new ItemStack(blockMetal, 1, 3), new Object[] {
            "MMM", "MMM", "MMM", Character.valueOf('M'), itemstack
        });
        if(enableCraftingNuke)
        {
            ModLoader.AddRecipe(new ItemStack(blockNuke, 1), new Object[] {
                "GUG", "UGU", "GUG", Character.valueOf('G'), Item.SULPHUR, Character.valueOf('U'), itemstack
            });
        }
    }

    public void GenerateSurface(World world, Random random1, int i, int j)
    {
        BiomeBase biomegenbase = world.getWorldChunkManager().getBiome(i + 16, j + 16);
        int k = 0;
        if(biomegenbase == BiomeBase.TAIGA)
        {
            k += random1.nextInt(3);
        }
        if(biomegenbase == BiomeBase.FOREST)
        {
            k += random1.nextInt(5) + 1;
        }
        if(biomegenbase == BiomeBase.SWAMPLAND)
        {
            k += random1.nextInt(10) + 5;
        }
        if(random1.nextInt(100) + 1 <= k * 2)
        {
            (new WorldGenRubTree()).a(world, random1, i + random1.nextInt(16), k, j + random1.nextInt(16));
        }
        for(int l = 0; l < 15; l++)
        {
            int k1 = i + random1.nextInt(16);
            int j2 = random1.nextInt(40) + random1.nextInt(20) + 10;
            int i3 = j + random1.nextInt(16);
            (new WorldGenMinable(blockOreCopper.id, 10)).a(world, random1, k1, j2, i3);
        }

        for(int i1 = 0; i1 < 25; i1++)
        {
            int l1 = i + random1.nextInt(16);
            int k2 = random1.nextInt(40);
            int j3 = j + random1.nextInt(16);
            (new WorldGenMinable(blockOreTin.id, 6)).a(world, random1, l1, k2, j3);
        }

        for(int j1 = 0; j1 < 20; j1++)
        {
            int i2 = i + random1.nextInt(16);
            int l2 = random1.nextInt(64);
            int k3 = j + random1.nextInt(16);
            (new WorldGenMinable(blockOreUran.id, 3)).a(world, random1, i2, l2, k3);
        }

    }

    public static boolean OnTickInGame(List list, World world)
    {
        EntityPlayer entityplayer;
        for(Iterator iterator = list.iterator(); iterator.hasNext(); ItemNanoSaber.timedLoss(entityplayer))
        {
            entityplayer = (EntityPlayer)iterator.next();
            if(entityplayer.inventory.armor[2] != null && (entityplayer.inventory.armor[2].getItem() instanceof IItemTickListener))
            {
                ((IItemTickListener)entityplayer.inventory.armor[2].getItem()).onTick(entityplayer, entityplayer.inventory.armor[2]);
            }
            if(entityplayer != null && entityplayer.inventory.armor[0] != null && entityplayer.inventory.armor[0].id == itemArmorRubBoots.id)
            {
                ItemArmorRubBoots.absorbFalling(entityplayer);
            }
            ItemArmorQuantumSuit.performQuantum(entityplayer);
        }

        if(globalTicker % 128 == 0)
        {
            updateWind();
        }
        globalTicker++;
        AudioManager.onTick();
        EnergyNet.onTick();
        return true;
    }

    public static void updateWind()
    {
        int i = 10;
        int j = 10;
        if(windStrength > 20)
        {
            i -= windStrength - 20;
        }
        if(windStrength < 10)
        {
            j -= 10 - windStrength;
        }
        if(random.nextInt(100) <= i)
        {
            windStrength++;
            return;
        }
        if(random.nextInt(100) <= j)
        {
            windStrength--;
            return;
        } else
        {
            return;
        }
    }

    public static boolean getPlayerIsJumping()
    {
        return Platform.getPlayerInstance().aC;
    }

    public static int getidFor(String s, int i)
    {
        if(config == null)
        {
            return i;
        }
        try
        {
            return (new Integer(config.getOrCreateIntProperty(s, 1, i).value)).intValue();
        }
        catch(Exception exception)
        {
            System.out.println((new StringBuilder()).append("[IndustrialCraft] Error while trying to access ID-List, config wasn't loaded properly!").toString());
        }
        return i;
    }

    public static int getItemIdFor(String s, int i)
    {
        if(config == null)
        {
            return i;
        }
        try
        {
            return (new Integer(config.getOrCreateIntProperty(s, 2, i).value)).intValue();
        }
        catch(Exception exception)
        {
            System.out.println((new StringBuilder()).append("[IndustrialCraft] Error while trying to access ID-List, config wasn't loaded properly!").toString());
        }
        return i;
    }

    public static int getGuiIdFor(String s, int i)
    {
        if(config == null)
        {
            return i;
        }
        try
        {
            return (new Integer(config.getOrCreateIntProperty(s, 0, i).value)).intValue();
        }
        catch(Exception exception)
        {
            System.out.println((new StringBuilder()).append("[IndustrialCraft] Error while trying to access ID-List, config wasn't loaded properly!").toString());
        }
        return i;
    }

    public static float getFallDistanceOfEntity(Entity entity)
    {
        return entity.fallDistance;
    }

    public static void setFallDistanceOfEntity(Entity entity, float f)
    {
        entity.fallDistance = f;
    }

    public static boolean getIsJumpingOfEntityLiving(EntityLiving entityliving)
    {
        return entityliving.aC;
    }

    public static void setIsJumpingOfEntityLiving(EntityLiving entityliving, boolean flag)
    {
        entityliving.aC = flag;
    }

    public static void explodeMachineAt(World world, int i, int j, int k)
    {
        world.setTypeId(i, j, k, 0);
        ExplosionIC2 explosionic2 = new ExplosionIC2(world, null, 0.5D + (double)i, 0.5D + (double)j, 0.5D + (double)k, 2.5F, 0.75F, 0.75F);
        explosionic2.doExplosion();
    }

    public boolean DispenseEntity(World world, double d, double d1, double d2, 
            int i, int j, ItemStack itemstack)
    {
        if(itemstack.getItem().id == itemScrapBox.id)
        {
            EntityItem entityitem = new EntityItem(world, d, d1 - 0.29999999999999999D, d2, ((ItemScrapbox)itemScrapBox).getDrop(world));
            double d3 = random.nextDouble() * 0.10000000000000001D + 0.20000000000000001D;
            entityitem.motX = (double)i * d3;
            entityitem.motY = 0.20000000298023221D;
            entityitem.motZ = (double)j * d3;
            entityitem.motX += random.nextGaussian() * 0.0074999998323619366D * 6D;
            entityitem.motY += random.nextGaussian() * 0.0074999998323619366D * 6D;
            entityitem.motZ += random.nextGaussian() * 0.0074999998323619366D * 6D;
            world.playerJoinedWorld(entityitem);
            // TODO SOUND world.playAuxSFX(1000, (int)d, (int)d1, (int)d2, 0);
            return true;
        } else
        {
            return false;
        }
    }

    public static void addValuableOre(int i, int j)
    {
        addValuableOre(i, -1, j);
    }

    public static void addValuableOre(int i, int j, int k)
    {
        if(valuableOres.containsKey(Integer.valueOf(i)))
        {
            Map map = (Map)valuableOres.get(Integer.valueOf(i));
            if(map.containsKey(Integer.valueOf(-1)))
            {
                return;
            }
            if(j == -1)
            {
                map.clear();
                map.put(Integer.valueOf(-1), Integer.valueOf(k));
            } else
            {
                map.put(Integer.valueOf(j), Integer.valueOf(k));
            }
        } else
        {
            TreeMap treemap = new TreeMap();
            treemap.put(Integer.valueOf(j), Integer.valueOf(k));
            valuableOres.put(Integer.valueOf(i), treemap);
        }
    }

    private static String getValuableOreString()
    {
        StringBuilder stringbuilder = new StringBuilder();
        boolean flag = true;
        for(Iterator iterator = valuableOres.entrySet().iterator(); iterator.hasNext();)
        {
            java.util.Map.Entry entry = (java.util.Map.Entry)iterator.next();
            Iterator iterator1 = ((Map)entry.getValue()).entrySet().iterator();
            while(iterator1.hasNext()) 
            {
                java.util.Map.Entry entry1 = (java.util.Map.Entry)iterator1.next();
                if(flag)
                {
                    flag = false;
                } else
                {
                    stringbuilder.append(", ");
                }
                stringbuilder.append(entry.getKey());
                if(((Integer)entry1.getKey()).intValue() != -1)
                {
                    stringbuilder.append("-");
                    stringbuilder.append(entry1.getKey());
                }
                stringbuilder.append(":");
                stringbuilder.append(entry1.getValue());
            }
        }

        return stringbuilder.toString();
    }

    private static void setValuableOreFromString(String s)
    {
        valuableOres.clear();
        String as[] = s.trim().split("\\s*,\\s*");
        String as1[] = as;
        int i = as1.length;
        for(int j = 0; j < i; j++)
        {
            String s1 = as1[j];
            String as2[] = s1.split("\\s*:\\s*");
            if(as2.length == 0)
            {
                continue;
            }
            String as3[] = as2[0].split("\\s*-\\s*");
            if(as3.length == 0)
            {
                continue;
            }
            int k = Integer.parseInt(as3[0]);
            int l = -1;
            int i1 = 1;
            if(as3.length == 2)
            {
                l = Integer.parseInt(as3[1]);
            }
            if(as2.length == 2)
            {
                i1 = Integer.parseInt(as2[1]);
            }
            addValuableOre(k, l, i1);
        }

    }

    public static boolean shallApplyQuantumSpeed(EntityPlayer entityplayer)
    {
        if(enableQuantumSpeedOnSprint)
        {
            return true; //todo ok
        } else
        {
            return Platform.isKeyDownSuitActivate(entityplayer);
        }
    }

    public String Version()
    {
        return "v1.337b";
    }

    public static Configuration config;
    public static Block blockMachine = new BlockMachine(getidFor("blockMachine", 250));
    public static Block blockOreCopper = (new BlockTex(getidFor("blockOreCopper", 249), 32, Material.STONE)).c(3F).b(5F).a("blockOreCopper");
    public static Block blockOreTin = (new BlockTex(getidFor("blockOreTin", 248), 33, Material.STONE)).c(3F).b(5F).a("blockOreTin");
    public static Block blockOreUran = (new BlockTex(getidFor("blockOreUran", 247), 34, Material.STONE)).c(4F).b(6F).a("blockOreUran");
    public static Block blockRubWood = (new BlockRubWood(getidFor("blockRubWood", 243))).c(1.0F).a(Block.e).a("blockRubWood");
    public static Block blockRubLeaves = (new BlockRubLeaves(getidFor("blockRubLeaves", 242))).c(0.2F).f(1).a(Block.g).a("leaves").n();
    public static Block blockRubSapling = (new BlockRubSapling(getidFor("blockRubSapling", 241), 38)).c(0.0F).a(Block.g).a("blockRubSapling");
    public static Block blockHarz = (new BlockResin(getidFor("blockHarz", 240), 43)).c(1.6F).b(0.5F).a(Block.l).a("blockHarz");
    public static Block blockITNT = (new BlockITNT(getidFor("blockITNT", 239), 58, true)).c(0.0F).a(Block.g).a("blockITNT");
    public static Block blockNuke = (new BlockITNT(getidFor("blockNuke", 237), 61, false)).c(0.0F).a(Block.g).a("blockNuke");
    public static Block blockDynamite = (new BlockDynamite(getidFor("blockDynamite", 236), 57)).c(0.0F).a(Block.g).a("blockDynamite");
    public static Block blockDynamiteRemote = (new BlockDynamite(getidFor("blockDynamiteRemote", 235), 56)).c(0.0F).a(Block.g).a("blockDynamiteRemote");
    public static Block blockRubber = (new BlockRubberSheet(getidFor("blockRubber", 234), 40)).c(0.8F).b(2.0F).a(Block.k).a("blockRubber");
    public static Block blockReactorChamber = (new BlockReactorChamber(getidFor("blockReactorChamber", 233))).c(3F).a(Block.i).a("blockReactorChamber");
    public static Block blockFenceIron = (new BlockPoleFence(getidFor("blockFenceIron", 232), 1, Material.ORE)).c(1.5F).b(5F).a(Block.i).a("blockFenceIron");
    public static Block blockAlloy = (new BlockTex(getidFor("blockAlloy", 231), 12, Material.ORE)).c(4F).b(150F).a(Block.i).a("blockAlloy");
    public static Block blockAlloyGlass = (new BlockTexGlass(getidFor("blockAlloyGlass", 230), 13, Material.CORAL, false)).c(1.0F).b(150F).a(Block.j).a("blockAlloyGlass");
    public static Block blockDoorAlloy = (new BlockIC2Door(getidFor("blockDoorAlloy", 229), 14, 15, Material.ORE)).c(10F).b(150F).a(Block.i).a("blockDoorAlloy").n();//.setRequiresSelfNotify();
    public static Block blockMetal = (new BlockMetal(getidFor("blockMetal", 224), Material.ORE)).c(2.0F).a(Block.i);
    public static Block blockFoam = (new BlockFoam(getidFor("blockFoam", 222), 37, Material.CLOTH)).c(0.01F).b(10F).a("blockFoam").a(Block.k);
    public static Block blockWall = (new BlockWall(getidFor("blockWall", 221), 96, Material.STONE)).c(1.5F).b(60F).a("blockWall").a(Block.h);
    public static Block blockScaffold = (new BlockScaffold(getidFor("blockScaffold", 220))).c(0.5F).b(0.2F).a("blockScaffold").a(Block.e);
    public static Item itemFuelCanEmpty = (new ItemIC2(getItemIdFor("itemFuelCanEmpty", 29975), 51)).a("itemFuelCanEmpty");
    public static Item itemFuelCan = (new ItemIC2(getItemIdFor("itemFuelCan", 29976), 52)).a("itemFuelCan").c(1).a(itemFuelCanEmpty);
    public static Item itemToolBronzePickaxe = (new ItemIC2Pickaxe(getItemIdFor("itemToolBronzePickaxe", 29944), 80, EnumToolMaterial.IRON, 5F)).a("itemToolBronzePickaxe").d(350);
    public static Item itemToolBronzeAxe = (new ItemIC2Axe(getItemIdFor("itemToolBronzeAxe", 29943), 81, EnumToolMaterial.IRON, 5F)).a("itemToolBronzeAxe").d(350);
    public static Item itemToolBronzeSword = (new ItemIC2Sword(getItemIdFor("itemToolBronzeSword", 29942), 82, EnumToolMaterial.IRON, 7)).a("itemToolBronzeSword").d(350);
    public static Item itemToolBronzeSpade = (new ItemIC2Spade(getItemIdFor("itemToolBronzeSpade", 29941), 83, EnumToolMaterial.IRON, 5F)).a("itemToolBronzeSpade").d(350);
    public static Item itemToolBronzeHoe = (new ItemIC2Hoe(getItemIdFor("itemToolBronzeHoe", 29940), 84, EnumToolMaterial.IRON)).a("itemToolBronzeHoe").d(350);
    public static Item itemDoorAlloy = (new ItemIC2Door(getItemIdFor("itemDoorAlloy", 29929), 102, blockDoorAlloy)).a("itemDoorAlloy");
    public static Block blockGenerator = new BlockGenerator(getidFor("blockGenerator", 246));
    public static Block blockMiningPipe = (new BlockMiningPipe(getidFor("blockMiningPipe", 245), 35)).c(6F).b(10F).a("blockMiningPipe");
    public static Block blockMiningTip = (new BlockMiningTip(getidFor("blockMiningTip", 244), 36)).c(6F).b(10F).a("blockMiningTip");
    public static Block blockCable = new BlockCable(getidFor("blockCable", 228));
    public static Block blockElectric = new BlockElectric(getidFor("blockElectric", 227));
    public static Block blockPersonal = (new BlockPersonal(getidFor("blockPersonal", 225))).b(6000000F);
    public static Block blockMachine2 = new BlockMachine2(getidFor("blockMachine2", 223));
    public static Block blockLuminator = (new BlockLuminator(getidFor("blockLuminator", 226), true)).a("blockLuminator").a(1.0F);
    public static Block blockLuminatorDark = (new BlockLuminator(getidFor("blockLuminatorDark", 219), false)).a("blockLuminatorD");
    public static Item itemDustCoal = (new ItemIC2(getItemIdFor("itemDustCoal", 30000), 0)).a("itemDustCoal");
    public static Item itemDustIron = (new ItemIC2(getItemIdFor("itemDustIron", 29999), 1)).a("itemDustIron");
    public static Item itemDustGold = (new ItemIC2(getItemIdFor("itemDustGold", 29998), 2)).a("itemDustGold");
    public static Item itemDustCopper = (new ItemIC2(getItemIdFor("itemDustCopper", 29997), 3)).a("itemDustCopper");
    public static Item itemDustTin = (new ItemIC2(getItemIdFor("itemDustTin", 29996), 4)).a("itemDustTin");
    public static Item itemDustBronze = (new ItemIC2(getItemIdFor("itemDustBronze", 29995), 5)).a("itemDustBronze");
    public static Item itemDustIronSmall = (new ItemIC2(getItemIdFor("itemDustIronSmall", 29994), 6)).a("itemDustIronSmall");
    public static Item itemIngotAdvIron = (new ItemIC2(getItemIdFor("itemIngotAdvIron", 29993), 7)).a("itemIngotAdvIron");
    public static Item itemIngotCopper = (new ItemIC2(getItemIdFor("itemIngotCopper", 29992), 8)).a("itemIngotCopper");
    public static Item itemIngotTin = (new ItemIC2(getItemIdFor("itemIngotTin", 29991), 9)).a("itemIngotTin");
    public static Item itemIngotBronze = (new ItemIC2(getItemIdFor("itemIngotBronze", 29990), 10)).a("itemIngotBronze");
    public static Item itemIngotAlloy = (new ItemIC2(getItemIdFor("itemIngotAlloy", 29989), 11)).a("itemIngotAlloy");
    public static Item itemIngotUran = (new ItemIC2(getItemIdFor("itemIngotUran", 29988), 12)).a("itemIngotUran");
    public static Item itemOreUran = (new ItemIC2(getItemIdFor("itemOreUran", 29987), 13)).a("itemOreUran");
    public static Item itemBatRE = (new ItemBattery(getItemIdFor("itemBatRE", 29986), 16, 1, 100, true, 1)).a("itemBatRE");
    public static Item itemBatCrystal = (new ItemBattery(getItemIdFor("itemBatCrystal", 29985), 21, 10, 250, true, 2)).a("itemBatCrystal");
    public static Item itemBatLamaCrystal = (new ItemBattery(getItemIdFor("itemBatLamaCrystal", 29984), 26, 100, 600, true, 3)).a("itemBatLamaCrystal");
    public static Item itemBatREDischarged = (new ItemBatteryDischarged(getItemIdFor("itemBatREDischarged", 29983), 16, 1, 100, true, 1)).a("itemBatRE");
    public static Item itemBatSU = (new ItemBatterySU(getItemIdFor("itemBatSU", 29982), 31, 1, 1)).a("itemBatSU");
    public static Item itemCellEmpty = (new ItemCell(getItemIdFor("itemCellEmpty", 29981), 32)).a("itemCellEmpty");
    public static Item itemCellLava = (new ItemIC2(getItemIdFor("itemCellLava", 29980), 33)).a("itemCellLava");
    public static Item itemToolDrill = (new ItemElectricToolDrill(getItemIdFor("itemToolDrill", 29979), 48)).a("itemToolDrill");
    public static Item itemToolDDrill = (new ItemElectricToolDDrill(getItemIdFor("itemToolDDrill", 29978), 49)).a("itemToolDDrill");
    public static Item itemToolChainsaw = (new ItemElectricToolChainsaw(getItemIdFor("itemToolChainsaw", 29977), 50)).a("itemToolChainsaw");
    public static Item itemCellCoal = (new ItemIC2(getItemIdFor("itemCellCoal", 29974), 34)).a("itemCellCoal");
    public static Item itemCellBio = (new ItemIC2(getItemIdFor("itemCellBio", 29973), 35)).a("itemCellBio");
    public static Item itemCellCoalRef = (new ItemIC2(getItemIdFor("itemCellCoalRef", 29972), 34)).a("itemCellCoalRef");
    public static Item itemCellBioRef = (new ItemIC2(getItemIdFor("itemCellBioRef", 29971), 35)).a("itemCellBioRef");
    public static Item itemFuelCoalDust = (new ItemIC2(getItemIdFor("itemFuelCoalDust", 29970), 53)).a("itemFuelCoalDust");
    public static Item itemFuelCoalCmpr = (new ItemIC2(getItemIdFor("itemFuelCoalCmpr", 29969), 54)).a("itemFuelCoalCmpr");
    public static Item itemFuelPlantBall = (new ItemIC2(getItemIdFor("itemFuelPlantBall", 29968), 55)).a("itemFuelPlantBall");
    public static Item itemFuelPlantCmpr = (new ItemIC2(getItemIdFor("itemFuelPlantCmpr", 29967), 56)).a("itemFuelPlantCmpr");
    public static Item itemTinCan = (new ItemIC2(getItemIdFor("itemTinCan", 29966), 57)).a("itemTinCan");
    public static Item itemTinCanFilled = (new ItemTinCan(getItemIdFor("itemTinCanFilled", 29965), 58)).a("itemTinCanFilled");
    public static Item itemScanner = (new ItemScanner(getItemIdFor("itemScanner", 29964), 59, 1)).a("itemScanner");
    public static Item itemScannerAdv = (new ItemScannerAdv(getItemIdFor("itemScannerAdv", 29963), 60, 2)).a("itemScannerAdv");
    public static Item itemCellWater = (new ItemIC2(getItemIdFor("itemCellWater", 29962), 37)).a("itemCellWater");
    public static Item itemHarz = (new ItemResin(getItemIdFor("itemHarz", 29961), 64)).a("itemHarz");
    public static Item itemRubber = (new ItemIC2(getItemIdFor("itemRubber", 29960), 65)).a("itemRubber");
    public static Item itemDynamite = (new ItemDynamite(getItemIdFor("itemDynamite", 29959), 62, false)).a("itemDynamite");
    public static Item itemDynamiteSticky = (new ItemDynamite(getItemIdFor("itemDynamiteSticky", 29958), 63, true)).a("itemDynamiteSticky");
    public static Item itemRemote = (new ItemRemote(getItemIdFor("itemRemote", 29957), 61)).a("itemRemote");
    public static Item itemTreetap = (new ItemTreetap(getItemIdFor("itemTreetap", 29956), 66)).a("itemTreetap");
    public static Item itemArmorRubBoots = (new ItemArmorRubBoots(getItemIdFor("itemArmorRubBoots", 29955), 67, ModLoader.AddArmor("ic2/rubber"))).a("itemArmorRubBoots");
    public static Item itemArmorJetpack = (new ItemArmorJetpack(getItemIdFor("itemArmorJetpack", 29954), 68, ModLoader.AddArmor("ic2/jetpack"))).a("itemArmorJetpack");
    public static Item itemArmorJetpackElectric = (new ItemArmorJetpackElectric(getItemIdFor("itemArmorJetpackElectric", 29953), 69, ModLoader.AddArmor("ic2/jetpack"))).a("itemArmorJetpackElectric");
    public static Item itemToolMiningLaser = (new ItemToolMiningLaser(getItemIdFor("itemToolMiningLaser", 29952), 70)).a("itemToolMiningLaser");
    public static Item itemCellUran = (new ItemGradual(getItemIdFor("itemCellUran", 29951), 38)).a("itemCellUran");
    public static Item itemCellCoolant = (new ItemGradual(getItemIdFor("itemCellCoolant", 29950), 39)).a("itemCellCoolant");
    public static Item itemReactorPlating = (new ItemGradual(getItemIdFor("itemReactorPlating", 29949), 71)).a("itemReactorPlating");
    public static Item itemReactorCooler = (new ItemGradual(getItemIdFor("itemReactorCooler", 29948), 72)).a("itemReactorCooler");
    public static Item itemCellUranDepleted = (new ItemGradual(getItemIdFor("itemCellUranDepleted", 29947), 40)).a("itemCellUranDepleted");
    public static Item itemCellUranEnriched = (new ItemIC2(getItemIdFor("itemCellUranEnriched", 29946), 41)).a("itemCellUranEnriched");
    public static Item itemCellUranEmpty = (new ItemIC2(getItemIdFor("itemCellUranEmpty", 29945), 42)).a("itemCellUranEmpty");
    public static Item itemArmorBronzeHelmet = (new ItemArmorIC2(getItemIdFor("itemArmorBronzeHelmet", 29939), 85, 2, ModLoader.AddArmor("ic2/bronze"), 0, 15)).a("itemArmorBronzeHelmet");
    public static Item itemArmorBronzeChestplate = (new ItemArmorIC2(getItemIdFor("itemArmorBronzeChestplate", 29938), 86, 2, ModLoader.AddArmor("ic2/bronze"), 1, 15)).a("itemArmorBronzeChestplate");
    public static Item itemArmorBronzeLegs = (new ItemArmorIC2(getItemIdFor("itemArmorBronzeLegs", 29937), 87, 2, ModLoader.AddArmor("ic2/bronze"), 2, 15)).a("itemArmorBronzeLegs");
    public static Item itemArmorBronzeBoots = (new ItemArmorIC2(getItemIdFor("itemArmorBronzeBoots", 29936), 88, 2, ModLoader.AddArmor("ic2/bronze"), 3, 15)).a("itemArmorBronzeBoots");
    public static Item itemPartCircuit = (new ItemIC2(getItemIdFor("itemPartCircuit", 29935), 96)).a("itemPartCircuit");
    public static Item itemPartCircuitAdv = (new ItemIC2(getItemIdFor("itemPartCircuitAdv", 29934), 97)).a("itemPartCircuitAdv");
    public static Item itemScrap = (new ItemIC2(getItemIdFor("itemScrap", 29933), 98)).a("itemScrap");
    public static Item itemMatter = (new ItemIC2(getItemIdFor("itemMatter", 29932), 99)).a("itemMatter");
    public static Item itemPartAlloy = (new ItemIC2(getItemIdFor("itemPartAlloy", 29931), 100)).a("itemPartAlloy");
    public static Item itemCoin = (new ItemIC2(getItemIdFor("itemCoin", 29930), 101)).a("itemCoin");
    public static Item itemCable = new ItemCable(getItemIdFor("itemCable", 29928), 112);
    public static Item itemToolWrench = (new ItemToolWrench(getItemIdFor("itemToolWrench", 29927), 89)).a("itemToolWrench");
    public static Item itemToolMeter = (new ItemToolMeter(getItemIdFor("itemToolMEter", 29926), 90)).a("itemToolMeter");
    public static Item itemCellWaterElectro = (new ItemIC2(getItemIdFor("itemCellWaterElectro", 29925), 43)).a("itemCellWaterElectro");
    public static Item itemArmorBatpack = (new ItemArmorBatpack(getItemIdFor("itemArmorBatpack", 29924), 73, ModLoader.AddArmor("ic2/batpack"))).a("itemArmorBatpack");
    public static Item itemArmorAlloyChestplate = (new ItemArmorIC2(getItemIdFor("itemArmorAlloyChestplate", 29923), 103, 2, ModLoader.AddArmor("ic2/alloy"), 1, 50)).a("itemArmorAlloyChestplate");
    public static Item itemArmorNanoHelmet = (new ItemArmorNanoSuit(getItemIdFor("itemArmorNanoHelmet", 29922), 104, ModLoader.AddArmor("ic2/nano"), 0)).a("itemArmorNanoHelmet");
    public static Item itemArmorNanoChestplate = (new ItemArmorNanoSuit(getItemIdFor("itemArmorNanoChestplate", 29921), 105, ModLoader.AddArmor("ic2/nano"), 1)).a("itemArmorNanoChestplate");
    public static Item itemArmorNanoLegs = (new ItemArmorNanoSuit(getItemIdFor("itemArmorNanoLegs", 29920), 106, ModLoader.AddArmor("ic2/nano"), 2)).a("itemArmorNanoLegs");
    public static Item itemArmorNanoBoots = (new ItemArmorNanoSuit(getItemIdFor("itemArmorNanoBoots", 29919), 107, ModLoader.AddArmor("ic2/nano"), 3)).a("itemArmorNanoBoots");
    public static Item itemArmorQuantumHelmet = (new ItemArmorQuantumSuit(getItemIdFor("itemArmorQuantumHelmet", 29918), 108, ModLoader.AddArmor("ic2/quantum"), 0)).a("itemArmorQuantumHelmet");
    public static Item itemArmorQuantumChestplate = (new ItemArmorQuantumSuit(getItemIdFor("itemArmorQuantumChestplate", 29917), 109, ModLoader.AddArmor("ic2/quantum"), 1)).a("itemArmorQuantumChestplate");
    public static Item itemArmorQuantumLegs = (new ItemArmorQuantumSuit(getItemIdFor("itemArmorQuantumLegs", 29916), 110, ModLoader.AddArmor("ic2/quantum"), 2)).a("itemArmorQuantumLegs");
    public static Item itemArmorQuantumBoots = (new ItemArmorQuantumSuit(getItemIdFor("itemArmorQuantumBoots", 29915), 111, ModLoader.AddArmor("ic2/quantum"), 3)).a("itemArmorQuantumBoots");
    public static Item itemToolPainter = (new ItemIC2(getItemIdFor("itemToolPainter", 29914), 91)).a("itemToolPainter");
    public static Item itemToolPainterBlack = (new ItemToolPainter(getItemIdFor("itemToolPainterBlack", 29913), 0)).a("itemToolPainter");
    public static Item itemToolPainterRed = (new ItemToolPainter(getItemIdFor("itemToolPainterRed", 29912), 1)).a("itemToolPainter");
    public static Item itemToolPainterGreen = (new ItemToolPainter(getItemIdFor("itemToolPainterGreen", 29911), 2)).a("itemToolPainter");
    public static Item itemToolPainterBrown = (new ItemToolPainter(getItemIdFor("itemToolPainterBrown", 29910), 3)).a("itemToolPainter");
    public static Item itemToolPainterBlue = (new ItemToolPainter(getItemIdFor("itemToolPainterBlue", 29909), 4)).a("itemToolPainter");
    public static Item itemToolPainterPurple = (new ItemToolPainter(getItemIdFor("itemToolPainterPurple", 29908), 5)).a("itemToolPainter");
    public static Item itemToolPainterCyan = (new ItemToolPainter(getItemIdFor("itemToolPainterCyan", 29907), 6)).a("itemToolPainter");
    public static Item itemToolPainterLightGrey = (new ItemToolPainter(getItemIdFor("itemToolPainterLightGrey", 29906), 7)).a("itemToolPainter");
    public static Item itemToolPainterDarkGrey = (new ItemToolPainter(getItemIdFor("itemToolPainterDarkGrey", 29905), 8)).a("itemToolPainter");
    public static Item itemToolPainterPink = (new ItemToolPainter(getItemIdFor("itemToolPainterPink", 29904), 9)).a("itemToolPainter");
    public static Item itemToolPainterLime = (new ItemToolPainter(getItemIdFor("itemToolPainterLime", 29903), 10)).a("itemToolPainter");
    public static Item itemToolPainterYellow = (new ItemToolPainter(getItemIdFor("itemToolPainterYellow", 29902), 11)).a("itemToolPainter");
    public static Item itemToolPainterCloud = (new ItemToolPainter(getItemIdFor("itemToolPainterCloud", 29901), 12)).a("itemToolPainter");
    public static Item itemToolPainterMagenta = (new ItemToolPainter(getItemIdFor("itemToolPainterMagenta", 29900), 13)).a("itemToolPainter");
    public static Item itemToolPainterOrange = (new ItemToolPainter(getItemIdFor("itemToolPainterOrange", 29899), 14)).a("itemToolPainter");
    public static Item itemToolPainterWhite = (new ItemToolPainter(getItemIdFor("itemToolPainterWhite", 29898), 15)).a("itemToolPainter");
    public static Item itemToolCutter = (new ItemToolCutter(getItemIdFor("itemToolCutter", 29897), 92)).a("itemToolCutter");
    public static Item itemPartCarbonFibre = (new ItemIC2(getItemIdFor("itemPartCarbonFibre", 29896), 74)).a("itemPartCarbonFibre");
    public static Item itemPartCarbonMesh = (new ItemIC2(getItemIdFor("itemPartCarbonMesh", 29895), 75)).a("itemPartCarbonMesh");
    public static Item itemPartCarbonPlate = (new ItemIC2(getItemIdFor("itemPartCarbonPlate", 29894), 76)).a("itemPartCarbonPlate");
    public static Item itemNanoSaber = (new ItemNanoSaber(getItemIdFor("itemNanoSaber", 29893), 78, true)).a("itemNanoSaber");
    public static Item itemNanoSaberOff = (new ItemNanoSaber(getItemIdFor("itemNanoSaberOff", 29892), 77, false)).a("itemNanoSaber");
    public static Item itemPartIridium = (new ItemIC2(getItemIdFor("itemPartIridium", 29891), 93)).a("itemPartIridium");
    public static Item itemTFBP = (new ItemIC2(getItemIdFor("itemTFBP", 29890), 144)).a("itemTFBP");
    public static Item itemTFBPCultivation = (new ItemTFBPCultivation(getItemIdFor("itemTFBPCultivation", 29889), 145)).a("itemTFBPCultivation");
    public static Item itemTFBPIrrigation = (new ItemTFBPIrrigation(getItemIdFor("itemTFBPIrrigation", 29888), 146)).a("itemTFBPIrrigation");
    public static Item itemTFBPChilling = (new ItemTFBPChilling(getItemIdFor("itemTFBPChilling", 29887), 147)).a("itemTFBPChilling");
    public static Item itemTFBPDesertification = (new ItemTFBPDesertification(getItemIdFor("itemTFBPDesertification", 29886), 148)).a("itemTFBPDesertification");
    public static Item itemTFBPFlatification = (new ItemTFBPFlatification(getItemIdFor("itemTFBPFlatification", 29885), 149)).a("itemTFBPFlatification");
    public static Item itemToolWrenchElectric = (new ItemToolWrenchElectric(getItemIdFor("itemToolWrenchElectric", 29884), 94)).a("itemToolWrenchElectric");
    public static Item itemScrapBox = (new ItemScrapbox(getItemIdFor("itemScrapbox", 29883), 159)).a("itemScrapbox");
    public static Item itemPartCoalBall = (new ItemIC2(getItemIdFor("itemPartCoalBall", 29882), 158)).a("itemPartCoalBall");
    public static Item itemPartCoalBlock = (new ItemIC2(getItemIdFor("itemPartCoalBlock", 29881), 157)).a("itemPartCoalBlock");
    public static Item itemPartCoalChunk = (new ItemIC2(getItemIdFor("itemPartCoalChunk", 29880), 156)).a("itemPartCoalChunk");
    public static Item itemPartIndustrialDiamond = (new ItemIC2(getItemIdFor("itemPartIndustrialDiamond", 29879), 155)).a("itemPartIndustrialDiamond");
    public static Item itemFreq = (new ItemFrequencyTransmitter(getItemIdFor("itemFreq", 29878), 95)).a("itemFreq").c(1);
    public static Item itemDustClay = (new ItemIC2(getItemIdFor("itemDustClay", 29877), 14)).a("itemDustClay");
    public static Item itemPartPellet = (new ItemIC2(getItemIdFor("itemPartPellet", 29876), 44)).a("itemPartPellet");
    public static Item itemFoamSprayer = (new ItemSprayer(getItemIdFor("itemFoamSprayer", 29875), 45)).a("itemFoamSprayer");
    public static Item itemDustSilver = (new ItemIC2(getItemIdFor("itemDustSilver", 29874), 240)).a("itemDustSilver");
    public static Item itemArmorCFPack = (new ItemArmorCFPack(getItemIdFor("itemArmorCFPack", 29873), 46, ModLoader.AddArmor("ic2/batpack"))).a("itemArmorCFPack");
    public static Item itemOreIridium = (new ItemIC2(getItemIdFor("itemOreIridium", 29872), 151)).a("itemOreIridium");
    public static Item itemArmorLappack = (new ItemArmorLappack(getItemIdFor("itemArmorLappack", 29871), 150, ModLoader.AddArmor("ic2/lappack"))).a("itemArmorLappack");
    public static int guiIdIronFurnace = getGuiIdFor("guiIdIronFurnace", 4);
    public static int guiIdElecFurnace = getGuiIdFor("guiIdElecFurnace", 5);
    public static int guiIdMacerator = getGuiIdFor("guiIdMacerator", 6);
    public static int guiIdExtractor = getGuiIdFor("guiIdExtractor", 7);
    public static int guiIdCompressor = getGuiIdFor("guiIdCompressor", 8);
    public static int guiIdCanner = getGuiIdFor("guiIdCanner", 9);
    public static int guiIdMiner = getGuiIdFor("guiIdMiner", 10);
    public static int guiIdPump = getGuiIdFor("guiIdPump", 11);
    public static int guiIdElectrolyzer = getGuiIdFor("guiIdElectrolyzer", 12);
    public static int guiIdRecycler = getGuiIdFor("guiIdRecycler", 13);
    public static int guiIdInduction = getGuiIdFor("guiIdInduction", 14);
    public static int guiIdMatter = getGuiIdFor("guiIdMatter", 15);
    public static int guiIdElectricBatBox = getGuiIdFor("guiIdElectricBatBox", 16);
    public static int guiIdElectricMFE = getGuiIdFor("guiIdElectricMFE", 17);
    public static int guiIdElectricMFSU = getGuiIdFor("guiIdElectricMFSU", 18);
    public static int guiIdGenerator = getGuiIdFor("guiIdGenerator", 19);
    public static int guiIdGeoGenerator = getGuiIdFor("guiIdGeoGenerator", 20);
    public static int guiIdWaterGenerator = getGuiIdFor("guiIdWaterGenerator", 21);
    public static int guiIdSolarGenerator = getGuiIdFor("guiIdSolarGenerator", 22);
    public static int guiIdWindGenerator = getGuiIdFor("guiIdWindGenerator", 23);
    public static int guiIdTradeOMatOpen = getGuiIdFor("guiIdTradeOMatOpen", 24);
    public static int guiIdTradeOMatClosed = getGuiIdFor("guiIdTradeOMatClosed", 25);
    public static int guiIdNuclearReactor6x3 = getGuiIdFor("guiIdNuclearReactor6x3", 26);
    public static int guiIdNuclearReactor6x4 = getGuiIdFor("guiIdNuclearReactor6x4", 27);
    public static int guiIdNuclearReactor6x5 = getGuiIdFor("guiIdNuclearReactor6x5", 28);
    public static int guiIdNuclearReactor6x6 = getGuiIdFor("guiIdNuclearReactor6x6", 29);
    public static int guiIdNuclearReactor6x7 = getGuiIdFor("guiIdNuclearReactor6x7", 30);
    public static int guiIdNuclearReactor6x8 = getGuiIdFor("guiIdNuclearReactor6x8", 31);
    public static int guiIdNuclearReactor6x9 = getGuiIdFor("guiIdNuclearReactor6x9", 32);
    public static int cableRenderId;
    public static int fenceRenderId;
    public static int miningPipeRenderId;
    public static int luminatorRenderId;
    public static Random random = new Random();
    public static int windStrength;
    public static int globalTicker;
    public static Map valuableOres = new TreeMap();
    public static boolean enableCraftingBucket = true;
    public static boolean enableCraftingCoin = true;
    public static boolean enableCraftingGlowstoneDust = true;
    public static boolean enableCraftingGunpowder = true;
    public static boolean enableCraftingNuke = true;
    public static boolean enableCraftingRail = true;
    public static boolean enableLoggingWrench = true;
    public static boolean enableQuantumSpeedOnSprint = true;
    public static float explosionPowerNuke = 35F;
    public static float explosionPowerReactorMax = 45F;
    public static int energyGeneratorBase = 10;
    public static int energyGeneratorGeo = 20;
    public static int energyGeneratorWater = 100;
    public static int energyGeneratorSolar = 100;
    public static int energyGeneratorWind = 100;
    public static int energyGeneratorNuclear = 10;
    public static boolean suddenlyHoes = false;
    private static OreHandler oreHandler = new OreHandler();
    private static Map playerIsJumping = new HashMap();
    private static boolean silverDustSmeltingRegistered = false;
}
