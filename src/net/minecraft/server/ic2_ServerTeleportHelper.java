// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package net.minecraft.server;

import java.lang.reflect.Method;

// Referenced classes of package net.minecraft.server:
//            PlayerInstance, ServerConfigurationManager, EntityPlayerMP, PlayerManager

public class ic2_ServerTeleportHelper
{

    public ic2_ServerTeleportHelper()
    {
    }

    public static void playerInstanceAddPlayer(EntityPlayer entityplayermp, int i, int j)
    {
        getPlayerInstance(entityplayermp, i, j, true).a(entityplayermp);
    }

    public static void playerInstanceRemovePlayer(EntityPlayer entityplayermp, int i, int j)
    {
        PlayerInstance playerinstance = getPlayerInstance(entityplayermp, i, j, false);
        if(playerinstance != null)
        {
            playerinstance.b(entityplayermp);
        }
    }

    private static PlayerInstance getPlayerInstance(EntityPlayer entityplayermp, int i, int j, boolean flag)
    {
        try
        {
            Method method = null;
            Method amethod[] = (net.minecraft.server.ServerConfigurationManager.class).getDeclaredMethods();
            int k = amethod.length;
            int l = 0;
            do
            {
                if(l >= k)
                {
                    break;
                }
                Method method2 = amethod[l];
                if(method2.getReturnType() == (net.minecraft.server.PlayerManager.class))
                {
                    method2.setAccessible(true);
                    method = method2;
                    break;
                }
                l++;
            } while(true);
            PlayerManager playermanager = (PlayerManager)method.invoke(entityplayermp.b.serverConfigurationManager, new Object[] {
                Integer.valueOf(entityplayermp.dimension)
            });
            Method method1 = null;
            Method amethod1[] = (net.minecraft.server.PlayerManager.class).getDeclaredMethods();
            int i1 = amethod1.length;
            int j1 = 0;
            do
            {
                if(j1 >= i1)
                {
                    break;
                }
                Method method3 = amethod1[j1];
                if(method3.getReturnType() == (net.minecraft.server.PlayerInstance.class))
                {
                    method3.setAccessible(true);
                    method1 = method3;
                    break;
                }
                j1++;
            } while(true);
            return (PlayerInstance)method1.invoke(playermanager, new Object[] {
                Integer.valueOf(i), Integer.valueOf(j), Boolean.valueOf(flag)
            });
        }
        catch(Exception exception)
        {
            throw new RuntimeException(exception);
        }
    }
}
